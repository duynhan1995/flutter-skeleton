package vn.etelecom.appcall

import android.annotation.SuppressLint
import android.app.Activity
import android.app.KeyguardManager
import android.content.Context
import android.graphics.Typeface
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import vn.etelecom.appcall.utils.StringHandler

@RequiresApi(Build.VERSION_CODES.O)
class IncomingActivity: Activity(), View.OnClickListener {
  companion object {
    @SuppressLint("StaticFieldLeak")
    var shared: IncomingActivity? = null
  }

  private lateinit var holdBtn: View
  private lateinit var unHoldBtn: View
  private lateinit var speakerOffBtn: View
  private lateinit var speakerOnBtn: View
  private lateinit var micOffBtn: View
  private lateinit var micOnBtn: View
  private lateinit var answerBtn: View
  private lateinit var rejectBtn: View

  private lateinit var callingTime: TextView
  private lateinit var phoneNumber: TextView
  private lateinit var callingStateIndicator: ImageView
  private var thread: Thread? = null

  private var callAnswered = false

  @RequiresApi(Build.VERSION_CODES.O_MR1)
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    
    
    shared = this

    setShowWhenLocked(true)
    setTurnScreenOn(true)

    val keyguardManager = getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager
    keyguardManager.requestDismissKeyguard(this, null)
    
    setContentView(R.layout.incoming_view)
    actionBar?.hide()

    initButtons()
    preCheckStateOfButtons()

    callingTime.text = "CUỘC GỌI ĐẾN TỪ"
    val face: Typeface = resources.getFont(R.font.opensans_regular)
    callingTime.typeface = face
    callingTime.textSize = 13.0F

    phoneNumber.text = CallManager.callerParsed

    PortsipService.shared?.checkPermissions(CallManager.isVideoCall, this)

    if (CallManager.isVideoCall) {
      PortsipService.shared?.disableProximityMonitoring()
    } else {
      PortsipService.shared?.enableProximityMonitoring()
    }
    
  }
  
  override fun onDestroy() {
    super.onDestroy()
    PortsipService.shared?.disableProximityMonitoring()
    stopTimer()
    reset()
  }

  override fun onBackPressed() {
    moveTaskToBack(true)
  }
  override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
    if (keyCode == KeyEvent.KEYCODE_BACK) {
      super.onKeyDown(keyCode, event)
      moveTaskToBack(true)
      return true
    }
    return false
  }

  private fun reset() {
    phoneNumber.text = ""

    holdBtn.isClickable = false
    holdBtn.alpha = 0.5F

    callAnswered = false
  }
  
  override fun onClick(view: View) {
    PortsipService.shared?.cancelNotification()
    when (view.id) {
      R.id.answer_btn -> {

        val code = PortsipService.shared?.answerCall() ?: false
        if (code) {
          callAnswered = true
          answerBtn.visibility = View.GONE

          holdBtn.isClickable = true
          holdBtn.alpha = 1.0F

          unHoldBtn.isClickable = true

          callingStateIndicator.setBackgroundResource(R.drawable.talking_circle)
          startTimer()
        }

      }
      R.id.reject_btn -> {
        if (callAnswered) {
          PortsipService.shared?.hangUp()
        } else {
          PortsipService.shared?.rejectCall()
        }
        finishAndRemoveTask()
      }
      R.id.hold_btn -> {
        if (callAnswered) {
          holdBtn.visibility = View.INVISIBLE
          unHoldBtn.visibility = View.VISIBLE
          PortsipService.shared?.hold()
        }
      }
      R.id.un_hold_btn -> {
        holdBtn.visibility = View.VISIBLE
        unHoldBtn.visibility = View.INVISIBLE
        PortsipService.shared?.unHold()
      }
      R.id.speaker_on_btn -> {
        speakerOnBtn.visibility = View.INVISIBLE
        speakerOffBtn.visibility = View.VISIBLE
        PortsipService.shared?.speakerOn()
      }
      R.id.speaker_off_btn -> {
        speakerOnBtn.visibility = View.VISIBLE
        speakerOffBtn.visibility = View.INVISIBLE
        PortsipService.shared?.speakerOff()
      }
      R.id.mic_off_btn -> {
        micOnBtn.visibility = View.VISIBLE
        micOffBtn.visibility = View.INVISIBLE
        PortsipService.shared?.portsipSDK?.muteSession(CallManager.sessionID, false, true, false, false)
      }
      R.id.mic_on_btn -> {
        micOnBtn.visibility = View.INVISIBLE
        micOffBtn.visibility = View.VISIBLE
        PortsipService.shared?.portsipSDK?.muteSession(CallManager.sessionID, false, false, false, false)
      }
    }
  }

  private fun startTimer() {
    var timeCount = 0

    callingTime.text = StringHandler.secondsToTime(0)
    val face: Typeface = resources.getFont(R.font.opensans_semibold)
    callingTime.typeface = face
    callingTime.textSize = 17.0F

    thread = object : Thread() {
      override fun run() {
        try {
          while (!this.isInterrupted) {
            sleep(1000)
            runOnUiThread {
              timeCount += 1
              callingTime.text = StringHandler.secondsToTime(timeCount)
            }
          }
        } catch (e: InterruptedException) {
          Log.e("E-TELECOM ERROR", "COUNT CALLING TIME ERROR: $e")
        }
      }
    }

    thread?.start()
  }

  private fun stopTimer() {
    callingTime.text = ""
    callingStateIndicator.setBackgroundResource(R.drawable.talking_circle_pending)

    thread?.interrupt()
  }

  private fun initButtons() {

    answerBtn = findViewById(R.id.answer_btn)
    answerBtn.setOnClickListener(this)

    rejectBtn = findViewById(R.id.reject_btn)
    rejectBtn.setOnClickListener(this)

    holdBtn = findViewById(R.id.hold_btn)
    holdBtn.setOnClickListener(this)

    unHoldBtn = findViewById(R.id.un_hold_btn)
    unHoldBtn.setOnClickListener(this)

    speakerOffBtn = findViewById(R.id.speaker_off_btn)
    speakerOffBtn.setOnClickListener(this)

    speakerOnBtn = findViewById(R.id.speaker_on_btn)
    speakerOnBtn.setOnClickListener(this)

    micOffBtn = findViewById(R.id.mic_off_btn)
    micOffBtn.setOnClickListener(this)

    micOnBtn = findViewById(R.id.mic_on_btn)
    micOnBtn.setOnClickListener(this)

    callingTime = findViewById(R.id.calling_time)
    phoneNumber = findViewById(R.id.phone_number)
    callingStateIndicator = findViewById(R.id.calling_state_indicator)

  }

  fun preCheckStateOfButtons() {
    holdBtn.visibility = if (CallManager.isHeld) View.INVISIBLE else View.VISIBLE
    unHoldBtn.visibility = if (!CallManager.isHeld) View.INVISIBLE else View.VISIBLE
    speakerOnBtn.visibility = if (CallManager.speakerOn) View.INVISIBLE else View.VISIBLE
    speakerOffBtn.visibility = if (!CallManager.speakerOn) View.INVISIBLE else View.VISIBLE
    micOnBtn.visibility = if (CallManager.micIsOn) View.INVISIBLE else View.VISIBLE
    micOffBtn.visibility = if (!CallManager.micIsOn) View.INVISIBLE else View.VISIBLE
  }
  
}
