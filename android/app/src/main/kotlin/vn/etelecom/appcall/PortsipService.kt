package vn.etelecom.appcall

import android.Manifest
import android.annotation.SuppressLint
import android.app.*
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothProfile
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.media.AudioAttributes
import android.media.AudioManager
import android.media.MediaPlayer
import android.os.*
import android.text.Html
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.android.volley.*
import com.android.volley.toolbox.HttpHeaderParser
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import com.portsip.PortSipEnumDefine
import com.portsip.PortSipErrorcode
import com.portsip.PortSipSdk
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel
import org.json.JSONException
import org.json.JSONObject
import vn.etelecom.appcall.utils.ProcessHandler
import java.io.UnsupportedEncodingException
import java.util.*

@RequiresApi(Build.VERSION_CODES.O)
class PortsipService: Service(), BasePortsipDelegate {

  companion object {
    var shared: PortsipService? = null
    var engineF: FlutterEngine? = null

    fun sendTelegram(text: String, context: Context) {
      try {

        val requestQueue: RequestQueue = Volley.newRequestQueue(context)
        val URL = "https://api.telegram.org/bot1746704472:AAHuIwK-0IZjRPKub_QirlCpoQmp6dAiN7M/sendMessage"
        val jsonBody = JSONObject()
        jsonBody.put("chat_id", "-285363565")
        jsonBody.put("text", text)
        val requestBody: String = jsonBody.toString()
        val stringRequest: StringRequest = object : StringRequest(Method.POST, URL,
          Response.Listener { response -> Log.i("VOLLEY", response!!) },
          Response.ErrorListener { error -> Log.e("VOLLEY", error.toString()) }) {

          override fun getBodyContentType(): String {
            return "application/json; charset=utf-8"
          }

          override fun getBody(): ByteArray? {
            return try {
              requestBody.toByteArray()
            } catch (uee: UnsupportedEncodingException) {
              VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8")
              null
            }
          }

          override fun parseNetworkResponse(response: NetworkResponse?): Response<String?>? {
            var responseString: String? = ""
            if (response != null) {
              responseString = java.lang.String.valueOf(response.statusCode)
              // can get more details such as response.headers
            }
            return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response))
          }
        }
        requestQueue.add(stringRequest)
      } catch (e: JSONException) {
        e.printStackTrace()
      }
    }
  }

  private var firebasePushToken: String = ""

  val BACKGROUND_INCOMING_CALL_NOTIFICATION_ID = 2020

  private val BACKGROUND_INCOMING_CALL_CHANNEL_ID = "vn.etelecom.appcall.calling"

  var portsipSDK: PortSipSdk? = null

  private var notiManager: NotificationManager? = null
  private var proximityMonitoring: PowerManager.WakeLock? = null
  private var mediaPlayer: MediaPlayer? = null
  private var vibrator: Vibrator? = null
  private var audioManager: AudioManager? = null

  override fun onBind(p0: Intent?): IBinder? {
    return null
  }

  override fun onCreate() {
    portsipSDK = PortSipSdk()
  }
  
  override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

    if (intent != null) {
      if (intent.action == FirebaseService.ACTION_REFRESH_PUSH_TOKEN) {
        firebasePushToken = intent.getStringExtra(FirebaseService.PUSH_TOKEN) ?: ""
        refreshPushToken(true)
        registerPortsip()
      }
      if (intent.action == FirebaseService.ACTION_PROCESS_PUSH_PBX) {

        if (CallManager.callerParsed.isNotEmpty()) {
          registerPortsip()
          showPendingCallNotification()
        }

      }
    }

    if (intent?.action == null || intent.action!!.isEmpty()) {
      FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
        if (!task.isSuccessful) {
          Log.w("FIREBASE", "Fetching FCM registration token failed", task.exception)
          return@OnCompleteListener
        }

        // Get new FCM registration token
        firebasePushToken = task.result ?: ""
        refreshPushToken(true)
        refreshRegistrationPortsip()
      })

      registerPortsip()
    }

    shared = this
    
    return super.onStartCommand(intent, flags, startId)
  }
  
  private fun initNotification() {
    
    notiManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    val currentChannel = notiManager!!.getNotificationChannel(BACKGROUND_INCOMING_CALL_CHANNEL_ID)
    
    if (currentChannel == null || currentChannel.id == null || currentChannel.id.isEmpty()) {
      val callChannel = NotificationChannel(BACKGROUND_INCOMING_CALL_CHANNEL_ID, "Cuộc gọi đến", NotificationManager.IMPORTANCE_HIGH)
      callChannel.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
      
      notiManager?.createNotificationChannel(callChannel)
    }
    
  }

  private fun refreshPushToken(willPush: Boolean) {
    if (firebasePushToken.isNotEmpty()) {
      portsipSDK?.clearAddedSipMessageHeaders()
      val pushMessage = "device-os=android;device-uid=$firebasePushToken;allow-call-push=$willPush;allow-message-push=$willPush;app-id=$packageName"
      portsipSDK?.addSipMessageHeader(-1, "REGISTER", 1, "x-p-push", pushMessage)

      val sharedPref = getSharedPreferences("CallManager", MODE_PRIVATE)
      with(sharedPref.edit()) {
        putString(getString(R.string.call_manager_push_message), pushMessage)
        apply()
      }
    }
  }
  
  private fun initPortsipSDK() {
    
    portsipSDK?.setOnPortSIPEvent(this)
    portsipSDK?.CreateCallManager(application)
    
    portsipSDK?.initialize(
      PortSipEnumDefine.ENUM_TRANSPORT_TCP, "0.0.0.0", 10002,
      PortSipEnumDefine.ENUM_LOG_LEVEL_NONE, "", 8, "PortSIP SDK for Android",
      0, 0, "", "", false, "")
    
    portsipSDK?.addAudioCodec(PortSipEnumDefine.ENUM_AUDIOCODEC_OPUS)
    portsipSDK?.addAudioCodec(PortSipEnumDefine.ENUM_AUDIOCODEC_G729)
    portsipSDK?.addAudioCodec(PortSipEnumDefine.ENUM_AUDIOCODEC_PCMA)
    portsipSDK?.addAudioCodec(PortSipEnumDefine.ENUM_AUDIOCODEC_PCMU)

    portsipSDK?.addVideoCodec(PortSipEnumDefine.ENUM_VIDEOCODEC_H264)
    portsipSDK?.addVideoCodec(PortSipEnumDefine.ENUM_VIDEOCODEC_VP8)
    portsipSDK?.addAudioCodec(PortSipEnumDefine.ENUM_VIDEOCODEC_VP9)
  
    portsipSDK?.setVideoBitrate(-1, 512)
    portsipSDK?.setVideoFrameRate(-1, 20)
    portsipSDK?.setVideoResolution(480, 640)
    portsipSDK?.setVideoNackStatus(true)
    
    portsipSDK?.setInstanceId("PORTSIP_INSTANCE_ID")
    
    portsipSDK?.setLicenseKey("PORTSIP_UC_LICENSE")
    
    Log.d("E-TELECOM", "initialSDK ${portsipSDK?.version}")
    
  }
  
  private fun registerPortsip() {

    try {

      initNotification()
      
      if (CallManager.portsipRegistered) {
        return refreshRegistrationPortsip()
      }

      initPortsipSDK()

      val sharedPref = getSharedPreferences("CallManager", MODE_PRIVATE)
      val psExtension = sharedPref.getString(getString(R.string.call_manager_ps_extension), "")
      val psPassword = sharedPref.getString(getString(R.string.call_manager_ps_password), "")
      val psDomain = sharedPref.getString(getString(R.string.call_manager_ps_domain), "")
      val psServer = sharedPref.getString(getString(R.string.call_manager_ps_server), "210.211.106.202")
      val pushMessage = sharedPref.getString(getString(R.string.call_manager_push_message), "") ?: ""

      if (pushMessage.isNotEmpty()) {
        portsipSDK?.clearAddedSipMessageHeaders()
        portsipSDK?.addSipMessageHeader(-1, "REGISTER", 1, "x-p-push", pushMessage)
      }

      portsipSDK?.removeUser()
      var code = portsipSDK?.setUser(
        psExtension, psExtension, psExtension, psPassword, psDomain, psServer, 5063,
        "", 0, "", 0)
      Log.d("E-TELECOM", "ON SET USER $code")
      
      if (code != PortSipErrorcode.ECoreErrorNone) {
        return
      }
      
      portsipSDK?.unRegisterServer()
      code = portsipSDK?.registerServer(90, 0)
      Log.d("E-TELECOM", "ON REGISTER SERVER $code")
      
    } catch (e: Exception) {
      Log.e("ERROR E-TELECOM", ": $e")
    }

  }

  fun unregisterPortsip() {
    refreshPushToken(false)

    CallManager.portsipRegistered = false

    val sharedPref = getSharedPreferences("CallManager", MODE_PRIVATE)
    with(sharedPref.edit()) {
      putString(getString(R.string.call_manager_ps_extension), "")
      putString(getString(R.string.call_manager_ps_password), "")
      putString(getString(R.string.call_manager_ps_domain), "")
      putString(getString(R.string.call_manager_ps_server), "")
      putString(getString(R.string.call_manager_push_message), "")
      apply()
    }

    portsipSDK?.removeUser()
    portsipSDK?.unRegisterServer()
    portsipSDK?.DeleteCallManager()

    proximityMonitoring?.release()

    notiManager?.cancelAll()
    notiManager?.deleteNotificationChannel(BACKGROUND_INCOMING_CALL_CHANNEL_ID)
    notiManager = null
  }
  
  private fun refreshRegistrationPortsip() {
    portsipSDK?.refreshRegistration(0)
  }
  
  private fun showPendingCallNotification() {
    
    val fullScreenIntent = Intent(this, IncomingActivity::class.java)
    fullScreenIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK

    val fullScreenPendingIntent = PendingIntent.getActivity(this, 0, fullScreenIntent, PendingIntent.FLAG_UPDATE_CURRENT)

    val notification = Notification.Builder(this, BACKGROUND_INCOMING_CALL_CHANNEL_ID)
      .setSmallIcon(R.drawable.ic_notification)
      .setColor(Color.argb(1, 47, 204, 112))
      .setContentTitle("Cuộc gọi đến từ")
      .setContentText(Html.fromHtml("<strong>${CallManager.callerParsed}</strong>", Html.FROM_HTML_MODE_LEGACY))
      .setOngoing(true)
      .setContentIntent(fullScreenPendingIntent)
      .setFullScreenIntent(fullScreenPendingIntent, true)
      .build()
    
    notiManager?.notify(BACKGROUND_INCOMING_CALL_NOTIFICATION_ID, notification)
  
  }

  fun callOut(phoneNumber: String, videoCall: Boolean = false): Boolean {
    try {

      CallManager.isVideoCall = videoCall
      CallManager.sessionID = portsipSDK?.call(phoneNumber, true, videoCall) ?: return false

      Log.d("E-TELECOM", "calling Session ID: ${CallManager.sessionID}")

      if (CallManager.sessionID <= 0) {
        return false
      }
      
      playOutgoingCallSound()
      
      if (videoCall) {
        speakerOn()
      } else {
        speakerOff()
      }

      return true
    } catch (e: Exception) {
      return false
    }
  }

  fun hangUp(): Boolean {
    try {
      val code = portsipSDK?.hangUp(CallManager.sessionID)

      if (code != PortSipErrorcode.ECoreErrorNone) {
        return false
      }

      endACall()

      return true
    } catch (e: Exception) {
      return false
    }
  }

  fun answerCall(): Boolean {
    try {
      stopVibration()
      
      val code = portsipSDK?.answerCall(CallManager.sessionID, CallManager.isVideoCall)

      if (code != PortSipErrorcode.ECoreErrorNone) {
        return false
      }

      return true
    } catch (e: Exception) {
      return false
    }
  }

  fun rejectCall(): Boolean {
    try {
      val code = portsipSDK?.rejectCall(CallManager.sessionID, 486)
      
      if (code != PortSipErrorcode.ECoreErrorNone) {
        return false
      }

      endACall()

      return true
    } catch (e: Exception) {
      return false
    }
  }

  fun hold(): Boolean {
    try {
      val code = portsipSDK?.hold(CallManager.sessionID)

      if (code != PortSipErrorcode.ECoreErrorNone) {
        return false
      }

      return true
    } catch (e: Exception) {
      return false
    }
  }

  fun unHold(): Boolean {
    try {
      val code = portsipSDK?.unHold(CallManager.sessionID)

      if (code != PortSipErrorcode.ECoreErrorNone) {
        return false
      }

      return true
    } catch (e: Exception) {
      return false
    }
  }

  fun speakerOn() {
    CallManager.speakerOn = true
    
    val bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
    if (BluetoothProfile.STATE_CONNECTED == bluetoothAdapter.getProfileConnectionState(BluetoothProfile.HEADSET)) {
      return
    }
  
    if (!CallManager.isIncomingCall) {
      if (audioManager == null) {
        audioManager = getSystemService(Context.AUDIO_SERVICE) as AudioManager
      }
      audioManager!!.isSpeakerphoneOn = true
      audioManager!!.mode = AudioManager.MODE_NORMAL
    }
    portsipSDK?.setAudioDevice(PortSipEnumDefine.AudioDevice.SPEAKER_PHONE)
    
    disableProximityMonitoring()
  }

  fun speakerOff() {
    CallManager.speakerOn = false
  
    val bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
    if (BluetoothProfile.STATE_CONNECTED == bluetoothAdapter.getProfileConnectionState(BluetoothProfile.HEADSET)) {
      return
    }
  
    if (!CallManager.isIncomingCall) {
      if (audioManager == null) {
        audioManager = getSystemService(Context.AUDIO_SERVICE) as AudioManager
      }
      audioManager!!.isSpeakerphoneOn = false
      audioManager!!.mode = AudioManager.MODE_IN_COMMUNICATION
    }
    portsipSDK?.setAudioDevice(PortSipEnumDefine.AudioDevice.EARPIECE)
    
    enableProximityMonitoring()
  }

  fun turnOnMicrophone() {
    CallManager.micIsOn = true
    if (CallManager.callIsHappening) {
      portsipSDK?.muteSession(
        CallManager.sessionID,
        false,
        false,
        false,
        false
      )
    }
  }

  fun turnOffMicrophone() {
    CallManager.micIsOn = false
    if (CallManager.callIsHappening) {
      portsipSDK?.muteSession(
        CallManager.sessionID,
        false,
        true,
        false,
        false
      )
    }
  }

  fun switchToFrontCamera() {
    CallManager.frontCamera = true
  }

  fun switchToBackCamera() {
    CallManager.frontCamera = false
  }

  fun turnOnCamera() {
    CallManager.camIsOn = true
  }

  fun turnOffCamera() {
    CallManager.camIsOn = false
  }

  fun sendDtmf(code: Int): Int {
    return portsipSDK?.sendDtmf(CallManager.sessionID, PortSipEnumDefine.ENUM_DTMF_MOTHOD_RFC2833, code, 160, true) ?: -1
  }

  fun refer(referTo: String): Int {
    return portsipSDK?.refer(CallManager.sessionID, referTo) ?: -1
  }

  private fun endACall() {
    stopSound()
    stopVibration()

    flutterMethodChannel("callEnded", "")

    speakerOff()
    turnOnMicrophone()
    turnOnCamera()
    switchToFrontCamera()

    CallManager.resetStates()

    disableProximityMonitoring()
  }

  fun cancelNotification() {
    notiManager?.cancel(BACKGROUND_INCOMING_CALL_NOTIFICATION_ID)
  }

  private fun initMediaSession() {

    CallManager.callIsHappening = true

    portsipSDK?.muteSession(
      CallManager.sessionID,
      false,
      !CallManager.micIsOn,
      false,
      false
    )
  
    val bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
    if (BluetoothProfile.STATE_CONNECTED == bluetoothAdapter.getProfileConnectionState(BluetoothProfile.HEADSET)) {
      portsipSDK?.setAudioDevice(PortSipEnumDefine.AudioDevice.BLUETOOTH)
    } else {
      portsipSDK?.setAudioDevice(
        if (CallManager.speakerOn) PortSipEnumDefine.AudioDevice.SPEAKER_PHONE
        else PortSipEnumDefine.AudioDevice.EARPIECE
      )
    }
  
  }
  
  private fun playOutgoingCallSound() {
    if (mediaPlayer == null) {
      mediaPlayer = MediaPlayer.create(this, R.raw.outgoing_tone)
    }
    mediaPlayer!!.isLooping = true
    mediaPlayer!!.setAudioAttributes(AudioAttributes.Builder()
      .setUsage(AudioAttributes.USAGE_VOICE_COMMUNICATION)
      .build())
  
    mediaPlayer!!.start()
  }
  
  private fun playIncomingCallSound() {
    if (mediaPlayer == null) {
      mediaPlayer = MediaPlayer.create(this, R.raw.incoming_tone)
    }
    mediaPlayer!!.isLooping = true
    
    mediaPlayer!!.start()
  }
  
  private fun stopSound() {
    if (mediaPlayer != null) {
      mediaPlayer!!.stop()
      mediaPlayer!!.release()
      mediaPlayer = null
    }
    
    if (audioManager != null) {
      audioManager = null
    }
  }
  
  private fun startVibration() {
    if (vibrator == null) {
      vibrator = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
    }
    vibrator!!.vibrate(
      VibrationEffect.createWaveform(
        longArrayOf(0, 500, 1500), intArrayOf(0, VibrationEffect.DEFAULT_AMPLITUDE, 0), 0
      ),
      AudioAttributes.Builder().setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
        .setUsage(AudioAttributes.USAGE_ALARM)
        .build()
    )
  }
  
  private fun stopVibration() {
    if (vibrator != null) {
      vibrator!!.cancel()
      vibrator = null
    }
  }
  
  @SuppressLint("WakelockTimeout")
  fun enableProximityMonitoring() {
    if (proximityMonitoring != null) {
      return
    }

    val powerManager = getSystemService(Context.POWER_SERVICE) as PowerManager
    proximityMonitoring = powerManager.newWakeLock(PowerManager.PROXIMITY_SCREEN_OFF_WAKE_LOCK, "AppCall:ProximityMonitoring")
    proximityMonitoring?.acquire()
  }

  fun disableProximityMonitoring() {
    if (proximityMonitoring != null) {
      proximityMonitoring?.release()
      proximityMonitoring = null
    }
  }

  fun checkPermissions(videoCall: Boolean, activity: Activity) {
    when (PackageManager.PERMISSION_GRANTED) {
      ContextCompat.checkSelfPermission(
        activity, Manifest.permission.RECORD_AUDIO
      ) -> {

        if (videoCall) {

          if (PackageManager.PERMISSION_GRANTED != ContextCompat.checkSelfPermission(
              activity, Manifest.permission.CAMERA)
          ) {
            ActivityCompat.requestPermissions(
              activity,
              arrayOf(Manifest.permission.CAMERA),
              0
            )
          }

        }

      }
      else -> {
        if (videoCall) {
          if (PackageManager.PERMISSION_GRANTED != ContextCompat.checkSelfPermission(
              activity, Manifest.permission.CAMERA)
          ) {

            ActivityCompat.requestPermissions(
              activity,
              arrayOf(Manifest.permission.RECORD_AUDIO, Manifest.permission.CAMERA),
              0
            )

          }
        } else {
          ActivityCompat.requestPermissions(
            activity,
            arrayOf(Manifest.permission.RECORD_AUDIO),
            0
          )
        }
      }
    }
  }
  
  override fun onRegisterSuccess(reason: String?, code: Int, sipMessage: String?) {
    Log.d("E-TELECOM", "REGISTER SUCCESS $sipMessage")

    CallManager.portsipRegistered = true

    flutterMethodChannel("onRegisterFailure", "{\"reason\": \"\", \"code\": \"\"}")
  }
  
  override fun onRegisterFailure(reason: String?, code: Int, sipMessage: String?) {
    Log.d("E-TELECOM", "REGISTER FAILED: reason: $reason, code: $code, sipMessage: $sipMessage")

    flutterMethodChannel("onRegisterFailure", "{\"reason\": \"$reason\", \"code\": \"$code\"}")

    CallManager.portsipRegistered = false

    registerPortsip()
  }
  
  override fun onAudioDeviceChanged(
    currentDevice: PortSipEnumDefine.AudioDevice?,
    availableDevices: MutableSet<PortSipEnumDefine.AudioDevice>?
  ) {
    Log.d("ON AUDIO DEVICE CHANGED", "currentDevice: $currentDevice - available: $availableDevices")
    if (currentDevice != null && availableDevices != null) {
      if (currentDevice != PortSipEnumDefine.AudioDevice.BLUETOOTH) {
        if (availableDevices.contains(PortSipEnumDefine.AudioDevice.BLUETOOTH)) {
          portsipSDK?.setAudioDevice(PortSipEnumDefine.AudioDevice.BLUETOOTH)
        } else if (CallManager.speakerOn && currentDevice != PortSipEnumDefine.AudioDevice.SPEAKER_PHONE) {
          speakerOn()
        } else if (!CallManager.speakerOn && currentDevice != PortSipEnumDefine.AudioDevice.EARPIECE) {
          speakerOff()
        }
      } else {
        if (!availableDevices.contains(PortSipEnumDefine.AudioDevice.BLUETOOTH)) {
          if (CallManager.speakerOn) {
            speakerOn()
          } else {
            speakerOff()
          }
        }
      }
    }
  }
  
  // Out Call begins
  override fun onInviteSessionProgress(
    sessionId: Long,
    audioCodecs: String,
    videoCodecs: String,
    existsEarlyMedia: Boolean,
    existsAudio: Boolean,
    existsVideo: Boolean,
    sipMessage: String
  ) {
    Log.d("E-TELECOM", "ON_INVITE_SESSION_PROGRESS $sipMessage")
    val parsedSipMessage: String = sipMessage.replace(Regex("[\r\n\" ]*"), "")

    if (parsedSipMessage.lowercase().contains("application/sdp")) {
      stopSound()
    }

    initMediaSession()

    flutterMethodChannel("sipMessageResponse", "{\"sipMessage\": \"$parsedSipMessage\"}")
  }

  override fun onInviteRinging(sessionId: Long, statusText: String?, statusCode: Int, sipMessage: String?) {
    Log.d("E-TELECOM", "ON_INVITE_RINGING $sipMessage")
    val parsedSipMessage: String = sipMessage?.replace(Regex("[\r\n\" ]*"), "") ?: ""

    if (parsedSipMessage.lowercase().contains("application/sdp")) {
      Log.d("E-TELECOM", "STOP RING BACK TONE")
      stopSound()
    }

    initMediaSession()

    flutterMethodChannel("sipMessageResponse", "{\"sipMessage\": \"$parsedSipMessage\"}")
  }
  
  // Out Call is answered
  override fun onInviteAnswered(
    sessionId: Long,
    callerDisplayName: String,
    caller: String,
    calleeDisplayName: String,
    callee: String,
    audioCodecNames: String,
    videoCodecNames: String,
    existsAudio: Boolean,
    existsVideo: Boolean,
    sipMessage: String
  ) {
    Log.d("E-TELECOM", "ON_INVITE_ANSWERED $existsVideo")
    try {
      
      if (!existsVideo) {
        flutterMethodChannel("callAnswered", "")
      }
      
    } catch (e: Exception) {
      Log.e("ERROR ON_INVITE_ANSWERED", "ERROR --> $e")
    }
  }
  
  // Out Call is rejected
  override fun onInviteFailure(sessionId: Long, reason: String?, code: Int, sipMessage: String?) {
    Log.d("E-TELECOM", "ON_INVITE_FAILURE - reason: $reason - sipMessage: $sipMessage")
    try {
      endACall()
    } catch (e: Exception) {
      Log.e("ERROR ON_INVITE_FAILURE", "ERROR --> $e")
    }
  }
  
  // In Call begins
  override fun onInviteIncoming(
    sessionId: Long,
    callerDisplayName: String,
    caller: String,
    calleeDisplayName: String,
    callee: String,
    audioCodecs: String,
    videoCodecs: String,
    existsAudio: Boolean,
    existsVideo: Boolean,
    sipMessage: String
  ) {
    Log.d("E-TELECOM", "ONINVITEINCOMING $sipMessage")
    try {

      if (CallManager.sessionID == 0.toLong()) {

        CallManager.sessionID = sessionId
        CallManager.isVideoCall = existsVideo
        CallManager.isIncomingCall = true

        val parsedSipMessage: String = sipMessage.replace(Regex("[\r\n\" ]*"), "")
        flutterMethodChannel("sipMessageResponse", "{\"sipMessage\": \"$parsedSipMessage\"}")

        if (ProcessHandler.isAppActive(
            getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager,
            packageName
          )) {

          flutterMethodChannel("callIn", "{\"caller\": \"$caller\", \"isVideo\": $existsVideo}")
          
          checkPermissions(existsVideo, MainActivity.shared)

        }
        
        playIncomingCallSound()
        startVibration()
        
      }
    } catch (e: Exception) {
      Log.e("ERROR ONINVITEINCOMING", "ERROR --> $e")
    }
  }
  
  override fun onInviteConnected(sessionId: Long) {
    Log.d("E-TELECOM", "ON_INVITE_CONNECTED")
    stopSound()

    initMediaSession()

    if (CallManager.isVideoCall) {

      speakerOn()

      flutterMethodChannel("disposeCamera", "")
      
    }
  }
  
  // Processing Calls are closed from remote side
  override fun onInviteClosed(sessionId: Long, sipMessage: String) {
    Log.d("E-TELECOM", "ON_INVITE_CLOSED")
    try {
      endACall()
  
      cancelNotification()
      
      VideoCallActivity.activity?.finishAndRemoveTask()
      IncomingActivity.shared?.finishAndRemoveTask()
    } catch (e: Exception) {
      Log.e("ERROR ON_INVITE_CLOSED", "ERROR --> $e")
    }
  }

  override fun onReferAccepted(sessionId: Long) {
    Log.d("E-TELECOM", "ON_REFER_ACCEPTED")

    hangUp()
  }

  private fun flutterMethodChannel(method: String, args: String) {
    if (engineF == null) {
      return
    }

    MethodChannel(engineF!!.dartExecutor.binaryMessenger, MainActivity.GENERAL_APP_ID)
      .invokeMethod(method, args)
  }

}
