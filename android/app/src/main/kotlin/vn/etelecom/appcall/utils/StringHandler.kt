package vn.etelecom.appcall.utils

class StringHandler {
  companion object {
    fun secondsToTime(value: Int): String {
      
      if (value == 0) {
        return "00:00"
      }
      
      var cloneValue: Int = value
  
      val hours: Int = cloneValue / 3600
      cloneValue %= 3600
  
      val minutes: Int = cloneValue / 60
      val seconds: Int = cloneValue % 60
      
      val hoursString: String = if (hours < 10) "0$hours" else "$hours"
      val minutesString: String = if (minutes < 10) "0$minutes" else "$minutes"
      val secondsString: String = if (seconds < 10) "0$seconds" else "$seconds"

      if (hours == 0) {
        return "$minutesString:$secondsString"
      }
      
      return "$hoursString:$minutesString:$secondsString"
      
    }
    
  }
}
