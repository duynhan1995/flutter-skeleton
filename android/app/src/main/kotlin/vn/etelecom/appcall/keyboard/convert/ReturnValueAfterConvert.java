package vn.etelecom.appcall.keyboard.convert;

import android.view.inputmethod.InputConnection;

import java.util.ArrayList;
import java.util.Arrays;

public class ReturnValueAfterConvert {

    public static String characterUO = "";
    public static ArrayList<String> arrayow = new ArrayList<>(Arrays.asList("ơ","ớ","ở","ở","ỡ","ợ",
            "Ơ","Ớ","Ờ","Ở","Ỡ","Ợ"));
    public static ArrayList<String> arrayoo = new ArrayList<>(Arrays.asList("ô","ố","ồ","ổ","ỗ","ộ",
            "Ô","Ố","Ồ","Ổ","Ỗ","Ộ"));
    public static ArrayList<String> arrayDD = new ArrayList<>(Arrays.asList("d","đ","D","Đ"));
    public ArrayList<String> result = new ArrayList<String>();

    public static String unikeyCustomsAddText(String textBefore,String textInput, InputConnection inputConnection){
//        let arrayTextBefore = textBefore.map { String($0) };
        ArrayList<String> arrayTextBefore = new ArrayList<String>(Arrays.asList(textBefore.split("")));
        String characterLastest = arrayTextBefore.get(arrayTextBefore.size()-1);

        int typeConvert = InitTypeConvert.initTypeConvertCharacter(arrayTextBefore, textInput);

        switch (typeConvert){
            case 1:
                if(arrayTextBefore.size() > 1 ){
                    ArrayList<String> arrayTwo = new ArrayList<>(Arrays.asList("k","z","q","w","j",
                            "K","Z","Q","W","J")) ;
                    ArrayList<String> arraySpecialWithK = new ArrayList<>(Arrays.asList("i","í","ì","ỉ","ĩ","ị",
                            "e","é","è","ẻ","ẽ","ẹ",
                            "ê","ế","ề","ể","ễ","ệ",
                            "I","Í","Ì","Ỉ","Ĩ","Ị",
                            "E","É","È","Ẻ","Ẽ","Ẹ",
                            "Ê","Ế","Ề","Ể","Ễ","Ệ"));
                    if(arrayTwo.contains(arrayTextBefore.get(arrayTextBefore.size()-2))){
                        if(arrayTextBefore.size() == 2 && arrayTextBefore.get(arrayTextBefore.size()-2) == "k" && arraySpecialWithK.contains(arrayTextBefore.get(arrayTextBefore.size()-1))){
                            return ConvertOneLastCharacter.convert(characterLastest, textInput, inputConnection);
                        }
                        if(arrayTextBefore.size() == 2 && arrayTextBefore.get(arrayTextBefore.size()-2) == "K" && arraySpecialWithK.contains(arrayTextBefore.get(arrayTextBefore.size()-1))){
                            return ConvertOneLastCharacter.convert(characterLastest, textInput, inputConnection);
                        }
                        return textInput;
                    }
                    if(arrayTextBefore.size() > 2){
                        ArrayList<String> arraySpecial = new ArrayList<>(Arrays.asList("u","ư","o","i",
                                "U","Ư","O","I"));
                        ArrayList<String> arrayValidateTwoFisrt = new ArrayList<>(Arrays.asList("th","ch","ng","gh","kh","nh",
                                "Th","Ch","Ng","Gh","Kh","Nh",
                                "tH","cH","nG","gH","kH","nH",
                                "TH","CH","NG","GH","KH","NH"));
                        String twoTextFirst = arrayTextBefore.get(0) + arrayTextBefore.get(1);
                        if(!arraySpecial.contains(arrayTextBefore.get(1)) && !arrayValidateTwoFisrt.contains(twoTextFirst)){
                            return textInput;
                        }
                    }
                }
                return ConvertOneLastCharacter.convert(characterLastest, textInput, inputConnection);
            case 2:
                String twoCharacterLastest = arrayTextBefore.get(arrayTextBefore.size()-2);
                if(arrayTextBefore.size() > 2 ){
                    ArrayList<String> arrayTwo =  new ArrayList<>(Arrays.asList("k","z","q","w","j","s","f","r","x","j",
                            "K","Z","Q","W","J","S","F","R","X","J"));
                    if(arrayTwo.contains(arrayTextBefore.get(arrayTextBefore.size()-1))){
                        return textInput;
                    }
                    if(arrayTextBefore.size() > 3 && arrayTwo.contains(arrayTextBefore.get(arrayTextBefore.size()-3))){
                        return textInput;
                    }
                }
                ArrayList<String> validateStringSpecial = new ArrayList<>(Arrays.asList("i","u","ư",
                    "I","U","Ư"));
                if(validateStringSpecial.contains(twoCharacterLastest)){
                    ArrayList<String> arrayUn = new ArrayList<>(Arrays.asList("n","c","t"));
                    if(arrayUn.contains(arrayTextBefore.get(arrayTextBefore.size() - 1))){
                        inputConnection.deleteSurroundingText(1,0);
                        return ConvertOneLastCharacter.convert(twoCharacterLastest, textInput, inputConnection) + arrayTextBefore.get(arrayTextBefore.size() - 1);
                    }
                    twoCharacterLastest = arrayTextBefore.get(arrayTextBefore.size()-1);
                    String resultText = ConvertOneLastCharacter.convert(twoCharacterLastest, textInput, inputConnection);

                    String characterD = "";
                    ArrayList<String> arrayDD = new ArrayList<>(Arrays.asList("d","đ","D","Đ"));
                    if(arrayDD.contains(arrayTextBefore.get(0))){
                        characterD = arrayTextBefore.get(0);
                    }

                    if(validateStringSpecial.contains(arrayTextBefore.get(arrayTextBefore.size()-1)) &&
                            validateStringSpecial.contains(arrayTextBefore.get(arrayTextBefore.size()-2))){
                        resultText = ConvertOneLastCharacter.convert(arrayTextBefore.get(arrayTextBefore.size()-2), textInput, inputConnection);
                        return resultText + arrayTextBefore.get(arrayTextBefore.size()-1);
                    }

                    if(arrayow.contains(resultText) && arrayTextBefore.get(arrayTextBefore.size()-2) == "u" ){
                        inputConnection.deleteSurroundingText(1,0);
                        return characterD + "ư" + ConvertOneLastCharacter.convert(twoCharacterLastest, textInput, inputConnection);
                    }
                    if(arrayoo.contains(resultText) && arrayTextBefore.get(arrayTextBefore.size()-2) == "ư" ){
                        inputConnection.deleteSurroundingText(1,0);
                        return characterD + "u" + ConvertOneLastCharacter.convert(twoCharacterLastest, textInput, inputConnection);
                    }
                    if(arrayow.contains(resultText) && arrayTextBefore.get(arrayTextBefore.size()-2) == "U" ){
                        inputConnection.deleteSurroundingText(1,0);
                        return  characterD + "Ư" + ConvertOneLastCharacter.convert(twoCharacterLastest, textInput, inputConnection);
                    }
                    if(arrayoo.contains(resultText) && arrayTextBefore.get(arrayTextBefore.size()-2) == "Ư" ){
                        inputConnection.deleteSurroundingText(1,0);
                        return characterD + "U" + ConvertOneLastCharacter.convert(twoCharacterLastest, textInput, inputConnection);
                    }

                    return resultText;
                }

                inputConnection.deleteSurroundingText(1,0);
                String resultTwo = ConvertOneLastCharacter.convert(twoCharacterLastest, textInput, inputConnection);

                if(arrayTextBefore.size()>2){
                    if(arrayow.contains(resultTwo) && arrayTextBefore.get(arrayTextBefore.size()-3) == "u" ){
                        inputConnection.deleteSurroundingText(1,0);
                        characterUO = "ư";
                    }
                    if(arrayoo.contains(resultTwo) && arrayTextBefore.get(arrayTextBefore.size()-3) == "ư" ){
                        inputConnection.deleteSurroundingText(1,0);
                        characterUO = "u";
                    }
                    if(arrayTextBefore.size() > 3 && arrayow.contains(resultTwo) && arrayTextBefore.get(arrayTextBefore.size()-4) == "U" ){
                        inputConnection.deleteSurroundingText(1,0);
                        characterUO = "Ư";
                    }
                    if(arrayTextBefore.size() > 3 && arrayoo.contains(resultTwo) && arrayTextBefore.get(arrayTextBefore.size()-4) == "Ư" ){
                        inputConnection.deleteSurroundingText(1,0);
                        characterUO = "U";
                    }
                }

                ArrayList<String> result = new ArrayList<>(Arrays.asList(resultTwo.split("")));
            if(result.size()>1){
                return characterUO + result.get(0) + arrayTextBefore.get(arrayTextBefore.size()-1) + result.get(1);
            }
            return characterUO + resultTwo + arrayTextBefore.get(arrayTextBefore.size()-1);
            case 3:
                String threeCharacterLastest = arrayTextBefore.get(arrayTextBefore.size()-3);
                String nodeTwoCharacterLastest = arrayTextBefore.get(arrayTextBefore.size()-2) + arrayTextBefore.get(arrayTextBefore.size()-1);
                ArrayList<String> arrayValidateTwoLastest = new ArrayList<>(Arrays.asList("ng","nh","ch",
                        "Ng","Nh","Ch",
                        "nG","nH","cH",
                        "NG","NH","CH"));
                if(arrayTextBefore.size() <= 7 && arrayValidateTwoLastest.contains(nodeTwoCharacterLastest)){
                    if(arrayTextBefore.size() > 3){
                        ArrayList<String> arrayTwo = new ArrayList<>(Arrays.asList("k","z","q","w","j","f","j",
                                "K","Z","Q","W","J","F","J"));

                        if(arrayTwo.contains(arrayTextBefore.get(arrayTextBefore.size()-4))){
                            return textInput;
                        }

                        ArrayList<String> arrayTwoSpecial = new ArrayList<>(Arrays.asList("s","r","x","d",
                                "S","R","X","D"));
                        if(!arrayTwoSpecial.contains(arrayTextBefore.get(0)) && arrayTwoSpecial.contains(arrayTextBefore.get(arrayTextBefore.size()-4))){
                            return textInput;
                        }

                        String resultThree = ConvertOneLastCharacter.convert(threeCharacterLastest, textInput, inputConnection);


                        if(arrayow.contains(resultThree) && arrayTextBefore.get(arrayTextBefore.size()-4) == "u" ){
                            inputConnection.deleteSurroundingText(1,0);
                            characterUO = "ư";
                        }
                        if(arrayoo.contains(resultThree) && arrayTextBefore.get(arrayTextBefore.size()-4) == "ư" ){
                            inputConnection.deleteSurroundingText(1,0);
                            characterUO = "u";
                        }
                        if(arrayow.contains(resultThree) && arrayTextBefore.get(arrayTextBefore.size()-4) == "U" ){
                            inputConnection.deleteSurroundingText(1,0);
                            characterUO = "Ư";
                        }
                        if(arrayoo.contains(resultThree) && arrayTextBefore.get(arrayTextBefore.size()-4) == "Ư" ){
                            inputConnection.deleteSurroundingText(1,0);
                            characterUO = "U";
                        }
                        inputConnection.deleteSurroundingText(2,0);

                        result = new ArrayList<>(Arrays.asList(resultThree));
                        if(result.size()>1){
                            return characterUO + result.get(0) + nodeTwoCharacterLastest + result.get(1);
                        }
                        return characterUO + resultThree + nodeTwoCharacterLastest;
                    }
                }
                break;
            case 4:

                if(arrayTextBefore.size() < 6){
                    if(arrayDD.contains(arrayTextBefore.get(0)) && arrayDD.contains(textInput)){
                        String stringD = "";
                        if(arrayTextBefore.size() > 1){
                            for(int i = 1; i < arrayTextBefore.size()-1;i++){
                                stringD = stringD + arrayTextBefore.get(1);
                            }
                        }
                        String resultD = ConvertOneLastCharacter.convert(arrayTextBefore.get(0), textInput, inputConnection);
                        result = new ArrayList<>(Arrays.asList(resultD));
                        if(result.size() > 1){
                            return result.get(0) + stringD + result.get(1);
                        }
                        return result.get(0) + stringD;
                    }
                }
                break;
            case 5:
                //            let arrayEE = ["e","E","ê","Ê"]
                if(arrayTextBefore.size() > 2 && arrayTextBefore.size() < 6){
                    String resultE = ConvertOneLastCharacter.convert(arrayTextBefore.get(arrayTextBefore.size() - 1), textInput, inputConnection);
                    return resultE;
                    //                let result = resultE.map{String($0)}
                    //                if(result.count>1){
                    //                    return result[0] + stringD + result[1];
                    //                }
                    //                return result[0] + stringD;
                }
                break;
            default:
                break;
        }

        return textInput;
    }
}
