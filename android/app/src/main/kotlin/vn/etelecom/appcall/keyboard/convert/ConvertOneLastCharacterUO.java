package vn.etelecom.appcall.keyboard.convert;

import android.view.inputmethod.InputConnection;

public class ConvertOneLastCharacterUO {
    static String convert(String characterLastest, String textInput, InputConnection inputConnection){
        String valueText = textInput;
        switch (characterLastest){
            case "u":
                switch (textInput) {
                    case "s":
                        valueText = "ú";
                        break;
                    case "f":
                        valueText = "ù";
                        break;
                    case "r":
                        valueText = "ủ";
                        break;
                    case "x":
                        valueText = "ũ";
                        break;
                    case "j":
                        valueText = "ụ";
                        break;
                    case "w":
                        valueText = "ư";
                        break;
                    case "S":
                        valueText = "ú";
                        break;
                    case "F":
                        valueText = "ù";
                        break;
                    case "R":
                        valueText = "ủ";
                        break;
                    case "X":
                        valueText = "ũ";
                        break;
                    case "J":
                        valueText = "ụ";
                        break;
                    case "W":
                        valueText = "ư";
                        break;
                    default:
                        break;
                }
                break;
            case "ú":
                switch (textInput) {
                    case "s":
                        valueText = "us";
                        break;
                    case "f":
                        valueText = "ù";
                        break;
                    case "r":
                        valueText = "ủ";
                        break;
                    case "x":
                        valueText = "ũ";
                        break;
                    case "j":
                        valueText = "ụ";
                        break;
                    case "w":
                        valueText = "ứ";
                        break;
                    case "S":
                        valueText = "uS";
                        break;
                    case "F":
                        valueText = "ù";
                        break;
                    case "R":
                        valueText = "ủ";
                        break;
                    case "X":
                        valueText = "ũ";
                        break;
                    case "J":
                        valueText = "ụ";
                        break;
                    case "W":
                        valueText = "ứ";
                        break;
                    default:
                        break;
                }
                break;
            case "ù":
                switch (textInput) {
                    case "s":
                        valueText = "ú";
                        break;
                    case "f":
                        valueText = "uf";
                        break;
                    case "r":
                        valueText = "ủ";
                        break;
                    case "x":
                        valueText = "ũ";
                        break;
                    case "j":
                        valueText = "ụ";
                        break;
                    case "w":
                        valueText = "ừ";
                        break;
                    case "S":
                        valueText = "ú";
                        break;
                    case "F":
                        valueText = "uF";
                        break;
                    case "R":
                        valueText = "ủ";
                        break;
                    case "X":
                        valueText = "ũ";
                        break;
                    case "J":
                        valueText = "ụ";
                        break;
                    case "W":
                        valueText = "ừ";
                        break;
                    default:
                        break;
                }
                break;
            case "ủ":
                switch (textInput) {
                    case "s":
                        valueText = "ú";
                        break;
                    case "f":
                        valueText = "ù";
                        break;
                    case "r":
                        valueText = "ur";
                        break;
                    case "x":
                        valueText = "ũ";
                        break;
                    case "j":
                        valueText = "ụ";
                        break;
                    case "w":
                        valueText = "ử";
                        break;
                    case "S":
                        valueText = "ú";
                        break;
                    case "F":
                        valueText = "ù";
                        break;
                    case "R":
                        valueText = "uR";
                        break;
                    case "X":
                        valueText = "ũ";
                        break;
                    case "J":
                        valueText = "ụ";
                        break;
                    case "W":
                        valueText = "ử";
                        break;
                    default:
                        break;
                }
                break;
            case "ũ":
                switch (textInput) {
                    case "s":
                        valueText = "ú";
                        break;
                    case "f":
                        valueText = "ù";
                        break;
                    case "r":
                        valueText = "ủ";
                        break;
                    case "x":
                        valueText = "ux";
                        break;
                    case "j":
                        valueText = "ụ";
                        break;
                    case "w":
                        valueText = "ữ";
                        break;
                    case "S":
                        valueText = "ú";
                        break;
                    case "F":
                        valueText = "ù";
                        break;
                    case "R":
                        valueText = "ủ";
                        break;
                    case "X":
                        valueText = "uX";
                        break;
                    case "J":
                        valueText = "ụ";
                        break;
                    case "W":
                        valueText = "ữ";
                        break;
                    default:
                        break;
                }
                break;
            case "ụ":
                switch (textInput) {
                    case "s":
                        valueText = "ú";
                        break;
                    case "f":
                        valueText = "ù";
                        break;
                    case "r":
                        valueText = "ủ";
                        break;
                    case "x":
                        valueText = "ũ";
                        break;
                    case "j":
                        valueText = "uj";
                        break;
                    case "w":
                        valueText = "ự";
                        break;
                    case "S":
                        valueText = "ú";
                        break;
                    case "F":
                        valueText = "ù";
                        break;
                    case "R":
                        valueText = "ủ";
                        break;
                    case "X":
                        valueText = "ũ";
                        break;
                    case "J":
                        valueText = "uJ";
                        break;
                    case "W":
                        valueText = "ự";
                        break;
                    default:
                        break;
                }
                break;
            case "ư":
                switch (textInput) {
                    case "s":
                        valueText = "ứ";
                        break;
                    case "f":
                        valueText = "ừ";
                        break;
                    case "r":
                        valueText = "ử";
                        break;
                    case "x":
                        valueText = "ữ";
                        break;
                    case "j":
                        valueText = "ự";
                        break;
                    case "w":
                        valueText = "uw";
                        break;
                    case "S":
                        valueText = "ứ";
                        break;
                    case "F":
                        valueText = "ừ";
                        break;
                    case "R":
                        valueText = "ử";
                        break;
                    case "X":
                        valueText = "ữ";
                        break;
                    case "J":
                        valueText = "ự";
                        break;
                    case "W":
                        valueText = "uW";
                        break;
                    default:
                        break;
                }
                break;
            case "ứ":
                switch (textInput) {
                    case "s":
                        valueText = "ưs";
                        break;
                    case "f":
                        valueText = "ừ";
                        break;
                    case "r":
                        valueText = "ử";
                        break;
                    case "x":
                        valueText = "ữ";
                        break;
                    case "j":
                        valueText = "ự";
                        break;
                    case "w":
                        valueText = "úw";
                        break;
                    case "S":
                        valueText = "ưS";
                        break;
                    case "F":
                        valueText = "ừ";
                        break;
                    case "R":
                        valueText = "ử";
                        break;
                    case "X":
                        valueText = "ữ";
                        break;
                    case "J":
                        valueText = "ự";
                        break;
                    case "W":
                        valueText = "úW";
                        break;
                    default:
                        break;
                }
                break;
            case "ừ":
                switch (textInput) {
                    case "s":
                        valueText = "ứ";
                        break;
                    case "f":
                        valueText = "ưf";
                        break;
                    case "r":
                        valueText = "ử";
                        break;
                    case "x":
                        valueText = "ữ";
                        break;
                    case "j":
                        valueText = "ự";
                        break;
                    case "w":
                        valueText = "ùw";
                        break;
                    case "S":
                        valueText = "ứ";
                        break;
                    case "F":
                        valueText = "ưF";
                        break;
                    case "R":
                        valueText = "ử";
                        break;
                    case "X":
                        valueText = "ữ";
                        break;
                    case "J":
                        valueText = "ự";
                        break;
                    case "W":
                        valueText = "ùW";
                        break;
                    default:
                        break;
                }
                break;
            case "ử":
                switch (textInput) {
                    case "s":
                        valueText = "ứ";
                        break;
                    case "f":
                        valueText = "ừ";
                        break;
                    case "r":
                        valueText = "ưr";
                        break;
                    case "x":
                        valueText = "ữ";
                        break;
                    case "j":
                        valueText = "ự";
                        break;
                    case "w":
                        valueText = "ủw";
                        break;
                    case "S":
                        valueText = "ứ";
                        break;
                    case "F":
                        valueText = "ừ";
                        break;
                    case "R":
                        valueText = "ưR";
                        break;
                    case "X":
                        valueText = "ữ";
                        break;
                    case "J":
                        valueText = "ự";
                        break;
                    case "W":
                        valueText = "ủW";
                        break;
                    default:
                        break;
                }
                break;
            case "ữ":
                switch (textInput) {
                    case "s":
                        valueText = "ứ";
                        break;
                    case "f":
                        valueText = "ừ";
                        break;
                    case "r":
                        valueText = "ử";
                        break;
                    case "x":
                        valueText = "ữ";
                        break;
                    case "j":
                        valueText = "ự";
                        break;
                    case "w":
                        valueText = "ũW";
                        break;
                    case "S":
                        valueText = "ứ";
                        break;
                    case "F":
                        valueText = "ừ";
                        break;
                    case "R":
                        valueText = "ử";
                        break;
                    case "X":
                        valueText = "ữ";
                        break;
                    case "J":
                        valueText = "ự";
                        break;
                    case "W":
                        valueText = "ũW";
                        break;
                    default:
                        break;
                }
                break;
            case "ự":
                switch (textInput) {
                    case "s":
                        valueText = "ứ";
                        break;
                    case "f":
                        valueText = "ừ";
                        break;
                    case "r":
                        valueText = "ử";
                        break;
                    case "x":
                        valueText = "ữ";
                        break;
                    case "j":
                        valueText = "ưj";
                        break;
                    case "w":
                        valueText = "ụw";
                        break;
                    case "S":
                        valueText = "ứ";
                        break;
                    case "F":
                        valueText = "ừ";
                        break;
                    case "R":
                        valueText = "ử";
                        break;
                    case "X":
                        valueText = "ữ";
                        break;
                    case "J":
                        valueText = "ưJ";
                        break;
                    case "W":
                        valueText = "ụW";
                        break;
                    default:
                        break;
                }
                break;
            case "o":
                switch (textInput) {
                    case "s":
                        valueText = "ó";
                        break;
                    case "f":
                        valueText = "ò";
                        break;
                    case "r":
                        valueText = "ỏ";
                        break;
                    case "x":
                        valueText = "õ";
                        break;
                    case "j":
                        valueText = "ọ";
                        break;
                    case "w":
                        valueText = "ơ";
                        break;
                    case "o":
                        valueText = "ô";
                        break;
                    case "S":
                        valueText = "ó";
                        break;
                    case "F":
                        valueText = "ò";
                        break;
                    case "R":
                        valueText = "ỏ";
                        break;
                    case "X":
                        valueText = "õ";
                        break;
                    case "J":
                        valueText = "ọ";
                        break;
                    case "W":
                        valueText = "ơ";
                        break;
                    case "O":
                        valueText = "ô";
                        break;
                    default:
                        break;
                }
                break;
            case "ó":
                switch (textInput) {
                    case "s":
                        valueText = "os";
                        break;
                    case "f":
                        valueText = "ò";
                        break;
                    case "r":
                        valueText = "ỏ";
                        break;
                    case "x":
                        valueText = "õ";
                        break;
                    case "j":
                        valueText = "ọ";
                        break;
                    case "w":
                        valueText = "ớ";
                        break;
                    case "o":
                        valueText = "ố";
                        break;
                    case "S":
                        valueText = "oS";
                        break;
                    case "F":
                        valueText = "ò";
                        break;
                    case "R":
                        valueText = "ỏ";
                        break;
                    case "X":
                        valueText = "õ";
                        break;
                    case "J":
                        valueText = "ọ";
                        break;
                    case "W":
                        valueText = "ớ";
                        break;
                    case "O":
                        valueText = "ố";
                        break;
                    default:
                        break;
                }
                break;
            case "ò":
                switch (textInput) {
                    case "s":
                        valueText = "ó";
                        break;
                    case "f":
                        valueText = "of";
                        break;
                    case "r":
                        valueText = "ỏ";
                        break;
                    case "x":
                        valueText = "õ";
                        break;
                    case "j":
                        valueText = "ọ";
                        break;
                    case "w":
                        valueText = "ờ";
                        break;
                    case "o":
                        valueText = "ồ";
                        break;
                    case "S":
                        valueText = "ó";
                        break;
                    case "F":
                        valueText = "oF";
                        break;
                    case "R":
                        valueText = "ỏ";
                        break;
                    case "X":
                        valueText = "õ";
                        break;
                    case "J":
                        valueText = "ọ";
                        break;
                    case "W":
                        valueText = "ờ";
                        break;
                    case "O":
                        valueText = "ồ";
                        break;
                    default:
                        break;
                }
                break;
            case "ỏ":
                switch (textInput) {
                    case "s":
                        valueText = "ó";
                        break;
                    case "f":
                        valueText = "ò";
                        break;
                    case "r":
                        valueText = "or";
                        break;
                    case "x":
                        valueText = "õ";
                        break;
                    case "j":
                        valueText = "ọ";
                        break;
                    case "w":
                        valueText = "ở";
                        break;
                    case "o":
                        valueText = "ổ";
                        break;
                    case "S":
                        valueText = "ó";
                        break;
                    case "F":
                        valueText = "ò";
                        break;
                    case "R":
                        valueText = "oR";
                        break;
                    case "X":
                        valueText = "õ";
                        break;
                    case "J":
                        valueText = "ọ";
                        break;
                    case "W":
                        valueText = "ở";
                        break;
                    case "O":
                        valueText = "ổ";
                        break;
                    default:
                        break;
                }
                break;
            case "õ":
                switch (textInput) {
                    case "s":
                        valueText = "ó";
                        break;
                    case "f":
                        valueText = "ò";
                        break;
                    case "r":
                        valueText = "ỏ";
                        break;
                    case "x":
                        valueText = "ox";
                        break;
                    case "j":
                        valueText = "ọ";
                        break;
                    case "w":
                        valueText = "ỡ";
                        break;
                    case "o":
                        valueText = "ỗ";
                        break;
                    case "S":
                        valueText = "ó";
                        break;
                    case "F":
                        valueText = "ò";
                        break;
                    case "R":
                        valueText = "ỏ";
                        break;
                    case "X":
                        valueText = "oX";
                        break;
                    case "J":
                        valueText = "ọ";
                        break;
                    case "W":
                        valueText = "ỡ";
                        break;
                    case "O":
                        valueText = "ỗ";
                        break;
                    default:
                        break;
                }
                break;
            case "ọ":
                switch (textInput) {
                    case "s":
                        valueText = "ó";
                        break;
                    case "f":
                        valueText = "ò";
                        break;
                    case "r":
                        valueText = "ỏ";
                        break;
                    case "x":
                        valueText = "õ";
                        break;
                    case "j":
                        valueText = "oj";
                        break;
                    case "w":
                        valueText = "ợ";
                        break;
                    case "o":
                        valueText = "ộ";
                        break;
                    case "S":
                        valueText = "ó";
                        break;
                    case "F":
                        valueText = "ò";
                        break;
                    case "R":
                        valueText = "ỏ";
                        break;
                    case "X":
                        valueText = "õ";
                        break;
                    case "J":
                        valueText = "oJ";
                        break;
                    case "W":
                        valueText = "ợ";
                        break;
                    case "O":
                        valueText = "ộ";
                        break;
                    default:
                        break;
                }
                break;
            case "ơ":
                switch (textInput) {
                    case "s":
                        valueText = "ớ";
                        break;
                    case "f":
                        valueText = "ờ";
                        break;
                    case "r":
                        valueText = "ở";
                        break;
                    case "x":
                        valueText = "ỡ";
                        break;
                    case "j":
                        valueText = "ợ";
                        break;
                    case "w":
                        valueText = "ow";
                        break;
                    case "o":
                        valueText = "ô";
                        break;
                    case "S":
                        valueText = "ớ";
                        break;
                    case "F":
                        valueText = "ờ";
                        break;
                    case "R":
                        valueText = "ở";
                        break;
                    case "X":
                        valueText = "ỡ";
                        break;
                    case "J":
                        valueText = "ợ";
                        break;
                    case "W":
                        valueText = "oW";
                        break;
                    case "O":
                        valueText = "ô";
                        break;
                    default:
                        break;
                }
                break;
            case "ớ":
                switch (textInput) {
                    case "s":
                        valueText = "ơs";
                        break;
                    case "f":
                        valueText = "ờ";
                        break;
                    case "r":
                        valueText = "ở";
                        break;
                    case "x":
                        valueText = "ỡ";
                        break;
                    case "j":
                        valueText = "ợ";
                        break;
                    case "w":
                        valueText = "ów";
                        break;
                    case "o":
                        valueText = "ố";
                        break;
                    case "S":
                        valueText = "ơS";
                        break;
                    case "F":
                        valueText = "ờ";
                        break;
                    case "R":
                        valueText = "ở";
                        break;
                    case "X":
                        valueText = "ỡ";
                        break;
                    case "J":
                        valueText = "ợ";
                        break;
                    case "W":
                        valueText = "óW";
                        break;
                    case "O":
                        valueText = "ố";
                        break;
                    default:
                        break;
                }
                break;
            case "ờ":
                switch (textInput) {
                    case "s":
                        valueText = "ớ";
                        break;
                    case "f":
                        valueText = "ơf";
                        break;
                    case "r":
                        valueText = "ở";
                        break;
                    case "x":
                        valueText = "ỡ";
                        break;
                    case "j":
                        valueText = "ợ";
                        break;
                    case "w":
                        valueText = "òw";
                        break;
                    case "o":
                        valueText = "ồ";
                        break;
                    case "S":
                        valueText = "ớ";
                        break;
                    case "F":
                        valueText = "ơF";
                        break;
                    case "R":
                        valueText = "ở";
                        break;
                    case "X":
                        valueText = "ỡ";
                        break;
                    case "J":
                        valueText = "ợ";
                        break;
                    case "W":
                        valueText = "òW";
                        break;
                    case "O":
                        valueText = "ồ";
                        break;
                    default:
                        break;
                }
                break;
            case "ở":
                switch (textInput) {
                    case "s":
                        valueText = "ớ";
                        break;
                    case "f":
                        valueText = "ờ";
                        break;
                    case "r":
                        valueText = "ơr";
                        break;
                    case "x":
                        valueText = "ỡ";
                        break;
                    case "j":
                        valueText = "ợ";
                        break;
                    case "w":
                        valueText = "ỏw";
                        break;
                    case "o":
                        valueText = "ổ";
                        break;
                    case "S":
                        valueText = "ớ";
                        break;
                    case "F":
                        valueText = "ờ";
                        break;
                    case "R":
                        valueText = "ơR";
                        break;
                    case "X":
                        valueText = "ỡ";
                        break;
                    case "J":
                        valueText = "ợ";
                        break;
                    case "W":
                        valueText = "ỏW";
                        break;
                    case "O":
                        valueText = "ổ";
                        break;
                    default:
                        break;
                }
                break;
            case "ỡ":
                switch (textInput) {
                    case "s":
                        valueText = "ớ";
                        break;
                    case "f":
                        valueText = "ờ";
                        break;
                    case "r":
                        valueText = "ở";
                        break;
                    case "x":
                        valueText = "ơx";
                        break;
                    case "j":
                        valueText = "ợ";
                        break;
                    case "w":
                        valueText = "õw";
                        break;
                    case "o":
                        valueText = "ỗ";
                        break;
                    case "S":
                        valueText = "ớ";
                        break;
                    case "F":
                        valueText = "ờ";
                        break;
                    case "R":
                        valueText = "ở";
                        break;
                    case "X":
                        valueText = "ơX";
                        break;
                    case "J":
                        valueText = "ợ";
                        break;
                    case "W":
                        valueText = "õW";
                        break;
                    case "O":
                        valueText = "ỗ";
                        break;
                    default:
                        break;
                }
                break;
            case "ợ":
                switch (textInput) {
                    case "s":
                        valueText = "ớ";
                        break;
                    case "f":
                        valueText = "ờ";
                        break;
                    case "r":
                        valueText = "ở";
                        break;
                    case "x":
                        valueText = "ỡ";
                        break;
                    case "j":
                        valueText = "ơj";
                        break;
                    case "w":
                        valueText = "ọw";
                        break;
                    case "o":
                        valueText = "ộ";
                        break;
                    case "S":
                        valueText = "ớ";
                        break;
                    case "F":
                        valueText = "ờ";
                        break;
                    case "R":
                        valueText = "ở";
                        break;
                    case "X":
                        valueText = "ỡ";
                        break;
                    case "J":
                        valueText = "ơJ";
                        break;
                    case "W":
                        valueText = "ọW";
                        break;
                    case "O":
                        valueText = "ộ";
                        break;
                    default:
                        break;
                }
                break;
            case "ô":
                switch (textInput) {
                    case "s":
                        valueText = "ố";
                        break;
                    case "f":
                        valueText = "ồ";
                        break;
                    case "r":
                        valueText = "ổ";
                        break;
                    case "x":
                        valueText = "ỗ";
                        break;
                    case "j":
                        valueText = "ộ";
                        break;
                    case "o":
                        valueText = "oo";
                        break;
                    case "w":
                        valueText = "ơ";
                        break;
                    case "S":
                        valueText = "ố";
                        break;
                    case "F":
                        valueText = "ồ";
                        break;
                    case "R":
                        valueText = "ổ";
                        break;
                    case "X":
                        valueText = "ỗ";
                        break;
                    case "J":
                        valueText = "ộ";
                        break;
                    case "O":
                        valueText = "oO";
                        break;
                    case "W":
                        valueText = "ơ";
                        break;
                    default:
                        break;
                }
                break;
            case "ố":
                switch (textInput) {
                    case "s":
                        valueText = "ôs";
                        break;
                    case "f":
                        valueText = "ồ";
                        break;
                    case "r":
                        valueText = "ổ";
                        break;
                    case "x":
                        valueText = "ỗ";
                        break;
                    case "j":
                        valueText = "ộ";
                        break;
                    case "o":
                        valueText = "óo";
                        break;
                    case "w":
                        valueText = "ớ";
                        break;
                    case "S":
                        valueText = "ôS";
                        break;
                    case "F":
                        valueText = "ồ";
                        break;
                    case "R":
                        valueText = "ổ";
                        break;
                    case "X":
                        valueText = "ỗ";
                        break;
                    case "J":
                        valueText = "ộ";
                        break;
                    case "O":
                        valueText = "óO";
                        break;
                    case "W":
                        valueText = "ớ";
                        break;
                    default:
                        break;
                }
                break;
            case "ồ":
                switch (textInput) {
                    case "s":
                        valueText = "ố";
                        break;
                    case "f":
                        valueText = "ôf";
                        break;
                    case "r":
                        valueText = "ổ";
                        break;
                    case "x":
                        valueText = "ỗ";
                        break;
                    case "j":
                        valueText = "ộ";
                        break;
                    case "o":
                        valueText = "òo";
                        break;
                    case "w":
                        valueText = "ờ";
                        break;
                    case "S":
                        valueText = "ố";
                        break;
                    case "F":
                        valueText = "ôF";
                        break;
                    case "R":
                        valueText = "ổ";
                        break;
                    case "X":
                        valueText = "ỗ";
                        break;
                    case "J":
                        valueText = "ộ";
                        break;
                    case "O":
                        valueText = "òO";
                        break;
                    case "W":
                        valueText = "ờ";
                        break;
                    default:
                        break;
                }
                break;
            case "ổ":
                switch (textInput) {
                    case "s":
                        valueText = "ố";
                        break;
                    case "f":
                        valueText = "ồ";
                        break;
                    case "r":
                        valueText = "ôr";
                        break;
                    case "x":
                        valueText = "ỗ";
                        break;
                    case "j":
                        valueText = "ộ";
                        break;
                    case "o":
                        valueText = "ỏo";
                        break;
                    case "w":
                        valueText = "ở";
                        break;
                    case "S":
                        valueText = "ố";
                        break;
                    case "F":
                        valueText = "ồ";
                        break;
                    case "R":
                        valueText = "ôR";
                        break;
                    case "X":
                        valueText = "ỗ";
                        break;
                    case "J":
                        valueText = "ộ";
                        break;
                    case "O":
                        valueText = "ỏO";
                        break;
                    case "W":
                        valueText = "ở";
                        break;
                    default:
                        break;
                }
                break;
            case "ỗ":
                switch (textInput) {
                    case "s":
                        valueText = "ố";
                        break;
                    case "f":
                        valueText = "ồ";
                        break;
                    case "r":
                        valueText = "ổ";
                        break;
                    case "x":
                        valueText = "ôx";
                        break;
                    case "j":
                        valueText = "ộ";
                        break;
                    case "o":
                        valueText = "õo";
                        break;
                    case "w":
                        valueText = "ỡ";
                        break;
                    case "S":
                        valueText = "ố";
                        break;
                    case "F":
                        valueText = "ồ";
                        break;
                    case "R":
                        valueText = "ổ";
                        break;
                    case "X":
                        valueText = "ôX";
                        break;
                    case "J":
                        valueText = "ộ";
                        break;
                    case "O":
                        valueText = "õO";
                        break;
                    case "W":
                        valueText = "ỡ";
                        break;
                    default:
                        break;
                }
                break;
            case "ộ":
                switch (textInput) {
                    case "s":
                        valueText = "ố";
                        break;
                    case "f":
                        valueText = "ồ";
                        break;
                    case "r":
                        valueText = "ổ";
                        break;
                    case "x":
                        valueText = "ỗ";
                        break;
                    case "j":
                        valueText = "ộ";
                        break;
                    case "o":
                        valueText = "ọo";
                        break;
                    case "w":
                        valueText = "ợ";
                        break;
                    case "S":
                        valueText = "ố";
                        break;
                    case "F":
                        valueText = "ồ";
                        break;
                    case "R":
                        valueText = "ổ";
                        break;
                    case "X":
                        valueText = "ỗ";
                        break;
                    case "J":
                        valueText = "ộ";
                        break;
                    case "O":
                        valueText = "ọO";
                        break;
                    case "W":
                        valueText = "ợ";
                        break;
                    default:
                        break;
                }
                break;
//            // UP CHARACTER
            case "U":
                switch (textInput) {
                    case "s":
                        valueText = "Ú";
                        break;
                    case "f":
                        valueText = "Ù";
                        break;
                    case "r":
                        valueText = "Ủ";
                        break;
                    case "x":
                        valueText = "Ũ";
                        break;
                    case "j":
                        valueText = "Ụ";
                        break;
                    case "w":
                        valueText = "Ư";
                        break;
                    case "S":
                        valueText = "Ú";
                        break;
                    case "F":
                        valueText = "Ù";
                        break;
                    case "R":
                        valueText = "Ủ";
                        break;
                    case "X":
                        valueText = "Ũ";
                        break;
                    case "J":
                        valueText = "Ụ";
                        break;
                    case "W":
                        valueText = "Ư";
                        break;
                    default:
                        break;
                }
                break;
            case "Ú":
                switch (textInput) {
                    case "s":
                        valueText = "Us";
                        break;
                    case "f":
                        valueText = "Ù";
                        break;
                    case "r":
                        valueText = "Ủ";
                        break;
                    case "x":
                        valueText = "Ũ";
                        break;
                    case "j":
                        valueText = "Ụ";
                        break;
                    case "w":
                        valueText = "Ư";
                        break;
                    case "S":
                        valueText = "US";
                        break;
                    case "F":
                        valueText = "Ù";
                        break;
                    case "R":
                        valueText = "Ủ";
                        break;
                    case "X":
                        valueText = "Ũ";
                        break;
                    case "J":
                        valueText = "Ụ";
                        break;
                    case "W":
                        valueText = "Ứ";
                        break;
                    default:
                        break;
                }
                break;
            case "Ù":
                switch (textInput) {
                    case "s":
                        valueText = "Ú";
                        break;
                    case "f":
                        valueText = "Uf";
                        break;
                    case "r":
                        valueText = "Ủ";
                        break;
                    case "x":
                        valueText = "Ũ";
                        break;
                    case "j":
                        valueText = "Ụ";
                        break;
                    case "w":
                        valueText = "Ư";
                        break;
                    case "S":
                        valueText = "Ú";
                        break;
                    case "F":
                        valueText = "UF";
                        break;
                    case "R":
                        valueText = "Ủ";
                        break;
                    case "X":
                        valueText = "Ũ";
                        break;
                    case "J":
                        valueText = "Ụ";
                        break;
                    case "W":
                        valueText = "Ừ";
                        break;
                    default:
                        break;
                }
                break;
            case "Ủ":
                switch (textInput) {
                    case "s":
                        valueText = "Ú";
                        break;
                    case "f":
                        valueText = "Ù";
                        break;
                    case "r":
                        valueText = "Ur";
                        break;
                    case "x":
                        valueText = "Ũ";
                        break;
                    case "j":
                        valueText = "Ụ";
                        break;
                    case "w":
                        valueText = "Ư";
                        break;
                    case "S":
                        valueText = "Ú";
                        break;
                    case "F":
                        valueText = "Ù";
                        break;
                    case "R":
                        valueText = "UR";
                        break;
                    case "X":
                        valueText = "Ũ";
                        break;
                    case "J":
                        valueText = "Ụ";
                        break;
                    case "W":
                        valueText = "Ử";
                        break;
                    default:
                        break;
                }
                break;
            case "Ũ":
                switch (textInput) {
                    case "s":
                        valueText = "Ú";
                        break;
                    case "f":
                        valueText = "Ù";
                        break;
                    case "r":
                        valueText = "Ủ";
                        break;
                    case "x":
                        valueText = "Ux";
                        break;
                    case "j":
                        valueText = "Ụ";
                        break;
                    case "w":
                        valueText = "Ư";
                        break;
                    case "S":
                        valueText = "Ú";
                        break;
                    case "F":
                        valueText = "Ù";
                        break;
                    case "R":
                        valueText = "Ủ";
                        break;
                    case "X":
                        valueText = "UX";
                        break;
                    case "J":
                        valueText = "Ụ";
                        break;
                    case "W":
                        valueText = "Ữ";
                        break;
                    default:
                        break;
                }
                break;
            case "Ụ":
                switch (textInput) {
                    case "s":
                        valueText = "Ú";
                        break;
                    case "f":
                        valueText = "Ù";
                        break;
                    case "r":
                        valueText = "Ủ";
                        break;
                    case "x":
                        valueText = "Ũ";
                        break;
                    case "j":
                        valueText = "Uj";
                        break;
                    case "w":
                        valueText = "Ư";
                        break;
                    case "S":
                        valueText = "Ú";
                        break;
                    case "F":
                        valueText = "Ù";
                        break;
                    case "R":
                        valueText = "Ủ";
                        break;
                    case "X":
                        valueText = "Ũ";
                        break;
                    case "J":
                        valueText = "UJ";
                        break;
                    case "W":
                        valueText = "Ự";
                        break;
                    default:
                        break;
                }
                break;
            case "Ư":
                switch (textInput) {
                    case "s":
                        valueText = "Ứ";
                        break;
                    case "f":
                        valueText = "Ừ";
                        break;
                    case "r":
                        valueText = "Ử";
                        break;
                    case "x":
                        valueText = "Ữ";
                        break;
                    case "j":
                        valueText = "Ự";
                        break;
                    case "w":
                        valueText = "Uw";
                        break;
                    case "S":
                        valueText = "Ứ";
                        break;
                    case "F":
                        valueText = "Ừ";
                        break;
                    case "R":
                        valueText = "Ử";
                        break;
                    case "X":
                        valueText = "Ữ";
                        break;
                    case "J":
                        valueText = "Ự";
                        break;
                    case "W":
                        valueText = "UW";
                        break;
                    default:
                        break;
                }
                break;
            case "Ứ":
                switch (textInput) {
                    case "s":
                        valueText = "Ưs";
                        break;
                    case "f":
                        valueText = "Ừ";
                        break;
                    case "r":
                        valueText = "Ử";
                        break;
                    case "x":
                        valueText = "Ữ";
                        break;
                    case "j":
                        valueText = "Ự";
                        break;
                    case "w":
                        valueText = "Úw";
                        break;
                    case "S":
                        valueText = "ƯS";
                        break;
                    case "F":
                        valueText = "Ừ";
                        break;
                    case "R":
                        valueText = "Ử";
                        break;
                    case "X":
                        valueText = "Ữ";
                        break;
                    case "J":
                        valueText = "Ự";
                        break;
                    case "W":
                        valueText = "ÚW";
                        break;
                    default:
                        break;
                }
                break;
            case "Ừ":
                switch (textInput) {
                    case "s":
                        valueText = "Ứ";
                        break;
                    case "f":
                        valueText = "Ưf";
                        break;
                    case "r":
                        valueText = "Ử";
                        break;
                    case "x":
                        valueText = "Ữ";
                        break;
                    case "j":
                        valueText = "Ự";
                        break;
                    case "w":
                        valueText = "Ùw";
                        break;
                    case "S":
                        valueText = "Ứ";
                        break;
                    case "F":
                        valueText = "ƯF";
                        break;
                    case "R":
                        valueText = "Ử";
                        break;
                    case "X":
                        valueText = "Ữ";
                        break;
                    case "J":
                        valueText = "Ự";
                        break;
                    case "W":
                        valueText = "ÙW";
                        break;
                    default:
                        break;
                }
                break;
            case "Ử":
                switch (textInput) {
                    case "s":
                        valueText = "Ứ";
                        break;
                    case "f":
                        valueText = "Ừ";
                        break;
                    case "r":
                        valueText = "Ưr";
                        break;
                    case "x":
                        valueText = "Ữ";
                        break;
                    case "j":
                        valueText = "Ự";
                        break;
                    case "w":
                        valueText = "Ủw";
                        break;
                    case "S":
                        valueText = "Ứ";
                        break;
                    case "F":
                        valueText = "Ừ";
                        break;
                    case "R":
                        valueText = "ƯR";
                        break;
                    case "X":
                        valueText = "Ữ";
                        break;
                    case "J":
                        valueText = "Ự";
                        break;
                    case "W":
                        valueText = "ỦW";
                        break;
                    default:
                        break;
                }
                break;
            case "Ữ":
                switch (textInput) {
                    case "s":
                        valueText = "Ứ";
                        break;
                    case "f":
                        valueText = "Ừ";
                        break;
                    case "r":
                        valueText = "Ử";
                        break;
                    case "x":
                        valueText = "Ưx";
                        break;
                    case "j":
                        valueText = "Ự";
                        break;
                    case "w":
                        valueText = "Ũw";
                        break;
                    case "S":
                        valueText = "Ứ";
                        break;
                    case "F":
                        valueText = "Ừ";
                        break;
                    case "R":
                        valueText = "Ử";
                        break;
                    case "X":
                        valueText = "ƯX";
                        break;
                    case "J":
                        valueText = "Ự";
                        break;
                    case "W":
                        valueText = "ŨW";
                        break;
                    default:
                        break;
                }
                break;
            case "Ự":
                switch (textInput) {
                    case "s":
                        valueText = "Ứ";
                        break;
                    case "f":
                        valueText = "Ừ";
                        break;
                    case "r":
                        valueText = "Ử";
                        break;
                    case "x":
                        valueText = "Ữ";
                        break;
                    case "j":
                        valueText = "Ưj";
                        break;
                    case "w":
                        valueText = "Ụw";
                        break;
                    case "S":
                        valueText = "Ứ";
                        break;
                    case "F":
                        valueText = "Ừ";
                        break;
                    case "R":
                        valueText = "Ử";
                        break;
                    case "X":
                        valueText = "Ữ";
                        break;
                    case "J":
                        valueText = "ƯJ";
                        break;
                    case "W":
                        valueText = "ỤW";
                        break;
                    default:
                        break;
                }
                break;
            case "O":
                switch (textInput) {
                    case "s":
                        valueText = "Ó";
                        break;
                    case "f":
                        valueText = "Ò";
                        break;
                    case "r":
                        valueText = "Ỏ";
                        break;
                    case "x":
                        valueText = "Õ";
                        break;
                    case "j":
                        valueText = "Ọ";
                        break;
                    case "w":
                        valueText = "Ơ";
                        break;
                    case "o":
                        valueText = "Ô";
                        break;
                    case "S":
                        valueText = "Ó";
                        break;
                    case "F":
                        valueText = "Ò";
                        break;
                    case "R":
                        valueText = "Ỏ";
                        break;
                    case "X":
                        valueText = "Õ";
                        break;
                    case "J":
                        valueText = "Ọ";
                        break;
                    case "W":
                        valueText = "Ơ";
                        break;
                    case "O":
                        valueText = "Ô";
                        break;
                    default:
                        break;
                }
                break;
            case "Ó":
                switch (textInput) {
                    case "s":
                        valueText = "Os";
                        break;
                    case "f":
                        valueText = "Ò";
                        break;
                    case "r":
                        valueText = "Ỏ";
                        break;
                    case "x":
                        valueText = "Õ";
                        break;
                    case "j":
                        valueText = "Ọ";
                        break;
                    case "w":
                        valueText = "Ớ";
                        break;
                    case "o":
                        valueText = "Ố";
                        break;
                    case "S":
                        valueText = "OS";
                        break;
                    case "F":
                        valueText = "Ò";
                        break;
                    case "R":
                        valueText = "Ỏ";
                        break;
                    case "X":
                        valueText = "Õ";
                        break;
                    case "J":
                        valueText = "Ọ";
                        break;
                    case "W":
                        valueText = "Ớ";
                        break;
                    case "O":
                        valueText = "Ố";
                        break;
                    default:
                        break;
                }
                break;
            case "Ò":
                switch (textInput) {
                    case "s":
                        valueText = "Ó";
                        break;
                    case "f":
                        valueText = "Of";
                        break;
                    case "r":
                        valueText = "Ỏ";
                        break;
                    case "x":
                        valueText = "Õ";
                        break;
                    case "j":
                        valueText = "Ọ";
                        break;
                    case "w":
                        valueText = "Ờ";
                        break;
                    case "o":
                        valueText = "Ồ";
                        break;
                    case "S":
                        valueText = "Ó";
                        break;
                    case "F":
                        valueText = "OF";
                        break;
                    case "R":
                        valueText = "Ỏ";
                        break;
                    case "X":
                        valueText = "Õ";
                        break;
                    case "J":
                        valueText = "Ọ";
                        break;
                    case "W":
                        valueText = "Ờ";
                        break;
                    case "O":
                        valueText = "Ồ";
                        break;
                    default:
                        break;
                }
                break;
            case "Ỏ":
                switch (textInput) {
                    case "s":
                        valueText = "Ó";
                        break;
                    case "f":
                        valueText = "Ò";
                        break;
                    case "r":
                        valueText = "Or";
                        break;
                    case "x":
                        valueText = "Õ";
                        break;
                    case "j":
                        valueText = "Ọ";
                        break;
                    case "w":
                        valueText = "Ơ";
                        break;
                    case "o":
                        valueText = "Ô";
                        break;
                    case "S":
                        valueText = "Ó";
                        break;
                    case "F":
                        valueText = "Ò";
                        break;
                    case "R":
                        valueText = "OR";
                        break;
                    case "X":
                        valueText = "Õ";
                        break;
                    case "J":
                        valueText = "Ọ";
                        break;
                    case "W":
                        valueText = "Ở";
                        break;
                    case "O":
                        valueText = "Ổ";
                        break;
                    default:
                        break;
                }
                break;
            case "Õ":
                switch (textInput) {
                    case "s":
                        valueText = "Ó";
                        break;
                    case "f":
                        valueText = "Ò";
                        break;
                    case "r":
                        valueText = "Ỏ";
                        break;
                    case "x":
                        valueText = "Ox";
                        break;
                    case "j":
                        valueText = "Ọ";
                        break;
                    case "w":
                        valueText = "Ỡ";
                        break;
                    case "o":
                        valueText = "Ỗ";
                        break;
                    case "S":
                        valueText = "Ó";
                        break;
                    case "F":
                        valueText = "Ò";
                        break;
                    case "R":
                        valueText = "Ỏ";
                        break;
                    case "X":
                        valueText = "OX";
                        break;
                    case "J":
                        valueText = "Ọ";
                        break;
                    case "W":
                        valueText = "Ỡ";
                        break;
                    case "O":
                        valueText = "Ỗ";
                        break;
                    default:
                        break;
                }
                break;
            case "Ọ":
                switch (textInput) {
                    case "s":
                        valueText = "Ó";
                        break;
                    case "f":
                        valueText = "Ò";
                        break;
                    case "r":
                        valueText = "Ỏ";
                        break;
                    case "x":
                        valueText = "Õ";
                        break;
                    case "j":
                        valueText = "Oj";
                        break;
                    case "w":
                        valueText = "Ợ";
                        break;
                    case "o":
                        valueText = "Ộ";
                        break;
                    case "S":
                        valueText = "Ó";
                        break;
                    case "F":
                        valueText = "Ò";
                        break;
                    case "R":
                        valueText = "Ỏ";
                        break;
                    case "X":
                        valueText = "Õ";
                        break;
                    case "J":
                        valueText = "OJ";
                        break;
                    case "W":
                        valueText = "Ợ";
                        break;
                    case "O":
                        valueText = "Ộ";
                        break;
                    default:
                        break;
                }
                break;
            case "Ơ":
                switch (textInput) {
                    case "s":
                        valueText = "Ớ";
                        break;
                    case "f":
                        valueText = "Ờ";
                        break;
                    case "r":
                        valueText = "Ở";
                        break;
                    case "x":
                        valueText = "Ỡ";
                        break;
                    case "j":
                        valueText = "Ợ";
                        break;
                    case "w":
                        valueText = "Ow";
                        break;
                    case "o":
                        valueText = "Ô";
                        break;
                    case "S":
                        valueText = "Ớ";
                        break;
                    case "F":
                        valueText = "Ờ";
                        break;
                    case "R":
                        valueText = "Ở";
                        break;
                    case "X":
                        valueText = "Ỡ";
                        break;
                    case "J":
                        valueText = "Ợ";
                        break;
                    case "W":
                        valueText = "OW";
                        break;
                    case "O":
                        valueText = "Ô";
                        break;
                    default:
                        break;
                }
                break;
            case "Ớ":
                switch (textInput) {
                    case "s":
                        valueText = "Ơs";
                        break;
                    case "f":
                        valueText = "Ờ";
                        break;
                    case "r":
                        valueText = "Ở";
                        break;
                    case "x":
                        valueText = "Ỡ";
                        break;
                    case "j":
                        valueText = "Ợ";
                        break;
                    case "w":
                        valueText = "Ów";
                        break;
                    case "o":
                        valueText = "Ố";
                        break;
                    case "S":
                        valueText = "ƠS";
                        break;
                    case "F":
                        valueText = "Ờ";
                        break;
                    case "R":
                        valueText = "Ở";
                        break;
                    case "X":
                        valueText = "Ỡ";
                        break;
                    case "J":
                        valueText = "Ợ";
                        break;
                    case "W":
                        valueText = "ÓW";
                        break;
                    case "O":
                        valueText = "Ố";
                        break;
                    default:
                        break;
                }
                break;
            case "Ờ":
                switch (textInput) {
                    case "s":
                        valueText = "Ớ";
                        break;
                    case "f":
                        valueText = "Ơf";
                        break;
                    case "r":
                        valueText = "Ở";
                        break;
                    case "x":
                        valueText = "Ỡ";
                        break;
                    case "j":
                        valueText = "Ợ";
                        break;
                    case "w":
                        valueText = "Òw";
                        break;
                    case "o":
                        valueText = "Ồ";
                        break;
                    case "S":
                        valueText = "Ớ";
                        break;
                    case "F":
                        valueText = "ƠF";
                        break;
                    case "R":
                        valueText = "Ở";
                        break;
                    case "X":
                        valueText = "Ỡ";
                        break;
                    case "J":
                        valueText = "Ợ";
                        break;
                    case "W":
                        valueText = "ÒW";
                        break;
                    case "O":
                        valueText = "Ồ";
                        break;
                    default:
                        break;
                }
                break;
            case "Ở":
                switch (textInput) {
                    case "s":
                        valueText = "Ớ";
                        break;
                    case "f":
                        valueText = "Ờ";
                        break;
                    case "r":
                        valueText = "Ơr";
                        break;
                    case "x":
                        valueText = "Ỡ";
                        break;
                    case "j":
                        valueText = "Ợ";
                        break;
                    case "w":
                        valueText = "Ỏw";
                        break;
                    case "o":
                        valueText = "Ổ";
                        break;
                    case "S":
                        valueText = "Ớ";
                        break;
                    case "F":
                        valueText = "Ờ";
                        break;
                    case "R":
                        valueText = "ƠR";
                        break;
                    case "X":
                        valueText = "Ỡ";
                        break;
                    case "J":
                        valueText = "Ợ";
                        break;
                    case "W":
                        valueText = "ỎW";
                        break;
                    case "O":
                        valueText = "Ổ";
                        break;
                    default:
                        break;
                }
                break;
            case "Ỡ":
                switch (textInput) {
                    case "s":
                        valueText = "Ớ";
                        break;
                    case "f":
                        valueText = "Ờ";
                        break;
                    case "r":
                        valueText = "Ở";
                        break;
                    case "x":
                        valueText = "Ơx";
                        break;
                    case "j":
                        valueText = "Ợ";
                        break;
                    case "w":
                        valueText = "Õw";
                        break;
                    case "o":
                        valueText = "Ỗ";
                        break;
                    case "S":
                        valueText = "Ớ";
                        break;
                    case "F":
                        valueText = "Ờ";
                        break;
                    case "R":
                        valueText = "Ở";
                        break;
                    case "X":
                        valueText = "ƠX";
                        break;
                    case "J":
                        valueText = "Ợ";
                        break;
                    case "W":
                        valueText = "ÕW";
                        break;
                    case "O":
                        valueText = "Ỗ";
                        break;
                    default:
                        break;
                }
                break;
            case "Ợ":
                switch (textInput) {
                    case "s":
                        valueText = "Ớ";
                        break;
                    case "f":
                        valueText = "Ờ";
                        break;
                    case "r":
                        valueText = "Ở";
                        break;
                    case "x":
                        valueText = "Ỡ";
                        break;
                    case "j":
                        valueText = "Ơj";
                        break;
                    case "w":
                        valueText = "Ọw";
                        break;
                    case "o":
                        valueText = "ÔJ";
                        break;
                    case "S":
                        valueText = "Ớ";
                        break;
                    case "F":
                        valueText = "Ờ";
                        break;
                    case "R":
                        valueText = "Ở";
                        break;
                    case "X":
                        valueText = "Ỡ";
                        break;
                    case "J":
                        valueText = "ƠJ";
                        break;
                    case "W":
                        valueText = "ỌW";
                        break;
                    case "O":
                        valueText = "Ộ";
                        break;
                    default:
                        break;
                }
                break;
            case "Ô":
                switch (textInput) {
                    case "s":
                        valueText = "Ố";
                        break;
                    case "f":
                        valueText = "Ồ";
                        break;
                    case "r":
                        valueText = "Ổ";
                        break;
                    case "x":
                        valueText = "Ỗ";
                        break;
                    case "j":
                        valueText = "Ộ";
                        break;
                    case "o":
                        valueText = "Oo";
                        break;
                    case "w":
                        valueText = "Ơ";
                        break;
                    case "S":
                        valueText = "Ố";
                        break;
                    case "F":
                        valueText = "Ồ";
                        break;
                    case "R":
                        valueText = "Ổ";
                        break;
                    case "X":
                        valueText = "Ỗ";
                        break;
                    case "J":
                        valueText = "Ộ";
                        break;
                    case "O":
                        valueText = "OO";
                        break;
                    case "W":
                        valueText = "Ơ";
                        break;
                    default:
                        break;
                }
                break;
            case "Ố":
                switch (textInput) {
                    case "s":
                        valueText = "Ôs";
                        break;
                    case "f":
                        valueText = "Ồ";
                        break;
                    case "r":
                        valueText = "Ổ";
                        break;
                    case "x":
                        valueText = "Ỗ";
                        break;
                    case "j":
                        valueText = "Ộ";
                        break;
                    case "o":
                        valueText = "Óo";
                        break;
                    case "w":
                        valueText = "Ớ";
                        break;
                    case "S":
                        valueText = "ÔS";
                        break;
                    case "F":
                        valueText = "Ồ";
                        break;
                    case "R":
                        valueText = "Ổ";
                        break;
                    case "X":
                        valueText = "Ỗ";
                        break;
                    case "J":
                        valueText = "Ộ";
                        break;
                    case "O":
                        valueText = "ÓO";
                        break;
                    case "W":
                        valueText = "Ớ";
                        break;
                    default:
                        break;
                }
                break;
            case "Ồ":
                switch (textInput) {
                    case "s":
                        valueText = "Ố";
                        break;
                    case "f":
                        valueText = "Ôf";
                        break;
                    case "r":
                        valueText = "Ổ";
                        break;
                    case "x":
                        valueText = "Ỗ";
                        break;
                    case "j":
                        valueText = "Ộ";
                        break;
                    case "o":
                        valueText = "Òo";
                        break;
                    case "w":
                        valueText = "Ờ";
                        break;
                    case "S":
                        valueText = "Ố";
                        break;
                    case "F":
                        valueText = "ÔF";
                        break;
                    case "R":
                        valueText = "Ổ";
                        break;
                    case "X":
                        valueText = "Ỗ";
                        break;
                    case "J":
                        valueText = "Ộ";
                        break;
                    case "O":
                        valueText = "ÒO";
                        break;
                    case "W":
                        valueText = "Ờ";
                        break;
                    default:
                        break;
                }
                break;
            case "Ổ":
                switch (textInput) {
                    case "s":
                        valueText = "Ố";
                        break;
                    case "f":
                        valueText = "Ồ";
                        break;
                    case "r":
                        valueText = "Ôr";
                        break;
                    case "x":
                        valueText = "Ỗ";
                        break;
                    case "j":
                        valueText = "Ộ";
                        break;
                    case "o":
                        valueText = "Ỏo";
                        break;
                    case "w":
                        valueText = "Ở";
                        break;
                    case "S":
                        valueText = "Ố";
                        break;
                    case "F":
                        valueText = "Ồ";
                        break;
                    case "R":
                        valueText = "ÔR";
                        break;
                    case "X":
                        valueText = "Ỗ";
                        break;
                    case "J":
                        valueText = "Ộ";
                        break;
                    case "O":
                        valueText = "ỎO";
                        break;
                    case "W":
                        valueText = "Ở";
                        break;
                    default:
                        break;
                }
                break;
            case "Ỗ":
                switch (textInput) {
                    case "s":
                        valueText = "Ố";
                        break;
                    case "f":
                        valueText = "Ồ";
                        break;
                    case "r":
                        valueText = "Ổ";
                        break;
                    case "x":
                        valueText = "Ôx";
                        break;
                    case "j":
                        valueText = "Ộ";
                        break;
                    case "o":
                        valueText = "Õo";
                        break;
                    case "w":
                        valueText = "Ỡ";
                        break;
                    case "S":
                        valueText = "Ố";
                        break;
                    case "F":
                        valueText = "Ồ";
                        break;
                    case "R":
                        valueText = "Ổ";
                        break;
                    case "X":
                        valueText = "ÔX";
                        break;
                    case "J":
                        valueText = "Ộ";
                        break;
                    case "O":
                        valueText = "ÕO";
                        break;
                    case "W":
                        valueText = "Ỡ";
                        break;
                    default:
                        break;
                }
                break;
            case "Ộ":
                switch (textInput) {
                    case "s":
                        valueText = "Ố";
                        break;
                    case "f":
                        valueText = "Ồ";
                        break;
                    case "r":
                        valueText = "Ổ";
                        break;
                    case "x":
                        valueText = "Ỗ";
                        break;
                    case "j":
                        valueText = "Ôj";
                        break;
                    case "o":
                        valueText = "Ọo";
                        break;
                    case "w":
                        valueText = "Ợ";
                        break;
                    case "S":
                        valueText = "Ố";
                        break;
                    case "F":
                        valueText = "Ồ";
                        break;
                    case "R":
                        valueText = "Ổ";
                        break;
                    case "X":
                        valueText = "Ỗ";
                        break;
                    case "J":
                        valueText = "ÔJ";
                        break;
                    case "O":
                        valueText = "ỌO";
                        break;
                    case "W":
                        valueText = "Ợ";
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }
        if(valueText != (textInput)){
//            textDocumentProxy.deleteBackward();
            inputConnection.deleteSurroundingText(1,0);
        }
        return valueText;
    }
}
