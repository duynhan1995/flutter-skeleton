package vn.etelecom.appcall.keyboard.convert;

import android.view.inputmethod.InputConnection;

public class ConvertOneLastCharacterIY {
    //i, y
    static String convert(String characterLastest, String textInput, InputConnection inputConnection){
        String valueText = textInput;
        switch (characterLastest){
            case "i":
                switch (textInput) {
                    case "s":
                        valueText = "í";
                        break;
                    case "f":
                        valueText = "ì";
                        break;
                    case "r":
                        valueText = "ỉ";
                        break;
                    case "x":
                        valueText = "ĩ";
                        break;
                    case "j":
                        valueText = "ị";
                        break;
                    case "S":
                        valueText = "í";
                        break;
                    case "F":
                        valueText = "ì";
                        break;
                    case "R":
                        valueText = "ỉ";
                        break;
                    case "X":
                        valueText = "ĩ";
                        break;
                    case "J":
                        valueText = "ị";
                        break;
                    default:
                        break;
                }
                break;
            case "í":
                switch (textInput) {
                    case "s":
                        valueText = "is";
                        break;
                    case "f":
                        valueText = "ì";
                        break;
                    case "r":
                        valueText = "ỉ";
                        break;
                    case "x":
                        valueText = "ĩ";
                        break;
                    case "j":
                        valueText = "ị";
                        break;
                    case "S":
                        valueText = "iS";
                        break;
                    case "F":
                        valueText = "ì";
                        break;
                    case "R":
                        valueText = "ỉ";
                        break;
                    case "X":
                        valueText = "ĩ";
                        break;
                    case "J":
                        valueText = "ị";
                        break;
                    default:
                        break;
                }
                break;
            case "ì":
                switch (textInput) {
                    case "s":
                        valueText = "í";
                        break;
                    case "f":
                        valueText = "if";
                        break;
                    case "r":
                        valueText = "ỉ";
                        break;
                    case "x":
                        valueText = "ĩ";
                        break;
                    case "j":
                        valueText = "ị";
                        break;
                    case "S":
                        valueText = "í";
                        break;
                    case "F":
                        valueText = "iF";
                        break;
                    case "R":
                        valueText = "ỉ";
                        break;
                    case "X":
                        valueText = "ĩ";
                        break;
                    case "J":
                        valueText = "ị";
                        break;
                    default:
                        break;
                }
                break;
            case "ỉ":
                switch (textInput) {
                    case "s":
                        valueText = "í";
                        break;
                    case "f":
                        valueText = "ì";
                        break;
                    case "r":
                        valueText = "ir";
                        break;
                    case "x":
                        valueText = "ĩ";
                        break;
                    case "j":
                        valueText = "ị";
                        break;
                    case "S":
                        valueText = "í";
                        break;
                    case "F":
                        valueText = "ì";
                        break;
                    case "R":
                        valueText = "iR";
                        break;
                    case "X":
                        valueText = "ĩ";
                        break;
                    case "J":
                        valueText = "ị";
                        break;
                    default:
                        break;
                }
                break;
            case "ĩ":
                switch (textInput) {
                    case "s":
                        valueText = "í";
                        break;
                    case "f":
                        valueText = "ì";
                        break;
                    case "r":
                        valueText = "ỉ";
                        break;
                    case "x":
                        valueText = "ix";
                        break;
                    case "j":
                        valueText = "ị";
                        break;
                    case "S":
                        valueText = "í";
                        break;
                    case "F":
                        valueText = "ì";
                        break;
                    case "R":
                        valueText = "ỉ";
                        break;
                    case "X":
                        valueText = "iX";
                        break;
                    case "J":
                        valueText = "ị";
                        break;
                    default:
                        break;
                }
                break;
            case "ị":
                switch (textInput) {
                    case "s":
                        valueText = "í";
                        break;
                    case "f":
                        valueText = "ì";
                        break;
                    case "r":
                        valueText = "ỉ";
                        break;
                    case "x":
                        valueText = "ĩ";
                        break;
                    case "j":
                        valueText = "ij";
                        break;
                    case "S":
                        valueText = "í";
                        break;
                    case "F":
                        valueText = "ì";
                        break;
                    case "R":
                        valueText = "ỉ";
                        break;
                    case "X":
                        valueText = "ĩ";
                        break;
                    case "J":
                        valueText = "iJ";
                        break;
                    default:
                        break;
                }
                break;
            case "y":
                switch (textInput) {
                    case "s":
                        valueText = "ý";
                        break;
                    case "f":
                        valueText = "ỳ";
                        break;
                    case "r":
                        valueText = "ỷ";
                        break;
                    case "x":
                        valueText = "ỹ";
                        break;
                    case "j":
                        valueText = "ỵ";
                        break;
                    case "S":
                        valueText = "ý";
                        break;
                    case "F":
                        valueText = "ỳ";
                        break;
                    case "R":
                        valueText = "ỷ";
                        break;
                    case "X":
                        valueText = "ỹ";
                        break;
                    case "J":
                        valueText = "ỵ";
                        break;
                    default:
                        break;
                }
                break;
            case "ý":
                switch (textInput) {
                    case "s":
                        valueText = "ys";
                        break;
                    case "f":
                        valueText = "ỳ";
                        break;
                    case "r":
                        valueText = "ỷ";
                        break;
                    case "x":
                        valueText = "ỹ";
                        break;
                    case "j":
                        valueText = "ỵ";
                        break;
                    case "S":
                        valueText = "yS";
                        break;
                    case "F":
                        valueText = "ỳ";
                        break;
                    case "R":
                        valueText = "ỷ";
                        break;
                    case "X":
                        valueText = "ỹ";
                        break;
                    case "J":
                        valueText = "ỵ";
                        break;
                    default:
                        break;
                }
                break;
            case "ỳ":
                switch (textInput) {
                    case "s":
                        valueText = "ý";
                        break;
                    case "f":
                        valueText = "yf";
                        break;
                    case "r":
                        valueText = "ỷ";
                        break;
                    case "x":
                        valueText = "ỹ";
                        break;
                    case "j":
                        valueText = "ỵ";
                        break;
                    case "S":
                        valueText = "ý";
                        break;
                    case "F":
                        valueText = "yF";
                        break;
                    case "R":
                        valueText = "ỷ";
                        break;
                    case "X":
                        valueText = "ỹ";
                        break;
                    case "J":
                        valueText = "ỵ";
                        break;
                    default:
                        break;
                }
                break;
            case "ỷ":
                switch (textInput) {
                    case "s":
                        valueText = "ý";
                        break;
                    case "f":
                        valueText = "ỳ";
                        break;
                    case "r":
                        valueText = "yr";
                        break;
                    case "x":
                        valueText = "ỹ";
                        break;
                    case "j":
                        valueText = "ỵ";
                        break;
                    case "S":
                        valueText = "ý";
                        break;
                    case "F":
                        valueText = "ỳ";
                        break;
                    case "R":
                        valueText = "yR";
                        break;
                    case "X":
                        valueText = "ỹ";
                        break;
                    case "J":
                        valueText = "ỵ";
                        break;
                    default:
                        break;
                }
                break;
            case "ỹ":
                switch (textInput) {
                    case "s":
                        valueText = "ý";
                        break;
                    case "f":
                        valueText = "ỳ";
                        break;
                    case "r":
                        valueText = "ỷ";
                        break;
                    case "x":
                        valueText = "yx";
                        break;
                    case "j":
                        valueText = "ỵ";
                        break;
                    case "S":
                        valueText = "ý";
                        break;
                    case "F":
                        valueText = "ỳ";
                        break;
                    case "R":
                        valueText = "ỷ";
                        break;
                    case "X":
                        valueText = "yX";
                        break;
                    case "J":
                        valueText = "ỵ";
                        break;
                    default:
                        break;
                }
                break;
            case "ỵ":
                switch (textInput) {
                    case "s":
                        valueText = "ý";
                        break;
                    case "f":
                        valueText = "ỳ";
                        break;
                    case "r":
                        valueText = "ỷ";
                        break;
                    case "x":
                        valueText = "ỹ";
                        break;
                    case "j":
                        valueText = "yj";
                        break;
                    case "S":
                        valueText = "ý";
                        break;
                    case "F":
                        valueText = "ỳ";
                        break;
                    case "R":
                        valueText = "ỷ";
                        break;
                    case "X":
                        valueText = "ỹ";
                        break;
                    case "J":
                        valueText = "yj";
                        break;
                    default:
                        break;
                }
                break;
            case "I":
                switch (textInput) {
                    case "s":
                        valueText = "Í";
                        break;
                    case "f":
                        valueText = "Ì";
                        break;
                    case "r":
                        valueText = "Ỉ";
                        break;
                    case "x":
                        valueText = "Ĩ";
                        break;
                    case "j":
                        valueText = "Ị";
                        break;
                    case "S":
                        valueText = "Í";
                        break;
                    case "F":
                        valueText = "Ì";
                        break;
                    case "R":
                        valueText = "Ỉ";
                        break;
                    case "X":
                        valueText = "Ĩ";
                        break;
                    case "J":
                        valueText = "Ị";
                        break;
                    default:
                        break;
                }
                break;
            case "Í":
                switch (textInput) {
                    case "s":
                        valueText = "Is";
                        break;
                    case "f":
                        valueText = "Ì";
                        break;
                    case "r":
                        valueText = "Ỉ";
                        break;
                    case "x":
                        valueText = "Ĩ";
                        break;
                    case "j":
                        valueText = "Ị";
                        break;
                    case "S":
                        valueText = "IS";
                        break;
                    case "F":
                        valueText = "Ì";
                        break;
                    case "R":
                        valueText = "Ỉ";
                        break;
                    case "X":
                        valueText = "Ĩ";
                        break;
                    case "J":
                        valueText = "Ị";
                        break;
                    default:
                        break;
                }
                break;
            case "Ì":
                switch (textInput) {
                    case "s":
                        valueText = "Í";
                        break;
                    case "f":
                        valueText = "If";
                        break;
                    case "r":
                        valueText = "Ỉ";
                        break;
                    case "x":
                        valueText = "Ĩ";
                        break;
                    case "j":
                        valueText = "Ị";
                        break;
                    case "S":
                        valueText = "Í";
                        break;
                    case "F":
                        valueText = "IF";
                        break;
                    case "R":
                        valueText = "Ỉ";
                        break;
                    case "X":
                        valueText = "Ĩ";
                        break;
                    case "J":
                        valueText = "Ị";
                        break;
                    default:
                        break;
                }
                break;
            case "Ỉ":
                switch (textInput) {
                    case "s":
                        valueText = "Í";
                        break;
                    case "f":
                        valueText = "Ì";
                        break;
                    case "r":
                        valueText = "Ir";
                        break;
                    case "x":
                        valueText = "Ĩ";
                        break;
                    case "j":
                        valueText = "Ị";
                        break;
                    case "S":
                        valueText = "Í";
                        break;
                    case "F":
                        valueText = "Ì";
                        break;
                    case "R":
                        valueText = "IR";
                        break;
                    case "X":
                        valueText = "Ĩ";
                        break;
                    case "J":
                        valueText = "Ị";
                        break;
                    default:
                        break;
                }
                break;
            case "Ĩ":
                switch (textInput) {
                    case "s":
                        valueText = "Í";
                        break;
                    case "f":
                        valueText = "Ì";
                        break;
                    case "r":
                        valueText = "Ỉ";
                        break;
                    case "x":
                        valueText = "Ix";
                        break;
                    case "j":
                        valueText = "Ị";
                        break;
                    case "S":
                        valueText = "Í";
                        break;
                    case "F":
                        valueText = "Ì";
                        break;
                    case "R":
                        valueText = "Ỉ";
                        break;
                    case "X":
                        valueText = "IX";
                        break;
                    case "J":
                        valueText = "Ị";
                        break;
                    default:
                        break;
                }
                break;
            case "Ị":
                switch (textInput) {
                    case "s":
                        valueText = "Í";
                        break;
                    case "f":
                        valueText = "Ì";
                        break;
                    case "r":
                        valueText = "Ỉ";
                        break;
                    case "x":
                        valueText = "Ĩ";
                        break;
                    case "j":
                        valueText = "Ij";
                        break;
                    case "S":
                        valueText = "Í";
                        break;
                    case "F":
                        valueText = "Ì";
                        break;
                    case "R":
                        valueText = "Ỉ";
                        break;
                    case "X":
                        valueText = "Ĩ";
                        break;
                    case "J":
                        valueText = "IJ";
                        break;
                    default:
                        break;
                }
                break;
            case "Y":
                switch (textInput) {
                    case "s":
                        valueText = "Ý";
                        break;
                    case "f":
                        valueText = "Ỳ";
                        break;
                    case "r":
                        valueText = "Ỷ";
                        break;
                    case "x":
                        valueText = "Ỹ";
                        break;
                    case "j":
                        valueText = "Ỵ";
                        break;
                    case "S":
                        valueText = "Ý";
                        break;
                    case "F":
                        valueText = "Ỳ";
                        break;
                    case "R":
                        valueText = "Ỷ";
                        break;
                    case "X":
                        valueText = "Ỹ";
                        break;
                    case "J":
                        valueText = "Ỵ";
                        break;
                    default:
                        break;
                }
                break;
            case "Ý":
                switch (textInput) {
                    case "s":
                        valueText = "Ys";
                        break;
                    case "f":
                        valueText = "Ỳ";
                        break;
                    case "r":
                        valueText = "Ỷ";
                        break;
                    case "x":
                        valueText = "Ỹ";
                        break;
                    case "j":
                        valueText = "Ỵ";
                        break;
                    case "S":
                        valueText = "YS";
                        break;
                    case "F":
                        valueText = "Ỳ";
                        break;
                    case "R":
                        valueText = "Ỷ";
                        break;
                    case "X":
                        valueText = "Ỹ";
                        break;
                    case "J":
                        valueText = "Ỵ";
                        break;
                    default:
                        break;
                }
                break;
            case "Ỳ":
                switch (textInput) {
                    case "s":
                        valueText = "Ý";
                        break;
                    case "f":
                        valueText = "Yf";
                        break;
                    case "r":
                        valueText = "Ỷ";
                        break;
                    case "x":
                        valueText = "Ỹ";
                        break;
                    case "j":
                        valueText = "Ỵ";
                        break;
                    case "S":
                        valueText = "Ý";
                        break;
                    case "F":
                        valueText = "YF";
                        break;
                    case "R":
                        valueText = "Ỷ";
                        break;
                    case "X":
                        valueText = "Ỹ";
                        break;
                    case "J":
                        valueText = "Ỵ";
                        break;
                    default:
                        break;
                }
                break;
            case "Ỷ":
                switch (textInput) {
                    case "s":
                        valueText = "Ý";
                        break;
                    case "f":
                        valueText = "Ỳ";
                        break;
                    case "r":
                        valueText = "Yr";
                        break;
                    case "x":
                        valueText = "Ỹ";
                        break;
                    case "j":
                        valueText = "Ỵ";
                        break;
                    case "S":
                        valueText = "Ý";
                        break;
                    case "F":
                        valueText = "Ỳ";
                        break;
                    case "R":
                        valueText = "YR";
                        break;
                    case "X":
                        valueText = "Ỹ";
                        break;
                    case "J":
                        valueText = "Ỵ";
                        break;
                    default:
                        break;
                }
                break;
            case "Ỹ":
                switch (textInput) {
                    case "s":
                        valueText = "Ý";
                        break;
                    case "f":
                        valueText = "Ỳ";
                        break;
                    case "r":
                        valueText = "Ỷ";
                        break;
                    case "x":
                        valueText = "Yx";
                        break;
                    case "j":
                        valueText = "Ỵ";
                        break;
                    case "S":
                        valueText = "Ý";
                        break;
                    case "F":
                        valueText = "Ỳ";
                        break;
                    case "R":
                        valueText = "Ỷ";
                        break;
                    case "X":
                        valueText = "YX";
                        break;
                    case "J":
                        valueText = "Ỵ";
                        break;
                    default:
                        break;
                }
                break;
            case "Ỵ":
                switch (textInput) {
                    case "s":
                        valueText = "Ý";
                        break;
                    case "f":
                        valueText = "Ỳ";
                        break;
                    case "r":
                        valueText = "Ỷ";
                        break;
                    case "x":
                        valueText = "Ỹ";
                        break;
                    case "j":
                        valueText = "Yj";
                        break;
                    case "S":
                        valueText = "Ý";
                        break;
                    case "F":
                        valueText = "Ỳ";
                        break;
                    case "R":
                        valueText = "Ỷ";
                        break;
                    case "X":
                        valueText = "Ỹ";
                        break;
                    case "J":
                        valueText = "YJ";
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }
        if(valueText != (textInput)){
            inputConnection.deleteSurroundingText(1,0);
        }
        return valueText;
    }
}
