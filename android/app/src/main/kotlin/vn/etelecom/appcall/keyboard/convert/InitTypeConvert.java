package vn.etelecom.appcall.keyboard.convert;

import java.util.ArrayList;
import java.util.Arrays;

public class InitTypeConvert {
    public static ArrayList<String> arraydD = new ArrayList<>(Arrays.asList("d","đ","D","Đ"));
    public static ArrayList<String> arrayeE = new ArrayList<>(Arrays.asList("e","E","ê","Ê"));
    public static ArrayList<String> arraySpecialNMCT = new ArrayList<>(Arrays.asList("n","m","c","t"));
    public static ArrayList<String> arrayoo = new ArrayList<>(Arrays.asList("ô","ố","ồ","ổ","ỗ","ộ","Ô","Ố","Ồ","Ổ","Ỗ","Ộ"));
    public static ArrayList<String> arrayValidateInput = new ArrayList<>(Arrays.asList("a","o","w","e","d","s","f","x","r","j",
            "A","O","W","E","D","S","F","X","R","J"));

    public static ArrayList<String> arrayValidateOne = new ArrayList<>(Arrays.asList("a","á","à","ả","ã","ạ",
            "ă","ắ","ằ","ẳ","ẵ","ặ",
            "â","ấ","ầ","ẩ","ẫ","ậ",
            "o","ó","ò","ỏ","õ","ọ",
            "ô","ố","ồ","ổ","ỗ","ộ",
            "ơ","ớ","ờ","ở","ỡ","ợ",
            "u","ú","ù","ủ","ũ","ụ",
            "ư","ứ","ừ","ử","ữ","ự",
            "i","í","ì","ỉ","ĩ","ị",
            "y","ý","ỳ","ỷ","ỹ","ỵ",
            "e","é","è","ẻ","ẽ","ẹ",
            "ê","ế","ề","ể","ễ","ệ",
            "A","Á","À","Ả","Ã","Ạ",
            "Ă","Ắ","Ằ","Ẳ","Ẵ","Ặ",
            "Â","Ấ","Ầ","Ẩ","Ẫ","Ậ",
            "O","Ó","Ò","Ỏ","Õ","Ọ",
            "Ơ","Ớ","Ờ","Ở","Ỡ","Ợ",
            "Ô","Ố","Ồ","Ổ","Ỗ","Ộ",
            "U","Ú","Ù","Ủ","Ũ","Ụ",
            "Ư","Ứ","Ừ","Ử","Ữ","Ự",
            "I","Í","Ì","Ỉ","Ĩ","Ị",
            "Y","Ý","Ỳ","Ủ","Ỹ","Ỵ",
            "E","É","È","Ẻ","Ẽ","Ẹ",
            "Ê","Ế","Ề","Ể","Ễ","Ệ")) ;

    public static int initTypeConvertCharacter(ArrayList<String> arrayString, String textInput){
        String characterLastest = arrayString.get(arrayString.size()-1);

        if(arrayValidateInput.contains(textInput) && arrayString.size() <= 7){

            if(arraydD.contains(arrayString.get(0)) && arraydD.contains(textInput) ){
                return 4;
            }else if(arraydD.contains(textInput)){
                return 0;
            }


            if(arrayString.size()>2 && arrayeE.contains(textInput) && arrayeE.contains(arrayString.get(arrayString.size() - 1)) && arrayString.get(arrayString.size() - 2) == "y" &&
                    arrayString.get(arrayString.size() - 3) == "u"){
                return 5;
            }

            if(arrayValidateOne.contains(characterLastest) && arrayString.size() == 1){
                return 1;
            }else if(arrayString.size()>1){
                String characterTwoLastest = arrayString.get(arrayString.size()-2);

                if(!arrayValidateOne.contains(characterTwoLastest)){
                    if(arrayString.size() >= 3){
                        String characterThreeLastest = arrayString.get(arrayString.size()-3);
                        if(arrayValidateOne.contains(characterThreeLastest)){
                            return 3;
                        }
                    }
                    return 1;
                }else{
                    if(arrayString.size() == 3 && arrayString.get(0) == "q"){
                        return 1;
                    }
                    if(arrayString.size() == 3 && arrayString.get(0) == "Q"){
                        return 1;
                    }
                    if(arrayValidateOne.contains(characterTwoLastest)){

                        if((textInput.equals("o") || textInput.equals("O")) && arrayString.size()>=2 && !arrayoo.contains(arrayString.get(arrayString.size()-2)) && !arrayString.get(arrayString.size()-1).equals("i") ){

                            if(arraySpecialNMCT.contains(arrayString.get(arrayString.size() - 1)) && !arrayValidateOne.contains(arrayString.get(0))){
                                return 2;
                            }
                            return 1;
                        }

                        //                        if(arraydD.contains(textInput) )
                        return 2;
                    }
                }
            }
        }
        return 0;
    }
}
