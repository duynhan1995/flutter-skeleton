package vn.etelecom.appcall

import android.content.Intent
import android.os.Build
import android.util.Log
import android.view.KeyEvent
import androidx.annotation.NonNull
import androidx.annotation.RequiresApi
import com.portsip.PortSipErrorcode
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel

@RequiresApi(Build.VERSION_CODES.O)
class MainActivity : FlutterActivity() {
  
  companion object {
    lateinit var shared: MainActivity
    const val GENERAL_APP_ID = "vn.etelecom.appcall"
  }
  
  override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
    super.configureFlutterEngine(flutterEngine)
    
    shared = this
    PortsipService.engineF = flutterEngine
  
    MethodChannel(flutterEngine.dartExecutor.binaryMessenger, GENERAL_APP_ID).setMethodCallHandler { call, result ->
      // Note: this method is invoked on the main thread.
      when (call.method) {
        "registerPortsip" -> {
          val username = call.argument<String>("username")
          val password = call.argument<String>("password")
          val domain = call.argument<String>("domain")
          val server = call.argument<String>("sipServer")
    
          if (username != null && password != null && domain != null && server != null) {
            val res = registerPortsip(username, password, domain, server)
            if (res == 0 || res == PortSipErrorcode.ECoreAlreadyRegistered ||
              res == PortSipErrorcode.ECoreAllowOnlyOneUser ||
              res == PortSipErrorcode.ECoreCreateTransportFailure
            ) {
              result.success(res)
            } else {
              result.error("REGISTER FAILED", "Kết nối không thành công.", null)
            }
          } else {
            result.error("NOT AUTHENTICATED", "Username và password không đúng.", null)
          }
        }
        "unregisterPortsip" -> {
          unregisterPortsip()
        }
        "callOut" -> {
          val phoneNumber = call.argument<String>("phoneNumber")
          val videoCall = call.argument<Boolean>("videoCall")

          if (phoneNumber != null) {
            val res = callOut(phoneNumber, videoCall ?: false)
            if (res) {
              result.success(null)
            } else {
              result.error("CALL FAILED", "Không thể thực hiện cuộc gọi", null)
            }
          }
        }
        "hangUp" -> {
          val res = hangUp()
          if (res) {
            result.success(null)
          } else {
            result.error("HANGUP FAILED", "Không thể kết thúc cuộc gọi", null)
          }
        }
        "answerCall" -> {
          val res = answerCall()
          if (res) {
            result.success(null)
          } else {
            result.error("ANSWER_CALL FAILED", "Không thể trả lời cuộc gọi", null)
          }
        }
        "rejectCall" -> {
          val res = rejectCall()
          if (res) {
            result.success(null)
          } else {
            result.error("REJECT_CALL FAILED", "Không thể từ chối cuộc gọi", null)
          }
        }
        "hold" -> {
          val res = hold()
          if (res) {
            result.success(null)
          } else {
            result.error("HOLD FAILED", "Không thể giữ máy", null)
          }
        }
        "unHold" -> {
          val res = unHold()
          if (res) {
            result.success(null)
          } else {
            result.error("UN_HOLD FAILED", "Không thể tiếp tục giữ máy", null)
          }
        }
        "speakerOn" -> {
          PortsipService.shared?.speakerOn()
          result.success(null)
        }
        "speakerOff" -> {
          PortsipService.shared?.speakerOff()
          result.success(null)
        }
        "microphoneOn" -> {
          PortsipService.shared?.turnOnMicrophone()
          result.success(null)
        }
        "microphoneOff" -> {
          PortsipService.shared?.turnOffMicrophone()
          result.success(null)
        }
        "frontCamera" -> {
          PortsipService.shared?.switchToFrontCamera()
          result.success(null)
        }
        "backCamera" -> {
          PortsipService.shared?.switchToBackCamera()
          result.success(null)
        }
        "cameraOn" -> {
          PortsipService.shared?.turnOnCamera()
          result.success(null)
        }
        "cameraOff" -> {
          PortsipService.shared?.turnOffCamera()
          result.success(null)
        }
        "sendDtmf" -> {
          val code = call.argument<Int>("code")
          val res = code?.let { PortsipService.shared?.sendDtmf(code) }
          if (res == 0) {
            result.success(null)
          } else {
            result.error("SEND DTMF FAILED", "Có lỗi xảy ra khi gửi DTMF", null)
          }
        }
        "refer" -> {
          val referTo = call.argument<String>("referTo")
          val res = referTo?.let { PortsipService.shared?.refer(referTo) }
          if (res == 0) {
            result.success(null)
          } else {
            result.error("REFER FAILED", "Có lỗi xảy ra khi chuyển tiếp cuộc gọi", null)
          }
        }
        "finishDisposingCamera" -> {
            val intent = Intent(this, VideoCallActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }
        else -> {
          result.notImplemented()
        }
      }
    }
  }
  
  override fun onBackPressed() {
    moveTaskToBack(true)
  }
  override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
    if (keyCode == KeyEvent.KEYCODE_BACK) {
      super.onKeyDown(keyCode, event)
      moveTaskToBack(true)
      return true
    }
    return false
  }
  
  private fun registerPortsip(username: String, password: String, domain: String, server: String): Int {

    val sharedPref = getSharedPreferences("CallManager", MODE_PRIVATE) ?: return 0
    with(sharedPref.edit()) {
      putString(getString(R.string.call_manager_ps_extension), username)
      putString(getString(R.string.call_manager_ps_password), password)
      putString(getString(R.string.call_manager_ps_domain), domain)
      putString(getString(R.string.call_manager_ps_server), server)
      apply()
    }

    Intent(this, PortsipService::class.java).also { intent ->
      startService(intent)
    }
    
    return 0

  }
  
  private fun unregisterPortsip() {
    if (PortsipService.shared != null) {
      PortsipService.shared!!.unregisterPortsip()
    }
  }
  
  private fun callOut(phoneNumber: String, videoCall: Boolean = false): Boolean {
    return PortsipService.shared?.callOut(phoneNumber, videoCall) ?: false
  }
  
  private fun hangUp(): Boolean {
    return PortsipService.shared?.hangUp() ?: false
  }
  
  private fun answerCall(): Boolean {
    return PortsipService.shared?.answerCall() ?: false
  }
  
  private fun rejectCall(): Boolean {
    return PortsipService.shared?.rejectCall() ?: false
  }
  
  private fun hold(): Boolean {
    return PortsipService.shared?.hold() ?: false
  }
  
  private fun unHold(): Boolean {
    return PortsipService.shared?.unHold() ?: false
  }
  
}
