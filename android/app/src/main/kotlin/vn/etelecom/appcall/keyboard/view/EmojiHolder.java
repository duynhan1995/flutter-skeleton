package vn.etelecom.appcall.keyboard.view;

import android.view.View;
import android.view.inputmethod.InputConnection;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import vn.etelecom.appcall.R;

public class EmojiHolder extends RecyclerView.ViewHolder {

    private TextView textView;
    private InputConnection _inputConnection;

    public EmojiHolder(@NonNull View itemView, InputConnection inputConnection) {
        super(itemView);
        this._inputConnection = inputConnection;
        textView = (TextView) itemView.findViewById(R.id.tv_emoji);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _inputConnection.commitText(textView.getText(),1);
            }
        });
    }
    public TextView getTextView(){
        return textView;
    }
}
