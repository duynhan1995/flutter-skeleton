package vn.etelecom.appcall

import android.annotation.SuppressLint
import android.app.Activity
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.widget.TextView
import com.portsip.PortSIPVideoRenderer
import vn.etelecom.appcall.utils.StringHandler

class VideoCallActivity: Activity(), View.OnClickListener {

  private lateinit var holdBtn: View
  private lateinit var unHoldBtn: View
  private lateinit var camOffBtn: View
  private lateinit var camOnBtn: View
  private lateinit var micOffBtn: View
  private lateinit var micOnBtn: View

  private lateinit var callingTime: TextView

  private lateinit var localVideo: PortSIPVideoRenderer
  private lateinit var remoteVideo: PortSIPVideoRenderer

  private var thread: Thread? = null

  companion object {
    @SuppressLint("StaticFieldLeak")
    var activity: VideoCallActivity? = null
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
  
    setContentView(R.layout.video_call_view)
    actionBar?.hide()
    
    activity = this

    initButtons()

    preCheckStateOfButtons()
    startTimer()

    localVideo = findViewById(R.id.local_video_view)
    remoteVideo = findViewById(R.id.remote_video_view)

    PortsipService.shared?.portsipSDK?.setRemoteVideoWindow(CallManager.sessionID, remoteVideo)

    PortsipService.shared?.portsipSDK?.setVideoDeviceId(if (CallManager.frontCamera) 1 else 0)
    PortsipService.shared?.portsipSDK?.displayLocalVideo(true, true, localVideo)
    PortsipService.shared?.portsipSDK?.sendVideo(CallManager.sessionID, CallManager.camIsOn)

  }
  
  override fun onDestroy() {
    super.onDestroy()

    stopTimer()
    callingTime.text = StringHandler.secondsToTime(0)

    PortsipService.shared?.portsipSDK?.displayLocalVideo(false, false, null)
    localVideo.release()

    PortsipService.shared?.portsipSDK?.setRemoteVideoWindow(CallManager.sessionID, null)
    remoteVideo.release()
  }

  override fun onBackPressed() {}
  override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
    if (keyCode == KeyEvent.KEYCODE_BACK) {
      super.onKeyDown(keyCode, event)
      return true
    }
    return false
  }
  
  override fun onClick(view: View) {

    when (view.id) {
      R.id.hangup_btn -> {
        PortsipService.shared?.hangUp()
        finishAndRemoveTask()
      }
      R.id.hold_btn -> {
        holdBtn.visibility = View.INVISIBLE
        unHoldBtn.visibility = View.VISIBLE
        PortsipService.shared?.hold()
        PortsipService.shared?.portsipSDK?.sendVideo(CallManager.sessionID, false)
      }
      R.id.un_hold_btn -> {
        holdBtn.visibility = View.VISIBLE
        unHoldBtn.visibility = View.INVISIBLE
        PortsipService.shared?.unHold()
        PortsipService.shared?.portsipSDK?.sendVideo(CallManager.sessionID, true)
      }
      R.id.cam_off_btn -> {
        camOnBtn.visibility = View.VISIBLE
        camOffBtn.visibility = View.INVISIBLE
        PortsipService.shared?.portsipSDK?.sendVideo(CallManager.sessionID, false)
      }
      R.id.cam_on_btn -> {
        camOnBtn.visibility = View.INVISIBLE
        camOffBtn.visibility = View.VISIBLE
        PortsipService.shared?.portsipSDK?.sendVideo(CallManager.sessionID, true)
      }
      R.id.mic_off_btn -> {
        micOnBtn.visibility = View.VISIBLE
        micOffBtn.visibility = View.INVISIBLE
        PortsipService.shared?.portsipSDK?.muteSession(CallManager.sessionID, false, true, false, false)
      }
      R.id.mic_on_btn -> {
        micOnBtn.visibility = View.INVISIBLE
        micOffBtn.visibility = View.VISIBLE
        PortsipService.shared?.portsipSDK?.muteSession(CallManager.sessionID, false, false, false, false)
      }
      R.id.flip_btn -> {
        CallManager.frontCamera = !CallManager.frontCamera
        if (CallManager.frontCamera) {
          PortsipService.shared?.portsipSDK?.setVideoDeviceId(1)
        } else {
          PortsipService.shared?.portsipSDK?.setVideoDeviceId(0)
        }
      }
    }
  }

  private fun initButtons() {
    findViewById<View>(R.id.hangup_btn).setOnClickListener(this)

    holdBtn = findViewById(R.id.hold_btn)
    holdBtn.setOnClickListener(this)

    unHoldBtn = findViewById(R.id.un_hold_btn)
    unHoldBtn.setOnClickListener(this)

    camOffBtn = findViewById(R.id.cam_off_btn)
    camOffBtn.setOnClickListener(this)

    camOnBtn = findViewById(R.id.cam_on_btn)
    camOnBtn.setOnClickListener(this)

    micOffBtn = findViewById(R.id.mic_off_btn)
    micOffBtn.setOnClickListener(this)

    micOnBtn = findViewById(R.id.mic_on_btn)
    micOnBtn.setOnClickListener(this)

    callingTime = findViewById(R.id.calling_time)

    findViewById<View>(R.id.flip_btn).setOnClickListener(this)

  }

  fun preCheckStateOfButtons() {
    holdBtn.visibility = if (CallManager.isHeld) View.INVISIBLE else View.VISIBLE
    unHoldBtn.visibility = if (!CallManager.isHeld) View.INVISIBLE else View.VISIBLE
    camOnBtn.visibility = if (CallManager.camIsOn) View.INVISIBLE else View.VISIBLE
    camOffBtn.visibility = if (!CallManager.camIsOn) View.INVISIBLE else View.VISIBLE
    micOnBtn.visibility = if (CallManager.micIsOn) View.INVISIBLE else View.VISIBLE
    micOffBtn.visibility = if (!CallManager.micIsOn) View.INVISIBLE else View.VISIBLE
  }

  private fun startTimer() {
    var timeCount = 0

    thread = object : Thread() {
      override fun run() {
        try {
          while (!this.isInterrupted) {
            sleep(1000)
            runOnUiThread {
              timeCount += 1
              callingTime.text = StringHandler.secondsToTime(timeCount)
            }
          }
        } catch (e: InterruptedException) {
          Log.e("E-TELECOM ERROR", "COUNT CALLING TIME ERROR: $e")
        }
      }
    }

    thread?.start()
  }

  private fun stopTimer() {
    thread?.interrupt()
  }

}
