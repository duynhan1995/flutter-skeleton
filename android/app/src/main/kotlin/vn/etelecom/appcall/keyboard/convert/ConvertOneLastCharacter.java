package vn.etelecom.appcall.keyboard.convert;

import android.view.inputmethod.InputConnection;

import java.util.ArrayList;
import java.util.Arrays;

public class ConvertOneLastCharacter {

    public ConvertOneLastCharacter() {}
    static ArrayList<String> arrayDAAA = new ArrayList<>(Arrays.asList("d","đ","D","Đ",
            "a","á","à","ả","ã","ạ",
            "ă","ắ","ằ","ẳ","ẵ","ặ",
            "â","ấ","ầ","ẩ","ẫ","ậ",
            "A","Á","À","Ả","Ã","Ạ",
            "Ă","Ắ","Ằ","Ẳ","Ẵ","Ặ",
            "Â","Ấ","Ầ","Ẩ","Ẫ","Ậ"));
    static ArrayList<String> arrayEE = new ArrayList<>(Arrays.asList(
            "e","é","è","ẻ","ẽ","ẹ",
            "ê","ế","ề","ể","ễ","ệ",
            "E","É","È","Ẻ","Ẽ","Ẹ",
            "Ê","Ế","Ề","Ể","Ễ","Ệ"));
    static ArrayList<String> arrayIY = new ArrayList<>(Arrays.asList(
            "i","í","ì","ỉ","ĩ","ị",
            "y","ý","ỳ","ỷ","ỹ","ỵ",
            "I","Í","Ì","Ỉ","Ĩ","Ị",
            "Y","Ý","Ỳ","Ủ","Ỹ","Ỵ"));
    static ArrayList<String> arrayUO = new ArrayList<>(Arrays.asList(
            "o","ó","ò","ỏ","õ","ọ",
            "ô","ố","ồ","ổ","ỗ","ộ",
            "ơ","ớ","ờ","ở","ỡ","ợ",
            "u","ú","ù","ủ","ũ","ụ",
            "ư","ứ","ừ","ử","ữ","ự",
            "O","Ó","Ò","Ỏ","Õ","Ọ",
            "Ơ","Ớ","Ờ","Ở","Ỡ","Ợ",
            "Ô","Ố","Ồ","Ổ","Ỗ","Ộ",
            "U","Ú","Ù","Ủ","Ũ","Ụ",
            "Ư","Ứ","Ừ","Ử","Ữ","Ự"));

    public static String convert(String characterLastest, String textInput, InputConnection inputConnection){
        String valueText = textInput;
        if(arrayDAAA.contains(characterLastest)){
            return ConvertOneLastCharacterDAAA.convert(characterLastest, textInput, inputConnection);
        }else if(arrayEE.contains(characterLastest)){
            return ConvertOneLastCharaceterEE.convert(characterLastest, textInput, inputConnection);
        }else if(arrayIY.contains(characterLastest)){
            return ConvertOneLastCharacterIY.convert(characterLastest, textInput, inputConnection);
        }else if(arrayUO.contains(characterLastest)){
            return ConvertOneLastCharacterUO.convert(characterLastest, textInput, inputConnection);
        }
        return valueText;
    }
}
