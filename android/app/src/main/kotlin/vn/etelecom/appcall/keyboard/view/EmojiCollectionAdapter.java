package vn.etelecom.appcall.keyboard.view;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputConnection;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import vn.etelecom.appcall.R;

public class  EmojiCollectionAdapter extends RecyclerView.Adapter<EmojiCollectionAdapter.ViewHolder>{
//    int[] emojiSmile = new int[] {1,2};
    private static InputConnection _inputConnection;

    public EmojiCollectionAdapter(InputConnection inputConnection){
        this._inputConnection = inputConnection;
    }

    public void updateInputConnection(InputConnection inputConnection){
        this._inputConnection = inputConnection;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_collection_object, parent, false);

        return new ViewHolder(v, this._inputConnection);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        switch (position){
            case 1:
                holder.getRecyclerView().setAdapter(new EmojiAnimalList(_inputConnection));
                break;
            case 2:
                holder.getRecyclerView().setAdapter(new EmojiFoodAdapter(_inputConnection));
                break;
            case 3:
                holder.getRecyclerView().setAdapter(new EmojiTravelAdapter(_inputConnection));
                break;
            case 4:
                holder.getRecyclerView().setAdapter(new EmojiActivityAdapter(_inputConnection));
                break;
            case 5:
                holder.getRecyclerView().setAdapter(new EmojiObjectAdapter(_inputConnection));
                break;
            case 6:
                holder.getRecyclerView().setAdapter(new EmojiSymbolAdapter(_inputConnection));
                break;
            case 7:
                holder.getRecyclerView().setAdapter(new EmojiFlagAdapter(_inputConnection));
                break;
            default:
                holder.getRecyclerView().setAdapter(new EmojiAdapter(_inputConnection));
                break;
        }
    }

    @Override
    public int getItemCount() {
        return 8;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final RecyclerView recyclerView;

        private GridLayoutManager gridLayoutManager;
        private boolean update = false;
        private InputConnection _inputConnection;

        public ViewHolder(@NonNull View itemView, InputConnection inputConnection) {
            super(itemView);
            recyclerView = (RecyclerView) itemView.findViewById(R.id.rc_listEmoji);
            this._inputConnection = inputConnection;
            gridLayoutManager = new GridLayoutManager(itemView.getContext(),8);
            recyclerView.setLayoutManager(gridLayoutManager);
        }
        public RecyclerView getRecyclerView() {
            return recyclerView;
        }
    }
}
