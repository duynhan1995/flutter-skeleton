package vn.etelecom.appcall

import android.util.Log
import com.portsip.OnPortSIPEvent
import com.portsip.PortSipEnumDefine

interface BasePortsipDelegate: OnPortSIPEvent {
  
  override fun onRegisterSuccess(reason: String?, code: Int, sipMessage: String?) {
    Log.d("BASE", "ONREGISTERSUCCESS")
  }
  
  override fun onRegisterFailure(reason: String?, code: Int, sipMessage: String?) {
    Log.d("BASE", "ONREGISTERFAILURE")
  }
  
  override fun onInviteIncoming(
    sessionId: Long,
    callerDisplayName: String,
    caller: String,
    calleeDisplayName: String,
    callee: String,
    audioCodecs: String,
    videoCodecs: String,
    existsAudio: Boolean,
    existsVideo: Boolean,
    sipMessage: String
  ) {
    Log.d("BASE", "ONINVITEINCOMING")
  }
  
  override fun onInviteTrying(p0: Long) {
    Log.d("BASE", "ONINVITETRYING Session ID $p0")
  }
  
  override fun onInviteSessionProgress(
    sessionId: Long,
    audioCodecs: String,
    videoCodecs: String,
    existsEarlyMedia: Boolean,
    existsAudio: Boolean,
    existsVideo: Boolean,
    sipMessage: String
  ) {
    Log.d("BASE", "ONINVITESESSIONPROGRESS $sipMessage")
  }
  
  override fun onInviteRinging(sessionId: Long, statusText: String?, statusCode: Int, sipMessage: String?) {
    Log.d("BASE", "ONINVITERINGING")
  }
  
  override fun onInviteAnswered(
    sessionId: Long,
    callerDisplayName: String,
    caller: String,
    calleeDisplayName: String,
    callee: String,
    audioCodecNames: String,
    videoCodecNames: String,
    existsAudio: Boolean,
    existsVideo: Boolean,
    sipMessage: String
  ) {
    Log.d("BASE", "ONINVITEANSWERED")
  }
  
  override fun onInviteFailure(sessionId: Long, reason: String?, code: Int, sipMessage: String?) {
    Log.d("BASE", "ONINVITEFAILURE")
  }
  
  override fun onInviteConnected(sessionId: Long) {
    Log.d("BASE", "ONINVITECONNECTED")
  }
  
  override fun onInviteUpdated(p0: Long, p1: String?, p2: String?, p3: String?, p4: Boolean, p5: Boolean, p6: Boolean, p7: String?) {
    Log.d("BASE", "ONINVITEUPDATED")
  }
  
  override fun onInviteBeginingForward(p0: String?) {
    Log.d("BASE", "ONINVITEBEGININGFORWARD")
  }
  
  override fun onInviteClosed(sessionId: Long, sipMessage: String) {
    Log.d("BASE", "ON_INVITE_CLOSED")
  }
  
  override fun onDialogStateUpdated(p0: String?, p1: String?, p2: String?, p3: String?) {
    Log.d("BASE", "ONDIALOGSTATEUPDATED")
  }
  
  override fun onRemoteHold(p0: Long) {
    Log.d("BASE", "ONREMOTEHOLD")
  }
  
  override fun onRemoteUnHold(p0: Long, p1: String?, p2: String?, p3: Boolean, p4: Boolean) {
    Log.d("BASE", "ONREMOTEUNHOLD")
  }
  
  override fun onReceivedRefer(p0: Long, p1: Long, p2: String?, p3: String?, p4: String?) {
    Log.d("BASE", "ONRECEIVEDREFER")
  }
  
  override fun onReferAccepted(p0: Long) {
    Log.d("BASE", "ONREFERACCEPTED")
  }
  
  override fun onReferRejected(p0: Long, p1: String?, p2: Int) {
    Log.d("BASE", "ONREFERREJECTED")
  }
  
  override fun onTransferTrying(p0: Long) {
    Log.d("BASE", "ONTRANSFERTRYING")
  }
  
  override fun onTransferRinging(p0: Long) {
    Log.d("BASE", "ONTRANSFERRINGING")
  }
  
  override fun onACTVTransferSuccess(p0: Long) {
    Log.d("BASE", "ONACTVTRANSFERSUCCESS")
  }
  
  override fun onACTVTransferFailure(p0: Long, p1: String?, p2: Int) {
    Log.d("BASE", "ONACTVTRANSFERFAILURE")
  }
  
  override fun onReceivedSignaling(p0: Long, p1: String?) {
    Log.d("BASE", "ONRECEIVEDSIGNALING")
  }
  
  override fun onSendingSignaling(p0: Long, p1: String?) {
    Log.d("BASE", "ONSENDINGSIGNALING")
  }
  
  override fun onWaitingVoiceMessage(p0: String?, p1: Int, p2: Int, p3: Int, p4: Int) {
    Log.d("BASE", "ONWAITINGVOICEMESSAGE")
  }
  
  override fun onWaitingFaxMessage(p0: String?, p1: Int, p2: Int, p3: Int, p4: Int) {
    Log.d("BASE", "ONWAITINGFAXMESSAGE")
  }
  
  override fun onRecvDtmfTone(p0: Long, p1: Int) {
    Log.d("BASE", "ONRECVDTMFTONE")
  }
  
  override fun onRecvOptions(p0: String?) {
    Log.d("BASE", "ONRECVOPTIONS")
  }
  
  override fun onRecvInfo(p0: String?) {
    Log.d("BASE", "ONRECVINFO")
  }
  
  override fun onRecvNotifyOfSubscription(p0: Long, p1: String?, p2: ByteArray?, p3: Int) {
    Log.d("BASE", "ONRECVNOTIFYOFSUBSCRIPTION")
  }
  
  override fun onPresenceRecvSubscribe(p0: Long, p1: String?, p2: String?, p3: String?) {
    Log.d("BASE", "ONPRESENCERECVSUBSCRIBE")
  }
  
  override fun onPresenceOnline(p0: String?, p1: String?, p2: String?) {
    Log.d("BASE", "ONPRESENCEONLINE")
  }
  
  override fun onPresenceOffline(p0: String?, p1: String?) {
    Log.d("BASE", "ONPRESENCEOFFLINE")
  }
  
  override fun onRecvMessage(p0: Long, p1: String?, p2: String?, p3: ByteArray?, p4: Int) {
    Log.d("BASE", "ONRECVMESSAGE")
  }
  
  override fun onRecvOutOfDialogMessage(p0: String?, p1: String?, p2: String?, p3: String?, p4: String?, p5: String?, p6: ByteArray?, p7: Int, p8: String?) {
    Log.d("BASE", "ONRECVOUTOFDIALOGMESSAGE")
  }
  
  override fun onSendMessageSuccess(p0: Long, p1: Long) {
    Log.d("BASE", "ONSENDMESSAGESUCCESS")
  }
  
  override fun onSendMessageFailure(p0: Long, p1: Long, p2: String?, p3: Int) {
    Log.d("BASE", "ONSENDMESSAGEFAILURE")
  }
  
  override fun onSendOutOfDialogMessageSuccess(p0: Long, p1: String?, p2: String?, p3: String?, p4: String?) {
    Log.d("BASE", "ONSENDOUTOFDIALOGMESSAGESUCCESS")
  }
  
  override fun onSendOutOfDialogMessageFailure(p0: Long, p1: String?, p2: String?, p3: String?, p4: String?, p5: String?, p6: Int) {
    Log.d("BASE", "ONSENDOUTOFDIALOGMESSAGEFAILURE")
  }
  
  override fun onSubscriptionFailure(p0: Long, p1: Int) {
    Log.d("BASE", "ONSUBSCRIPTIONFAILURE")
  }
  
  override fun onSubscriptionTerminated(p0: Long) {
    Log.d("BASE", "ONSUBSCRIPTIONTERMINATED")
  }
  
  override fun onPlayAudioFileFinished(p0: Long, p1: String?) {
    Log.d("BASE", "ONPLAYAUDIOFILEFINISHED")
  }
  
  override fun onPlayVideoFileFinished(p0: Long) {
    Log.d("BASE", "ONPLAYVIDEOFILEFINISHED")
  }
  
  override fun onAudioRawCallback(p0: Long, p1: Int, p2: ByteArray?, p3: Int, p4: Int) {
    Log.d("BASE", "ONAUDIORAWCALLBACK")
  }
  
  override fun onVideoRawCallback(p0: Long, p1: Int, p2: Int, p3: Int, p4: ByteArray?, p5: Int) {
    Log.d("BASE", "ONVIDEORAWCALLBACK")
  }
  
  override fun onAudioDeviceChanged(currentDevice: PortSipEnumDefine.AudioDevice?, availableDevices: MutableSet<PortSipEnumDefine.AudioDevice>?) {
    Log.d("BASE", "ONAUDIODEVICECHANGED")
  }
  
  override fun onRTPPacketCallback(p0: Long, p1: Int, p2: Int, p3: ByteArray?, p4: Int) {
    Log.d("BASE", "ONRTPPACKETCALLBACK")
  }
  
}
