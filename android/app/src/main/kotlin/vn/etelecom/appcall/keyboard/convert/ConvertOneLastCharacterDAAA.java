package vn.etelecom.appcall.keyboard.convert;

import android.view.inputmethod.InputConnection;

public final class ConvertOneLastCharacterDAAA {

    public ConvertOneLastCharacterDAAA() {}

    // d, đ, a, á, à, ả, ạ, ã
    public static String convert(String characterLastest, String textInput, InputConnection inputConnection){
        String valueText = textInput;
        switch (characterLastest){
            case "d":
                if(textInput == "d" || textInput == "D"){
                    valueText = "đ";
                }
            break;
            case "đ":
                if(textInput == "d" || textInput == "D"){
                    valueText = "dd";
                }
            break;
            case "a":
                switch (textInput) {
                case "s":
                case "S":
                    valueText = "á";
                    break;
                case "f":
                case "F":
                    valueText = "à";
                    break;
                case "r":
                case "R":
                    valueText = "ả";
                    break;
                case "x":
                case "X":
                    valueText = "ã";
                    break;
                case "j":
                case "J":
                    valueText = "ạ";
                    break;
                case "a":
                case "A":
                    valueText = "â";
                    break;
                case "w":
                case "W":
                    valueText = "ă";
                    break;
                default:
                    break;
            }
            break;
            case "á":
                switch (textInput) {
                case "s":
                    case "S":
                    valueText = "as";
                    break;
                case "f":
                    case "F":
                    valueText = "à";
                    break;
                case "r":
                    case "R":
                    valueText = "ả";
                    break;
                case "x":
                    case "X":
                    valueText = "ã";
                    break;
                case "j":
                    case "J":
                    valueText = "ạ";
                    break;
                case "a":
                    case "A":
                    valueText = "ấ";
                    break;
                case "w":
                    case "W":
                default:
                    break;
            }
            break;
            case "à":
                switch (textInput) {
                case "s":
                    valueText = "á";
                    break;
                case "f":
                    valueText = "af";
                    break;
                case "r":
                    valueText = "ả";
                    break;
                case "x":
                    valueText = "ã";
                    break;
                case "j":
                    valueText = "ạ";
                    break;
                case "a":
                    valueText = "ầ";
                    break;
                case "w":
                    valueText = "ằ";
                    break;
                case "S":
                    valueText = "á";
                    break;
                case "F":
                    valueText = "af";
                    break;
                case "R":
                    valueText = "ả";
                    break;
                case "X":
                    valueText = "ã";
                    break;
                case "J":
                    valueText = "ạ";
                    break;
                case "A":
                    valueText = "ầ";
                    break;
                case "W":
                    valueText = "ằ";
                    break;
                default:
                    break;
            }
            break;
            case "ả":
                switch (textInput) {
                case "s":
                    valueText = "á";
                    break;
                case "f":
                    valueText = "à";
                    break;
                case "r":
                    valueText = "ar";
                    break;
                case "x":
                    valueText = "ã";
                    break;
                case "j":
                    valueText = "ạ";
                    break;
                case "a":
                    valueText = "ẩ";
                    break;
                case "w":
                    valueText = "ẳ";
                    break;
                case "S":
                    valueText = "á";
                    break;
                case "F":
                    valueText = "à";
                    break;
                case "R":
                    valueText = "ar";
                    break;
                case "X":
                    valueText = "ã";
                    break;
                case "J":
                    valueText = "ạ";
                    break;
                case "A":
                    valueText = "ẩ";
                    break;
                case "W":
                    valueText = "ẳ";
                    break;
                default:
                    break;
            }
            break;
            case "ã":
                switch (textInput) {
                case "s":
                    valueText = "á";
                    break;
                case "f":
                    valueText = "à";
                    break;
                case "r":
                    valueText = "ả";
                    break;
                case "x":
                    valueText = "ax";
                    break;
                case "j":
                    valueText = "ạ";
                    break;
                case "a":
                    valueText = "ẫ";
                    break;
                case "w":
                    valueText = "ẵ";
                    break;
                case "S":
                    valueText = "á";
                    break;
                case "F":
                    valueText = "à";
                    break;
                case "R":
                    valueText = "ả";
                    break;
                case "X":
                    valueText = "ax";
                    break;
                case "J":
                    valueText = "ạ";
                    break;
                case "A":
                    valueText = "ẫ";
                    break;
                case "W":
                    valueText = "ẵ";
                    break;
                default:
                    break;
            }
            break;
            case "ạ":
                switch (textInput) {
                case "s":
                    valueText = "á";
                    break;
                case "f":
                    valueText = "à";
                    break;
                case "r":
                    valueText = "ả";
                    break;
                case "x":
                    valueText = "ã";
                    break;
                case "j":
                    valueText = "aj";
                    break;
                case "a":
                    valueText = "ậ";
                    break;
                case "w":
                    valueText = "ặ";
                    break;
                case "S":
                    valueText = "á";
                    break;
                case "F":
                    valueText = "à";
                    break;
                case "R":
                    valueText = "ả";
                    break;
                case "X":
                    valueText = "ã";
                    break;
                case "J":
                    valueText = "aj";
                    break;
                case "A":
                    valueText = "ậ";
                    break;
                case "W":
                    valueText = "ặ";
                    break;
                default:
                    break;
            }
            break;
            case "ă":
                switch (textInput) {
                    case "s":
                        valueText = "ắ";
                        break;
                    case "f":
                        valueText = "ằ";
                        break;
                    case "r":
                        valueText = "ẳ";
                        break;
                    case "x":
                        valueText = "ẵ";
                        break;
                    case "j":
                        valueText = "ặ";
                        break;
                    case "w":
                        valueText = "aw";
                        break;
                    case "a":
                        valueText = "â";
                        break;
                    case "S":
                        valueText = "ắ";
                        break;
                    case "F":
                        valueText = "ằ";
                        break;
                    case "R":
                        valueText = "ẳ";
                        break;
                    case "X":
                        valueText = "ẵ";
                        break;
                    case "J":
                        valueText = "ặ";
                        break;
                    case "W":
                        valueText = "aw";
                        break;
                    case "A":
                        valueText = "â";
                        break;
                    default:
                        break;
                }
                break;
            case "ắ":
                switch (textInput) {
                    case "s":
                        valueText = "ăs";
                        break;
                    case "f":
                        valueText = "ằ";
                        break;
                    case "r":
                        valueText = "ẳ";
                        break;
                    case "x":
                        valueText = "ẵ";
                        break;
                    case "j":
                        valueText = "ặ";
                        break;
                    case "w":
                        valueText = "áw";
                        break;
                    case "a":
                        valueText = "ấ";
                        break;
                    case "S":
                        valueText = "ăs";
                        break;
                    case "F":
                        valueText = "ằ";
                        break;
                    case "R":
                        valueText = "ẳ";
                        break;
                    case "X":
                        valueText = "ẵ";
                        break;
                    case "J":
                        valueText = "ặ";
                        break;
                    case "W":
                        valueText = "áw";
                        break;
                    case "A":
                        valueText = "ấ";
                        break;
                    default:
                        break;
                }
                break;
            case "ằ":
                switch (textInput) {
                    case "s":
                        valueText = "ắ";
                        break;
                    case "f":
                        valueText = "ăf";
                        break;
                    case "r":
                        valueText = "ẳ";
                        break;
                    case "x":
                        valueText = "ẵ";
                        break;
                    case "j":
                        valueText = "ặ";
                        break;
                    case "w":
                        valueText = "àw";
                        break;
                    case "a":
                        valueText = "ầ";
                        break;
                    case "S":
                        valueText = "ắ";
                        break;
                    case "F":
                        valueText = "ăf";
                        break;
                    case "R":
                        valueText = "ẳ";
                        break;
                    case "X":
                        valueText = "ẵ";
                        break;
                    case "J":
                        valueText = "ặ";
                        break;
                    case "W":
                        valueText = "àw";
                        break;
                    case "A":
                        valueText = "ầ";
                        break;
                    default:
                        break;
                }
                break;
            case "ẳ":
                switch (textInput) {
                    case "s":
                        valueText = "ắ";
                        break;
                    case "f":
                        valueText = "ằ";
                        break;
                    case "r":
                        valueText = "ăr";
                        break;
                    case "x":
                        valueText = "ẵ";
                        break;
                    case "j":
                        valueText = "ặ";
                        break;
                    case "w":
                        valueText = "ảw";
                        break;
                    case "a":
                        valueText = "ẩ";
                        break;
                    case "S":
                        valueText = "ắ";
                        break;
                    case "F":
                        valueText = "ằ";
                        break;
                    case "R":
                        valueText = "ăr";
                        break;
                    case "X":
                        valueText = "ẵ";
                        break;
                    case "J":
                        valueText = "ặ";
                        break;
                    case "W":
                        valueText = "ảw";
                        break;
                    case "A":
                        valueText = "ẩ";
                        break;
                    default:
                        break;
                }
                break;
            case "ẵ":
                switch (textInput) {
                    case "s":
                        valueText = "ắ";
                        break;
                    case "f":
                        valueText = "ằ";
                        break;
                    case "r":
                        valueText = "ẳ";
                        break;
                    case "x":
                        valueText = "ăx";
                        break;
                    case "j":
                        valueText = "ặ";
                        break;
                    case "w":
                        valueText = "ãw";
                        break;
                    case "a":
                        valueText = "ẫ";
                        break;
                    case "S":
                        valueText = "ắ";
                        break;
                    case "F":
                        valueText = "ằ";
                        break;
                    case "R":
                        valueText = "ẳ";
                        break;
                    case "X":
                        valueText = "ăx";
                        break;
                    case "J":
                        valueText = "ặ";
                        break;
                    case "W":
                        valueText = "ãw";
                        break;
                    case "A":
                        valueText = "ẫ";
                        break;
                    default:
                        break;
                }
                break;
            case "ặ":
                switch (textInput) {
                    case "s":
                        valueText = "ắ";
                        break;
                    case "f":
                        valueText = "ằ";
                        break;
                    case "r":
                        valueText = "ẳ";
                        break;
                    case "x":
                        valueText = "ẵ";
                        break;
                    case "j":
                        valueText = "ăj";
                        break;
                    case "w":
                        valueText = "ạw";
                        break;
                    case "a":
                        valueText = "ậ";
                        break;
                    case "S":
                        valueText = "ắ";
                        break;
                    case "F":
                        valueText = "ằ";
                        break;
                    case "R":
                        valueText = "ẳ";
                        break;
                    case "X":
                        valueText = "ẵ";
                        break;
                    case "J":
                        valueText = "ăj";
                        break;
                    case "W":
                        valueText = "ạw";
                        break;
                    case "A":
                        valueText = "ậ";
                        break;
                    default:
                        break;
                }
                break;
            case "â":
                switch (textInput) {
                    case "s":
                        valueText = "ấ";
                        break;
                    case "f":
                        valueText = "ầ";
                        break;
                    case "r":
                        valueText = "ẩ";
                        break;
                    case "x":
                        valueText = "ẫ";
                        break;
                    case "j":
                        valueText = "ậ";
                        break;
                    case "a":
                        valueText = "aa";
                        break;
                    case "w":
                        valueText = "ă";
                        break;
                    case "S":
                        valueText = "ấ";
                        break;
                    case "F":
                        valueText = "ầ";
                        break;
                    case "R":
                        valueText = "ẩ";
                        break;
                    case "X":
                        valueText = "ẫ";
                        break;
                    case "J":
                        valueText = "ậ";
                        break;
                    case "A":
                        valueText = "aa";
                        break;
                    case "W":
                        valueText = "ă";
                        break;
                    default:
                        break;
                }
                break;
            case "ấ":
                switch (textInput) {
                    case "s":
                        valueText = "âs";
                        break;
                    case "f":
                        valueText = "ầ";
                        break;
                    case "r":
                        valueText = "ẩ";
                        break;
                    case "x":
                        valueText = "ẫ";
                        break;
                    case "j":
                        valueText = "ậ";
                        break;
                    case "a":
                        valueText = "áa";
                        break;
                    case "w":
                        valueText = "ắ";
                        break;
                    case "S":
                        valueText = "âS";
                        break;
                    case "F":
                        valueText = "ầ";
                        break;
                    case "R":
                        valueText = "ẩ";
                        break;
                    case "X":
                        valueText = "ẫ";
                        break;
                    case "J":
                        valueText = "ậ";
                        break;
                    case "A":
                        valueText = "áA";
                        break;
                    case "W":
                        valueText = "ắ";
                        break;
                    default:
                        break;
                }
                break;
            case "ầ":
                switch (textInput) {
                    case "s":
                        valueText = "ấ";
                        break;
                    case "f":
                        valueText = "âf";
                        break;
                    case "r":
                        valueText = "ẩ";
                        break;
                    case "x":
                        valueText = "ẫ";
                        break;
                    case "j":
                        valueText = "ậ";
                        break;
                    case "a":
                        valueText = "àa";
                        break;
                    case "w":
                        valueText = "ằ";
                        break;
                    case "S":
                        valueText = "ấ";
                        break;
                    case "F":
                        valueText = "âF";
                        break;
                    case "R":
                        valueText = "ẩ";
                        break;
                    case "X":
                        valueText = "ẫ";
                        break;
                    case "J":
                        valueText = "ậ";
                        break;
                    case "A":
                        valueText = "àA";
                        break;
                    case "W":
                        valueText = "ằ";
                        break;
                    default:
                        break;
                }
                break;
            case "ẩ":
                switch (textInput) {
                    case "s":
                        valueText = "ấ";
                        break;
                    case "f":
                        valueText = "ầ";
                        break;
                    case "r":
                        valueText = "âr";
                        break;
                    case "x":
                        valueText = "ẫ";
                        break;
                    case "j":
                        valueText = "ậ";
                        break;
                    case "a":
                        valueText = "ảa";
                        break;
                    case "w":
                        valueText = "ẳ";
                        break;
                    case "S":
                        valueText = "ấ";
                        break;
                    case "F":
                        valueText = "ầ";
                        break;
                    case "R":
                        valueText = "âR";
                        break;
                    case "X":
                        valueText = "ẫ";
                        break;
                    case "J":
                        valueText = "ậ";
                        break;
                    case "A":
                        valueText = "ảA";
                        break;
                    case "W":
                        valueText = "ẳ";
                        break;
                    default:
                        break;
                }
                break;
            case "ẫ":
                switch (textInput) {
                    case "s":
                        valueText = "ấ";
                        break;
                    case "f":
                        valueText = "ầ";
                        break;
                    case "r":
                        valueText = "ẩ";
                        break;
                    case "x":
                        valueText = "âx";
                        break;
                    case "j":
                        valueText = "ậ";
                        break;
                    case "a":
                        valueText = "ãa";
                        break;
                    case "w":
                        valueText = "ẵ";
                        break;
                    case "S":
                        valueText = "ấ";
                        break;
                    case "F":
                        valueText = "ầ";
                        break;
                    case "R":
                        valueText = "ẩ";
                        break;
                    case "X":
                        valueText = "âX";
                        break;
                    case "J":
                        valueText = "ậ";
                        break;
                    case "A":
                        valueText = "ãA";
                        break;
                    case "W":
                        valueText = "ẵ";
                        break;
                    default:
                        break;
                }
                break;
            case "ậ":
                switch (textInput) {
                    case "s":
                        valueText = "ấ";
                        break;
                    case "f":
                        valueText = "ầ";
                        break;
                    case "r":
                        valueText = "ẩ";
                        break;
                    case "x":
                        valueText = "ẫ";
                        break;
                    case "j":
                        valueText = "âj";
                        break;
                    case "a":
                        valueText = "ạa";
                        break;
                    case "w":
                        valueText = "ặ";
                        break;
                    case "S":
                        valueText = "ấ";
                        break;
                    case "F":
                        valueText = "ầ";
                        break;
                    case "R":
                        valueText = "ẩ";
                        break;
                    case "X":
                        valueText = "ẫ";
                        break;
                    case "J":
                        valueText = "âJ";
                        break;
                    case "A":
                        valueText = "ạA";
                        break;
                    case "W":
                        valueText = "ặ";
                        break;
                    default:
                        break;
                }
                break;
                        case "D":
                switch (textInput) {
                    case "d":
                        valueText = "Đ";
                        break;
                    case "D":
                        valueText = "Đ";
                    default:
                        break;
                }
                break;
            case "Đ":
                switch (textInput) {
                    case "d":
                        valueText = "Dd";
                        break;
                    case "D":
                        valueText = "DD";
                        break;
                    default:
                        break;
                }
                break;
            case "A":
                switch (textInput) {
                    case "s":
                        valueText = "Á";
                        break;
                    case "S":
                        valueText = "Á";
                        break;
                    case "f":
                        valueText = "À";
                        break;
                    case "F":
                        valueText = "À";
                        break;
                    case "r":
                        valueText = "Ả";
                        break;
                    case "R":
                        valueText = "Ả";
                        break;
                    case "x":
                        valueText = "Ã";
                        break;
                    case "X":
                        valueText = "Ã";
                        break;
                    case "j":
                        valueText = "Ạ";
                        break;
                    case "J":
                        valueText = "Ạ";
                        break;
                    case "a":
                        valueText = "Â";
                        break;
                    case "A":
                        valueText = "Â";
                        break;
                    case "w":
                        valueText = "Ă";
                        break;
                    case "W":
                        valueText = "Ă";
                        break;
                    default:
                        break;
                }
                break;
            case "Á":
                switch (textInput) {
                    case "s":
                        valueText = "As";
                        break;
                    case "f":
                        valueText = "À";
                        break;
                    case "r":
                        valueText = "Ả";
                        break;
                    case "x":
                        valueText = "Ã";
                        break;
                    case "j":
                        valueText = "Ạ";
                        break;
                    case "a":
                        valueText = "Ấ";
                        break;
                    case "w":
                        valueText = "Ắ";
                        break;
                    case "S":
                        valueText = "AS";
                        break;
                    case "F":
                        valueText = "À";
                        break;
                    case "R":
                        valueText = "Ả";
                        break;
                    case "X":
                        valueText = "Ã";
                        break;
                    case "J":
                        valueText = "Ạ";
                        break;
                    case "A":
                        valueText = "Ấ";
                        break;
                    case "W":
                        valueText = "Ắ";
                        break;
                    default:
                        break;
                }
                break;
            case "À":
                switch (textInput) {
                    case "s":
                        valueText = "Á";
                        break;
                    case "f":
                        valueText = "Af";
                        break;
                    case "r":
                        valueText = "Ả";
                        break;
                    case "x":
                        valueText = "Ã";
                        break;
                    case "j":
                        valueText = "Ạ";
                        break;
                    case "a":
                        valueText = "Ầ";
                        break;
                    case "w":
                        valueText = "Ằ";
                        break;
                    case "S":
                        valueText = "Á";
                        break;
                    case "F":
                        valueText = "AF";
                        break;
                    case "R":
                        valueText = "Ả";
                        break;
                    case "X":
                        valueText = "Ã";
                        break;
                    case "J":
                        valueText = "Ạ";
                        break;
                    case "A":
                        valueText = "Ầ";
                        break;
                    case "W":
                        valueText = "Ằ";
                        break;
                    default:
                        break;
                }
                break;
            case "Ả":
                switch (textInput) {
                    case "s":
                        valueText = "Á";
                        break;
                    case "f":
                        valueText = "À";
                        break;
                    case "r":
                        valueText = "Ar";
                        break;
                    case "x":
                        valueText = "Ã";
                        break;
                    case "j":
                        valueText = "Ạ";
                        break;
                    case "a":
                        valueText = "Ấ";
                        break;
                    case "w":
                        valueText = "Ắ";
                        break;
                    case "S":
                        valueText = "Ả";
                        break;
                    case "F":
                        valueText = "À";
                        break;
                    case "R":
                        valueText = "AR";
                        break;
                    case "X":
                        valueText = "Ã";
                        break;
                    case "J":
                        valueText = "Ạ";
                        break;
                    case "A":
                        valueText = "Ẩ";
                        break;
                    case "W":
                        valueText = "Ẳ";
                        break;
                    default:
                        break;
                }
                break;
            case "Ã":
                switch (textInput) {
                    case "s":
                        valueText = "Á";
                        break;
                    case "f":
                        valueText = "À";
                        break;
                    case "r":
                        valueText = "Ả";
                        break;
                    case "x":
                        valueText = "Ax";
                        break;
                    case "j":
                        valueText = "Ạ";
                        break;
                    case "a":
                        valueText = "Ầ";
                        break;
                    case "w":
                        valueText = "Ằ";
                        break;
                    case "S":
                        valueText = "Á";
                        break;
                    case "F":
                        valueText = "À";
                        break;
                    case "R":
                        valueText = "Ả";
                        break;
                    case "X":
                        valueText = "AX";
                        break;
                    case "J":
                        valueText = "Ạ";
                        break;
                    case "A":
                        valueText = "Ẫ";
                        break;
                    case "W":
                        valueText = "Ẵ";
                        break;
                    default:
                        break;
                }
                break;
            case "Ạ":
                switch (textInput) {
                    case "s":
                        valueText = "Á";
                        break;
                    case "f":
                        valueText = "À";
                        break;
                    case "r":
                        valueText = "Ả";
                        break;
                    case "x":
                        valueText = "Ã";
                        break;
                    case "j":
                        valueText = "Aj";
                        break;
                    case "a":
                        valueText = "Ầ";
                        break;
                    case "w":
                        valueText = "Ằ";
                        break;
                    case "S":
                        valueText = "Á";
                        break;
                    case "F":
                        valueText = "À";
                        break;
                    case "R":
                        valueText = "Ả";
                        break;
                    case "X":
                        valueText = "Ã";
                        break;
                    case "J":
                        valueText = "AJ";
                        break;
                    case "A":
                        valueText = "Ầ";
                        break;
                    case "W":
                        valueText = "Ằ";
                        break;
                    default:
                        break;
                }
                break;
            case "Ă":
                switch (textInput) {
                    case "s":
                        valueText = "Ắ";
                        break;
                    case "f":
                        valueText = "Ằ";
                        break;
                    case "r":
                        valueText = "Ẳ";
                        break;
                    case "x":
                        valueText = "Ẵ";
                        break;
                    case "j":
                        valueText = "Ặ";
                        break;
                    case "w":
                        valueText = "Aw";
                        break;
                    case "a":
                        valueText = "Â";
                        break;
                    case "S":
                        valueText = "Ắ";
                        break;
                    case "F":
                        valueText = "Ằ";
                        break;
                    case "R":
                        valueText = "Ẳ";
                        break;
                    case "X":
                        valueText = "Ẵ";
                        break;
                    case "J":
                        valueText = "Ặ";
                        break;
                    case "W":
                        valueText = "AW";
                        break;
                    case "A":
                        valueText = "Â";
                        break;
                    default:
                        break;
                }
                break;
            case "Ắ":
                switch (textInput) {
                    case "s":
                        valueText = "Ăs";
                        break;
                    case "f":
                        valueText = "Ằ";
                        break;
                    case "r":
                        valueText = "Ẳ";
                        break;
                    case "x":
                        valueText = "Ẵ";
                        break;
                    case "j":
                        valueText = "Ặ";
                        break;
                    case "w":
                        valueText = "Áw";
                        break;
                    case "a":
                        valueText = "Ấ";
                        break;
                    case "S":
                        valueText = "ĂS";
                        break;
                    case "F":
                        valueText = "Ằ";
                        break;
                    case "R":
                        valueText = "Ẳ";
                        break;
                    case "X":
                        valueText = "Ẵ";
                        break;
                    case "J":
                        valueText = "Ặ";
                        break;
                    case "W":
                        valueText = "ÁW";
                        break;
                    case "A":
                        valueText = "Ấ";
                        break;
                    default:
                        break;
                }
                break;
            case "Ằ":
                switch (textInput) {
                    case "s":
                        valueText = "Ắ";
                        break;
                    case "f":
                        valueText = "Ăf";
                        break;
                    case "r":
                        valueText = "Ẳ";
                        break;
                    case "x":
                        valueText = "Ẵ";
                        break;
                    case "j":
                        valueText = "Ặ";
                        break;
                    case "w":
                        valueText = "Àw";
                        break;
                    case "a":
                        valueText = "Ầ";
                        break;
                    case "S":
                        valueText = "Ắ";
                        break;
                    case "F":
                        valueText = "ĂF";
                        break;
                    case "R":
                        valueText = "Ẳ";
                        break;
                    case "X":
                        valueText = "Ẵ";
                        break;
                    case "J":
                        valueText = "Ặ";
                        break;
                    case "W":
                        valueText = "ÀW";
                        break;
                    case "A":
                        valueText = "Ầ";
                    default:
                        break;
                }
                break;
            case "Ẳ":
                switch (textInput) {
                    case "s":
                        valueText = "Ắ";
                        break;
                    case "f":
                        valueText = "Ằ";
                        break;
                    case "r":
                        valueText = "Ăr";
                        break;
                    case "x":
                        valueText = "Ẵ";
                        break;
                    case "j":
                        valueText = "Ặ";
                        break;
                    case "w":
                        valueText = "Ảw";
                        break;
                    case "a":
                        valueText = "Â";
                        break;
                    case "S":
                        valueText = "Ắ";
                        break;
                    case "F":
                        valueText = "Ằ";
                        break;
                    case "R":
                        valueText = "ĂR";
                        break;
                    case "X":
                        valueText = "Ẵ";
                        break;
                    case "J":
                        valueText = "Ặ";
                        break;
                    case "W":
                        valueText = "ẢW";
                        break;
                    case "A":
                        valueText = "Ẩ";
                    default:
                        break;
                }
                break;
            case "Ẵ":
                switch (textInput) {
                    case "s":
                        valueText = "Ắ";
                        break;
                    case "f":
                        valueText = "Ằ";
                        break;
                    case "r":
                        valueText = "Ẳ";
                        break;
                    case "x":
                        valueText = "Ăx";
                        break;
                    case "j":
                        valueText = "Ặ";
                        break;
                    case "w":
                        valueText = "Ãw";
                        break;
                    case "a":
                        valueText = "Ẫ";
                        break;
                    case "S":
                        valueText = "Ắ";
                        break;
                    case "F":
                        valueText = "Ằ";
                        break;
                    case "R":
                        valueText = "Ẳ";
                        break;
                    case "X":
                        valueText = "ĂX";
                        break;
                    case "J":
                        valueText = "Ặ";
                        break;
                    case "W":
                        valueText = "Ãw";
                        break;
                    case "A":
                        valueText = "Ẫ";
                        break;
                    default:
                        break;
                }
                break;
            case "Ặ":
                switch (textInput) {
                    case "s":
                        valueText = "Ắ";
                        break;
                    case "f":
                        valueText = "Ằ";
                        break;
                    case "r":
                        valueText = "Ẳ";
                        break;
                    case "x":
                        valueText = "Ẵ";
                        break;
                    case "j":
                        valueText = "Ăj";
                        break;
                    case "w":
                        valueText = "Ạw";
                        break;
                    case "a":
                        valueText = "Ậ";
                        break;
                    case "S":
                        valueText = "Ắ";
                        break;
                    case "F":
                        valueText = "Ằ";
                        break;
                    case "R":
                        valueText = "Ẳ";
                        break;
                    case "X":
                        valueText = "Ẵ";
                        break;
                    case "J":
                        valueText = "ĂJ";
                        break;
                    case "W":
                        valueText = "ẠW";
                        break;
                    case "A":
                        valueText = "Ậ";
                    default:
                        break;
                }
                break;
            case "Â":
                switch (textInput) {
                    case "s":
                        valueText = "Ấ";
                        break;
                    case "f":
                        valueText = "Ầ";
                        break;
                    case "r":
                        valueText = "Ẩ";
                        break;
                    case "x":
                        valueText = "Ẫ";
                        break;
                    case "j":
                        valueText = "Ậ";
                        break;
                    case "a":
                        valueText = "Aa";
                        break;
                    case "w":
                        valueText = "Ă";
                        break;
                    case "S":
                        valueText = "Ấ";
                        break;
                    case "F":
                        valueText = "Ầ";
                        break;
                    case "R":
                        valueText = "Ẩ";
                        break;
                    case "X":
                        valueText = "Ẫ";
                        break;
                    case "J":
                        valueText = "Ậ";
                        break;
                    case "A":
                        valueText = "AA";
                        break;
                    case "W":
                        valueText = "Ă";
                        break;
                    default:
                        break;
                }
                break;
            case "Ấ":
                switch (textInput) {
                    case "s":
                        valueText = "ÂS";
                        break;
                    case "f":
                        valueText = "Ầ";
                        break;
                    case "r":
                        valueText = "Ẩ";
                        break;
                    case "x":
                        valueText = "Ẫ";
                        break;
                    case "j":
                        valueText = "Ậ";
                        break;
                    case "a":
                        valueText = "Áa";
                        break;
                    case "w":
                        valueText = "Ă";
                        break;
                    case "S":
                        valueText = "ÂS";
                        break;
                    case "F":
                        valueText = "Ầ";
                        break;
                    case "R":
                        valueText = "Ẩ";
                        break;
                    case "X":
                        valueText = "Ẫ";
                        break;
                    case "J":
                        valueText = "Ậ";
                        break;
                    case "A":
                        valueText = "ÁA";
                        break;
                    case "W":
                        valueText = "Ă";
                        break;
                    default:
                        break;
                }
                break;
            case "Ầ":
                switch (textInput) {
                    case "s":
                        valueText = "Ấ";
                        break;
                    case "f":
                        valueText = "ÂF";
                        break;
                    case "r":
                        valueText = "Ẩ";
                        break;
                    case "x":
                        valueText = "Ẫ";
                        break;
                    case "j":
                        valueText = "Ậ";
                        break;
                    case "a":
                        valueText = "Àa";
                        break;
                    case "w":
                        valueText = "Ă";
                        break;
                    case "S":
                        valueText = "Ấ";
                        break;
                    case "F":
                        valueText = "ÂF";
                        break;
                    case "R":
                        valueText = "Ẩ";
                        break;
                    case "X":
                        valueText = "Ẫ";
                        break;
                    case "J":
                        valueText = "Ậ";
                        break;
                    case "A":
                        valueText = "ÀA";
                        break;
                    case "W":
                        valueText = "Ă";
                        break;
                    default:
                        break;
                }
                break;
            case "Ẩ":
                switch (textInput) {
                    case "s":
                        valueText = "Ấ";
                        break;
                    case "f":
                        valueText = "Ầ";
                        break;
                    case "r":
                        valueText = "ÂR";
                        break;
                    case "x":
                        valueText = "Ẫ";
                        break;
                    case "j":
                        valueText = "Ậ";
                        break;
                    case "a":
                        valueText = "Ảa";
                        break;
                    case "w":
                        valueText = "Ă";
                        break;
                    case "S":
                        valueText = "Ấ";
                        break;
                    case "F":
                        valueText = "Ầ";
                        break;
                    case "R":
                        valueText = "ÂR";
                        break;
                    case "X":
                        valueText = "Ẫ";
                        break;
                    case "J":
                        valueText = "Ậ";
                        break;
                    case "A":
                        valueText = "ẢA";
                        break;
                    case "W":
                        valueText = "Ă";
                        break;
                    default:
                        break;
                }
                break;
            case "Ẫ":
                switch (textInput) {
                    case "s":
                        valueText = "Ấ";
                        break;
                    case "f":
                        valueText = "Ầ";
                        break;
                    case "r":
                        valueText = "Ẩ";
                        break;
                    case "x":
                        valueText = "Âx";
                        break;
                    case "j":
                        valueText = "Ậ";
                        break;
                    case "a":
                        valueText = "Ãa";
                        break;
                    case "w":
                        valueText = "Ă";
                        break;
                    case "S":
                        valueText = "Ấ";
                        break;
                    case "F":
                        valueText = "Ầ";
                        break;
                    case "R":
                        valueText = "Ẩ";
                        break;
                    case "X":
                        valueText = "ÂX";
                        break;
                    case "J":
                        valueText = "Ậ";
                        break;
                    case "A":
                        valueText = "ÃA";
                        break;
                    case "W":
                        valueText = "Ă";
                        break;
                    default:
                        break;
                }
                break;
            case "Ậ":
                switch (textInput) {
                    case "s":
                        valueText = "Ấ";
                        break;
                    case "f":
                        valueText = "Ầ";
                        break;
                    case "r":
                        valueText = "Ẩ";
                        break;
                    case "x":
                        valueText = "Ẫ";
                        break;
                    case "j":
                        valueText = "Âj";
                        break;
                    case "a":
                        valueText = "Ạa";
                        break;
                    case "w":
                        valueText = "Ă";
                        break;
                    case "S":
                        valueText = "Ấ";
                        break;
                    case "F":
                        valueText = "Ầ";
                        break;
                    case "R":
                        valueText = "Ẩ";
                        break;
                    case "X":
                        valueText = "Ẫ";
                        break;
                    case "J":
                        valueText = "ÂJ";
                        break;
                    case "A":
                        valueText = "ẠA";
                        break;
                    case "W":
                        valueText = "Ă";
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }
        if(valueText != (textInput)){
//            textDocumentProxy.deleteBackward();
            inputConnection.deleteSurroundingText(1,0);
        }
        return valueText;
    }

}
