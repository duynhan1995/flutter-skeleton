package vn.etelecom.appcall.keyboard;

import android.annotation.SuppressLint;
import android.inputmethodservice.InputMethodService;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.media.AudioManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager2.widget.ViewPager2;

import vn.etelecom.appcall.R;
import vn.etelecom.appcall.keyboard.convert.ReturnValueAfterConvert;
import vn.etelecom.appcall.keyboard.view.EmojiCollectionAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class EtopKeyboard extends InputMethodService implements KeyboardView.OnKeyboardActionListener, View.OnClickListener {

    private InputConnection _inputConnection;

    private KeyboardView keyboardView;
    private Keyboard keyboard;
    private Keyboard keyboardSymbol;
    private Keyboard keyboardSymbolShift;
//    private RelativeLayout relativeLayout;
    private LinearLayout linearLayout;
    private LinearLayout lnViewEtop;
    //keyboard qwer
    private TextView tvShowEmoji;
    private LinearLayout lnKeyboardText;

    //emoji
    private EmojiCollectionAdapter emojiCollectionAdapter;
    private ViewPager2 viewPager;
    private LinearLayout lnEmojiView;
    private LinearLayout lnDeleteEmojiView;
    private TextView tvHideEmojiView;
    private TextView tvHideIconEmojiView;
    private TextView tvEmojiSmileList;
    private TextView tvEmojiAnimalList;
    private TextView tvEmojiFoodList;
    private TextView tvEmojiTravelList;
    private TextView tvEmojiActivityList;
    private TextView tvEmojiObjectList;
    private TextView tvEmojiSymbolList;
    private TextView tvEmojiFlagList;
    private Keyboard.Key shiftKey;


    Boolean isCaps = false;
    int countClickShift = 0;
    String textBefore = "";

    int numberChangeKeyboard = 0;
    int numberSymbolKeyboard = 0;

    private ArrayList<String> arraySaveTextInput = new ArrayList<>();


    @Override
    public void onPress(int primaryCode) {
        if(primaryCode == -1){
            List<Keyboard.Key> keys = keyboard.getKeys();

            for(int i = 0; i < keys.size() - 1; i++ )
            {
                Keyboard.Key currentKey = keys.get(i);

                //If your Key contains more than one code, then you will have to check if the codes array contains the primary code
                if(currentKey.codes[0] == primaryCode)
                {
                    shiftKey = currentKey;
                    if(countClickShift == 1){
                        countClickShift++;
                        currentKey.label = null;
                        currentKey.icon = getResources().getDrawable(R.drawable.double_shift_blue_24);
                    }else if(countClickShift == 0 || countClickShift == 2){
                        isCaps = !isCaps;
                        keyboard.setShifted(isCaps);
                        keyboardView.invalidateAllKeys();
                        if(countClickShift == 2){
                            countClickShift = 0;
                            currentKey.label = null;
                            currentKey.icon = getResources().getDrawable(R.drawable.shift_gray_24);
                        }else {
                            countClickShift++;
                            currentKey.label = null;
                            currentKey.icon = getResources().getDrawable(R.drawable.shift_blue_24);
                        }
                    }
                    break; // leave the loop once you find your match
                }
            }
        }else {
            if(countClickShift == 1){
                countClickShift = 0;
                shiftKey.label = null;
                shiftKey.icon = getResources().getDrawable(R.drawable.shift_gray_24);
            }
        }
    }

    @Override
    public void onRelease(int primaryCode) {

    }

    @SuppressLint("InflateParams")
    @Override
    public View onCreateInputView() {
//        keyboardView = (KeyboardView) getLayoutInflater().inflate(R.layout.keyboard,null);
        linearLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.keyboard, null);
        keyboardView = linearLayout.findViewById(R.id.keyboard_view);
        keyboard = new Keyboard(this, R.xml.qwerty);
        keyboardSymbol = new Keyboard(this, R.xml.rows_symbols);
        keyboardSymbolShift = new Keyboard(this, R.xml.rows_symbols_shift);
        keyboardView.setKeyboard(keyboard);
        keyboardView.setOnKeyboardActionListener(this);
        keyboardView.setPreviewEnabled(false);

        lnViewEtop = (LinearLayout) linearLayout.findViewById(R.id.ln_view_etop);

        this._inputConnection = getCurrentInputConnection();

        initKeyboardQWER();
//        initEmojiView();
        return linearLayout;
    }

    @Override
    public void onWindowShown() {
        super.onWindowShown();
    }

    @Override
    public void onStartInputView(EditorInfo info, boolean restarting) {
        super.onStartInputView(info, restarting);
        if(!restarting){
            initEmojiView();
        }
    }

    private void initKeyboardQWER(){
        lnKeyboardText = (LinearLayout) linearLayout.findViewById(R.id.ln_keyboard_text);
        tvShowEmoji = (TextView) linearLayout.findViewById(R.id.tv_show_emoji);
        tvShowEmoji.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lnEmojiView.setVisibility(View.VISIBLE);
                lnKeyboardText.setVisibility(View.INVISIBLE);
            }
        });
    }

    private void initEmojiView(){
        emojiCollectionAdapter = new EmojiCollectionAdapter(getCurrentInputConnection());
        viewPager = linearLayout.findViewById(R.id.pager);
        viewPager.setAdapter(emojiCollectionAdapter);

        lnEmojiView = linearLayout.findViewById(R.id.ln_emoji);
        lnDeleteEmojiView = linearLayout.findViewById(R.id.ln_delete_emoji);
        tvHideEmojiView = linearLayout.findViewById(R.id.tv_hide_emoji);

        tvHideEmojiView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lnEmojiView.setVisibility(View.INVISIBLE);
                lnKeyboardText.setVisibility(View.VISIBLE);
            }
        });

        tvHideIconEmojiView = (TextView) linearLayout.findViewById(R.id.tv_hide_emoji_view);
        tvHideIconEmojiView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lnEmojiView.setVisibility(View.INVISIBLE);
                lnKeyboardText.setVisibility(View.VISIBLE);
            }
        });

        lnDeleteEmojiView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getCurrentInputConnection().deleteSurroundingText(1,0);
            }
        });
        tvEmojiSmileList = (TextView) linearLayout.findViewById(R.id.tv_emoji_smile_list);
        tvEmojiSmileList.setOnClickListener(this);
        tvEmojiAnimalList = (TextView) linearLayout.findViewById(R.id.tv_emoji_animal_list);
        tvEmojiAnimalList.setOnClickListener(this);
        tvEmojiFoodList = (TextView) linearLayout.findViewById(R.id.tv_emoji_food_list);
        tvEmojiFoodList.setOnClickListener(this);
        tvEmojiTravelList = (TextView) linearLayout.findViewById(R.id.tv_emoji_travel_list);
        tvEmojiTravelList.setOnClickListener(this);
        tvEmojiActivityList = (TextView) linearLayout.findViewById(R.id.tv_emoji_activity_list);
        tvEmojiActivityList.setOnClickListener(this);
        tvEmojiObjectList = (TextView) linearLayout.findViewById(R.id.tv_emoji_object_list);
        tvEmojiObjectList.setOnClickListener(this);
        tvEmojiSymbolList = (TextView) linearLayout.findViewById(R.id.tv_emoji_symbol_list);
        tvEmojiSymbolList.setOnClickListener(this);
        tvEmojiFlagList = (TextView) linearLayout.findViewById(R.id.tv_emoji_flag_list);
        tvEmojiFlagList.setOnClickListener(this);

    }

    @Override
    public void onKey(int primaryCode, int[] keyCodes) {
        _inputConnection = getCurrentInputConnection();

        playClick(primaryCode);
        switch (primaryCode){

            case Keyboard.KEYCODE_DELETE:
                _inputConnection.deleteSurroundingText(1,0);
                if(arraySaveTextInput.size() > 1){
                    textBefore = String.valueOf(_inputConnection.getTextBeforeCursor(99999,0));
                    ArrayList<String> arrayListTextBefore = new ArrayList<>(Arrays.asList(textBefore.split("")));
                    _inputConnection.deleteSurroundingText(arrayListTextBefore.size(),0);
                    _inputConnection.commitText(arraySaveTextInput.get(arraySaveTextInput.size()-1),1);
                    arraySaveTextInput.remove(arraySaveTextInput.size()-1);
                }
                break;

            case Keyboard.KEYCODE_SHIFT:
//                if(countClickShift == 1){
//                    countClickShift++;
//                }else if(countClickShift == 0 || countClickShift == 2){
//                    isCaps = !isCaps;
//                    keyboard.setShifted(isCaps);
//                    keyboardView.invalidateAllKeys();
//                    if(countClickShift == 2){
//                        countClickShift = 0;
//                    }else {
//                        countClickShift++;
//                    }
//                }
//                linearLayout.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 1500));
//                lnViewEtop.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,100));
                break;

            case Keyboard.KEYCODE_DONE:
                _inputConnection.sendKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN,KeyEvent.KEYCODE_ENTER));
                break;
            case -9:
                if(numberChangeKeyboard%2==0){
                    keyboardView.setKeyboard(keyboardSymbol);
                }else {
                    keyboardView.setKeyboard(keyboard);
                }
                numberChangeKeyboard++;
                break;
            case -11:
                if(numberSymbolKeyboard%2==0){
                    keyboardView.setKeyboard(keyboardSymbolShift);
                }else {
                    keyboardView.setKeyboard(keyboardSymbol);
                }
                numberSymbolKeyboard++;
                break;
            default:
                char code = (char) primaryCode;
                if(Character.isLetter(code) && isCaps){
                    code = Character.toUpperCase(code);
                    if(countClickShift<2){
                        isCaps = false;
                        countClickShift = 0;
                        keyboard.setShifted(isCaps);
                        keyboardView.invalidateAllKeys();
                    }
                }

                String textInput = String.valueOf(code);
                textBefore = String.valueOf(_inputConnection.getTextBeforeCursor(99999,0));

                arraySaveTextInput.add(textBefore);
                ArrayList<String> arrayListTextBefore = new ArrayList<>(Arrays.asList(textBefore.split("")));
                if(arrayListTextBefore.size() > 0 && arrayListTextBefore.get(arrayListTextBefore.size()-1).equals(" ")){
                    _inputConnection.commitText(textInput, 1);
                    arrayListTextBefore.clear();
                    return;
                }
                ArrayList<String> arraySplitString = new ArrayList<>(Arrays.asList(textBefore.split(" ")));
                if(arraySplitString.size() > 0){
                    textInput = ReturnValueAfterConvert.unikeyCustomsAddText(arraySplitString.get(arraySplitString.size()-1),textInput,_inputConnection);
                }
                _inputConnection.commitText(textInput,1);
        }

    }

    private void playClick(int i){

//        AudioManager audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
//        switch(i){
//            case 32:
//                audioManager.playSoundEffect(AudioManager.FX_KEYPRESS_SPACEBAR);
//                break;
//
//            case Keyboard.KEYCODE_DONE:
//            case 10:
//                audioManager.playSoundEffect(AudioManager.FX_KEYPRESS_RETURN);
//                break;
//
//            case Keyboard.KEYCODE_DELETE:
//                audioManager.playSoundEffect(AudioManager.FX_KEYPRESS_DELETE);
//                break;
//
//            default:
//                audioManager.playSoundEffect(AudioManager.FX_KEYPRESS_STANDARD);
//        }

    }

    @Override
    public void onText(CharSequence text) {

    }

    @Override
    public void swipeLeft() {

    }

    @Override
    public void swipeRight() {

    }

    @Override
    public void swipeDown() {

    }

    @Override
    public void swipeUp() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_emoji_smile_list:
                viewPager.setCurrentItem(0);
                break;
            case R.id.tv_emoji_animal_list:
                viewPager.setCurrentItem(1);
                break;
            case R.id.tv_emoji_food_list:
                viewPager.setCurrentItem(2);
                break;
            case R.id.tv_emoji_travel_list:
                viewPager.setCurrentItem(3);
                break;
            case R.id.tv_emoji_activity_list:
                viewPager.setCurrentItem(4);
                break;
            case R.id.tv_emoji_object_list:
                viewPager.setCurrentItem(5);
                break;
            case R.id.tv_emoji_symbol_list:
                viewPager.setCurrentItem(6);
                break;
            case R.id.tv_emoji_flag_list:
                viewPager.setCurrentItem(7);
                break;
            default:
                break;
        }
    }
}
