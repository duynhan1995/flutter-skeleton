package vn.etelecom.appcall

import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import vn.etelecom.appcall.utils.ProcessHandler

@RequiresApi(Build.VERSION_CODES.O)
class FirebaseService : FirebaseMessagingService() {

  companion object {
    const val ACTION_REFRESH_PUSH_TOKEN = "REFRESH_PUSH_TOKEN"
    const val ACTION_PROCESS_PUSH_PBX = "PROCESS_PUSH_PBX"
    const val PUSH_TOKEN = "PUSH_TOKEN"
  }

  override fun onCreate() {
    super.onCreate()
    Log.d("FIREBASE SERVICE", "-----------------------------------------------")
  }

  override fun onMessageReceived(remoteMessage: RemoteMessage) {
    val data: Map<String, String> = remoteMessage.data
    Log.d("E-TELECOM MESSAGE DATA", data.toString())

    val sharedPref = getSharedPreferences("CallManager", MODE_PRIVATE)

    val sendTo = data["send_to"] ?: ""
    val callee = sendTo.split("sip:")[1].split("@")[0]

    if (!ProcessHandler.isAppActive(
          getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager,
          packageName)
      && callee == sharedPref.getString(getString(R.string.call_manager_ps_extension), "")
    ) {

      CallManager.isVideoCall = data["msg_type"] == "video"

      val sendFrom = data["send_from"] ?: ""
      CallManager.callerParsed = sendFrom.split("sip:")[1].split("@")[0]

      Intent(this, PortsipService::class.java).also { intent ->
        intent.action = ACTION_PROCESS_PUSH_PBX
        startService(intent)
      }

    }
  }

  override fun onNewToken(newToken: String) {
    Log.d("ON NEW TOKEN", newToken)
    Intent(this, PortsipService::class.java).also { intent ->
      intent.action = ACTION_REFRESH_PUSH_TOKEN
      intent.putExtra(PUSH_TOKEN, newToken)
      startService(intent)
    }
  }

}
