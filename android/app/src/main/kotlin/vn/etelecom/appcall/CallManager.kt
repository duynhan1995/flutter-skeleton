package vn.etelecom.appcall

class CallManager {
  companion object {
    var frontCamera: Boolean = true
    var isHeld: Boolean = false
    var camIsOn: Boolean = true
    var micIsOn: Boolean = true
    var speakerOn: Boolean = false

    var sessionID: Long = 0
    var callIsHappening: Boolean = false
    var isVideoCall: Boolean = false
    var isIncomingCall: Boolean = false

    var portsipRegistered: Boolean = false
    var callerParsed: String = ""

    fun resetStates() {
      frontCamera = true
      isHeld = false
      camIsOn = true
      micIsOn = true
      speakerOn = false
      callIsHappening = false
      sessionID = 0
      isVideoCall = false
      isIncomingCall = false
      callerParsed = ""
    }
  }
}
