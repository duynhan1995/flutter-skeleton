package vn.etelecom.appcall.utils

import android.app.ActivityManager
import java.util.HashSet

class ProcessHandler {
  companion object {
    
    private fun getActivePackages(activityManager: ActivityManager): Array<String> {
      val activePackages: MutableSet<String> = HashSet()
      val processInfos = activityManager.runningAppProcesses
      for (processInfo in processInfos) {
        if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
          activePackages.addAll(listOf(*processInfo.pkgList))
        }
      }
      return activePackages.toTypedArray()
    }
    
    fun isAppActive(activityManager: ActivityManager, packageName: String): Boolean {
      val activities: Array<String> = getActivePackages(activityManager)
  
      if (activities.isNotEmpty()) {
        for (activity in activities) {
          if (activity.contains(packageName)) {
            return true
          }
        }
        return false
      }
      return false
    }
    
  }
}
