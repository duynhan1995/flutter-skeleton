package vn.etelecom.appcall.keyboard.view;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputConnection;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import vn.etelecom.appcall.R;

public class EmojiAdapter extends RecyclerView.Adapter<EmojiHolder> {
    private static int unicodeSmile = 0x1F600;
    private static InputConnection _inputConnection;

    private static String[] smileList = new String[]{"😀","😁","😂","😃","😄","😅","😆","😉","😊","😋","😎","🥰","😍","😘","😗","😙","😚","☺️","🙂","🤗","😇","🤔","😐","😑","😶","🙄","😏","😣","😥","😮","🤐","😯","😪","😫","😴","😌","🤓","😛","😜","😝","🙁","😒","😓","😔","😕","😖","🙃","😷","🤒","🤕","🤑","😲","😞","😟","😤","😢","😭","😦","😧","😨","😩","😬","😰","😱","😳","😵","😡","😠","😈","👿","👹","👺","💀","👻","👽","👾","🤖","💩","😺","😸","😹","😻","😼","😽","🙀","😿","😾","🙈","🙉","🙊","👦","👧","👨","👩","👴","👵","👶","👱","👮","👲","👳","👷","👸","💂","🕵","🎅","👼","👯","💆","💇","👰","🙍","🙎","🙅","🙆","💁","🙋","🙇","🙌","🙏","🗣","👤","👥","🚶","🏃","💃","🕴","👫","👬","👭","💏","👨‍❤️‍💋‍👨","👩‍❤️‍💋‍👩","💑","👨‍❤️‍👨","👩‍❤️‍👩","👪","👨‍👨‍👦","👨‍👨‍👦‍👦","👨‍👨‍👧","👨‍👨‍👧‍👦","👨‍👨‍👧‍👧","👨‍👩‍👦","👨‍👩‍👦‍👦","👨‍👩‍👧","👨‍👩‍👧‍👦","👨‍👩‍👧‍👧","👩‍👩‍👦","👩‍👩‍👦‍👦","👩‍👩‍👧","👩‍👩‍👧‍👦","👩‍👩‍👧‍👧","💪","👈","👉","☝️","👆","🖕","👇","✌️","🖖","🤘","🖐","✊","✋","👊","👌","👍","👎","👋","👏","👐","💅","👂","👃","👣","👀","👁","👅","👄","💋","💘","❤️","💓","💔","💕","💖","💗","💙","💚","💛","💜","💝","💞","💟","💌","💤","💢","💣","💥","💦","💨","💫","💬","🗨","🗯","💭","👁‍🗨","🕳","👓","🕶","👔","👕","👖","👗","👘","👙","👚","👛","👜","👝","🛍","🎒","👞","👟","👠","👡","👢","👑","👒","🎩","🎓","📿","💄","💍","💎"};

    public EmojiAdapter(InputConnection inputConnection){
        this._inputConnection = inputConnection;
    }

    @NonNull
    @Override
    public EmojiHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_emoji, parent, false);

        return new EmojiHolder(v, this._inputConnection);
    }

    @Override
    public void onBindViewHolder(@NonNull EmojiHolder holder, int position) {
        if(position < smileList.length){
            holder.getTextView().setText(smileList[position]);
        }
    }

    @Override
    public int getItemCount() {
        return smileList.length;
    }
}
