package vn.etelecom.appcall.keyboard.convert;

import android.view.inputmethod.InputConnection;

public class ConvertOneLastCharaceterEE {
    // e, é, è, ẻ, ẹ, ẽ, ê, ế, ề. ể, ễ, ệ
    static String convert(String characterLastest, String textInput, InputConnection inputConnection){
        String valueText = textInput;
        switch (characterLastest){
            case "e":
                switch (textInput) {
                    case "s":
                        valueText = "é";
                        break;
                    case "f":
                        valueText = "è";
                        break;
                    case "r":
                        valueText = "ẻ";
                        break;
                    case "x":
                        valueText = "ẽ";
                        break;
                    case "j":
                        valueText = "ẹ";
                        break;
                    case "e":
                        valueText = "ê";
                        break;
                    case "S":
                        valueText = "é";
                        break;
                    case "F":
                        valueText = "è";
                        break;
                    case "R":
                        valueText = "ẻ";
                        break;
                    case "X":
                        valueText = "ẽ";
                        break;
                    case "J":
                        valueText = "ẹ";
                        break;
                    case "E":
                        valueText = "ê";
                        break;
                    default:
                        break;
                }
                break;
            case "é":
                switch (textInput) {
                    case "s":
                        valueText = "es";
                        break;
                    case "f":
                        valueText = "è";
                        break;
                    case "r":
                        valueText = "ẻ";
                        break;
                    case "x":
                        valueText = "ẽ";
                        break;
                    case "j":
                        valueText = "ẹ";
                        break;
                    case "e":
                        valueText = "ế";
                        break;
                    case "S":
                        valueText = "eS";
                        break;
                    case "F":
                        valueText = "è";
                        break;
                    case "R":
                        valueText = "ẻ";
                        break;
                    case "X":
                        valueText = "ẽ";
                        break;
                    case "J":
                        valueText = "ẹ";
                        break;
                    case "E":
                        valueText = "ế";
                        break;
                    default:
                        break;
                }
                break;
            case "è":
                switch (textInput) {
                    case "s":
                        valueText = "é";
                        break;
                    case "f":
                        valueText = "ef";
                        break;
                    case "r":
                        valueText = "ẻ";
                        break;
                    case "x":
                        valueText = "ẽ";
                        break;
                    case "j":
                        valueText = "ẹ";
                        break;
                    case "e":
                        valueText = "ề";
                        break;
                    case "S":
                        valueText = "é";
                        break;
                    case "F":
                        valueText = "eF";
                        break;
                    case "R":
                        valueText = "ẻ";
                        break;
                    case "X":
                        valueText = "ẽ";
                        break;
                    case "J":
                        valueText = "ẹ";
                        break;
                    case "E":
                        valueText = "ề";
                        break;
                    default:
                        break;
                }
                break;
            case "ẻ":
                switch (textInput) {
                    case "s":
                        valueText = "é";
                        break;
                    case "f":
                        valueText = "è";
                        break;
                    case "r":
                        valueText = "er";
                        break;
                    case "x":
                        valueText = "ẽ";
                        break;
                    case "j":
                        valueText = "ẹ";
                        break;
                    case "e":
                        valueText = "ể";
                        break;
                    case "S":
                        valueText = "é";
                        break;
                    case "F":
                        valueText = "è";
                        break;
                    case "R":
                        valueText = "er";
                        break;
                    case "X":
                        valueText = "ẽ";
                        break;
                    case "J":
                        valueText = "ẹ";
                        break;
                    case "E":
                        valueText = "ể";
                        break;
                    default:
                        break;
                }
                break;
            case "ẽ":
                switch (textInput) {
                    case "s":
                        valueText = "é";
                        break;
                    case "f":
                        valueText = "è";
                        break;
                    case "r":
                        valueText = "ẻ";
                        break;
                    case "x":
                        valueText = "ex";
                        break;
                    case "j":
                        valueText = "ẹ";
                        break;
                    case "e":
                        valueText = "ễ";
                        break;
                    case "S":
                        valueText = "é";
                        break;
                    case "F":
                        valueText = "è";
                        break;
                    case "R":
                        valueText = "ẻ";
                        break;
                    case "X":
                        valueText = "eX";
                        break;
                    case "J":
                        valueText = "ẹ";
                        break;
                    case "E":
                        valueText = "ễ";
                        break;
                    default:
                        break;
                }
                break;
            case "ẹ":
                switch (textInput) {
                    case "s":
                        valueText = "é";
                        break;
                    case "f":
                        valueText = "è";
                        break;
                    case "r":
                        valueText = "ẻ";
                        break;
                    case "x":
                        valueText = "ẽ";
                        break;
                    case "j":
                        valueText = "ej";
                        break;
                    case "e":
                        valueText = "ệ";
                        break;
                    case "S":
                        valueText = "é";
                        break;
                    case "F":
                        valueText = "è";
                        break;
                    case "R":
                        valueText = "ẻ";
                        break;
                    case "X":
                        valueText = "ẽ";
                        break;
                    case "J":
                        valueText = "eJ";
                        break;
                    case "E":
                        valueText = "ệ";
                        break;
                    default:
                        break;
                }
                break;
            case "ê":
                switch (textInput) {
                    case "s":
                        valueText = "ế";
                        break;
                    case "f":
                        valueText = "ề";
                        break;
                    case "r":
                        valueText = "ể";
                        break;
                    case "x":
                        valueText = "ễ";
                        break;
                    case "j":
                        valueText = "ệ";
                        break;
                    case "e":
                        valueText = "ee";
                        break;
                    case "S":
                        valueText = "ế";
                        break;
                    case "F":
                        valueText = "ề";
                        break;
                    case "R":
                        valueText = "ể";
                        break;
                    case "X":
                        valueText = "ễ";
                        break;
                    case "J":
                        valueText = "ệ";
                        break;
                    case "E":
                        valueText = "eE";
                        break;
                    default:
                        break;
                }
                break;
            case "ế":
                switch (textInput) {
                    case "s":
                        valueText = "ês";
                        break;
                    case "f":
                        valueText = "ề";
                        break;
                    case "r":
                        valueText = "ể";
                        break;
                    case "x":
                        valueText = "ễ";
                        break;
                    case "j":
                        valueText = "ệ";
                        break;
                    case "e":
                        valueText = "ée";
                        break;
                    case "S":
                        valueText = "êS";
                        break;
                    case "F":
                        valueText = "ề";
                        break;
                    case "R":
                        valueText = "ể";
                        break;
                    case "X":
                        valueText = "ễ";
                        break;
                    case "J":
                        valueText = "ệ";
                        break;
                    case "E":
                        valueText = "ée";
                        break;
                    default:
                        break;
                }
                break;
            case "ề":
                switch (textInput) {
                    case "s":
                        valueText = "ế";
                        break;
                    case "f":
                        valueText = "êf";
                        break;
                    case "r":
                        valueText = "ể";
                        break;
                    case "x":
                        valueText = "ễ";
                        break;
                    case "j":
                        valueText = "ệ";
                        break;
                    case "e":
                        valueText = "èe";
                        break;
                    case "S":
                        valueText = "ế";
                        break;
                    case "F":
                        valueText = "êF";
                        break;
                    case "R":
                        valueText = "ể";
                        break;
                    case "X":
                        valueText = "ễ";
                        break;
                    case "J":
                        valueText = "ệ";
                        break;
                    case "E":
                        valueText = "èE";
                        break;
                    default:
                        break;
                }
                break;
            case "ể":
                switch (textInput) {
                    case "s":
                        valueText = "ế";
                        break;
                    case "f":
                        valueText = "ề";
                        break;
                    case "r":
                        valueText = "êr";
                        break;
                    case "x":
                        valueText = "ễ";
                        break;
                    case "j":
                        valueText = "ệ";
                        break;
                    case "e":
                        valueText = "ẻe";
                        break;
                    case "S":
                        valueText = "ế";
                        break;
                    case "F":
                        valueText = "ề";
                        break;
                    case "R":
                        valueText = "êR";
                        break;
                    case "X":
                        valueText = "ễ";
                        break;
                    case "J":
                        valueText = "ệ";
                        break;
                    case "E":
                        valueText = "ẻe";
                        break;
                    default:
                        break;
                }
                break;
            case "ễ":
                switch (textInput) {
                    case "s":
                        valueText = "ế";
                        break;
                    case "f":
                        valueText = "ề";
                        break;
                    case "r":
                        valueText = "ể";
                        break;
                    case "x":
                        valueText = "êx";
                        break;
                    case "j":
                        valueText = "ệ";
                        break;
                    case "e":
                        valueText = "ẽe";
                        break;
                    case "S":
                        valueText = "ế";
                        break;
                    case "F":
                        valueText = "ề";
                        break;
                    case "R":
                        valueText = "ể";
                        break;
                    case "X":
                        valueText = "êX";
                        break;
                    case "J":
                        valueText = "ệ";
                        break;
                    case "E":
                        valueText = "ẽE";
                        break;
                    default:
                        break;
                }
                break;
            case "ệ":
                switch (textInput) {
                    case "s":
                        valueText = "ế";
                        break;
                    case "f":
                        valueText = "ề";
                        break;
                    case "r":
                        valueText = "ể";
                        break;
                    case "x":
                        valueText = "ễ";
                        break;
                    case "j":
                        valueText = "êj";
                        break;
                    case "e":
                        valueText = "ẹe";
                        break;
                    case "S":
                        valueText = "ế";
                        break;
                    case "F":
                        valueText = "ề";
                        break;
                    case "R":
                        valueText = "ể";
                        break;
                    case "X":
                        valueText = "ễ";
                        break;
                    case "J":
                        valueText = "êJ";
                        break;
                    case "E":
                        valueText = "ẹE";
                        break;
                    default:
                        break;
                }
                break;
                        case "E":
                switch (textInput) {
                    case "s":
                        valueText = "É";
                        break;
                    case "f":
                        valueText = "È";
                        break;
                    case "r":
                        valueText = "Ẻ";
                        break;
                    case "x":
                        valueText = "Ẽ";
                        break;
                    case "j":
                        valueText = "Ẹ";
                        break;
                    case "e":
                        valueText = "Ê";
                        break;
                    case "S":
                        valueText = "É";
                        break;
                    case "F":
                        valueText = "È";
                        break;
                    case "R":
                        valueText = "Ẻ";
                        break;
                    case "X":
                        valueText = "Ẽ";
                        break;
                    case "J":
                        valueText = "Ẹ";
                        break;
                    case "E":
                        valueText = "Ê";
                        break;
                    default:
                        break;
                }
                break;
            case "É":
                switch (textInput) {
                    case "s":
                        valueText = "Es";
                        break;
                    case "f":
                        valueText = "È";
                        break;
                    case "r":
                        valueText = "Ẻ";
                        break;
                    case "x":
                        valueText = "Ẽ";
                        break;
                    case "j":
                        valueText = "Ẹ";
                        break;
                    case "e":
                        valueText = "Ế";
                        break;
                    case "S":
                        valueText = "ES";
                        break;
                    case "F":
                        valueText = "È";
                        break;
                    case "R":
                        valueText = "Ẻ";
                        break;
                    case "X":
                        valueText = "Ẽ";
                        break;
                    case "J":
                        valueText = "Ẹ";
                        break;
                    case "E":
                        valueText = "Ế";
                        break;
                    default:
                        break;
                }
                break;
            case "È":
                switch (textInput) {
                    case "s":
                        valueText = "É";
                        break;
                    case "f":
                        valueText = "Ef";
                        break;
                    case "r":
                        valueText = "Ẻ";
                        break;
                    case "x":
                        valueText = "Ẽ";
                        break;
                    case "j":
                        valueText = "Ẹ";
                        break;
                    case "e":
                        valueText = "Ề";
                        break;
                    case "S":
                        valueText = "É";
                        break;
                    case "F":
                        valueText = "EF";
                        break;
                    case "R":
                        valueText = "Ẻ";
                        break;
                    case "X":
                        valueText = "Ẽ";
                        break;
                    case "J":
                        valueText = "Ẹ";
                        break;
                    case "E":
                        valueText = "Ề";
                        break;
                    default:
                        break;
                }
                break;
            case "Ẻ":
                switch (textInput) {
                    case "s":
                        valueText = "É";
                        break;
                    case "f":
                        valueText = "È";
                        break;
                    case "r":
                        valueText = "Er";
                        break;
                    case "x":
                        valueText = "Ẽ";
                        break;
                    case "j":
                        valueText = "Ẹ";
                        break;
                    case "e":
                        valueText = "Ể";
                        break;
                    case "S":
                        valueText = "É";
                        break;
                    case "F":
                        valueText = "È";
                        break;
                    case "R":
                        valueText = "ER";
                        break;
                    case "X":
                        valueText = "Ẽ";
                        break;
                    case "J":
                        valueText = "Ẹ";
                        break;
                    case "E":
                        valueText = "Ể";
                        break;
                    default:
                        break;
                }
                break;
            case "Ẽ":
                switch (textInput) {
                    case "s":
                        valueText = "É";
                        break;
                    case "f":
                        valueText = "È";
                        break;
                    case "r":
                        valueText = "Ẻ";
                        break;
                    case "x":
                        valueText = "Ex";
                        break;
                    case "j":
                        valueText = "Ẹ";
                        break;
                    case "e":
                        valueText = "Ễ";
                        break;
                    case "S":
                        valueText = "É";
                        break;
                    case "F":
                        valueText = "È";
                        break;
                    case "R":
                        valueText = "Ẻ";
                        break;
                    case "X":
                        valueText = "EX";
                        break;
                    case "J":
                        valueText = "Ẹ";
                        break;
                    case "E":
                        valueText = "Ễ";
                        break;
                    default:
                        break;
                }
                break;
            case "Ẹ":
                switch (textInput) {
                    case "s":
                        valueText = "É";
                        break;
                    case "f":
                        valueText = "È";
                        break;
                    case "r":
                        valueText = "Ẻ";
                        break;
                    case "x":
                        valueText = "Ẽ";
                        break;
                    case "j":
                        valueText = "Ej";
                        break;
                    case "e":
                        valueText = "Ệ";
                        break;
                    case "S":
                        valueText = "É";
                        break;
                    case "F":
                        valueText = "È";
                        break;
                    case "R":
                        valueText = "Ẻ";
                        break;
                    case "X":
                        valueText = "Ẽ";
                        break;
                    case "J":
                        valueText = "EJ";
                        break;
                    case "E":
                        valueText = "Ệ";
                        break;
                    default:
                        break;
                }
                break;
            case "Ê":
                switch (textInput) {
                    case "s":
                        valueText = "Ế";
                        break;
                    case "f":
                        valueText = "Ề";
                        break;
                    case "r":
                        valueText = "Ể";
                        break;
                    case "x":
                        valueText = "Ễ";
                        break;
                    case "j":
                        valueText = "Ệ";
                        break;
                    case "e":
                        valueText = "Ee";
                        break;
                    case "S":
                        valueText = "Ế";
                        break;
                    case "F":
                        valueText = "Ề";
                        break;
                    case "R":
                        valueText = "Ể";
                        break;
                    case "X":
                        valueText = "Ễ";
                        break;
                    case "J":
                        valueText = "Ệ";
                        break;
                    case "E":
                        valueText = "EE";
                        break;
                    default:
                        break;
                }
                break;
            case "Ế":
                switch (textInput) {
                    case "s":
                        valueText = "Ês";
                        break;
                    case "f":
                        valueText = "Ề";
                        break;
                    case "r":
                        valueText = "Ể";
                        break;
                    case "x":
                        valueText = "Ễ";
                        break;
                    case "j":
                        valueText = "Ệ";
                        break;
                    case "e":
                        valueText = "Ée";
                        break;
                    case "S":
                        valueText = "ÊS";
                        break;
                    case "F":
                        valueText = "Ề";
                        break;
                    case "R":
                        valueText = "Ể";
                        break;
                    case "X":
                        valueText = "Ễ";
                        break;
                    case "J":
                        valueText = "Ệ";
                        break;
                    case "E":
                        valueText = "ÉE";
                        break;
                    default:
                        break;
                }
                break;
            case "Ề":
                switch (textInput) {
                    case "s":
                        valueText = "Ế";
                        break;
                    case "f":
                        valueText = "Êf";
                        break;
                    case "r":
                        valueText = "Ể";
                        break;
                    case "x":
                        valueText = "Ễ";
                        break;
                    case "j":
                        valueText = "Ệ";
                        break;
                    case "e":
                        valueText = "Ee";
                        break;
                    case "S":
                        valueText = "Ế";
                        break;
                    case "F":
                        valueText = "ÊF";
                        break;
                    case "R":
                        valueText = "Ể";
                        break;
                    case "X":
                        valueText = "Ễ";
                        break;
                    case "J":
                        valueText = "Ệ";
                        break;
                    case "E":
                        valueText = "ÈE";
                        break;
                    default:
                        break;
                }
                break;
            case "Ể":
                switch (textInput) {
                    case "s":
                        valueText = "Ế";
                        break;
                    case "f":
                        valueText = "Ề";
                        break;
                    case "r":
                        valueText = "ÊR";
                        break;
                    case "x":
                        valueText = "Ễ";
                        break;
                    case "j":
                        valueText = "Ệ";
                        break;
                    case "e":
                        valueText = "Ẻe";
                        break;
                    case "S":
                        valueText = "Ế";
                        break;
                    case "F":
                        valueText = "Ề";
                        break;
                    case "R":
                        valueText = "ÊR";
                        break;
                    case "X":
                        valueText = "Ễ";
                        break;
                    case "J":
                        valueText = "Ệ";
                        break;
                    case "E":
                        valueText = "ẺE";
                        break;
                    default:
                        break;
                }
                break;
            case "Ễ":
                switch (textInput) {
                    case "s":
                        valueText = "Ế";
                        break;
                    case "f":
                        valueText = "Ề";
                        break;
                    case "r":
                        valueText = "Ể";
                        break;
                    case "x":
                        valueText = "Êx";
                        break;
                    case "j":
                        valueText = "Ệ";
                        break;
                    case "e":
                        valueText = "Ẽe";
                        break;
                    case "S":
                        valueText = "Ế";
                        break;
                    case "F":
                        valueText = "Ề";
                        break;
                    case "R":
                        valueText = "Ể";
                        break;
                    case "X":
                        valueText = "ÊX";
                        break;
                    case "J":
                        valueText = "Ệ";
                        break;
                    case "E":
                        valueText = "ẼE";
                        break;
                    default:
                        break;
                }
                break;
            case "Ệ":
                switch (textInput) {
                    case "s":
                        valueText = "Ế";
                        break;
                    case "f":
                        valueText = "Ề";
                        break;
                    case "r":
                        valueText = "Ể";
                        break;
                    case "x":
                        valueText = "Ễ";
                        break;
                    case "j":
                        valueText = "Êj";
                        break;
                    case "e":
                        valueText = "Ẹe";
                        break;
                    case "S":
                        valueText = "Ế";
                        break;
                    case "F":
                        valueText = "Ề";
                        break;
                    case "R":
                        valueText = "Ể";
                        break;
                    case "X":
                        valueText = "Ễ";
                        break;
                    case "J":
                        valueText = "ÊJ";
                        break;
                    case "E":
                        valueText = "ẸE";
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }
        if(valueText != (textInput)){
            inputConnection.deleteSurroundingText(1,0);
        }
        return valueText;
    }
}
