import 'package:etelecom/widgets/bases/base-appbar.dart';
import 'package:etelecom/widgets/bases/base-menu.dart';
import 'package:etelecom/widgets/bases/base-scaffold.dart';
import 'package:flutter/material.dart';

class OrdersPage extends Page {
  OrdersPage() : super(key: ValueKey('OrdersPage'));

  @override
  Route createRoute(BuildContext context) {
    return PageRouteBuilder(
        settings: this,
        pageBuilder: (BuildContext context, animation, animation2) {
          return OrdersScreen();
        });
  }
}

class OrdersScreen extends StatefulWidget {
  OrdersScreen({Key? key, this.title}) : super(key: key);
  final String? title;

  @override
  _OrdersScreenState createState() => _OrdersScreenState();
}

class _OrdersScreenState extends State<OrdersScreen> {
  @override
  Widget build(BuildContext context) {
    return EScaffold(
        appBar: EAppBar(title: 'Đơn hàng'),
        body: Container(
          padding: EdgeInsets.all(20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                children: [
                  Expanded(
                      child:
                          Text('Đang phát triển', textAlign: TextAlign.center))
                ],
              )
            ],
          ),
        ),
        bottomMenu: EMenu(menuItems: [
          MenuItem(
              icon: Icons.phone_in_talk_outlined,
              label: 'Nghe gọi',
              route: '/call-center'),
          MenuItem(
              icon: Icons.confirmation_number_outlined,
              label: 'Ticket',
              route: '/tickets'),
          MenuItem(icon: Icons.list, label: 'Lịch sử', route: '/call-logs'),
          MenuItem(
              icon: Icons.account_circle_outlined,
              label: 'Danh bạ',
              route: '/contacts'),
          MenuItem(
              icon: Icons.settings_outlined,
              label: 'Thiết lập',
              route: '/setting'),
        ]));
  }
}
