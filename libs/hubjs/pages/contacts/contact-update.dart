import 'package:etelecom/api/shop/contact.api.dart';
import 'package:etelecom/models/contact/Contact.dart';
import 'package:etelecom/services/contact.service.dart';
import 'package:etelecom/theme/theme.dart';
import 'package:etelecom/utils/toast.dart';
import 'package:etelecom/widgets/bases/base-appbar.dart';
import 'package:flutter/material.dart';

class ContactUpdate extends StatefulWidget {
  final Contact contact;

  ContactUpdate({
    required this.contact
  });

  @override
  State<StatefulWidget> createState() => _ContactUpdateState(this.contact);
}

class _ContactUpdateState extends State<ContactUpdate> {
  _ContactUpdateState(Contact contact) {
    fullnameController.text = contact.fullName ?? "";
    phoneController.text = contact.phone ?? "";
  }


  final fullnameController = TextEditingController();
  final phoneController = TextEditingController();

  bool loading = false;

  Future<void> updateContact(BuildContext context, String contactID) async {
    setState(() {
      loading = true;
    });
    try {
      final UpdateContactRequest _req = UpdateContactRequest(
          fullName: fullnameController.text,
          phone: phoneController.text,
          id: contactID
      );
      await ContactService.updateContact(context, _req);
      Navigator.of(context).pop();

      EToast.success(context, 'Cập nhật liên hệ thành công');
    } catch (e) {
      EToast.error(context, 'Cập nhật liên hệ không thành công. ${(e as dynamic)["msg"]}');
    }
    setState(() {
      loading = false;
    });
  }

  @override
  void dispose() {
    fullnameController.dispose();
    phoneController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: EAppBar(title: 'Chỉnh sửa liên hệ'),
      body: Container(
        padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
        child: Column(children: [
          Container(
            margin: EdgeInsets.only(bottom: 20),
            child: TextFormField(
              decoration: InputDecoration(
                hintText: 'Nhập tên',
                hintStyle: TextStyle(fontWeight: FontWeight.w400),
                labelText: 'Tên',
                labelStyle: TextStyle(fontWeight: FontWeight.w400),
                contentPadding: EdgeInsets.only(bottom: 0, top: 0),
              ),
              style: TextStyle(
                color: MainTheme.gray555,
                fontWeight: FontWeight.w700
              ),
              controller: fullnameController,
            )
          ),
          Container(
            margin: EdgeInsets.only(bottom: 20),
            child: TextFormField(
              keyboardType: TextInputType.phone,
              decoration: InputDecoration(
                hintText: 'Nhập số điện thoại',
                hintStyle: TextStyle(fontWeight: FontWeight.w400),
                labelText: 'Số điện thoại',
                labelStyle: TextStyle(fontWeight: FontWeight.w400),
                contentPadding: EdgeInsets.only(bottom: 0, top: 0),
              ),
              style: TextStyle(
                color: MainTheme.gray555,
                fontWeight: FontWeight.w700
              ),
              controller: phoneController,
            )
          ),
          Container(
            child: Row(
              children: [
                Expanded(
                  child: MaterialButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(7),
                    ),
                    onPressed: loading ? null : () {
                      updateContact(context, widget.contact.id!);
                    },
                    child: Text('Cập nhật',
                      style: TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.w400
                      )
                    ),
                    textColor: Colors.white,
                    color: MainTheme.primaryColor,
                    height: 45
                  )
                )
              ],
            )
          )]
        )
      ),
    );
  }
}
