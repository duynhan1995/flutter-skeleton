import 'package:etelecom/models/contact/Contact.dart';
import 'package:etelecom/services/contact.service.dart';
import 'package:etelecom/theme/theme.dart';
import 'package:etelecom/utils/toast.dart';
import 'package:flutter/material.dart';

class ContactDelete extends StatefulWidget {
  final Contact contact;

  ContactDelete({required this.contact});

  @override
  State<StatefulWidget> createState() => _ContactDeleteState();
}

class _ContactDeleteState extends State<ContactDelete> {

  bool loading = false;

  Future<void> deleteContact(BuildContext context, String id) async {
    setState(() {
      loading = true;
    });
    try {
      await ContactService.deleteContact(context, id);
      Navigator.of(context).pop();
      EToast.success(context, 'Xoá liên hệ thành công');
    } catch (e) {
      EToast.error(context, 'Xoá liên hệ không thành công. ${(e as dynamic)["msg"]}');
    }
    setState(() {
      loading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(
          'Xoá liên hệ',
          style: TextStyle(
              fontWeight: FontWeight.w700,
              fontSize: 20
          )
      ),
      content: Text(
        'Bạn có chắc muốn xoá ${widget.contact.fullName} ra khỏi danh bạ?',
        style: TextStyle(
            fontSize: 15
        ),
      ),
      actions: [
        TextButton(
          child: Text(
            'Đóng',
            style: TextStyle(
                color: MainTheme.gray999,
                fontSize: 17
            ),
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        TextButton(
          child: Text(
              'Xoá',
              style: TextStyle(
                  color: Colors.red,
                  fontSize: 17
              )
          ),
          onPressed: loading ? null : () {
            deleteContact(context, widget.contact.id!);
          },
        ),
      ],
    );
  }
}
