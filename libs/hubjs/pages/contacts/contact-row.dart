import 'package:etelecom/models/contact/Contact.dart';
import 'package:etelecom/services/portsip.service.dart';
import 'package:etelecom/theme/theme.dart';
import 'package:etelecom/utils/toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

import 'contact-delete.dart';
import 'contact-update.dart';

final List<Map<String, dynamic>> popupMenuItems = [
  {"title": "Chỉnh sửa", "value": "update", "color": MainTheme.gray555},
  {"title": "Xoá", "value": "delete", "color": Colors.red}
];

Future<void> callOut(BuildContext context, String phone) async {
  try {
    await PortsipService.portsipCallOut(phone, context: context);
  } catch (e) {
    if ((e as dynamic)["code"] == "not_enough_balance") {
      EToast.error(context, "Không đủ số dư để thực hiện cuộc gọi");
    }
  }
}

class ContactRow extends StatelessWidget {
  final Contact contact;

  ContactRow({required this.contact, Key? key});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 10),
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border.symmetric(
              vertical: BorderSide.none,
              horizontal: BorderSide(color: MainTheme.grayF5))),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(child: ContactInfo(contact: contact)),
          ContactManager(contact: contact)
        ],
      ),
    );
  }
}

class ContactInfo extends StatelessWidget {
  final Contact contact;

  ContactInfo({required this.contact});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          child: Icon(
            Icons.account_circle_outlined,
            color: MainTheme.grayCCC,
            size: 50,
          ),
        ),
        Expanded(
            child: Container(
          padding: EdgeInsets.only(left: 10, right: 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                margin: EdgeInsets.only(bottom: 2.5),
                child: Text(
                  contact.fullName ?? '-',
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      color: MainTheme.gray555,
                      fontWeight: FontWeight.w800,
                      fontSize: 15),
                ),
              ),
              Text(
                contact.phone ?? '-',
                style: TextStyle(
                  fontSize: 12,
                  color: MainTheme.gray555,
                ),
              )
            ],
          ),
        ))
      ],
    );
  }
}

class ContactManager extends StatelessWidget {

  final Contact contact;

  ContactManager({required this.contact});

  void alertDeleteContact(BuildContext context) {
    showDialog(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return ContactDelete(contact: contact);
      },
    );
  }

  void bottomSheetUpdateContact(BuildContext context) {
    CupertinoScaffold.showCupertinoModalBottomSheet(
        context: context,
        duration: Duration(milliseconds: 400),
        builder: (BuildContext context) {
          return ContactUpdate(contact: this.contact);
        });
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
            child: PopupMenuButton(
          padding: EdgeInsets.zero,
          itemBuilder: (BuildContext context) {
            return popupMenuItems
                .map((item) => PopupMenuItem(
                    height: 35,
                    value: item["value"],
                    child: Text(item["title"],
                        style: TextStyle(color: item["color"], fontSize: 13))))
                .toList();
          },
          onSelected: (value) {
            switch (value) {
              case "update":
                return bottomSheetUpdateContact(context);
              case "delete":
                return alertDeleteContact(context);
            }
          },
          child: Container(
            height: 35,
            padding: EdgeInsets.symmetric(vertical: 2.5, horizontal: 10),
            decoration: BoxDecoration(
                border: Border.all(color: MainTheme.grayF5),
                borderRadius: BorderRadius.circular(20)),
            child: Row(
              children: [
                Icon(Icons.arrow_drop_down, size: 20),
                Container(
                  child: Text('Thao tác', style: TextStyle(fontSize: 13)),
                )
              ],
            ),
          ),
        )),
        Container(
          width: 35,
          height: 35,
          decoration: BoxDecoration(
              color: MainTheme.primaryColor,
              borderRadius: BorderRadius.circular(100)),
          margin: EdgeInsets.only(left: 7.5),
          child: IconButton(
              icon: Icon(
                Icons.call_outlined,
                color: Colors.white,
                size: 20,
              ),
              onPressed: () => callOut(context, contact.phone!)),
        )
      ],
    );
  }
}
