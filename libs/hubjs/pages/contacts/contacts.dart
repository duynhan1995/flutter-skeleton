import 'package:etelecom/models/contact/Contact.dart';
import 'package:etelecom/providers/contact/contact.provider.dart';
import 'package:etelecom/services/contact.service.dart';
import 'package:etelecom/widgets/bases/base-appbar.dart';
import 'package:etelecom/widgets/bases/base-scaffold.dart';
import 'package:etelecom/widgets/list-view/list-view.dart';
import 'package:etelecom/widgets/bases/base-menu.dart';
import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:provider/provider.dart';

import 'contact-create.dart';
import 'contact-row.dart';

class ContactsPage extends Page {
  ContactsPage() : super(key: ValueKey('ContactsPage'));

  @override
  Route createRoute(BuildContext context) {
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      context.read<ContactProvider>().setPaging(after: ".");
      ContactService.getContacts(context: context);
    });

    return PageRouteBuilder(
        settings: this,
        pageBuilder: (BuildContext context, animation, animation2) {
          return ContactsScreen();
        });
  }
}

class ContactsScreen extends StatefulWidget {
  ContactsScreen({Key? key, this.title}) : super(key: key);
  final String? title;

  @override
  _ContactsScreenState createState() => _ContactsScreenState();
}

class _ContactsScreenState extends State<ContactsScreen> {
  void bottomSheetCreateContact(BuildContext context) {
    CupertinoScaffold.showCupertinoModalBottomSheet(
        context: context,
        duration: Duration(milliseconds: 400),
        builder: (BuildContext context) {
          return ContactCreate(contact: Contact(id: null));
        });
  }

  @override
  Widget build(BuildContext context) {
    return EScaffold(
        appBar: EAppBar(
          title: 'Danh bạ',
          rightActionButtons: [EAppBarActionButton(title: '+ Thêm', onPressed: (context) => bottomSheetCreateContact(context))],
        ),
        body: EListView(
          isLoading: context.watch<ContactProvider>().loadingContacts,
          isEmpty: context.watch<ContactProvider>().contacts.length == 0,
          currentNextPointer: context.watch<ContactProvider>().currentPaging.after,
          listLength: context.watch<ContactProvider>().contacts.length,
          itemChildBuilder: (BuildContext context, int index) {
            final Contact contactRow = context.watch<ContactProvider>().contacts[index];
            return ContactRow(contact: contactRow, key: ObjectKey(contactRow.id));
          },
          onLoadMore: () async {
            // Wrapped in a WidgetsBinding.instance!.addPostFrameCallback BECAUSE
            // the context of `build` function cannot be accessed when all items in ListView
            // are not completety rendered.
            WidgetsBinding.instance!.addPostFrameCallback((_) async {
              context.read<ContactProvider>().setLoading(true);

              Future.delayed(Duration(seconds: 1), () async {
                await ContactService.getContacts(context: context);
              });
            });
          },
          onRefresh: () async {
            context.read<ContactProvider>().setPaging(after: ".");
            await ContactService.getContacts(context: context);
          },
        ),
        bottomMenu: EMenu(
            menuItems: [
                MenuItem(icon: Icons.phone_in_talk_outlined, label: 'Nghe gọi', route: '/call-center'),
                MenuItem(icon: Icons.list, label: 'Lịch sử', route: '/call-logs'),
                MenuItem(icon: Icons.account_circle_outlined, label: 'Danh bạ', route: '/contacts'),
                // MenuItem(icon: Icons.shopping_cart_outlined, label: 'Đơn hàng', route: '/orders'),
                MenuItem(icon: Icons.settings_outlined, label: 'Thiết lập', route: '/setting'),
            ]
        )
    );
  }
}
