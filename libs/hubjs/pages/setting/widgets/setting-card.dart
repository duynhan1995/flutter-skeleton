import 'package:etelecom/theme/theme.dart';
import 'package:flutter/material.dart';

class SettingCard extends StatelessWidget {
  final Widget child;
  final EdgeInsetsGeometry padding;

  SettingCard({
    required this.child,
    this.padding = EdgeInsets.zero
  });

  @override
  Widget build(BuildContext context) {

    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border(
          bottom: BorderSide(
            color: MainTheme.grayF5
          )
        )
      ),
      padding: padding,
      child: child,
    );
  }

}
