import 'package:etelecom/theme/theme.dart';
import 'package:flutter/material.dart';

import 'shops-list-item.dart';

class ShopsListWrapper extends StatelessWidget {
  final String label;
  final List<ShopsListItem> shopListItems;

  ShopsListWrapper({
    required this.label,
    required this.shopListItems
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 10),
      decoration: BoxDecoration(
        color: Colors.white,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            child: Text(
              label,
              style: TextStyle(fontWeight: FontWeight.w400)
            ),
            padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(
                  color: MainTheme.grayF5
                )
              )
            )
          ),
          Container(child: Column(children: shopListItems))
        ]
      )
    );

  }

}
