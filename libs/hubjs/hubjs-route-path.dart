class HubjsRoutePath {
  final String path;

  HubjsRoutePath.login() : path = "/";
  HubjsRoutePath.callCenter() : path = "/call-center";
  HubjsRoutePath.callLogs() : path = "/call-logs";
  HubjsRoutePath.contacts() : path = "/contacts";
  HubjsRoutePath.orders() : path = "/orders";
  HubjsRoutePath.setting() : path = "/setting";
  HubjsRoutePath.settingShopsList() : path = "/setting/shops";

  bool get isLoginPage => path == "/";
  bool get isCallCenterPage => path == "/call-center";
  bool get isCallLogsPage => path == "/call-logs";
  bool get isContactsPage => path == "/contacts";
  bool get isOrdersPage => path == "/orders";
  bool get isSettingPage => path == "/setting";
  bool get isSettingShopsListPage => path == "/setting/shops";
}
