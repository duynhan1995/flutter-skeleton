import 'package:etelecom/app_config.dart';

import 'hubjs_app.dart' as App;

void main() {
  // set config to prod
  App.main(env: AppEnv.prod);
}
