import 'package:flutter/widgets.dart';

import 'hubjs-route-path.dart';

class HubjsRouteInformationParser extends RouteInformationParser<HubjsRoutePath> {
  @override
  Future<HubjsRoutePath> parseRouteInformation(
      RouteInformation routeInformation) async {
    final uri = Uri.parse(routeInformation.location!);

    // Handle "/"

    if (uri.pathSegments.length == 0) {
      return HubjsRoutePath.login();
    }

    if (uri.pathSegments.length == 1) {
      switch (uri.pathSegments[0]) {
        // Handle "/call-center"
        case "call-center":
          return HubjsRoutePath.callCenter();
        // Handle "/call-logs"
        case "call-logs":
          return HubjsRoutePath.callLogs();
        // Handle "/contacts"
        case "contacts":
          return HubjsRoutePath.contacts();
        // Handle "/orders"
        case "orders":
          return HubjsRoutePath.orders();
        // Handle "/settings"
        case "setting":
          return HubjsRoutePath.setting();
        default:
          return HubjsRoutePath.login();
      }
    }

    if (uri.pathSegments.length == 2) {
      if (uri.pathSegments[0] == "setting") {
        switch (uri.pathSegments[1]) {
          case "shops":
            return HubjsRoutePath.settingShopsList();
          default:
            return HubjsRoutePath.login();
        }
      }
    }

    return HubjsRoutePath.login();
  }

  @override
  RouteInformation? restoreRouteInformation(HubjsRoutePath path) {
    if (path.isLoginPage) {
      return RouteInformation(location: "/");
    }
    if (path.isCallCenterPage) {
      return RouteInformation(location: "/call-center");
    }
    if (path.isCallLogsPage) {
      return RouteInformation(location: "/call-logs");
    }
    if (path.isContactsPage) {
      return RouteInformation(location: "/contacts");
    }
    if (path.isOrdersPage) {
      return RouteInformation(location: "/orders");
    }
    if (path.isSettingPage) {
      return RouteInformation(location: "/setting");
    }
    if (path.isSettingShopsListPage) {
      return RouteInformation(location: "/setting/shops");
    }

    return null;
  }
}
