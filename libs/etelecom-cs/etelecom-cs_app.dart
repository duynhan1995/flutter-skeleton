import 'dart:async';
import 'dart:isolate';

import 'package:etelecom/app_config.dart';
import 'package:etelecom/providers/authenticate/authenticate.provider.dart';
import 'package:etelecom/providers/call-log/call-log.provider.dart';
import 'package:etelecom/providers/call-manager/calls.provider.dart';
import 'package:etelecom/providers/contact/contact.provider.dart';
import 'package:etelecom/providers/extension/extension.provider.dart';
import 'package:etelecom/providers/navigate/navigate.provider.dart';
import 'package:etelecom/providers/network/network.provider.dart';
import 'package:etelecom/providers/permission/permission.provider.dart';
import 'package:etelecom/providers/account-user/account-user.provider.dart';
import 'package:etelecom/providers/shop/shop.provider.dart';
import 'package:etelecom/providers/ticket/ticket.provider.dart';
import 'package:etelecom/theme/theme.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:provider/provider.dart';
import 'package:google_fonts/google_fonts.dart';

import 'etelecom-cs-route-information-parser.dart';
import 'etelecom-cs-router-delegate.dart';

void main({AppEnv env = AppEnv.dev}) async {

  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  await FirebaseCrashlytics.instance.setCrashlyticsCollectionEnabled(!kDebugMode);

  runZonedGuarded<Future<void>>(() async {

    FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterError;

    await AppConfig().loadConfig(client: AppClient.etelecom_cs, env: env);

    initializeDateFormatting('vi');

    runApp(MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => NetworkProvider()),
        ChangeNotifierProvider(create: (_) => PermissionProvider()),
        ChangeNotifierProvider(create: (_) => AuthenticateProvider()),
        ChangeNotifierProvider(create: (_) => NavigateProvider()),
        ChangeNotifierProvider(create: (_) => CallsProvider()),
        ChangeNotifierProvider(create: (_) => ContactProvider()),
        ChangeNotifierProvider(create: (_) => CallLogProvider()),
        ChangeNotifierProvider(create: (_) => ShopProvider()),
        ChangeNotifierProvider(create: (_) => TicketProvider()),
        ChangeNotifierProvider(create: (_) => AccountUserProvider()),
        ChangeNotifierProvider(create: (_) => ExtensionProvider()),
        ChangeNotifierProvider(create: (_) => AppConfig()),
      ],
      child: Main(),
    ));

  }, FirebaseCrashlytics.instance.recordError);

  Isolate.current.addErrorListener(RawReceivePort((pair) async {
    final List<dynamic> errorAndStacktrace = pair;
    await FirebaseCrashlytics.instance.recordError(
      errorAndStacktrace.first,
      errorAndStacktrace.last,
    );
  }).sendPort);

}

class Main extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    EtelecomCsRouterDelegate _routerDelegate = EtelecomCsRouterDelegate();
    EtelecomCsRouteInformationParser _routeInformationParser =
        EtelecomCsRouteInformationParser();

    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus &&
            currentFocus.focusedChild != null) {
          FocusManager.instance.primaryFocus!.unfocus();
        }
      },
      child: MaterialApp.router(
          title: 'eTelecom',
          routerDelegate: _routerDelegate,
          routeInformationParser: _routeInformationParser,
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            fontFamily: GoogleFonts.openSans().fontFamily,
            primaryColor: MainTheme.primaryColor,
            visualDensity: VisualDensity.adaptivePlatformDensity,
          )
      )
    );
  }
}
