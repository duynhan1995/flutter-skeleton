import 'package:etelecom/providers/authenticate/authenticate.provider.dart';
import 'package:etelecom/providers/call-manager/calls.provider.dart';
import 'package:etelecom/providers/navigate/navigate.provider.dart';
import 'package:etelecom/services/http.service.dart';
import 'package:etelecom/services/portsip.service.dart';
import 'package:etelecom/widgets/calling/calling-state-incoming.dart';
import 'package:etelecom/widgets/calling/calling-state-outgoing.dart';
import 'package:etelecom/widgets/calling/calling-state-processing.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';

import 'etelecom-cs-route-path.dart';
import 'pages/call-center/call-center.dart';
import 'pages/call-logs/call-logs.dart';
import 'pages/contacts/contact-search.dart';
import 'pages/contacts/contacts.dart';
import 'pages/login/login.dart';
import 'pages/setting/account-info.dart';
import 'pages/setting/setting.dart';
import 'pages/setting/shops-list.dart';
import 'pages/tickets/ticket-detail.dart';
import 'pages/tickets/tickets.dart';

class EtelecomCsRouterDelegate extends RouterDelegate<EtelecomCsRoutePath>
    with ChangeNotifier, PopNavigatorRouterDelegateMixin<EtelecomCsRoutePath> {

  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  @override
  Widget build(BuildContext context) {
    PortsipService.hookNativeEvents(context);
    HttpService.injectContext(context);

    String _watchingToken = context.watch<AuthenticateProvider>().currentToken ?? '';
    bool _watchingAuthenticated =_watchingToken.isNotEmpty;

    List<String> _routeSegments =
        context.watch<NavigateProvider>().route.split("/");

    CallingState _callingState =
        context.watch<CallsProvider>().currentState;

    return Navigator(
      key: navigatorKey,
      pages: [
        if (!_watchingAuthenticated) LoginPage(),
        if (_watchingAuthenticated &&
            _routeSegments.length >= 2 &&
            _routeSegments[1] == 'call-center' &&
            _callingState == CallingState.none)
          CallCenterPage(),
        if (_watchingAuthenticated &&
            _routeSegments.length >= 2 &&
            _routeSegments[1] == 'call-logs' &&
            _callingState == CallingState.none)
          CallLogsPage(),
        // Start-Contact
        if (_watchingAuthenticated &&
            _routeSegments.length >= 2 &&
            _routeSegments[1] == 'contacts' &&
            _callingState == CallingState.none)
          ContactsPage(),
        if (_watchingAuthenticated &&
            _routeSegments.length >= 3 &&
            _routeSegments[2] == 'search' &&
            _callingState == CallingState.none)
          ContactsSearchPage(),
        // End-Contact
        // Start-Ticket
        if (_watchingAuthenticated &&
            _routeSegments.length >= 2 &&
            _routeSegments[1] == 'tickets' &&
            _callingState == CallingState.none)
          TicketPage(),
        if (_watchingAuthenticated &&
            _routeSegments.length >= 3 &&
            _routeSegments[2] == 'detail' &&
            _callingState == CallingState.none)
          TicketDetail(),
        // End-Ticket
        // Start-Setting
        if (_watchingAuthenticated &&
            _routeSegments.length >= 2 &&
            _routeSegments[1] == 'setting' &&
            _callingState == CallingState.none)
          SettingPage(),
        if (_watchingAuthenticated &&
            _routeSegments.length >= 3 &&
            _routeSegments[2] == 'shops' &&
            _callingState == CallingState.none)
          ShopsListPage(),
        if (_watchingAuthenticated &&
            _routeSegments.length >= 3 &&
            _routeSegments[2] == 'account' &&
            _callingState == CallingState.none)
          AccountInfoPage(),
        // End-Setting
        if (_watchingAuthenticated && _callingState == CallingState.outgoing)
          CallingStateOutgoingPage(),
        if (_watchingAuthenticated && _callingState == CallingState.incoming)
          CallingStateIncomingPage(),
        if (_watchingAuthenticated && _callingState == CallingState.processing)
          CallingStateProcessingPage(),
      ],
      onPopPage: (route, result) {
        if (!route.didPop(result)) {
          return false;
        }
        if (_routeSegments.length >= 3 && ['search'].contains(_routeSegments[2])) {
          context.read<NavigateProvider>().navigate('/contacts');
        }
        if (_routeSegments.length >= 3 && ['detail'].contains(_routeSegments[2])) {
          context.read<NavigateProvider>().navigate('/tickets');
        }
        if (_routeSegments.length >= 3 && ['shops', 'account'].contains(_routeSegments[2])) {
          context.read<NavigateProvider>().navigate('/setting');
        }
        return true;
      },
    );
  }

  @override
  Future<void> setNewRoutePath(EtelecomCsRoutePath path) async {
    print("setNewRoutePath $path");
  }
}
