import 'package:etelecom/api/shop/contact.api.dart';
import 'package:etelecom/api/shop/ticket.api.dart';
import 'package:etelecom/models/Filter.dart';
import 'package:etelecom/models/account-user/AccountUser.dart';
import 'package:etelecom/models/contact/Contact.dart';
import 'package:etelecom/models/ticket/Ticket.dart';
import 'package:etelecom/providers/account-user/account-user.provider.dart';
import 'package:etelecom/providers/contact/contact.provider.dart';
import 'package:etelecom/providers/ticket/ticket.provider.dart';
import 'package:etelecom/services/account-user.service.dart';
import 'package:etelecom/services/contact.service.dart';
import 'package:etelecom/services/ticket.service.dart';
import 'package:etelecom/theme/theme.dart';
import 'package:etelecom/utils/toast.dart';
import 'package:etelecom/widgets/bases/base-appbar.dart';
import 'package:etelecom/widgets/bases/base-text-field.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class TicketFilter extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _TicketFilterState();
}

class _TicketFilterState extends State<TicketFilter> {
  final ticketIdController = TextEditingController();
  String contactId = "";
  String phoneAfterInit = "";

  TicketStateFilterOption ticketStateSelected = TicketStateFilterOption(name: "Tất cả", value: null);
  TicketLabel? ticketLabelSelected;
  AccountUser? createdBySelected;
  AccountUser? assignedBySelected;
  AccountUser? closedBySelected;
  List<TicketStateFilterOption> tickStateOptions = [
    TicketStateFilterOption(name: "Tất cả", value: null),
    TicketStateFilterOption(name: "Mới", value: TicketState.open),
    TicketStateFilterOption(name: "Đã tiếp nhận", value: TicketState.received),
    TicketStateFilterOption(name: "Đang xử lý", value: TicketState.processing),
    TicketStateFilterOption(name: "Xử lý thành công", value: TicketState.success),
    TicketStateFilterOption(name: "Xử lý thất bại", value: TicketState.fail),
    TicketStateFilterOption(name: "Từ chối xử lý", value: TicketState.ignore),
  ];

  Future<void> filterTicket(BuildContext context) async {
    try {
      final GetTicketsFilter filters = GetTicketsFilter(
        types: [TicketType.internal],
        code: ticketIdController.text,
        state: ticketStateSelected.value,
      );
      if (ticketLabelSelected != null) filters.labelIds = [ticketLabelSelected!.id ?? ""];
      if (createdBySelected != null) filters.createdBy = createdBySelected!.id;
      if (assignedBySelected != null) filters.assignedUserId = [assignedBySelected!.id ?? ""];
      if (closedBySelected != null) filters.closedBy = closedBySelected!.id;
      context.read<TicketProvider>().setPaging(offset: 0);
      context.read<TicketProvider>().setFilter(filters);
      await TicketService.getTickets(context: context);
      Navigator.of(context).pop();
    } catch (e) {}
  }

  Future<void> removeFilterTicket(BuildContext context) async {
    ticketIdController.text = "";
    ticketStateSelected = TicketStateFilterOption(name: "Tất cả",value: null);
    ticketLabelSelected = null;
    createdBySelected = null;
    closedBySelected = null;
    ticketLabelSelected = null;
    assignedBySelected = null;
    context.read<TicketProvider>().setPaging(offset: 0);
  }

  @override
  void initState() {
    AccountUserService.getAccountUsers(context: context);
    final filters = context.read<TicketProvider>().filter;
    final ticketLabels = context.read<TicketProvider>().ticketLabels;
    final accountUsers = context.read<AccountUserProvider>().accountUsers;
    ticketIdController.text = filters!.code ?? "";
    if(filters.state != null) {
      ticketStateSelected = tickStateOptions.firstWhere((state) => state.value == filters.state);
    }
    if (filters.labelIds != null) {
      ticketLabelSelected =
          ticketLabels.firstWhere((ticketLabel) => ticketLabel.id == filters.labelIds!.first);
    }
    if (filters.createdBy != null) {
      createdBySelected =
          accountUsers.firstWhere((accountUser) => accountUser.id == filters.createdBy);
    }
    if (filters.assignedUserId != null) {
      assignedBySelected =
          accountUsers.firstWhere((accountUser) => accountUser.id == filters.assignedUserId!.first);
    }
    if (filters.closedBy != null) {
      closedBySelected =
          accountUsers.firstWhere((accountUser) => accountUser.id == filters.closedBy);
    }
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: EAppBar(
        title: 'Lọc',
        rightActionButtons: [
          EAppBarActionButton(title: 'Bỏ lọc', onPressed: (context) => removeFilterTicket(context))
        ],
      ),
      body: Container(
        padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
        child: ListView(children: [
          Container(
              margin: EdgeInsets.only(bottom: 20),
              child: ETextField(
                hintText: 'Nhập mã ticket',
                labelText: 'Mã ticket',
                textController: ticketIdController,
              )),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Trạng thái',
              ),
              DropdownButton(
                hint: Text(ticketStateSelected.name),
                isExpanded: true,
                iconSize: 30.0,
                items: tickStateOptions.map((state) {
                  return DropdownMenuItem(value: state, child: Text(state.name));
                }).toList(),
                onChanged: (val) {
                  setState(
                    () {
                      ticketStateSelected = val as TicketStateFilterOption;
                    },
                  );
                },
              ),
            ],
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Loại',
              ),
              if (context.watch<TicketProvider>().ticketLabels.isNotEmpty)
                DropdownButton(
                  hint: ticketLabelSelected == null
                      ? Text('Chọn loại ticket')
                      : Text(
                          ticketLabelSelected?.name ?? "",
                          style: TextStyle(color: MainTheme.primaryColor),
                        ),
                  isExpanded: true,
                  iconSize: 30.0,
                  items: context.watch<TicketProvider>().ticketLabels.map(
                    (label) {
                      return DropdownMenuItem<Object>(
                        value: label,
                        child: Text(label.name ?? ""),
                      );
                    },
                  ).toList(),
                  onChanged: (val) {
                    setState(
                      () {
                        ticketLabelSelected = val as TicketLabel;
                      },
                    );
                  },
                ),
            ],
          ),
          filterStaffSelect(title: "Người tạo",itemSelected:  createdBySelected,key:  "createdBy"),
          filterStaffSelect(title: "Người được phân công",itemSelected: assignedBySelected,key: "assignedBy"),
          filterStaffSelect(title: "Người hoàn thành",itemSelected: closedBySelected,key: "closedBy"),
          Container(
              child: Row(
            children: [
              Expanded(
                  child: MaterialButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(7),
                      ),
                      onPressed: () => filterTicket(context),
                      child: Text('Áp dụng',
                          style: TextStyle(fontSize: 17, fontWeight: FontWeight.w400)),
                      textColor: Colors.white,
                      color: MainTheme.primaryColor,
                      height: 45))
            ],
          ))
        ]),
      ),
    );
  }

  Widget filterStaffSelect({String title = "", AccountUser? itemSelected, String key = ""}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
        ),
        DropdownButton(
          hint: itemSelected == null
              ? Text('Chọn ${title.toLowerCase()}')
              : Text(
                  itemSelected.fullName ?? "",
                ),
          isExpanded: true,
          iconSize: 30.0,
          items: context.watch<AccountUserProvider>().accountUsers.map(
            (accountUser) {
              return DropdownMenuItem<AccountUser>(
                value: accountUser,
                child: Text("${accountUser.roleDisplay} - ${accountUser.fullName}"),
              );
            },
          ).toList(),
          onChanged: ( val) {
            setState(
              () {
                switch (key) {
                  case "createdBy":
                    createdBySelected = val as AccountUser;
                    break;
                  case "assignedBy":
                    assignedBySelected = val as AccountUser;
                    break;
                  case "closedBy":
                    closedBySelected = val as AccountUser;
                    break;
                }
              },
            );
          },
        ),
      ],
    );
  }
}
