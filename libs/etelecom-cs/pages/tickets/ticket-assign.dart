import 'package:etelecom/api/shop/account-user.api.dart';
import 'package:etelecom/models/account-user/AccountUser.dart';
import 'package:etelecom/providers/account-user/account-user.provider.dart';
import 'package:etelecom/providers/ticket/ticket.provider.dart';
import 'package:etelecom/services/account-user.service.dart';
import 'package:etelecom/services/ticket.service.dart';
import 'package:etelecom/theme/theme.dart';
import 'package:etelecom/utils/toast.dart';
import 'package:etelecom/widgets/bases/base-appbar.dart';
import 'package:etelecom/widgets/forms/search-textfield.dart';
import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:provider/provider.dart';

class TicketAssignScreen extends StatefulWidget {
  TicketAssignScreen({Key? key, this.title, this.func}) : super(key: key);
  final String? title;
  final Function? func;

  @override
  _TicketAssignScreenState createState() => _TicketAssignScreenState();
}

class _TicketAssignScreenState extends State<TicketAssignScreen> {
  var searchController = TextEditingController();
  List<AccountUser> accountUsers = [];

  Future<void> unAssignConfirm(BuildContext context, String userId) async {
    Navigator.of(context).pop();
    final ticket = context.read<TicketProvider>().ticketActive;
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Hủy phân công'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Container(
                  child: RichText(
                    text: TextSpan(
                        text: 'Bạn có chắc muốn Huỷ phân công',
                        style: TextStyle(color: Colors.black, fontSize: 15),
                        children: <TextSpan>[
                          TextSpan(
                            text:
                                ' ${ticket?.assignedUsers?[0].fullName ?? '-'} ',
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 15,
                                fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text: 'khỏi ticket này?',
                            style: TextStyle(color: Colors.black, fontSize: 15),
                          )
                        ]),
                  ),
                ),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Đóng', style: TextStyle(color: MainTheme.gray666)),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text('Xác nhận',
                  style: TextStyle(
                      color: Colors.red, fontWeight: FontWeight.w600)),
              onPressed: () {
                unAssignTicket(context, userId);
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> assignTicket(
      BuildContext context, String userId, String fullName) async {
    try {
      final ticket = context.read<TicketProvider>().ticketActive;
      await TicketService.assignTicket(context, ticket!.id, [userId]);
      widget.func!();
      Navigator.of(context).pop();
      EToast.success(context, 'Phân công ticket cho $fullName thành công');
    } catch (e) {
      Navigator.of(context).pop();
      EToast.error(
          context, 'Phân công ticket thất bại ${(e as dynamic)['msg']}');
      print('Phân công ticket thất bại.');
    }
  }

  Future<void> unAssignTicket(BuildContext context, String userId) async {
    try {
      final ticket = context.read<TicketProvider>().ticketActive;
      await TicketService.assignTicket(context, ticket!.id, []);
      widget.func!();
      Navigator.of(context).pop();
      EToast.success(context, 'Hủy phân công thành công');
    } catch (e) {
      Navigator.of(context).pop();
      EToast.error(context, 'Hủy phân công thất bại ${(e as dynamic)['msg']}');
      print('Hủy phân công ticket thất bại.');
    }
  }

  Future<void> search() async {
    final res = await AccountUserApi.getAccountUsers(GetAccountUsersRequest(filter: GetAccountUsersFilter(name: searchController.text)));
    final List<dynamic> _rawLists = res["account_users"];
    setState(() {
      accountUsers = _rawLists.map((e) => AccountUser.fromJson(e)).toList();
    });
  }

  @override
  void initState() {
    super.initState();
    if (context.read<AccountUserProvider>().accountUsers.length == 0) {
      AccountUserService.getAccountUsers(context: context);
    }
    accountUsers = context.read<AccountUserProvider>().accountUsers.where((e) => e.deleted == false).toList();
    
  }

  @override
  Widget build(BuildContext context) {
    final ticket = context.read<TicketProvider>().ticketActive;
    if (ticket!.assignedUserIds!.length > 0 && accountUsers[0].accountId!.isNotEmpty) {
      accountUsers.insert(0, AccountUser(accountId: null, fullName: 'Hủy phân công'));
      var index = accountUsers.lastIndexWhere((relationship) => relationship.userId == ticket.assignedUserIds?[0]);
      if (index > -1) accountUsers.removeAt(index);
    }
    return Scaffold(
        resizeToAvoidBottomInset: true,
        appBar: EAppBar(
          title: 'Phân công ticket',
        ),
        body: Container(
            child: Column(
          children: [
            Container(
                padding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                margin: EdgeInsets.only(bottom: 10, top: 20),
                child: ESearchTextField(
                  controller: searchController,
                  searchLabel: 'Tìm kiếm nhân viên',
                  searchHint: 'Tìm kiếm nhân viên',
                  autoFocus: true,
                  onChanged: () => search(),
                )),
            Expanded(
              child: ListView(
                shrinkWrap: true,
                controller: ModalScrollController.of(context),
                children: ListTile.divideTiles(
                    context: context,
                    tiles: List.generate(
                      accountUsers.length,
                      (index) => ListTile(
                          title: Text(
                            accountUsers[index].fullName ?? "",
                            style: TextStyle(
                                color: accountUsers[index].userId == null
                                    ? Colors.red
                                    : MainTheme.gray555),
                          ),
                          onTap: () => accountUsers[index].userId != null
                              ? assignTicket(
                                  context,
                                  accountUsers[index].userId!,
                                  accountUsers[index].fullName!)
                              : unAssignConfirm(context,
                                  accountUsers[index].userId ?? '')),
                    )).toList(),
              ),
            )
          ],
        )));
  }
}
