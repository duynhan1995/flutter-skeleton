import 'package:etelecom/api/shop/contact.api.dart';
import 'package:etelecom/api/shop/ticket.api.dart';
import 'package:etelecom/models/contact/Contact.dart';
import 'package:etelecom/models/ticket/Ticket.dart';
import 'package:etelecom/providers/contact/contact.provider.dart';
import 'package:etelecom/providers/ticket/ticket.provider.dart';
import 'package:etelecom/services/contact.service.dart';
import 'package:etelecom/services/ticket.service.dart';
import 'package:etelecom/theme/theme.dart';
import 'package:etelecom/utils/toast.dart';
import 'package:etelecom/widgets/bases/base-appbar.dart';
import 'package:etelecom/widgets/bases/base-text-field.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class TicketCreate extends StatefulWidget {
  final Contact contact;

  TicketCreate({required this.contact});

  @override
  State<StatefulWidget> createState() => _TicketCreateState();
}

class _TicketCreateState extends State<TicketCreate> {

  final fullnameController = TextEditingController();
  final phoneController = TextEditingController();
  final titleController = TextEditingController();
  final descriptionController = TextEditingController();
  String contactId = "";
  String phoneAfterInit = "";

  bool loading = false;
  TicketLabel? ticketLabelSelected;
  List<TicketLabel> ticketLabels = [];

  Future<void> createTicket(BuildContext context) async {
    setState(() => loading = true);
    try {
      if ((phoneController.text.isNotEmpty && fullnameController.text.isEmpty)
          || (phoneController.text.isEmpty && fullnameController.text.isNotEmpty)
      ) {
        setState(() => loading = false);
        return EToast.error(context, 'Tên và số điện thoại phải đầy đủ hoặc để trống!');
      }

      if (titleController.text.isNotEmpty && descriptionController.text.isNotEmpty) {
        final CreateTicketRequest _req = CreateTicketRequest(
            description: descriptionController.text,
            ref_id: null,
            ref_type: null,
            source: "telecom_app_call",
            title: titleController.text,
            label_ids: [],
            type: "internal"
        );

        final CreateContactRequest _contactReq = CreateContactRequest(
            fullName: fullnameController.text,
            phone: phoneController.text
        );

        if (phoneController.text.isNotEmpty) {
          String pattern = r'(^(?:[+0]9)?[0-9]{10,12}$)';
          RegExp regExp = RegExp(pattern);

          if (!regExp.hasMatch(phoneController.text)) {
            return EToast.error(context, 'Số điện thoại không đúng định dạng.');
          }
          if (contactId.isNotEmpty && phoneAfterInit == phoneController.text) {
            _req.ref_id = contactId;
            _req.ref_type = "contact";
          } else {
            await ContactService.getContacts(context: context, doLoading: true);
            final contacts = context.read<ContactProvider>().contacts;
            final contact = contacts.firstWhere(
              (contact) => contact.phone == phoneController.text,
              orElse: () => Contact(),
            );
            if (contact.id.isNotEmpty) {
              _req.ref_id = contact.id;
              _req.ref_type = "contact";
            } else {
              await ContactService.createContact(context, _contactReq);
              _req.ref_id = context.read<ContactProvider>().contacts.firstWhere(
                (contact) => contact.phone == phoneController.text,
                orElse: () => Contact()).id;
              _req.ref_type = "contact";
            }
          }
        }
        if (ticketLabelSelected != null) {
          _req.label_ids = [ticketLabelSelected!.id];
        }
        await TicketService.createTicket(context, _req);
        await TicketService.getTickets(context: context);
        Navigator.of(context).pop();

        EToast.success(context, 'Tạo ticket thành công');
      } else {
        EToast.error(context, 'Vui lòng nhập đầy đủ tiêu đề và nội dung ticket!');
      }
    } catch (e) {
      EToast.error(context, 'Tạo ticket thất bại. ${(e as dynamic)["msg"]}');
      print('Tạo ticket thất bại. $e');
    }
    setState(() => loading = false);
  }

  @override
  void initState() {
    super.initState();
    phoneController.text = widget.contact.phone;
    phoneAfterInit = widget.contact.phone;
    fullnameController.text = widget.contact.fullName;
    contactId = widget.contact.id;
  }

  @override
  void dispose() {
    fullnameController.dispose();
    phoneController.dispose();
    titleController.dispose();
    descriptionController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: EAppBar(title: 'Tạo ticket'),
      body: Container(
        padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
        child: ListView(children: <Widget>[
          Visibility(
            child: DropdownButton(
              hint: ticketLabelSelected == null
                  ? Text('Chọn loại ticket')
                  : Text(
                      ticketLabelSelected?.name ?? "",
                      style: TextStyle(color: MainTheme.primaryColor),
                    ),
              isExpanded: true,
              iconSize: 30.0,
              style: TextStyle(color: MainTheme.primaryColor),
              items: context.watch<TicketProvider>().ticketLabels.map(
                (label) {
                  return DropdownMenuItem<Object>(
                    value: label,
                    child: Text(label.name ?? ""),
                  );
                },
              ).toList(),
              onChanged: (val) {
                setState(
                  () {
                    ticketLabelSelected = val as TicketLabel;
                  },
                );
              },
            ),
            visible: context.watch<TicketProvider>().ticketLabels.isNotEmpty,
          ),
          Container(
              margin: EdgeInsets.only(bottom: 20),
              child: ETextField(
                hintText: 'Nhập tên khách hàng',
                labelText: 'Tên Khách hàng',
                textController: fullnameController,
              )),
          Container(
              margin: EdgeInsets.only(bottom: 20),
              child: ETextField(
                keyboardType: TextInputType.phone,
                hintText: 'Nhập số điện thoại KH',
                labelText: 'Số điện thoại KH',
                textController: phoneController,
              )),
          Container(
              margin: EdgeInsets.only(bottom: 20),
              child: ETextField(
                hintText: 'Nhập tiêu đề',
                labelText: 'Tiêu đề *',
                textController: titleController,
              )),
          Container(
              margin: EdgeInsets.only(bottom: 20),
              child: ETextField(
                hintText: 'Nhập nội dung',
                labelText: 'Nội dung *',
                textController: descriptionController,
              )),
          Container(
              child: Row(
                  children: [
                      Expanded(
                          child: MaterialButton(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(7),
                              ),
                              onPressed: loading ? null : () => createTicket(context),
                              child: Text('Xác nhận',
                                  style: TextStyle(
                                      fontSize: 17, fontWeight: FontWeight.w400
                                  )
                              ),
                              textColor: Colors.white,
                              color: MainTheme.primaryColor,
                              height: 45
                          )
                      )
                  ],
              )
          )
        ]),
      ),
    );
  }
}
