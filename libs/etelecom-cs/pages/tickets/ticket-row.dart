import 'package:etelecom/models/ticket/Ticket.dart';
import 'package:etelecom/providers/navigate/navigate.provider.dart';
import 'package:etelecom/providers/ticket/ticket.provider.dart';
import 'package:etelecom/services/ticket.service.dart';
import 'package:etelecom/theme/theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:etelecom/utils/string-handler.dart';

import 'package:provider/provider.dart';

class TicketRow extends StatelessWidget {
  final Ticket ticket;

  TicketRow({required this.ticket, Key? key});

  Widget ticketInfo() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(child: TicketFirstLine(ticket: ticket)),
        Text('${StringHandler.datetimeToString(ticket.createdAt)}',
            style: TextStyle(
                color: MainTheme.gray999,
                fontWeight: FontWeight.w800,
                fontSize: 10),
            overflow: TextOverflow.ellipsis,
            maxLines: 2),
        Text(
            ticket.title ?? "",
            style: TextStyle(
                color: MainTheme.gray555,
                fontWeight: FontWeight.w800,
                fontSize: 15),
            overflow: TextOverflow.ellipsis,
            maxLines: 2),
        Text(
            ticket.description ?? "",
            style: TextStyle(color: MainTheme.gray555, fontSize: 12),
            overflow: TextOverflow.ellipsis,
            maxLines: 4),
        
        Visibility(
          child: Chip(
            label: Text(ticket.label?.name ?? "", style: TextStyle(fontSize: 12 ,color:  Color(int.parse((ticket.label?.color ?? "#FF555555").replaceAll('#', '0xff'))))),
            backgroundColor: Color(int.parse((ticket.label?.color ?? "#FF555555").replaceAll('#', '0xff'))).withOpacity(0.1),
            padding: EdgeInsets.fromLTRB(18, 0, 18, 0),
            labelPadding: EdgeInsets.fromLTRB(0, -4, 0, -4)
          ),
          visible: ticket.label?.name != null,
        ),
      ]);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          TicketService.getTicketComments(
              context: context, ticketId: ticket.id);
          context.read<TicketProvider>().setTicketActive(ticket);
          context.read<NavigateProvider>().navigate('/tickets/detail');
        },
        child: Container(
          margin: EdgeInsets.only(bottom: 10),
          padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
          decoration: BoxDecoration(
              color: Colors.white,
              border: Border.symmetric(
                  vertical: BorderSide.none,
                  horizontal: BorderSide(color: MainTheme.grayF5))),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(child: ticketInfo()),
            ],
          ),
        ));
  }
}

class TicketFirstLine extends StatelessWidget {
  final Ticket ticket;

  TicketFirstLine({required this.ticket});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(child: TicketStatus(ticket: ticket)),
      ],
    );
  }
}

class TicketStatus extends StatelessWidget {
  final Ticket ticket;

  TicketStatus({required this.ticket});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          margin: EdgeInsets.only(bottom: 2.5),
          child: Text(
            ticket.code ??"",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
                color: MainTheme.primaryColor,
                fontWeight: FontWeight.w800,
                fontSize: 15),
          ),
        ),
        Container(
          margin: EdgeInsets.only(bottom: 2.5),
          child: Text(' - ${ticket.stateDisplay}',
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
                color: MainTheme.gray555,
                fontWeight: FontWeight.w800,
                fontSize: 15),
          ),
        )
    ]);
  }
}