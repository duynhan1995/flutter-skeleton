import 'package:etelecom/api/shop/ticket.api.dart';
import 'package:etelecom/models/contact/Contact.dart';
import 'package:etelecom/models/ticket/Ticket.dart';
import 'package:etelecom/providers/ticket/ticket.provider.dart';
import 'package:etelecom/services/ticket.service.dart';
import 'package:etelecom/widgets/bases/base-appbar.dart';
import 'package:etelecom/widgets/bases/base-scaffold.dart';
import 'package:etelecom/widgets/list-view/list-view.dart';
import 'package:etelecom/widgets/bases/base-menu.dart';
import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:provider/provider.dart';

import 'ticket-create.dart';
import 'ticket-filter.dart';
import 'ticket-row.dart';

class TicketPage extends Page {
  TicketPage() : super(key: ValueKey('TicketPage'));

  @override
  Route createRoute(BuildContext context) {

    WidgetsBinding.instance!.addPostFrameCallback((_) async {
      context.read<TicketProvider>().setPaging(offset: 0);
      context.read<TicketProvider>().setFilter(GetTicketsFilter(types: [TicketType.internal]));
      TicketService.getTickets(context: context);
    });

    return PageRouteBuilder(
        settings: this,
        pageBuilder: (BuildContext context, animation, animation2) {
          return TicketScreen();
        });
  }
}

class TicketScreen extends StatefulWidget {
  TicketScreen({Key? key, this.title}) : super(key: key);
  final String? title;

  @override
  _TicketScreenState createState() => _TicketScreenState();
}

class _TicketScreenState extends State<TicketScreen> {
  void bottomSheetCreateTicket(BuildContext context) {
    CupertinoScaffold.showCupertinoModalBottomSheet(
        context: context,
        duration: Duration(milliseconds: 400),
        builder: (BuildContext context) {
          return TicketCreate(contact: Contact());
        });
  }

  void bottomSheetFilterTicket(BuildContext context) {
    CupertinoScaffold.showCupertinoModalBottomSheet(
        context: context,
        duration: Duration(milliseconds: 400),
        builder: (BuildContext context) {
          return TicketFilter();
        });
  }

  @override
  Widget build(BuildContext context) {
    return EScaffold(
        appBar: EAppBar(
          title: 'Ticket',
          rightActionButtons: [
            EAppBarActionButton(
                icon: Icons.filter_alt_outlined,
                title: '',
                onPressed: (context) => bottomSheetFilterTicket(context)),
            EAppBarActionButton(
                title: '+ Thêm',
                onPressed: (context) => bottomSheetCreateTicket(context)),
          ],
        ),
        body: EListView(
          isLoading: context.watch<TicketProvider>().loadingTickets &&
            context.watch<TicketProvider>().tickets.isEmpty,
          isEmpty: context.watch<TicketProvider>().tickets.length == 0,
          currentOffset: context.watch<TicketProvider>().paging.offset ?? 0,
          listLength: context.watch<TicketProvider>().tickets.length,
          itemChildBuilder: (BuildContext context, int index) {
            final Ticket ticketRow =
            context.watch<TicketProvider>().tickets[index];
            return TicketRow(ticket: ticketRow, key: ObjectKey(ticketRow.id));
          },
          onLoadMore: () async {
            WidgetsBinding.instance!.addPostFrameCallback((_) async {
              context.read<TicketProvider>().setLoading(true);

              Future.delayed(Duration(seconds: 1), () async {
                await TicketService.getTickets(context: context);
              });
            });
          },
          onRefresh: () async {
            context.read<TicketProvider>().setPaging(offset: 0);
            await TicketService.getTickets(context: context);
          },
        ),
        bottomMenu: EMenu(menuItems: [
          MenuItem(
              icon: Icons.phone_in_talk_outlined,
              label: 'Nghe gọi',
              route: '/call-center'),
          MenuItem(
              icon: Icons.confirmation_number_outlined,
              label: 'Ticket',
              route: '/tickets'),
          MenuItem(icon: Icons.list, label: 'Lịch sử', route: '/call-logs'),
          MenuItem(
              icon: Icons.account_circle_outlined,
              label: 'Danh bạ',
              route: '/contacts'),
          MenuItem(
              icon: Icons.settings_outlined,
              label: 'Thiết lập',
              route: '/setting'),
        ]));
  }
}
