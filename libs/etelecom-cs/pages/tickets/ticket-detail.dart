import 'dart:io';
import 'dart:ui';

import 'package:etelecom/api/shop/ticket.api.dart';
import 'package:etelecom/models/ticket/Ticket.dart';
import 'package:etelecom/providers/authenticate/authenticate.provider.dart';
import 'package:etelecom/providers/ticket/ticket.provider.dart';
import 'package:etelecom/services/portsip.service.dart';
import 'package:etelecom/services/ticket.service.dart';
import 'package:etelecom/theme/theme.dart';
import 'package:etelecom/utils/string-handler.dart';
import 'package:etelecom/utils/toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:provider/provider.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:image_picker/image_picker.dart';
import 'package:etelecom/api/etop/file-uploader.dart';
import 'package:permission_handler/permission_handler.dart';

import 'ticket-assign.dart';

class TicketDetail extends Page {
  TicketDetail() : super(key: ValueKey('TicketDetail'));

  @override
  Route createRoute(BuildContext context) {
    WidgetsBinding.instance!.addPostFrameCallback((_) {});

    return PageRouteBuilder(
        settings: this,
        pageBuilder: (BuildContext context, animation, animation2) {
          var begin = Offset(1.0, 0.0);
          var end = Offset.zero;
          var tween = Tween(begin: begin, end: end);
          var offsetAnimation = animation.drive(tween);
          return SlideTransition(
            position: offsetAnimation,
            child: TicketDetailScreen(),
          );
        });
  }
}

class TicketDetailScreen extends StatefulWidget {
  TicketDetailScreen({Key? key, this.title}) : super(key: key);

  final String? title;

  @override
  State<StatefulWidget> createState() {
    return TicketDetailState();
  }
}

class TicketStateItem {
  final String? title;
  final TicketState? state;

  TicketStateItem({this.title, this.state});
}

class TicketDetailState extends State<TicketDetailScreen> with SingleTickerProviderStateMixin {
  late Ticket ticket;

  List<TicketLabel> ticketLabels = [];
  TabController? _tabController;
  final messageController = TextEditingController();
  final picker = ImagePicker();
  List<TicketComment> _ticketComments = [];

  // Current Index of tab
  int _currentIndex = 0;
  FocusNode? boxChatFocusNode;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: 2, initialIndex: _currentIndex);
    ticket = context.read<TicketProvider>().ticketActive!;
    print("TICKET DETAIL: ${ticket.toJson()}");
    boxChatFocusNode = FocusNode();
  }

  @override
  void dispose() {
    _tabController!.dispose();
    messageController.dispose();
    super.dispose();
  }

  Widget ticketInfo() {
    return Container(
      child: ListView(
        children: [
          Column(
            children: [
              Container(
                padding: EdgeInsets.all(10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Text(
                          ticket.code ?? "",
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              color: MainTheme.primaryColor,
                              fontWeight: FontWeight.w800,
                              fontSize: 15),
                        ),
                        Text(' - ${ticket.stateDisplay}',
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                                color: MainTheme.gray555,
                                fontWeight: FontWeight.w800,
                                fontSize: 15)),
                      ],
                    ),
                    Text('${StringHandler.datetimeToString(ticket.createdAt)}',
                        style: TextStyle(
                            color: MainTheme.gray999, fontWeight: FontWeight.w800, fontSize: 10))
                  ],
                ),
              ),
              Container(
                height: 10,
                color: MainTheme.grayF5,
              ),
              Container(
                padding: EdgeInsets.all(10),
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only(bottom: 10),
                      child: Row(
                        children: [
                          Flexible(
                            child: Text(ticket.title ?? "",
                                style: TextStyle(
                                    color: MainTheme.gray555,
                                    fontWeight: FontWeight.w800,
                                    fontSize: 15)),
                          ),
                        ],
                      ),
                    ),
                    Row(
                      children: [
                        Flexible(
                          child: Text(
                            ticket.description ?? "",
                            style: TextStyle(color: MainTheme.gray555, fontSize: 12),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
              Container(
                height: 10,
                color: MainTheme.grayF5,
              ),
              Container(
                padding: EdgeInsets.all(10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Liên hệ'),
                    if (ticket.contact == null) Text('-'),
                    if (ticket.contact != null)
                      InkWell(
                        onTap: () => callOut(context, ticket.contact?.phone),
                        child: Text(
                          ticket.contact!.fullName.isEmpty
                              ? '-'
                              : "${ticket.contact?.fullName} - ${ticket.contact?.phone}",
                          style:
                              TextStyle(color: MainTheme.primaryColor, fontWeight: FontWeight.bold),
                        ),
                      ),
                  ],
                ),
              ),
              Container(
                height: 10,
                color: MainTheme.grayF5,
              ),
              Container(
                child: Column(
                  children: [
                    Container(
                      padding: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                          border: Border(bottom: BorderSide(color: MainTheme.grayF5))),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text('Người tạo'),
                          Text(
                            ticket.createdByUser?.fullName ?? "-",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          )
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                          border: Border(bottom: BorderSide(color: MainTheme.grayF5))),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text('Người được phân công'),
                          Text(
                            ticket.assignedUsers!.isEmpty
                                ? '-'
                                : (ticket.assignedUsers?[0].fullName ?? '-'),
                            style: TextStyle(fontWeight: FontWeight.bold),
                          )
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text('Người hoàn thành'),
                          Text(ticket.closedByUser?.fullName ?? '-',
                              style: TextStyle(fontWeight: FontWeight.bold))
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  Widget ticketComments() {
    _ticketComments = context.watch<TicketProvider>().ticketComments;
    if (_ticketComments.length == 0) {
      return Center(
        child: Text('Chưa có phản hồi'),
      );
    } else {
      return ListView.builder(
        itemCount: _ticketComments.length,
        reverse: true,
        itemBuilder: (context, index) {
          return ticketCommentItem(_ticketComments[index]);
        },
      );
    }
  }

  Widget ticketCommentItem(TicketComment ticketComment) {
    return Row(
      children: [
        Container(
          padding: EdgeInsets.fromLTRB(8, 4, 8, 4),
          width: MediaQuery.of(context).size.width * 0.7,
          margin: EdgeInsets.fromLTRB(
              _ownerTicket(ticketComment.createdBy) ? MediaQuery.of(context).size.width * 0.3 : 0,
              0,
              _ownerTicket(ticketComment.createdBy) ? 0 : MediaQuery.of(context).size.width * 0.3,
              8.0),
          child: DecoratedBox(
            decoration: BoxDecoration(
                color: _ownerTicket(ticketComment.createdBy)
                    ? MainTheme.primaryColor.withOpacity(0.1)
                    : MainTheme.grayF5,
                borderRadius: BorderRadius.circular(8.0),
                border: Border.all(
                    color: _ownerTicket(ticketComment.createdBy)
                        ? MainTheme.primaryColor.withOpacity(0.1)
                        : MainTheme.grayEEE,
                    width: 1.0)),
            child: Container(
              padding: EdgeInsets.fromLTRB(12.0, 8.0, 12.0, 8.0),
              child: Column(
                children: [
                  Row(
                    children: [
                      Padding(
                        child: Text(
                          ticketComment.createdByUser!.fullName ?? '-',
                          style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        padding: EdgeInsets.only(bottom: 4.0),
                      )
                    ],
                  ),
                  Visibility(
                    child: Row(
                      children: [
                        Flexible(
                          child: Image.network(ticketComment.imageUrl ?? "",
                              fit: BoxFit.cover, width: MediaQuery.of(context).size.width * 0.7),
                        ),
                      ],
                    ),
                    visible: ticketComment.imageUrl != "",
                  ),
                  Visibility(
                    child: Row(
                      children: [
                        Flexible(
                            child: Linkify(
                                onOpen: (link) async {
                                  if (await canLaunch(link.url)) {
                                    await launch(link.url);
                                  } else {
                                    throw 'Could not launch $link';
                                  }
                                },
                                text: ticketComment.message ?? "",
                                style: TextStyle(fontSize: 13, fontWeight: FontWeight.w500),
                                options: LinkifyOptions(humanize: false))),
                      ],
                    ),
                    visible: ticketComment.message != "",
                  ),
                  Row(
                    children: [
                      Spacer(),
                      Padding(
                        padding: EdgeInsets.only(top: 8),
                        child: Text(
                          '${StringHandler.datetimeToString(ticketComment.createdAt)}',
                          style: TextStyle(
                            fontSize: 10,
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget boxChat() {
    return Container(
      decoration: BoxDecoration(
        border: Border(
          top: BorderSide(color: MainTheme.grayF5, width: 1),
        ),
      ),
      padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
      child: Row(
        children: [
          Container(
            padding: EdgeInsets.only(right: 12),
            child: InkWell(
              onTap: () {
                showMaterialModalBottomSheet(
                    expand: false,
                    context: context,
                    backgroundColor: Colors.transparent,
                    builder: (context) => imagePickerSelect());
              },
              child: Icon(
                Icons.image,
                color: MainTheme.primaryColor,
              ),
            ),
          ),
          Container(
            child: TextField(
              focusNode: boxChatFocusNode,
              keyboardType: TextInputType.multiline,
              maxLines: null,
              textInputAction: TextInputAction.newline,
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: 'Nhập nội dung phản hồi',
                hintStyle: TextStyle(
                  fontSize: 14.0,
                  color: Color(0xffAEA4A3),
                ),
              ),
              controller: messageController,
              onSubmitted: (_) {},
            ),
            width: MediaQuery.of(context).size.width - 100,
          ),
          Container(
            child: InkWell(
              onTap: () => createTicketComment(),
              child: Icon(
                Icons.send,
                color: MainTheme.primaryColor,
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget changeStateModal() {
    var listState = [
      TicketStateItem(title: 'Mới (Mở lại ticket)', state: TicketState.open),
      TicketStateItem(title: 'Đang xử lý', state: TicketState.processing),
      TicketStateItem(title: 'Thành công', state: TicketState.success),
      TicketStateItem(title: 'Thất bại', state: TicketState.fail),
      TicketStateItem(title: 'Từ chối xử lý', state: TicketState.ignore),
    ];
    if (ticket.state == TicketState.open || ticket.state == TicketState.received) {
      listState.removeAt(0);
    }
    if (ticket.state == TicketState.processing) {
      listState.removeRange(0, 2);
    }
    if (ticket.state == TicketState.success ||
        ticket.state == TicketState.fail ||
        ticket.state == TicketState.ignore) {
      listState = [
        TicketStateItem(title: 'Mới (Mở lại ticket)', state: TicketState.open),
      ];
    }
    return Material(
        child: SafeArea(
      top: false,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: ListTile.divideTiles(
            context: context,
            tiles: List.generate(
              listState.length,
              (index) => ListTile(
                  title: Text(
                    listState[index].title ?? "",
                  ),
                  onTap: () => updateStateTicket(listState[index].state!)),
            )).toList(),
      ),
    ));
  }

  Widget imagePickerSelect() {
    boxChatFocusNode!.unfocus();
    return Material(
        child: SafeArea(
      top: false,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          ListTile(
            title: Text('Chọn từ thư viện ảnh'),
            leading: Icon(Icons.image),
            onTap: () {
              Navigator.of(context).pop();
              getImage();
            },
          ),
          ListTile(
              title: Text('Chụp ảnh'),
              leading: Icon(Icons.camera),
              onTap: () {
                Navigator.of(context).pop();
                _getFromCamera();
              }),
        ],
      ),
    ));
  }

  Future<void> createTicketComment() async {
    try {
      if (messageController.text.isEmpty) {
        return EToast.error(context, 'Vui lòng nhập nội dung');
      }
      final CreateTicketCommentRequest request =
          CreateTicketCommentRequest(message: messageController.text, ticketId: ticket.id);
      await TicketService.createTicketComment(context, request);
      await TicketService.getTicketComments(context: context, ticketId: ticket.id);
      setState(() {
        _ticketComments = context.read<TicketProvider>().ticketComments;
        messageController.text = '';
      });
    } catch (e) {
      EToast.error(context, 'Gủi phản hồi thất bại. ${(e as dynamic)["msg"]}');
    }
  }

  Future<void> updateStateTicket(TicketState state) async {
    try {
      await updateState(state);
      Navigator.of(context).pop();
      EToast.success(context, 'Chuyển trạng thái thành công');
    } catch (e) {
      Navigator.of(context).pop();
      EToast.error(context, 'Chuyển trạng thái thất bại! ${(e as dynamic)["msg"]}');
    }
  }

  Future<void> updateState(TicketState state) async {
    switch (state) {
      case TicketState.open:
        await TicketService.reopenTicket(context, ticket.id);
        break;
      case TicketState.processing:
        await TicketService.confirmTicket(context, ticket.id);
        break;
      case TicketState.success:
        await TicketService.closeTicket(context, state, ticket.id);
        break;
      case TicketState.fail:
        await TicketService.closeTicket(context, state, ticket.id);
        break;
      case TicketState.ignore:
        await TicketService.closeTicket(context, state, ticket.id);
        break;
      case TicketState.unknown:
      case TicketState.received:
      case TicketState.cancel:
        break;
    }
  }

  Future getImage() async {
    var status = await Permission.photos.status;
    if (status.isGranted || status.isDenied) {
      PickedFile? pickedFile = await ImagePicker().getImage(
        source: ImageSource.gallery,
        imageQuality: 100,
        maxWidth: 1024,
        maxHeight: 1024,
      );
      await fileImage(pickedFile!);
    } else {
      showRequestPermission('Quyền truy cập thư viện ảnh',
          'eTelecom - Tổng đài CSKH muốn truy cập vào thư viện ảnh để gửi phản hồi bằng hình ảnh');
    }
  }

  Future _getFromCamera() async {
    final status = await Permission.camera.status;
    if (status.isGranted || status.isDenied) {
      PickedFile? pickedFile = await ImagePicker().getImage(
        source: ImageSource.camera,
        imageQuality: 100,
        maxWidth: 1024,
        maxHeight: 1024,
      );
      await fileImage(pickedFile!);
    } else {
      showRequestPermission('Quyền truy cập máy ảnh',
          'eTelecom - Tổng đài CSKH muốn truy cập máy ảnh để chụp và gửi phản hồi bằng hình ảnh');
    }
  }

  showRequestPermission(String title, String content) {
    return showDialog(
        context: context,
        builder: (BuildContext context) => CupertinoAlertDialog(
              title: Text(title),
              content: Text(content),
              actions: [
                CupertinoDialogAction(
                  child: Text('Đóng'),
                  onPressed: () => Navigator.of(context).pop(),
                ),
                CupertinoDialogAction(
                  child: Text('Thiết lập'),
                  onPressed: () => openSettingsApp(context),
                ),
              ],
            ));
  }

  openSettingsApp(BuildContext context) {
    Navigator.of(context).pop();
    openAppSettings();
  }

  Future<void> fileImage(PickedFile pickedFile) async {
    try {
      File imagePath = File(pickedFile.path);
      var res = await FileUploadApi.fileUpload(imagePath);
      var imageUrl = res['result'][0]['url'];
      final CreateTicketCommentRequest request =
          CreateTicketCommentRequest(imageUrl: imageUrl, ticketId: ticket.id);
      await TicketService.createTicketComment(context, request);
      await TicketService.getTicketComments(context: context, ticketId: ticket.id);
      setState(() {
        _ticketComments = context.read<TicketProvider>().ticketComments;
      });
    } catch (e) {
      EToast.error(context, 'Chuyển trạng thái thất bại! ${(e as dynamic)['errors'][0]['msg']}');
    }
  }

  _ownerTicket(id) {
    return id == context.read<AuthenticateProvider>().currentUser!.id;
  }

  Future<void> callOut(BuildContext context, String? phone) async {
    if (phone == null || phone.trim().isEmpty) {
      return;
    }
    try {
      await PortsipService.portsipCallOut(phone, context: context);
    } catch (e) {
      if ((e as dynamic)["code"] == "not_enough_balance") {
        EToast.error(context, "Không đủ số dư để thực hiện cuộc gọi");
      }
    }
  }

  Future<void> selfAssignTicket(BuildContext context) async {
    try {
      await TicketService.assignTicket(
          context, ticket.id, [context.read<AuthenticateProvider>().currentUser!.id ?? ""]);
      await TicketService.getTickets(context: context);
      EToast.success(context, 'Tự phân công ticket cho thành công');
    } catch (e) {
      EToast.error(context, 'Tự phân công ticket thất bại ${(e as dynamic)['msg']}');
      print('Tự phân công ticket thất bại.');
    }
    setState(() {
      ticket = context.read<TicketProvider>().ticketActive!;
    });
  }

  Future<void> unAssignTicket(BuildContext context) async {
    try {
      await TicketService.assignTicket(context, ticket.id, []);
      EToast.success(context, 'Hủy phân công thành công');
    } catch (e) {
      EToast.error(context, 'Hủy phân công thất bại ${(e as dynamic)['msg']}');
      print('Hủy phân công ticket thất bại.');
    }
  }

  updateTicket() {
    setState(() {
      ticket = context.read<TicketProvider>().ticketActive!;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        title: Text("Chi tiết ticket"),
        backgroundColor: Colors.white,
        shadowColor: Colors.white,
      ),
      body: Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
        CupertinoSegmentedControl(
            padding: EdgeInsets.all(0),
            borderColor: Colors.white,
            selectedColor: Colors.white,
            children: {
              0: Container(
                  padding: EdgeInsets.all(12.0),
                  width: MediaQuery.of(context).size.width / 2,
                  child: Center(
                    child: Text(
                      'Thông tin',
                      style: TextStyle(
                          color: _currentIndex == 0 ? MainTheme.primaryColor : MainTheme.gray888,
                          fontWeight: _currentIndex == 0 ? FontWeight.bold : FontWeight.normal),
                    ),
                  ),
                  decoration: BoxDecoration(
                      border: Border(
                          right: BorderSide(width: 1.0, color: MainTheme.grayF5),
                          bottom: BorderSide(
                              width: 3.0,
                              color: _currentIndex == 0
                                  ? MainTheme.primaryColor
                                  : MainTheme.grayEEE)))),
              1: Container(
                  padding: EdgeInsets.all(8.0),
                  width: MediaQuery.of(context).size.width / 2,
                  child: Center(
                    child: Text(
                      'Phản hồi',
                      style: TextStyle(
                          color: _currentIndex == 0 ? MainTheme.gray888 : MainTheme.primaryColor,
                          fontWeight: _currentIndex == 1 ? FontWeight.bold : FontWeight.normal),
                    ),
                  ),
                  decoration: BoxDecoration(
                      border: Border(
                          bottom: BorderSide(
                              width: 3.0,
                              color: _currentIndex == 0
                                  ? MainTheme.grayEEE
                                  : MainTheme.primaryColor))))
            },
            groupValue: _currentIndex,
            onValueChanged: (newValue) {
              _tabController!.animateTo(newValue as int);
              setState(() {
                _currentIndex = newValue;
              });
            }),
        Expanded(
          child: TabBarView(
              controller: _tabController,
              // Restrict scroll by user
              physics: const NeverScrollableScrollPhysics(),
              children: [
                // Sign In View
                ticketInfo(),
                // Sign Up View
                ticketComments()
              ]),
        ),
        Container(
          decoration: BoxDecoration(color: Theme.of(context).cardColor),
          child: SafeArea(
            bottom: true,
            child: Container(
              child: Row(
                children: [
                  Visibility(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        if (context
                                .read<AuthenticateProvider>()
                                .currentPermission!
                                .roles!
                                .contains("telecom_customerservice"))
                          ticket.assignedUserIds!.length == 0 ?
                          SizedBox(
                              width: MediaQuery.of(context).size.width / 2,
                              child: Container(
                                padding: EdgeInsets.fromLTRB(10, 0, 5, 0),
                                child: MaterialButton(
                                  onPressed: () => selfAssignTicket(context),
                                  child: Text('Tự phân công',
                                      style: TextStyle(color: MainTheme.primaryColor)),
                                  textColor: MainTheme.primaryColor,
                                  shape: RoundedRectangleBorder(
                                      side: BorderSide(
                                          color: MainTheme.primaryColor,
                                          width: 1,
                                          style: BorderStyle.solid),
                                      borderRadius: BorderRadius.circular(5)),
                                ),
                              ))
                              : SizedBox(
                              width: MediaQuery.of(context).size.width / 2,
                              child: Container(
                                padding: EdgeInsets.fromLTRB(10, 0, 5, 0),
                                child: MaterialButton(
                                  onPressed: () => unAssignTicket(context),
                                  child: Text('Hủy phân công',
                                      style: TextStyle(color: Colors.red)),
                                  shape: RoundedRectangleBorder(
                                      side: BorderSide(
                                          color: Colors.red,
                                          width: 1,
                                          style: BorderStyle.solid),
                                      borderRadius: BorderRadius.circular(5)),
                                ),
                              )),
                        if (!context
                            .read<AuthenticateProvider>()
                            .currentPermission!
                            .roles!
                            .contains("telecom_customerservice"))
                          SizedBox(
                              width: MediaQuery.of(context).size.width / 2,
                              child: Container(
                                padding: EdgeInsets.fromLTRB(10, 0, 5, 0),
                                child: MaterialButton(
                                  onPressed: () => Navigator.of(context).push(
                                    MaterialPageRoute(
                                        builder: (context) => TicketAssignScreen(
                                              func: updateTicket,
                                            )),
                                  ),
                                  child: Text('Phân công',
                                      style: TextStyle(color: MainTheme.primaryColor)),
                                  textColor: MainTheme.primaryColor,
                                  shape: RoundedRectangleBorder(
                                      side: BorderSide(
                                          color: MainTheme.primaryColor,
                                          width: 1,
                                          style: BorderStyle.solid),
                                      borderRadius: BorderRadius.circular(5)),
                                ),
                              )),
                        (context
                                    .read<AuthenticateProvider>()
                                    .currentPermission!
                                    .roles!
                                    .contains("telecom_customerservice") &&
                                ticket.assignedUserIds!.length == 0)
                            ? SizedBox()
                            : SizedBox(
                                width: MediaQuery.of(context).size.width / 2,
                                child: Container(
                                  padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
                                  child: MaterialButton(
                                    onPressed: () {
                                      showMaterialModalBottomSheet(
                                        expand: false,
                                        context: context,
                                        backgroundColor: Colors.transparent,
                                        builder: (context) => changeStateModal(),
                                      );
                                    },
                                    color: MainTheme.primaryColor,
                                    textColor: Colors.white,
                                    child: Text('Đổi trạng thái'),
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(5)),
                                  ),
                                )),
                      ],
                    ),
                    visible: _currentIndex == 0,
                  ),
                  Visibility(
                    child: boxChat(),
                    visible: _currentIndex == 1,
                  ),
                ],
              ),
            ),
          ),
        ),
      ]),
    );
  }
}
