import 'package:etelecom/api/etop/user-api.dart';
import 'package:etelecom/providers/authenticate/authenticate.provider.dart';
import 'package:etelecom/theme/theme.dart';
import 'package:etelecom/utils/toast.dart';
import 'package:etelecom/widgets/bases/base-appbar.dart';
import 'package:etelecom/widgets/bases/base-scaffold.dart';
import 'package:etelecom/widgets/bases/base-text-field.dart';
import 'package:etelecom/widgets/progress-indicators/circular-progress-indicator.dart';
import 'package:etelecom/widgets/spinner/spinner.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class LoginPage extends Page {
  LoginPage() : super(key: ValueKey('LoginPage'));

  @override
  Route createRoute(BuildContext context) {
    return MaterialPageRoute(
        settings: this,
        builder: (BuildContext context) {
          return LoginScreen();
        });
  }
}

class LoginScreen extends StatefulWidget {
  final String? title;

  LoginScreen({Key? key, this.title}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final UserApi userApi = UserApi();

  final usernameController = TextEditingController();
  final passwordController = TextEditingController();

  bool loading = false;

  Future<void> login(BuildContext context) async {
    if (usernameController.text.isEmpty || passwordController.text.isEmpty) {
      return EToast.error(context, 'Vui lòng nhập đầy đủ thông tin.');

    }
    setState(() => loading = true);

    try {

      final res = await UserApi.login(usernameController.text, passwordController.text);

      final String token = res["access_token"] ?? "";

      if (token.isEmpty) {
        EToast.error(context, 'Đăng nhập thất bại. ${(res as dynamic)["msg"]}');
      } else {
        context.read<AuthenticateProvider>().updateToken(token);
        context.read<AuthenticateProvider>().updateSessionInfo();
      }

    } catch (e) {
      print("ERROR in login $e");
    }

    setState(() => loading = false);
  }

  @override
  void dispose() {
    usernameController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return EScaffold(
      appBar: EAppBar(title: 'eTelecom - Tổng đài CSKH'),
      body: Container(
        padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
        child: Column(
          children: [
            Container(
                margin: EdgeInsets.only(bottom: 20),
                child: ETextField(
                  keyboardType: TextInputType.phone,
                  labelText: "Số điện thoại",
                  hintText: "Nhập số điện thoại",
                  textController: usernameController,
                )
            ),
            Container(
                margin: EdgeInsets.only(bottom: 20),
                child: ETextField(
                  obscureText: true,
                  hintText: "Nhập mật khẩu",
                  labelText: "Mật khẩu",
                  textController: passwordController,
                )
            ),
            Container(
                child: Row(
                  children: [
                    Expanded(
                        child: MaterialButton(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(7),
                                side: BorderSide(
                                    color: MainTheme.primaryColor, width: 1
                                )
                            ),
                            onPressed: loading ? null : () async {
                              await login(context);
                            },
                            child: Row(
                                children: [
                                  if (loading)
                                    Container(
                                      child: ECircularProgressIndicator(
                                        color: Colors.white,
                                        size: 14,
                                        padding: 0,
                                      ),
                                      margin: EdgeInsets.only(right: 10),
                                    ),
                                  Text(
                                      'Đăng nhập',
                                      style: TextStyle(
                                          fontSize: 17, fontWeight: FontWeight.w400
                                      )
                                  )
                                ],
                                mainAxisAlignment: MainAxisAlignment.center
                            ),
                            textColor: Colors.white,
                            color: MainTheme.primaryColor,
                            disabledColor: MainTheme.primaryColor.withOpacity(0.8),
                            disabledTextColor: Colors.white.withOpacity(0.8),
                            minWidth: 300,
                            height: 50
                        )
                    )
                  ],
            )),
          ],
        ),
      ),
    );
  }
}
