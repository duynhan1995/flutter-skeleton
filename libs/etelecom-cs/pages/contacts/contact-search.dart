import 'package:etelecom/models/contact/Contact.dart';
import 'package:etelecom/models/extension/Extension.dart';
import 'package:etelecom/models/account-user/AccountUser.dart';
import 'package:etelecom/providers/contact/contact.provider.dart';
import 'package:etelecom/providers/extension/extension.provider.dart';
import 'package:etelecom/services/contact.service.dart';
import 'package:etelecom/services/extension.service.dart';
import 'package:etelecom/services/account-user.service.dart';
import 'package:etelecom/theme/theme.dart';
import 'package:etelecom/utils/native-permissions.dart';
import 'package:etelecom/utils/string-handler.dart';
import 'package:etelecom/widgets/bases/base-appbar.dart';
import 'package:etelecom/widgets/bases/base-material-button.dart';
import 'package:etelecom/widgets/bases/base-scaffold.dart';
import 'package:etelecom/widgets/bases/base-text-field.dart';
import 'package:etelecom/widgets/forms/search-textfield.dart';
import 'package:etelecom/widgets/list-view/list-view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:provider/provider.dart';

import 'contact-create.dart';
import 'contact-device-row.dart';
import 'contact-internal-row.dart';
import 'contact-general-row.dart';
import 'package:contacts_service/contacts_service.dart' as ContactDevice;
import 'package:permission_handler/permission_handler.dart';

enum ContactSearchType {
  general,
  internal,
  device
}

class ContactsSearchPage extends Page {
  ContactsSearchPage() : super(key: ValueKey('ContactsSearchPage'));

  @override
  Route createRoute(BuildContext context) {

    return PageRouteBuilder(
        settings: this,
        pageBuilder: (BuildContext context, animation, animation2) {
          var begin = Offset(1.0, 0.0);
          var end = Offset.zero;
          var tween = Tween(begin: begin, end: end);
          var offsetAnimation = animation.drive(tween);
          return SlideTransition(
            position: offsetAnimation,
            child: ContactsSearchScreen(),
          );
        }
    );

  }

}

class ContactsSearchScreen extends StatefulWidget {
  ContactsSearchScreen({Key? key, this.title}) : super(key: key);
  final String? title;

  @override
  _ContactsSearchScreenState createState() => _ContactsSearchScreenState();
}

class _ContactsSearchScreenState extends State<ContactsSearchScreen> {

  var searchController = TextEditingController();
  ContactSearchType searchType = ContactSearchType.device;

  List<Contact> searchedContacts = [];
  List<AccountUser> searchedAccountUsers = [];
  List<ContactDevice.Contact> searchedContactDevices = [];

  bool searching = false;

  String get searchLabel {
    switch(searchType) {
      case ContactSearchType.device:
        return "Tìm kiếm danh bạ từ máy";
      case ContactSearchType.internal:
        return "Tìm kiếm danh bạ nội bộ";
      case ContactSearchType.general:
        return "Tìm kiếm danh bạ chung";
      default:
        return "Tìm kiếm";
    }
  }

  String get searchHint {
    switch(searchType) {
      case ContactSearchType.internal:
        return "Tìm tên, số đt hoặc máy nhánh";
      case ContactSearchType.device:
      case ContactSearchType.general:
      default:
        return "Tìm tên hoặc số đt";
    }
  }

  bool get isEmpty {
    switch(searchType) {
      case ContactSearchType.internal:
        return searchLength == 0;
      case ContactSearchType.general:
        return searchLength == 0;
      case ContactSearchType.device:
      default:
        return searchLength == 0;
    }
  }

  int get searchLength {
    switch(searchType) {
      case ContactSearchType.internal:
        return searchedAccountUsers.length;
      case ContactSearchType.general:
        return searchedContacts.length;
      case ContactSearchType.device:
      default:
        return searchedContactDevices.length;
    }
  }

  Widget get searchResult {
    if (searchType == ContactSearchType.device) {
      if (context.watch<ContactProvider>().contactsDevice.length == 0) {
        return Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
                Image.asset(
                  'assets/images/empty_box.png',
                  width: 200.0,
                  fit: BoxFit.cover,
                ),
                Container(
                    padding: EdgeInsets.all(16),
                    child: Text(
                        'Danh bạ từ máy của bạn sẽ xuất hiện tại đây. Vui lòng cấp quyền truy cập danh bạ để đồng bộ.',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 13,
                            fontWeight:  FontWeight.w600
                        )
                    )
                ),
                Container(
                    child: MaterialButton(
                        elevation: 0,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5),
                        ),
                        padding: EdgeInsets.fromLTRB(18, 0, 18, 0),
                        onPressed: () => getContactsFromDevice(),
                        textColor: Colors.white,
                        color: MainTheme.primaryColor,
                        child: Text(
                            'Cấp quyền truy cập danh bạ',
                            style: TextStyle(
                                fontSize: 13
                            )
                        )
                    )
                )
            ]
        );
      }
    }

    return EListView(
      isLoading: searching,
      isEmpty: isEmpty,
      listLength: searchLength,
      itemChildBuilder: (BuildContext context, int index) {
        switch(searchType) {
          case ContactSearchType.internal:
            final AccountUser _row = searchedAccountUsers[index];
            return ContactInternalRow(
                accountUser: _row,
                key: ObjectKey(_row.id)
            );
          case ContactSearchType.general:
            final Contact _row = searchedContacts[index];
            return ContactGeneralRow(
                contact: _row,
                key: ObjectKey(_row.id),
                afterDeleteCb: () {
                  setState(() => searchedContacts.removeAt(index));
                },
                afterUpdateCb: (contact) {
                   setState(() => searchedContacts[index] = contact);
                }
            );
          case ContactSearchType.device:
          default:
            final ContactDevice.Contact _row = searchedContactDevices[index];
            return ContactDeviceRow(
                contact: _row,
                key: ObjectKey(_row)
            );
        }
      },
      onLoadMore: () async => null,
      onRefresh: () async => null
    );
  }

  void getContactsFromDevice() async {
    try {
      final _permissionGranted = await NativePermissions.checkPermissions([Permission.contacts]);
      if (!_permissionGranted) {
        return NativePermissions.goToAppSettings(
            context: context,
            title: "Quyền truy cập danh bạ",
            description: "eTelecom - Tổng đài CSKH muốn truy cập danh bạ đê hiển thị danh bạ từ máy"
        );
      }
      Iterable<ContactDevice.Contact> contacts = await ContactDevice.ContactsService.getContacts();
      contacts = contacts.where((contact) => contact.phones!.toList().length > 0);
      context.read<ContactProvider>().setContactsDevice(contacts.toList());
    } catch (e) {
      print("ERROR in ContactSearch.getContactsFromDevice $e");
    }
  }

  Widget searchTypeSelector({
    required String label,
    required VoidCallback onPressed,
    bool isActive = false
  }) {
    return Container(
        margin: EdgeInsets.only(right: 10),
        child: EMaterialButton(
            padding: EdgeInsets.only(left: 30, right: 30),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            onPressed: onPressed,
            child: Text(
                label,
                style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w400
                )
            ),
            textColor: isActive ? Colors.white : Colors.black,
            fillColor: isActive ? MainTheme.primaryColor : MainTheme.grayF5,
            height: 35
        )
      );
  }

  void switchSearchType(ContactSearchType type) {
    setState(() {
      searchType = type;
    });
    search();
  }

  Future<void> search() async {
    setState(() => searching = true);

    switch(searchType) {
      case ContactSearchType.internal:
        searchedAccountUsers = await AccountUserService.searchAccountUsers(searchController.text);
        break;
      case ContactSearchType.general:
        searchedContacts = await ContactService.searchContacts(searchController.text);
        break;
      case ContactSearchType.device:

        final _contactDevices = context.read<ContactProvider>().contactsDevice;

        if (searchController.text.isEmpty) {
          if (context.read<ContactProvider>().contactsDevice.length >= 20) {
            searchedContactDevices = _contactDevices.getRange(0, 20).toList();
          } else {
            searchedContactDevices = _contactDevices.getRange(0, _contactDevices.length).toList();
          }
        } else {
          searchedContactDevices = _contactDevices.where((element) {
            if (element.displayName == null || element.phones == null) return false;

            final comparedText = element.displayName! + element.phones!.toList()[0].value.toString();
            final searchText = StringHandler.makeSearchText(searchController.text);

            return StringHandler.makeSearchText(comparedText).contains(searchText);
          }).toList();
        }

        break;
      default:
    }

    setState(() => searching = false);
  }

  @override
  void initState() {
    super.initState();
    search();
  }

  @override
  Widget build(BuildContext context) {
    return EScaffold(
        appBar: EAppBar(
            title: "Tìm danh bạ"
        ),
        body: Column(
            children: [
                Container(
                    padding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                    margin: EdgeInsets.only(bottom: 10, top: 20),
                    child: ESearchTextField(
                        controller: searchController,
                        searchLabel: searchLabel,
                        searchHint: searchHint,
                        autoFocus: true,
                        onChanged: () => search(),
                    )
                ),
                Container(
                    margin: EdgeInsets.only(bottom: 10),
                    padding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                    child: Row(
                        children: [
                            searchTypeSelector(
                                label: "Từ máy",
                                onPressed: () => switchSearchType(ContactSearchType.device),
                                isActive: searchType == ContactSearchType.device
                            ),
                            searchTypeSelector(
                                label: "Nội bộ",
                                onPressed: () => switchSearchType(ContactSearchType.internal),
                                isActive: searchType == ContactSearchType.internal
                            ),
                            searchTypeSelector(
                                label: "Chung",
                                onPressed: () => switchSearchType(ContactSearchType.general),
                                isActive: searchType == ContactSearchType.general
                            )
                        ]
                    )
                ),
                Expanded(
                    child: searchResult
                ),
            ],
        ),
    );
  }
}

