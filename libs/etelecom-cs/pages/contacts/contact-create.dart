import 'package:etelecom/api/shop/contact.api.dart';
import 'package:etelecom/models/contact/Contact.dart';
import 'package:etelecom/services/contact.service.dart';
import 'package:etelecom/theme/theme.dart';
import 'package:etelecom/utils/toast.dart';
import 'package:etelecom/widgets/bases/base-appbar.dart';
import 'package:etelecom/widgets/bases/base-text-field.dart';
import 'package:flutter/material.dart';

class ContactCreate extends StatefulWidget {
  final Contact contact;

  ContactCreate({required this.contact});

  @override
  State<StatefulWidget> createState() => _ContactCreateState(contact);
}

class _ContactCreateState extends State<ContactCreate> {
  _ContactCreateState(Contact contact) {
    phoneController.text = contact.phone;
    fullnameController.text = contact.fullName;
  }

  final fullnameController = TextEditingController();
  final phoneController = TextEditingController();

  bool loading = false;

  Future<void> createContact(BuildContext context) async {
    setState(() {
      loading = true;
    });
    try {
      if (fullnameController.text.length > 0 && phoneController.text.length > 0) {
        final CreateContactRequest _req = CreateContactRequest(
            fullName: fullnameController.text,
            phone: phoneController.text,
        );
        await ContactService.createContact(context, _req);
        Navigator.of(context).pop();

        EToast.success(context, 'Tạo liên hệ thành công');
      } else {
        EToast.error(context, 'Vui lòng nhập đầy đủ tên và số điện thoại!');
      }

    } catch (e) {
      EToast.error(context, 'Tạo liên hệ không thành công. ${(e as dynamic)["msg"]}');
    }
    setState(() {
      loading = false;
    });
  }

  @override
  void dispose() {
    fullnameController.dispose();
    phoneController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: EAppBar(title: 'Tạo liên hệ chung'),
      body: Container(
          padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
          child: Column(children: [
            Container(
              margin: EdgeInsets.only(bottom: 20),
              child: ETextField(
                hintText: "Nhập tên",
                labelText: "Tên",
                textController: fullnameController,
              )
            ),
            Container(
              margin: EdgeInsets.only(bottom: 20),
              child: ETextField(
                keyboardType: TextInputType.phone,
                hintText: 'Nhập số điện thoại',
                labelText: 'Số điện thoại',
                textController: phoneController,
              )
            ),
            Container(
              child: Row(
                children: [
                  Expanded(
                    child: MaterialButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(7),
                      ),
                      onPressed: loading ? null : () {
                        createContact(context);
                      },
                      child: Text('Tạo',
                          style: TextStyle(
                              fontSize: 17,
                              fontWeight: FontWeight.w400
                          )
                      ),
                      textColor: Colors.white,
                      color: MainTheme.primaryColor,
                      height: 45
                    )
                  )
                ],
              )
            )]
          )
      ),
    );
  }
}
