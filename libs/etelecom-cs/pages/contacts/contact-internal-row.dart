import 'package:etelecom/app_config.dart';
import 'package:etelecom/models/extension/Extension.dart';
import 'package:etelecom/models/account-user/AccountUser.dart';
import 'package:etelecom/providers/authenticate/authenticate.provider.dart';
import 'package:etelecom/providers/account-user/account-user.provider.dart';
import 'package:etelecom/services/portsip.service.dart';
import 'package:etelecom/theme/theme.dart';
import 'package:etelecom/utils/toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

Future<void> callOut(BuildContext context, String phone, {bool isVideoCall = false}) async {
  try {
    await PortsipService.portsipCallOut(phone, isVideoCall: isVideoCall, context: context);
  } catch (e) {
    if ((e as dynamic)["code"] == "not_enough_balance") {
      EToast.error(context, "Không đủ số dư để thực hiện cuộc gọi");
    }
  }
}

class ContactInternalRow extends StatelessWidget {
  final AccountUser accountUser;

  ContactInternalRow({required this.accountUser, Key? key});

  @override
  Widget build(BuildContext context) {
    final _authenticateWatcher = portsipContext.read<AuthenticateProvider>();
    return Container(
        margin: EdgeInsets.only(bottom: 10),
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border.symmetric(
                vertical: BorderSide.none,
                horizontal: BorderSide(
                    color: MainTheme.grayF5
                )
            )
        ),
        child: MaterialButton(
            padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
            highlightColor: Colors.transparent,
            onPressed: () async {
              if (accountUser.userId != _authenticateWatcher.currentUser!.id) {
                callOut(context, accountUser.extensionNumber ?? "");
              }
            },
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                    Expanded(
                        child: ContactInfo(
                            relationship: accountUser
                        )
                    ),
                    if (accountUser.userId != _authenticateWatcher.currentUser!.id)
                        ContactManager(relationship: accountUser)
                ]
            )
        )
    );
  }
}

class ContactInfo extends StatelessWidget {
  final AccountUser relationship;

  ContactInfo({required this.relationship});

  @override
  Widget build(BuildContext context) {
    final _authenticateWatcher = portsipContext.read<AuthenticateProvider>();
    String extensionNumber = relationship.extensionNumber ?? "-";
    if (relationship.userId == _authenticateWatcher.currentUser!.id) {
      extensionNumber += ' - Máy nhánh của bạn';
    }
    return Row(
      children: [
        Container(
          child: Icon(
            Icons.account_circle_outlined,
            color: MainTheme.grayCCC,
            size: 50,
          ),
        ),
        Expanded(
            child: Container(
                padding: EdgeInsets.only(left: 10, right: 15),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                        Container(
                          margin: EdgeInsets.only(bottom: 2.5),
                          child: Text(
                            relationship.fullName ?? '-',
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                                color: MainTheme.gray555,
                                fontWeight: FontWeight.w800,
                                fontSize: 15
                            ),
                          ),
                        ),
                        Text(
                          extensionNumber,
                          style: TextStyle(
                            fontSize: 12,
                            color: MainTheme.gray555,
                          ),
                        )
                    ]
                )
            )
        )
      ],
    );
  }
}

class ContactManager extends StatelessWidget {
  final AccountUser relationship;

  ContactManager({required this.relationship});


  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
            child: PopupMenuButton(
              padding: EdgeInsets.zero,
              itemBuilder: (BuildContext context) {
                final List<Map<String, dynamic>> popupMenuItems = [
                  {
                    "title": "Gọi thoại",
                    "value": "audio_call",
                    "color": MainTheme.gray555
                  },
                  {
                    "title": "Gọi video",
                    "value": "video_call",
                    "color": MainTheme.gray555
                  },
                ];

                if (!AppConfig.videoCall) {
                  popupMenuItems.removeAt(1);
                }

                return popupMenuItems.map((item) => PopupMenuItem(
                    height: 35,
                    value: item["value"],
                    child: Text(item["title"],
                        style: TextStyle(color: item["color"], fontSize: 13)
                    )
                )).toList();
              },
              onSelected: (value) async {
                switch (value) {
                  case "audio_call":
                    return await callOut(context, relationship.extensionNumber ?? "");
                  case "video_call":
                    return await callOut(context, relationship.extensionNumber ?? "", isVideoCall: true);
                }
              },
              child: Container(
                height: 35,
                padding: EdgeInsets.symmetric(vertical: 2.5, horizontal: 10),
                decoration: BoxDecoration(
                    border: Border.all(color: MainTheme.grayF5),
                    borderRadius: BorderRadius.circular(20)),
                child: Row(
                  children: [
                    Icon(Icons.arrow_drop_down, size: 20),
                    Container(
                      child: Text(
                          'Thao tác',
                          style: TextStyle(fontSize: 13)
                      ),
                    )
                  ],
                ),
              ),
            )),
      ],
    );
  }
}
