import 'package:etelecom/api/shop/contact.api.dart';
import 'package:etelecom/models/contact/Contact.dart';
import 'package:etelecom/services/contact.service.dart';
import 'package:etelecom/theme/theme.dart';
import 'package:etelecom/utils/toast.dart';
import 'package:etelecom/widgets/bases/base-appbar.dart';
import 'package:etelecom/widgets/bases/base-text-field.dart';
import 'package:flutter/material.dart';

class ContactUpdate extends StatefulWidget {
  final Contact contact;
  final Function(Contact contact)? afterUpdateCb;

  ContactUpdate({
    required this.contact,
    this.afterUpdateCb
  });

  @override
  State<StatefulWidget> createState() => _ContactUpdateState(this.contact);
}

class _ContactUpdateState extends State<ContactUpdate> {
  _ContactUpdateState(Contact contact) {
    fullnameController.text = contact.fullName;
    phoneController.text = contact.phone;
  }


  final fullnameController = TextEditingController();
  final phoneController = TextEditingController();

  bool loading = false;

  Future<void> updateContact(BuildContext context, String contactID) async {
    setState(() {
      loading = true;
    });
    try {
      final UpdateContactRequest _req = UpdateContactRequest(
          fullName: fullnameController.text,
          phone: phoneController.text,
          id: contactID
      );
      await ContactService.updateContact(context: context, request: _req, afterUpdateCb: widget.afterUpdateCb);
      Navigator.of(context).pop();

      EToast.success(context, 'Cập nhật liên hệ thành công');
    } catch (e) {
      EToast.error(context, 'Cập nhật liên hệ không thành công. ${(e as dynamic)["msg"]}');
    }
    setState(() {
      loading = false;
    });
  }

  @override
  void dispose() {
    fullnameController.dispose();
    phoneController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: EAppBar(title: 'Chỉnh sửa liên hệ'),
      body: Container(
        padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
        child: Column(children: [
          Container(
            margin: EdgeInsets.only(bottom: 20),
            child: ETextField(
              hintText: 'Nhập tên',
              labelText: 'Tên',
              textController: fullnameController,
            )
          ),
          Container(
            margin: EdgeInsets.only(bottom: 20),
            child: ETextField(
              keyboardType: TextInputType.phone,
              hintText: 'Nhập số điện thoại',
              labelText: 'Số điện thoại',
              textController: phoneController,
            )
          ),
          Container(
            child: Row(
              children: [
                Expanded(
                  child: MaterialButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(7),
                    ),
                    onPressed: loading ? null : () {
                      updateContact(context, widget.contact.id);
                    },
                    child: Text('Cập nhật',
                      style: TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.w400
                      )
                    ),
                    textColor: Colors.white,
                    color: MainTheme.primaryColor,
                    height: 45
                  )
                )
              ],
            )
          )]
        )
      ),
    );
  }
}
