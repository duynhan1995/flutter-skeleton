import 'package:etelecom/app_config.dart';
import 'package:etelecom/models/contact/Contact.dart';
import 'package:etelecom/services/portsip.service.dart';
import 'package:etelecom/theme/theme.dart';
import 'package:etelecom/utils/toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

import 'contact-delete.dart';
import 'contact-update.dart';

import '../tickets/ticket-create.dart';

Future<void> callOut(BuildContext context, String phone, {bool isVideoCall = false}) async {
  try {
    await PortsipService.portsipCallOut(phone, isVideoCall: isVideoCall, context: context);
  } catch (e) {
    if ((e as dynamic)["code"] == "not_enough_balance") {
      EToast.error(context, "Không đủ số dư để thực hiện cuộc gọi");
    }
  }
}

class ContactGeneralRow extends StatelessWidget {
  final Contact contact;
  final Function? afterDeleteCb;
  final Function(Contact contact)? afterUpdateCb;

  ContactGeneralRow({
    required this.contact,
    Key? key,
    this.afterDeleteCb,
    this.afterUpdateCb
  });

  void alertDeleteContact(BuildContext context) {
    showDialog(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return ContactDelete(contact: contact, afterDeleteCb: afterDeleteCb);
      },
    );
  }

  void bottomSheetUpdateContact(BuildContext context) {
    CupertinoScaffold.showCupertinoModalBottomSheet(
        context: context,
        duration: Duration(milliseconds: 400),
        builder: (BuildContext context) {
          return ContactUpdate(contact: this.contact, afterUpdateCb: afterUpdateCb);
        }
    );
  }

  void bottomSheetTicketCreate(BuildContext context) {
    CupertinoScaffold.showCupertinoModalBottomSheet(
        context: context,
        duration: Duration(milliseconds: 400),
        builder: (BuildContext context) {
          return TicketCreate(
              contact: Contact(
                  id: contact.id,
                  phone: contact.phone,
                  fullName: contact.fullName
              )
          );
        }
    );
  }

  @override
  Widget build(BuildContext context) {

    Widget contactInfo = Row(
        children: [
            Container(
                child: Icon(
                  Icons.account_circle_outlined,
                  color: MainTheme.grayCCC,
                  size: 50,
                )
            ),
            Expanded(
                child: Container(
                    padding: EdgeInsets.only(left: 10, right: 15),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                          Container(
                              margin: EdgeInsets.only(bottom: 2.5),
                              child: Text(
                                  contact.fullName,
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                      color: MainTheme.gray555,
                                      fontWeight: FontWeight.w800,
                                      fontSize: 15
                                  )
                              )
                          ),
                          Text(
                              contact.phone,
                              style: TextStyle(
                                  fontSize: 12,
                                  color: MainTheme.gray555
                              )
                          )
                      ]
                    )
                )
            )
        ]
    );

    Widget contactManager = Row(
        children: [
            Container(
                child: PopupMenuButton(
                    padding: EdgeInsets.zero,
                    itemBuilder: (BuildContext context) {

                      final List<Map<String, dynamic>> popupMenuItems = [
                        {
                          "title": "Gọi thoại",
                          "value": "audio_call",
                          "color": MainTheme.gray555
                        },
                        {
                          "title": "Gọi video",
                          "value": "video_call",
                          "color": MainTheme.gray555
                        },
                        {
                          "title": "Chỉnh sửa",
                          "value": "update",
                          "color": MainTheme.gray555
                        },
                        {
                          "title": "Tạo ticket",
                          "value": "ticket",
                          "color": MainTheme.gray555
                        },
                        {
                          "title": "Xoá",
                          "value": "delete",
                          "color": Colors.red
                        }
                      ];

                      if (!AppConfig.videoCall) {
                        popupMenuItems.removeAt(1);
                      }

                      return popupMenuItems.map((item) => PopupMenuItem(
                          height: 35,
                          value: item["value"],
                          child: Text(item["title"],
                              style: TextStyle(color: item["color"], fontSize: 13)
                          )
                      )).toList();
                    },
                    onSelected: (value) async {
                      switch (value) {
                        case "audio_call":
                          return await callOut(context, contact.phone);
                        case "video_call":
                          return await callOut(context, contact.phone, isVideoCall: true);
                        case "update":
                          return bottomSheetUpdateContact(context);
                        case "ticket":
                          return bottomSheetTicketCreate(context);
                        case "delete":
                          return alertDeleteContact(context);
                      }
                    },
                    child: Container(
                        height: 35,
                        padding: EdgeInsets.symmetric(vertical: 2.5, horizontal: 10),
                        decoration: BoxDecoration(
                            border: Border.all(color: MainTheme.grayF5),
                            borderRadius: BorderRadius.circular(20)),
                        child: Row(
                            children: [
                              Icon(Icons.arrow_drop_down, size: 20),
                              Container(
                                child: Text('Thao tác', style: TextStyle(fontSize: 13)),
                              )
                            ]
                        )
                    )
                )
            )
        ]
    );

    return Container(
        margin: EdgeInsets.only(bottom: 10),
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border.symmetric(
                vertical: BorderSide.none,
                horizontal: BorderSide(color: MainTheme.grayF5)
            )
        ),
        child: MaterialButton(
          padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
          highlightColor: Colors.transparent,
          onPressed: () async {
            callOut(context, contact.phone);
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(child: contactInfo),
              contactManager
            ],
          ),
        )
    );
  }
}
