import 'package:etelecom/models/contact/Contact.dart';
import 'package:etelecom/models/account-user/AccountUser.dart';
import 'package:etelecom/providers/contact/contact.provider.dart';
import 'package:etelecom/providers/account-user/account-user.provider.dart';
import 'package:etelecom/providers/navigate/navigate.provider.dart';
import 'package:etelecom/services/contact.service.dart';
import 'package:etelecom/services/account-user.service.dart';
import 'package:etelecom/theme/theme.dart';
import 'package:etelecom/utils/native-permissions.dart';
import 'package:etelecom/widgets/bases/base-appbar.dart';
import 'package:etelecom/widgets/bases/base-scaffold.dart';
import 'package:etelecom/widgets/list-view/list-view.dart';
import 'package:etelecom/widgets/bases/base-menu.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:provider/provider.dart';

import 'contact-create.dart';
import 'contact-device-row.dart';
import 'contact-internal-row.dart';
import 'contact-general-row.dart';
import 'package:contacts_service/contacts_service.dart' as ContactDevice;
import 'package:permission_handler/permission_handler.dart';

import 'contact-search.dart';

class ContactsPage extends Page {
  ContactsPage() : super(key: ValueKey('ContactsPage'));

  @override
  Route createRoute(BuildContext context) {
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      context.read<ContactProvider>().setPaging(after: ".");
      context.read<AccountUserProvider>().setPaging(after: ".");
      ContactService.getContacts(context: context);
      AccountUserService.getAccountUsers(context: context);
    });

    return PageRouteBuilder(
        settings: this,
        pageBuilder: (BuildContext context, animation, animation2) {
          return ContactsScreen();
        });
  }
}

class ContactsScreen extends StatefulWidget {
  ContactsScreen({Key? key, this.title}) : super(key: key);
  final String? title;

  @override
  _ContactsScreenState createState() => _ContactsScreenState();
}

class _ContactsScreenState extends State<ContactsScreen>
    with SingleTickerProviderStateMixin {
  void bottomSheetCreateContact(BuildContext context) {
    CupertinoScaffold.showCupertinoModalBottomSheet(
        context: context,
        duration: Duration(milliseconds: 400),
        builder: (BuildContext context) {
          return ContactCreate(contact: Contact());
        });
  }

  void bottomSheetSearchContact(BuildContext context) {
    CupertinoScaffold.showCupertinoModalBottomSheet(
        context: context,
        duration: Duration(milliseconds: 400),
        builder: (BuildContext context) {
          return ContactsSearchScreen();
        });
  }

  int _currentIndex = 0;
  TabController? _tabController;

  @override
  void initState() {
    super.initState();
    getContactsFromDevice();
    _tabController = TabController(vsync: this, length: 3, initialIndex: _currentIndex);
  }

  @override
  void dispose() {
    _tabController!.dispose();
    super.dispose();
  }

  void getContactsFromDevice() async {
    try {
      final _permissionGranted = await NativePermissions.checkPermissions([
        Permission.contacts
      ]);
      if (_permissionGranted) {
        Iterable<ContactDevice.Contact> contacts = await ContactDevice.ContactsService.getContacts();
        contacts = contacts.where((contact) => contact.phones!.toList().length > 0);
        context.read<ContactProvider>().setContactsDevice(contacts.toList());
      }
    } catch (e) {
      print("ERROR in ContactsPage.getContactsFromDevice $e");
    }
  }

  void requestContactsPermission() {
    NativePermissions.goToAppSettings(
        context: context,
        title: "Quyền truy cập danh bạ",
        description: "eTelecom - Tổng đài CSKH muốn truy cập danh bạ đê hiển thị danh bạ từ máy"
    );
  }

  Widget contactGeneral() {
    return EListView(
      isLoading: context.watch<ContactProvider>().loadingContacts &&
        context.watch<ContactProvider>().contacts.isEmpty,
      isEmpty: context.watch<ContactProvider>().contacts.length == 0,
      currentNextPointer: context.watch<ContactProvider>().currentPaging.after,
      listLength: context.watch<ContactProvider>().contacts.length,
      itemChildBuilder: (BuildContext context, int index) {
        final Contact _row = context.watch<ContactProvider>().contacts[index];
        return ContactGeneralRow(
            contact: _row,
            key: ObjectKey(_row.id)
        );
      },
      onLoadMore: () async {
        // Wrapped in a WidgetsBinding.instance!.addPostFrameCallback BECAUSE
        // the context of `build` function cannot be accessed when all items in ListView
        // are not completely rendered.
        WidgetsBinding.instance!.addPostFrameCallback((_) async {
          await ContactService.getContacts(context: context);
        });
      },
      onRefresh: () async {
        context.read<ContactProvider>().setPaging(after: ".");
        await ContactService.getContacts(context: context);
      },
    );
  }

  Widget contactInternal() {
    return EListView(
      isLoading: context.watch<AccountUserProvider>().loadingAccountUsers &&
        context.watch<AccountUserProvider>().accountUsers.isEmpty,
      isEmpty: context.watch<AccountUserProvider>().accountUsers.length == 0,
      currentNextPointer: context.watch<AccountUserProvider>().currentPaging.after,
      listLength: context.watch<AccountUserProvider>().accountUsers.length,
      itemChildBuilder: (BuildContext context, int index) {
        final AccountUser _row = context.watch<AccountUserProvider>().accountUsers[index];
        return ContactInternalRow(
            accountUser: _row,
            key: ObjectKey(_row.id)
        );
      },
      onRefresh: () async {
        context.read<AccountUserProvider>().setPaging(after: ".");
        await AccountUserService.getAccountUsers(context: context);
      },
      onLoadMore: () async {
        WidgetsBinding.instance!.addPostFrameCallback((_) async {
          context.read<AccountUserProvider>().setLoading(true);
          await AccountUserService.getAccountUsers(context: context);
        });
      },
    );
  }

  Widget contactDevice() {
    if (context.read<ContactProvider>().contactsDevice.length == 0) {
      return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
              Image.asset(
                  'assets/images/empty_box.png',
                  width: 200.0,
                  fit: BoxFit.cover
              ),
              Container(
                  padding: EdgeInsets.all(16),
                  child: Text(
                      'Danh bạ từ máy của bạn sẽ xuất hiện tại đây. Vui lòng cấp quyền truy cập danh bạ để đồng bộ.',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 13,
                          fontWeight:  FontWeight.w600
                      )
                  )
              ),

              Container(
                  child: MaterialButton(
                      elevation: 0,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5),
                      ),
                      padding: EdgeInsets.fromLTRB(18, 0, 18, 0),
                      onPressed: () => requestContactsPermission(),
                      textColor: Colors.white,
                      color: MainTheme.primaryColor,
                      child: Text(
                          'Cấp quyền truy cập danh bạ',
                          style: TextStyle(fontSize: 13)
                      )
                  )
              )
          ]
      );
    } else {
      return EListView(
        isEmpty: context.read<ContactProvider>().contactsDevice.length == 0,
        listLength: context.read<ContactProvider>().contactsDevice.length,
        itemChildBuilder: (BuildContext context, int index) {
          final ContactDevice.Contact _contact = context.read<ContactProvider>().contactsDevice[index];
          return ContactDeviceRow(
              contact: _contact,
              key: ObjectKey(_contact)
          );
        },
        onRefresh: () async => getContactsFromDevice(),
        onLoadMore: () async => null,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return EScaffold(
        appBar: EAppBar(
          title: 'Danh bạ',
          leftActionButton: EAppBarActionButton(
              title: '',
              icon: Icons.search,
              onPressed: (context) => context.read<NavigateProvider>().navigate('/contacts/search'),
          ),
          rightActionButtons: [
            EAppBarActionButton(
                title: '+ Thêm',
                onPressed: (context) => bottomSheetCreateContact(context)
            )
          ],
        ),
        body: Column(
          children: [
            CupertinoSegmentedControl(
                padding: EdgeInsets.all(0),
                borderColor: Colors.white,
                selectedColor: Colors.white,
                children: {
                  0: Container(
                      padding: EdgeInsets.all(12.0),
                      width: MediaQuery.of(context).size.width / 3,
                      child: Center(
                          child: Text(
                              'Từ máy',
                              style: TextStyle(
                                  color: _currentIndex == 0
                                      ? MainTheme.primaryColor
                                      : MainTheme.gray888,
                                  fontWeight: _currentIndex == 0
                                      ? FontWeight.bold
                                      : FontWeight.normal
                              )
                          )
                      ),
                      decoration: BoxDecoration(
                          border: Border(
                              bottom: BorderSide(
                                  width: 3.0,
                                  color: _currentIndex == 0
                                      ? MainTheme.primaryColor
                                      : MainTheme.grayEEE
                              )
                          )
                      )
                  ),
                  1: Container(
                      padding: EdgeInsets.all(12.0),
                      width: MediaQuery.of(context).size.width / 3,
                      child: Center(
                          child: Text(
                              'Nội bộ',
                              style: TextStyle(
                                  color: _currentIndex == 1
                                      ? MainTheme.primaryColor
                                      : MainTheme.gray888,
                                  fontWeight: _currentIndex == 1
                                      ? FontWeight.bold
                                      : FontWeight.normal
                              )
                          )
                      ),
                      decoration: BoxDecoration(
                          border: Border(
                              bottom: BorderSide(
                                  width: 3.0,
                                  color: _currentIndex == 1
                                      ? MainTheme.primaryColor
                                      : MainTheme.grayEEE
                              )
                          )
                      )
                  ),
                  2: Container(
                      padding: EdgeInsets.all(12.0),
                      width: MediaQuery.of(context).size.width / 3,
                      child: Center(
                          child: Text(
                              'Chung',
                              style: TextStyle(
                                  color: _currentIndex == 2
                                      ? MainTheme.primaryColor
                                      : MainTheme.gray888,
                                  fontWeight: _currentIndex == 2
                                      ? FontWeight.bold
                                      : FontWeight.normal
                              )
                          )
                      ),
                      decoration: BoxDecoration(
                          border: Border(
                              bottom: BorderSide(
                                  width: 3.0,
                                  color: _currentIndex == 2
                                      ? MainTheme.primaryColor
                                      : MainTheme.grayEEE
                              )
                          )
                      )
                  )
                },
                groupValue: _currentIndex,
                onValueChanged: (newValue) {
                  _tabController!.animateTo(newValue as int);
                  setState(() {
                    _currentIndex = newValue;
                  });
                }),
            Expanded(
              child: TabBarView(
                  controller: _tabController,
                  // Restrict scroll by user
                  physics: const NeverScrollableScrollPhysics(),
                  children: [
                    contactDevice(),
                    contactInternal(),
                    contactGeneral()
                  ]),
            ),
          ],
        ),
        bottomMenu: EMenu(menuItems: [
            MenuItem(
                icon: Icons.phone_in_talk_outlined,
                label: 'Nghe gọi',
                route: '/call-center'
            ),
            MenuItem(
                icon: Icons.confirmation_number_outlined,
                label: 'Ticket',
                route: '/tickets'
            ),
            MenuItem(
                icon: Icons.list,
                label: 'Lịch sử',
                route: '/call-logs'
            ),
            MenuItem(
                icon: Icons.account_circle_outlined,
                label: 'Danh bạ',
                route: '/contacts'
            ),
            MenuItem(
                icon: Icons.settings_outlined,
                label: 'Thiết lập',
                route: '/setting'
            )
        ])
    );
  }
}
