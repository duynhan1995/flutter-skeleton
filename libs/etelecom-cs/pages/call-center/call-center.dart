import 'package:etelecom/widgets/calling/calling-state-incoming.dart';
import 'package:etelecom/widgets/calling/calling-state-none.dart';
import 'package:etelecom/widgets/calling/calling-state-outgoing.dart';
import 'package:etelecom/widgets/calling/calling-state-processing.dart';
import 'package:flutter/material.dart';

class CallCenterPage extends Page {
  CallCenterPage() : super(key: ValueKey('CallCenterPage'));

  @override
  Route createRoute(BuildContext context) {
    return PageRouteBuilder(
        settings: this,
        pageBuilder: (BuildContext context, animation, animation2) {
          return CallCenterScreen();
        });
  }
}

class CallCenterScreen extends StatefulWidget {
  CallCenterScreen({Key? key, this.title}) : super(key: key);
  final String? title;

  @override
  _CallCenterScreenState createState() => _CallCenterScreenState();
}

class _CallCenterScreenState extends State<CallCenterScreen> {
  @override
  Widget build(BuildContext context) {
    return CallingStateNone();
  }
}
