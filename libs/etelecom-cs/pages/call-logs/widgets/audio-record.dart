import 'package:etelecom/providers/call-log/call-log.provider.dart';
import 'package:etelecom/theme/theme.dart';
import 'package:etelecom/utils/string-handler.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AudioRecord extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return _AudioRecordState();
  }

}

class _AudioRecordState extends State<AudioRecord> {

  @override
  Widget build(BuildContext context) {
    final audioPlayingWatcher = context.watch<CallLogProvider>().playing;
    final positionWatcher = context.watch<CallLogProvider>().currentPosition;
    final durationWatcher = context.watch<CallLogProvider>().currentDuration;
    final audioMutedWatcher = context.watch<CallLogProvider>().muted;

    return Row(
      children: [
        IconButton(
            icon: Icon(audioPlayingWatcher ? Icons.pause : Icons.play_arrow),
            iconSize: 20,
            padding: EdgeInsets.zero,
            splashRadius: 20,
            onPressed: () {
              if (audioPlayingWatcher) {
                context.read<CallLogProvider>().pauseAudio();
              } else {
                context.read<CallLogProvider>().continueAudio();
              }
            }
        ),
        Text('${StringHandler.compactSecondsToTime(positionWatcher)} / '),
        Text(StringHandler.compactSecondsToTime(durationWatcher)),
        Expanded(child: Container(
          padding: EdgeInsets.symmetric(vertical: 0, horizontal: 8),
          child: LinearProgressIndicator(
            value: durationWatcher > 0 ? (positionWatcher / durationWatcher) : 0,
            valueColor: AlwaysStoppedAnimation(MainTheme.primaryColor),
            backgroundColor: MainTheme.grayEEE,
          ),
        )),
        IconButton(
            icon: Icon(audioMutedWatcher ? Icons.volume_off : Icons.volume_up),
            iconSize: 20,
            padding: EdgeInsets.zero,
            splashRadius: 20,
            onPressed: () {
              if (audioMutedWatcher) {
                context.read<CallLogProvider>().unmuteAudio();
              } else {
                context.read<CallLogProvider>().muteAudio();
              }
            }
        ),
        IconButton(
            icon: Icon(Icons.close),
            iconSize: 20,
            padding: EdgeInsets.zero,
            splashRadius: 20,
            onPressed: () async {
              await context.read<CallLogProvider>().stopAudio();
            }
        ),
      ],
    );
  }

}
