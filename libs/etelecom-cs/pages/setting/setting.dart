import 'package:etelecom/providers/authenticate/authenticate.provider.dart';
import 'package:etelecom/providers/navigate/navigate.provider.dart';
import 'package:etelecom/theme/theme.dart';
import 'package:etelecom/widgets/avatar/avatar.dart';
import 'package:etelecom/widgets/bases/base-appbar.dart';
import 'package:etelecom/widgets/bases/base-menu.dart';
import 'package:etelecom/widgets/bases/base-scaffold.dart';
import 'package:etelecom/widgets/static-toast/network-status.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'widgets/setting-card.dart';
import 'widgets/setting-menu.dart';

class SettingPage extends Page {
  SettingPage() : super(key: ValueKey('SettingPage'));

  @override
  Route createRoute(BuildContext context) {
    return PageRouteBuilder(
        settings: this,
        pageBuilder: (BuildContext context, animation, animation2) {
          return SettingScreen();
        });
  }
}

class SettingScreen extends StatefulWidget {
  SettingScreen({Key? key, this.title}) : super(key: key);
  final String? title;

  @override
  _SettingScreenState createState() => _SettingScreenState();
}

class _SettingScreenState extends State<SettingScreen> {
  @override
  Widget build(BuildContext context) {
    final currentShopWatcher =
        context.watch<AuthenticateProvider>().currentShop;
    final currentUserWatcher =
        context.watch<AuthenticateProvider>().currentUser;

    return EScaffold(
        appBar: EAppBar(title: 'Thiết lập'),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.all(20),
              decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border(bottom: BorderSide(color: MainTheme.grayF5))),
              child: Row(
                children: [
                  EAvatar(
                    imageUrl: currentShopWatcher?.imageUrl ?? "",
                    width: 60,
                    height: 60,
                    margin: EdgeInsets.only(right: 10),
                  ),
                  Expanded(
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                              Container(
                                child: Text(
                                  currentUserWatcher?.full_name ?? "",
                                  style: TextStyle(
                                      fontWeight: FontWeight.w800, fontSize: 20),
                                ),
                              ),
                              Container(
                                child: Text(
                                  '${currentShopWatcher?.name} - ${currentShopWatcher?.code}',
                                  style: TextStyle(fontSize: 12),
                                ),
                              )
                          ],
                      )
                  )
                ],
              ),
            ),
            SettingMenu(
                label: 'Thông tin tài khoản',
                icon: Icons.supervised_user_circle,
                onPressed: () {
                  context.read<NavigateProvider>().navigate('/setting/account');
                }),
            SettingMenu(
                label: 'Danh sách cửa hàng',
                icon: Icons.storefront_outlined,
                onPressed: () {
                  context.read<NavigateProvider>().navigate('/setting/shops');
                }),
            SettingCard(
              child: MaterialButton(
                  onPressed: () =>
                      context.read<AuthenticateProvider>().updateToken(""),
                  padding: EdgeInsets.fromLTRB(16, 7, 16, 7),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                          child: Icon(
                            Icons.logout,
                            size: 20,
                            color: Colors.red[400],
                          ),
                          margin: EdgeInsets.only(right: 10)),
                      Expanded(
                        child: Text('Đăng xuất',
                            style: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                                color: Colors.red[400])),
                      ),
                    ],
                  )),
            )
          ],
        ),
        bottomMenu: EMenu(menuItems: [
          MenuItem(icon: Icons.phone_in_talk_outlined, label: 'Nghe gọi', route: '/call-center'),
          MenuItem(icon: Icons.confirmation_number_outlined, label: 'Ticket', route: '/tickets'),
          MenuItem(icon: Icons.list, label: 'Lịch sử', route: '/call-logs'),
          MenuItem(icon: Icons.account_circle_outlined, label: 'Danh bạ', route: '/contacts'),
          MenuItem(icon: Icons.settings_outlined, label: 'Thiết lập', route: '/setting'),
        ]));
  }
}
