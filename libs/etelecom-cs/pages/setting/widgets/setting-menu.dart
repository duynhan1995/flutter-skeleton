import 'package:etelecom/theme/theme.dart';
import 'package:flutter/material.dart';

import 'setting-card.dart';

class SettingMenu extends StatelessWidget {
  final String label;
  final VoidCallback onPressed;
  final IconData? icon;

  SettingMenu({
    required this.label,
    this.icon,
    required this.onPressed
  });

  @override
  Widget build(BuildContext context) {
    return SettingCard(
      child: MaterialButton(
        onPressed: onPressed,
        padding: EdgeInsets.all(20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(child: Row(
              children: [
                Container(
                  child: Icon(icon, size: 20),
                  margin: EdgeInsets.only(right: 10)
                ),
                Text(
                  label,
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontSize: 15,
                  ),
                )
              ],
            )),
            Icon(Icons.chevron_right)
          ],
        ),
      ),
    );
  }

}
