import 'package:etelecom/api/etop/user-api.dart';
import 'package:etelecom/theme/theme.dart';
import 'package:etelecom/utils/toast.dart';
import 'package:etelecom/widgets/bases/base-appbar.dart';
import 'package:etelecom/widgets/bases/base-text-field.dart';
import 'package:flutter/material.dart';

class ChangePassword extends StatefulWidget {
  ChangePassword();

  @override
  State<StatefulWidget> createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {
  final currentPasswordController = TextEditingController();
  final newPasswordController = TextEditingController();
  final confirmPasswordController = TextEditingController();

  bool loading = false;

  Future<void> changePassword(BuildContext context) async {
    setState(() {
      loading = true;
    });
    try {
      if (currentPasswordController.text.isNotEmpty &&
          newPasswordController.text.isNotEmpty &&
          confirmPasswordController.text.isNotEmpty) {
        if (newPasswordController.text != confirmPasswordController.text) {
          setState(() {
            loading = false;
          });
          return EToast.error(context, 'Mật khẩu xác nhận không trùng khớp');
        }
        await UserApi.changePassword(
            currentPasswordController.text,
            newPasswordController.text,
            confirmPasswordController.text
        );
        Navigator.of(context).pop();

        EToast.success(context, 'Đổi mật khẩu thành công');
      } else {
        EToast.error(context, 'Vui lòng nhập đầy đủ thông tin');
      }
    } catch (e) {
      EToast.error(context, 'Đổi mật khẩu thất bại. ${(e as dynamic)["msg"]}');
    }
    setState(() {
      loading = false;
    });
  }

  @override
  void dispose() {
    currentPasswordController.dispose();
    newPasswordController.dispose();
    confirmPasswordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: EAppBar(title: 'Đổi mật khẩu'),
      body: Container(
          padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
          child: Column(children: [
            Container(
                margin: EdgeInsets.only(bottom: 20),
                child: ETextField(
                    hintText: 'Mật khẩu hiện tại',
                    labelText: 'Mật khẩu hiện tại',
                    textController: currentPasswordController,
                    obscureText: true
                )
            ),
            Container(
                margin: EdgeInsets.only(bottom: 20),
                child: ETextField(
                    hintText: 'Mật khẩu mới',
                    labelText: 'Mật khẩu mới',
                    textController: newPasswordController,
                    obscureText: true
                )
            ),
            Container(
                margin: EdgeInsets.only(bottom: 20),
                child: ETextField(
                    hintText: 'Xác nhận mật khẩu mới',
                    labelText: 'Xác nhận mật khẩu mới',
                    textController: confirmPasswordController,
                    obscureText: true
                )
            ),
            Container(
                child: Row(
                    children: [
                        Expanded(
                            child: MaterialButton(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(7),
                                ),
                                onPressed: loading ? null : () => changePassword(context),
                                child: Text('Xác nhận',
                                    style: TextStyle(
                                        fontSize: 17,
                                        fontWeight: FontWeight.w400
                                    )
                                ),
                                textColor: Colors.white,
                                color: MainTheme.primaryColor,
                                height: 45
                            )
                        )
                    ],
                )
            )
          ])
      ),
    );
  }
}
