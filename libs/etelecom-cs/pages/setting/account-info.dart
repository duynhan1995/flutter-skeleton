import 'package:etelecom/providers/authenticate/authenticate.provider.dart';
import 'package:etelecom/widgets/bases/base-appbar.dart';
import 'package:etelecom/widgets/bases/base-scaffold.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'change-password.dart';

class AccountInfoPage extends Page {
  AccountInfoPage() : super(key: ValueKey('AccountInfoPage'));

  @override
  Route createRoute(BuildContext context) {
    return PageRouteBuilder(
        settings: this,
        pageBuilder: (BuildContext context, animation, animation2) {
          var begin = Offset(1.0, 0.0);
          var end = Offset.zero;
          var tween = Tween(begin: begin, end: end);
          var offsetAnimation = animation.drive(tween);
          return SlideTransition(
            position: offsetAnimation,
            child: AccountInfoScreen(),
          );
        });
  }
}

class AccountInfoScreen extends StatefulWidget {
  AccountInfoScreen({Key? key, this.title}) : super(key: key);

  final String? title;

  @override
  State<StatefulWidget> createState() => _AccountInfoScreenState();
}

class _AccountInfoScreenState extends State<AccountInfoScreen> {
  void bottomSheetChangePassword(BuildContext context) {
    showCupertinoModalBottomSheet(
        context: context,
        duration: Duration(milliseconds: 400),
        builder: (BuildContext context) {
          return ChangePassword();
        });
  }

  @override
  Widget build(BuildContext context) {
    final currentUserWatcher =
        context.watch<AuthenticateProvider>().currentUser;

    return EScaffold(
        appBar: EAppBar(title: 'Thông tin tài khoản'),
        body: Container(
          padding: EdgeInsets.fromLTRB(0, 12.0, 0, 0),
          child: Column(
            children: [
              Container(
                  padding: EdgeInsets.all(16.0),
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(width: 1.0, color: Colors.grey.shade100),
                    ),
                    color: Colors.white,
                  ),
                  child: Row(
                    children: <Widget>[
                      Text('Họ tên', textAlign: TextAlign.left),
                      Expanded(
                        child: Text(
                            currentUserWatcher!.full_name ?? "",
                            textAlign: TextAlign.right,
                            style: TextStyle(
                                fontSize: 14, fontWeight: FontWeight.bold)),
                      ),
                    ],
                  )),
              Container(
                  padding: EdgeInsets.all(16.0),
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(width: 1.0, color: Colors.grey.shade100),
                    ),
                    color: Colors.white,
                  ),
                  child: Row(
                    children: <Widget>[
                      Text('Số điện thoại', textAlign: TextAlign.left),
                      Expanded(
                        child: Text(
                            currentUserWatcher.phone ?? "",
                            textAlign: TextAlign.right,
                            style: TextStyle(
                                fontSize: 14, fontWeight: FontWeight.bold)),
                      ),
                    ],
                  )),
              Container(
                  color: Colors.white,
                  padding: EdgeInsets.all(16.0),
                  margin: EdgeInsets.fromLTRB(0, 0, 0, 16.0),
                  child: Row(
                    children: <Widget>[
                      Text('Email', textAlign: TextAlign.left),
                      Expanded(
                        child: Text(
                            currentUserWatcher.email ?? "",
                            textAlign: TextAlign.right,
                            style: TextStyle(
                                fontSize: 14, fontWeight: FontWeight.bold)),
                      ),
                    ],
                  )),
              FractionallySizedBox(
                widthFactor: 0.95,
                child: MaterialButton(
                  onPressed: () => bottomSheetChangePassword(context),
                  textColor: Colors.grey,
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0),
                      side: BorderSide(color: Colors.grey.shade400)),
                  child: const Text('Đổi mật khẩu',
                      style: TextStyle(fontSize: 14)),
                ),
              )
            ],
          ),
        ));
  }
}
