class EtelecomCsRoutePath {
  final String path;

  EtelecomCsRoutePath.login() : path = "/";
  EtelecomCsRoutePath.callCenter() : path = "/call-center";
  EtelecomCsRoutePath.callLogs() : path = "/call-logs";
  EtelecomCsRoutePath.orders() : path = "/orders";
  EtelecomCsRoutePath.contacts() : path = "/contacts";
  EtelecomCsRoutePath.contactsSearch() : path = "/contacts/search";
  EtelecomCsRoutePath.tickets() : path = "/tickets";
  EtelecomCsRoutePath.ticketDetail() : path = "/tickets/detail";
  EtelecomCsRoutePath.setting() : path = "/setting";
  EtelecomCsRoutePath.settingShopsList() : path = "/setting/shops";
  EtelecomCsRoutePath.accountInfo() : path = "/setting/account";

  bool get isLoginPage => path == "/";
  bool get isCallCenterPage => path == "/call-center";
  bool get isCallLogsPage => path == "/call-logs";
  bool get isOrdersPage => path == "/orders";
  bool get isContactsPage => path == "/contacts";
  bool get isContactsSearch => path == "/contacts/search";
  bool get isTicketPage => path == "/tickets";
  bool get isTicketDetail => path == "/tickets/detail";
  bool get isSettingPage => path == "/setting";
  bool get isSettingShopsListPage => path == "/setting/shops";
  bool get isAccountInfoPage => path == "/setting/account";
}
