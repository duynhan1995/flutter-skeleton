import 'package:flutter/widgets.dart';

import 'etelecom-cs-route-path.dart';

class EtelecomCsRouteInformationParser
    extends RouteInformationParser<EtelecomCsRoutePath> {
  @override
  Future<EtelecomCsRoutePath> parseRouteInformation(
      RouteInformation routeInformation) async {
    final uri = Uri.parse(routeInformation.location!);

    // Handle "/"

    if (uri.pathSegments.length == 0) {
      return EtelecomCsRoutePath.login();
    }

    if (uri.pathSegments.length == 1) {
      switch (uri.pathSegments[0]) {
        // Handle "/call-center"
        case "call-center":
          return EtelecomCsRoutePath.callCenter();
        // Handle "/call-logs"
        case "call-logs":
          return EtelecomCsRoutePath.callLogs();
        // Handle "/contacts"
        case "contacts":
          return EtelecomCsRoutePath.contacts();
        // Handle "/orders"
        case "orders":
          return EtelecomCsRoutePath.orders();
        // Handle "/tickets"
        case "tickets":
          return EtelecomCsRoutePath.tickets();
        // Handle "/settings"
        case "setting":
          return EtelecomCsRoutePath.setting();
        default:
          return EtelecomCsRoutePath.login();
      }
    }

    if (uri.pathSegments.length == 2) {
      if (uri.pathSegments[0] == "contacts") {
        switch (uri.pathSegments[1]) {
          case "search":
            return EtelecomCsRoutePath.contactsSearch();
          default:
            return EtelecomCsRoutePath.login();
        }
      }

      if (uri.pathSegments[0] == "tickets") {
        switch (uri.pathSegments[1]) {
          case "detail":
            return EtelecomCsRoutePath.ticketDetail();
          default:
            return EtelecomCsRoutePath.login();
        }
      }

      if (uri.pathSegments[0] == "setting") {
        switch (uri.pathSegments[1]) {
          case "shops":
            return EtelecomCsRoutePath.settingShopsList();
          case "account":
            return EtelecomCsRoutePath.accountInfo();
          default:
            return EtelecomCsRoutePath.login();
        }
      }
    }
    return EtelecomCsRoutePath.login();
  }

  @override
  RouteInformation? restoreRouteInformation(EtelecomCsRoutePath path) {
    if (path.isLoginPage) {
      return RouteInformation(location: "/");
    }
    if (path.isCallCenterPage) {
      return RouteInformation(location: "/call-center");
    }
    if (path.isCallLogsPage) {
      return RouteInformation(location: "/call-logs");
    }
    if (path.isOrdersPage) {
      return RouteInformation(location: "/orders");
    }
    if (path.isContactsPage) {
      return RouteInformation(location: "/contacts");
    }
    if (path.isContactsSearch) {
      return RouteInformation(location: "/contacts/search");
    }
    if (path.isTicketPage) {
      return RouteInformation(location: "/tickets");
    }
    if (path.isTicketDetail) {
      return RouteInformation(location: "/tickets/detail");
    }
    if (path.isSettingPage) {
      return RouteInformation(location: "/setting");
    }
    if (path.isSettingShopsListPage) {
      return RouteInformation(location: "/setting/shops");
    }
    if (path.isAccountInfoPage) {
      return RouteInformation(location: "/setting/account");
    }

    return null;
  }
}
