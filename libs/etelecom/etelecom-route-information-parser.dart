import 'package:flutter/widgets.dart';

import 'etelecom-route-path.dart';

class EtelecomRouteInformationParser
    extends RouteInformationParser<EtelecomRoutePath> {
  @override
  Future<EtelecomRoutePath> parseRouteInformation(
      RouteInformation routeInformation) async {
    final uri = Uri.parse(routeInformation.location!);

    // Handle "/"

    if (uri.pathSegments.length == 0) {
      return EtelecomRoutePath.login();
    }

    if (uri.pathSegments.length == 1) {
      switch (uri.pathSegments[0]) {
        // Handle "/call-center"
        case "call-center":
          return EtelecomRoutePath.callCenter();
        // Handle "/call-logs"
        case "call-logs":
          return EtelecomRoutePath.callLogs();
        // Handle "/contacts"
        case "contacts":
          return EtelecomRoutePath.contacts();
        // Handle "/orders"
        case "orders":
          return EtelecomRoutePath.orders();
        // Handle "/settings"
        case "setting":
          return EtelecomRoutePath.setting();
        default:
          return EtelecomRoutePath.login();
      }
    }

    if (uri.pathSegments.length == 2) {
      if (uri.pathSegments[0] == "setting") {
        switch (uri.pathSegments[1]) {
          case "shops":
            return EtelecomRoutePath.settingShopsList();
          default:
            return EtelecomRoutePath.login();
        }
      }
    }

    return EtelecomRoutePath.login();
  }

  @override
  RouteInformation? restoreRouteInformation(EtelecomRoutePath path) {
    if (path.isLoginPage) {
      return RouteInformation(location: "/");
    }
    if (path.isCallCenterPage) {
      return RouteInformation(location: "/call-center");
    }
    if (path.isCallLogsPage) {
      return RouteInformation(location: "/call-logs");
    }
    if (path.isContactsPage) {
      return RouteInformation(location: "/contacts");
    }
    if (path.isOrdersPage) {
      return RouteInformation(location: "/orders");
    }
    if (path.isSettingPage) {
      return RouteInformation(location: "/setting");
    }
    if (path.isSettingShopsListPage) {
      return RouteInformation(location: "/setting/shops");
    }

    return null;
  }
}
