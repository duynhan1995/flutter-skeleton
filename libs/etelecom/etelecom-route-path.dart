class EtelecomRoutePath {
  final String path;

  EtelecomRoutePath.login() : path = "/";
  EtelecomRoutePath.callCenter() : path = "/call-center";
  EtelecomRoutePath.callLogs() : path = "/call-logs";
  EtelecomRoutePath.contacts() : path = "/contacts";
  EtelecomRoutePath.orders() : path = "/orders";
  EtelecomRoutePath.setting() : path = "/setting";
  EtelecomRoutePath.settingShopsList() : path = "/setting/shops";

  bool get isLoginPage => path == "/";
  bool get isCallCenterPage => path == "/call-center";
  bool get isCallLogsPage => path == "/call-logs";
  bool get isContactsPage => path == "/contacts";
  bool get isOrdersPage => path == "/orders";
  bool get isSettingPage => path == "/setting";
  bool get isSettingShopsListPage => path == "/setting/shops";
}
