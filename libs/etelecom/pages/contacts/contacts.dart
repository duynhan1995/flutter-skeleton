import 'package:etelecom/models/contact/Contact.dart';
import 'package:etelecom/models/extension/Extension.dart';
import 'package:etelecom/providers/contact/contact.provider.dart';
import 'package:etelecom/providers/extension/extension.provider.dart';
import 'package:etelecom/services/contact.service.dart';
import 'package:etelecom/services/extension.service.dart';
import 'package:etelecom/services/account-user.service.dart';
import 'package:etelecom/theme/theme.dart';
import 'package:etelecom/widgets/bases/base-appbar.dart';
import 'package:etelecom/widgets/bases/base-scaffold.dart';
import 'package:etelecom/widgets/list-view/list-view.dart';
import 'package:etelecom/widgets/bases/base-menu.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:provider/provider.dart';

import 'contact-create.dart';
import 'contact-internal-row.dart';
import 'contact-general-row.dart';

class ContactsPage extends Page {
  ContactsPage() : super(key: ValueKey('ContactsPage'));

  @override
  Route createRoute(BuildContext context) {
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      context.read<ContactProvider>().setPaging(after: ".");
      ContactService.getContacts(context: context);
      ExtensionService.getExtensions(context: context);
      AccountUserService.getAccountUsers(context: context);
    });

    return PageRouteBuilder(
        settings: this,
        pageBuilder: (BuildContext context, animation, animation2) {
          return ContactsScreen();
        });
  }
}

class ContactsScreen extends StatefulWidget {
  ContactsScreen({Key? key, this.title}) : super(key: key);
  final String? title;

  @override
  _ContactsScreenState createState() => _ContactsScreenState();
}

class _ContactsScreenState extends State<ContactsScreen>
    with SingleTickerProviderStateMixin {
  void bottomSheetCreateContact(BuildContext context) {
    CupertinoScaffold.showCupertinoModalBottomSheet(
        context: context,
        duration: Duration(milliseconds: 400),
        builder: (BuildContext context) {
          return ContactCreate(contact: Contact(id: null));
        });
  }

  int _currentIndex = 0;
  TabController? _tabController;

  @override
  void initState() {
    super.initState();
    _tabController =
        TabController(vsync: this, length: 2, initialIndex: _currentIndex);
  }

  @override
  void dispose() {
    _tabController!.dispose();
    super.dispose();
  }

  Widget contactGeneral() {
    return EListView(
      isLoading: context.watch<ContactProvider>().loadingContacts,
      isEmpty: context.watch<ContactProvider>().contacts.length == 0,
      currentNextPointer: context.watch<ContactProvider>().currentPaging.after,
      listLength: context.watch<ContactProvider>().contacts.length,
      itemChildBuilder: (BuildContext context, int index) {
        final Contact contactRow =
        context.watch<ContactProvider>().contacts[index];
        return ContactGeneralRow(contact: contactRow, key: ObjectKey(contactRow.id));
      },
      onLoadMore: () async {
        // Wrapped in a WidgetsBinding.instance!.addPostFrameCallback BECAUSE
        // the context of `build` function cannot be accessed when all items in ListView
        // are not completely rendered.
        WidgetsBinding.instance!.addPostFrameCallback((_) async {
          context.read<ContactProvider>().setLoading(true);

          Future.delayed(Duration(seconds: 1), () async {
            await ContactService.getContacts(context: context);
          });
        });
      },
      onRefresh: () async {
        context.read<ContactProvider>().setPaging(after: ".");
        await ContactService.getContacts(context: context);
      },
    );
  }

  Widget contactInternal() {
    return EListView(
      isEmpty: context.read<ExtensionProvider>().extensions.length == 0,
      listLength: context.read<ExtensionProvider>().extensions.length,
      itemChildBuilder: (BuildContext context, int index) {
        final Extension _contactExtension =
        context.read<ExtensionProvider>().extensions[index];
        return ContactInternalRow(
            extension: _contactExtension,
            key: ObjectKey(_contactExtension.id));
      },
      onRefresh: () async {
        await ExtensionService.getExtensions(context: context);
      },
      onLoadMore: () async {},
    );
  }

  @override
  Widget build(BuildContext context) {
    return EScaffold(
        appBar: EAppBar(
          title: 'Danh bạ',
          rightActionButtons: [
            EAppBarActionButton(
                title: '+ Thêm',
                onPressed: (context) => bottomSheetCreateContact(context))
          ],
        ),
        body: Column(
          children: <Widget>[
            CupertinoSegmentedControl(
                padding: EdgeInsets.all(0),
                borderColor: Colors.white,
                selectedColor: Colors.white,
                children: {
                  0: Container(
                      padding: EdgeInsets.all(12.0),
                      width: MediaQuery.of(context).size.width / 2,
                      child: Center(
                        child: Text(
                          'Danh bạ chung',
                          style: TextStyle(
                              color: _currentIndex == 0
                                  ? MainTheme.primaryColor
                                  : MainTheme.gray888,
                              fontWeight: _currentIndex == 0
                                  ? FontWeight.bold
                                  : FontWeight.normal),
                        ),
                      ),
                      decoration: BoxDecoration(
                          border: Border(
                              right: BorderSide(
                                  width: 1.0, color: MainTheme.grayF5),
                              bottom: BorderSide(
                                  width: 3.0,
                                  color: _currentIndex == 0
                                      ? MainTheme.primaryColor
                                      : MainTheme.grayEEE)))),
                  1: Container(
                      padding: EdgeInsets.all(8.0),
                      width: MediaQuery.of(context).size.width / 2,
                      child: Center(
                        child: Text(
                          'Nội bộ',
                          style: TextStyle(
                              color: _currentIndex == 0
                                  ? MainTheme.gray888
                                  : MainTheme.primaryColor,
                              fontWeight: _currentIndex == 1
                                  ? FontWeight.bold
                                  : FontWeight.normal),
                        ),
                      ),
                      decoration: BoxDecoration(
                          border: Border(
                              bottom: BorderSide(
                                  width: 3.0,
                                  color: _currentIndex == 0
                                      ? MainTheme.grayEEE
                                      : MainTheme.primaryColor)))),
                },
                groupValue: _currentIndex,
                onValueChanged: (newValue) {
                  _tabController!.animateTo(newValue as int);
                  setState(() {
                    _currentIndex = newValue;
                  });
                }),
            Expanded(
              child: TabBarView(
                  controller: _tabController,
                  // Restrict scroll by user
                  physics: const NeverScrollableScrollPhysics(),
                  children: [
                    // Sign In View
                    contactGeneral(),
                    // Sign Up View
                    contactInternal()
                  ]),
            ),
          ],
        ),
        bottomMenu: EMenu(menuItems: [
          MenuItem(
              icon: Icons.phone_in_talk_outlined,
              label: 'Nghe gọi',
              route: '/call-center'),
          MenuItem(icon: Icons.list, label: 'Lịch sử', route: '/call-logs'),
          MenuItem(
              icon: Icons.account_circle_outlined,
              label: 'Danh bạ',
              route: '/contacts'),
          MenuItem(
              icon: Icons.settings_outlined,
              label: 'Thiết lập',
              route: '/setting'),
        ]));
  }
}
