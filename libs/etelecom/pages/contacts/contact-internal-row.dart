import 'package:etelecom/models/extension/Extension.dart';
import 'package:etelecom/providers/account-user/account-user.provider.dart';
import 'package:etelecom/services/portsip.service.dart';
import 'package:etelecom/theme/theme.dart';
import 'package:etelecom/utils/toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

Future<void> callOut(BuildContext context, String phone) async {
  try {
    await PortsipService.portsipCallOut(phone, context: context);
  } catch (e) {
    if ((e as dynamic)["code"] == "not_enough_balance") {
      EToast.error(context, "Không đủ số dư để thực hiện cuộc gọi");
    }
  }
}

class ContactInternalRow extends StatelessWidget {
  final Extension extension;

  ContactInternalRow({required this.extension, Key? key});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 10),
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border.symmetric(
              vertical: BorderSide.none,
              horizontal: BorderSide(color: MainTheme.grayF5))),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(child: ContactInfo(extension: extension)),
          ContactManager(extension: extension)
        ],
      ),
    );
  }
}

class ContactInfo extends StatelessWidget {
  final Extension extension;

  ContactInfo({required this.extension});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          child: Icon(
            Icons.account_circle_outlined,
            color: MainTheme.grayCCC,
            size: 50,
          ),
        ),
        Expanded(
            child: Container(
              padding: EdgeInsets.only(left: 10, right: 15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    margin: EdgeInsets.only(bottom: 2.5),
                    child: Text(
                      context.read<AccountUserProvider>().getAccountUserByUserId(extension.userId!).fullName ?? '-',
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          color: MainTheme.gray555,
                          fontWeight: FontWeight.w800,
                          fontSize: 15),
                    ),
                  ),
                  Text(
                    extension.extensionNumber ?? '-',
                    style: TextStyle(
                      fontSize: 12,
                      color: MainTheme.gray555,
                    ),
                  )
                ],
              ),
            ))
      ],
    );
  }
}

class ContactManager extends StatelessWidget {
  final portsip = PortsipService();

  final Extension extension;

  ContactManager({required this.extension});


  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          width: 35,
          height: 35,
          decoration: BoxDecoration(
              color: MainTheme.primaryColor,
              borderRadius: BorderRadius.circular(100)
          ),
          margin: EdgeInsets.only(left: 7.5),
          child: IconButton(
              icon: Icon(
                Icons.call_outlined,
                color: Colors.white,
                size: 20,
              ),
              onPressed: () => callOut(context, extension.extensionNumber ?? "")
          )
        )
      ],
    );
  }
}
