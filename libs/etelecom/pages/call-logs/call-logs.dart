import 'package:etelecom/models/call-log/CallLog.dart';
import 'package:etelecom/providers/authenticate/authenticate.provider.dart';
import 'package:etelecom/providers/call-log/call-log.provider.dart';
import 'package:etelecom/services/call-log.service.dart';
import 'package:etelecom/widgets/bases/base-appbar.dart';
import 'package:etelecom/widgets/bases/base-menu.dart';
import 'package:etelecom/widgets/bases/base-scaffold.dart';
import 'package:etelecom/widgets/list-view/list-view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'call-log-row.dart';

class CallLogsPage extends Page {
  CallLogsPage() : super(key: ValueKey('CallLogsPage'));

  @override
  Route createRoute(BuildContext context) {
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      final currentUserWatcher = context.read<AuthenticateProvider>().currentUser;
      if (currentUserWatcher!.extension != null && currentUserWatcher.extension!.id != null) {
        context.read<CallLogProvider>().setFilter(
            extensionIds: [currentUserWatcher.extension!.id ?? ""],
            hotlineIds: []
        );
        context.read<CallLogProvider>().setPaging(after: ".");
        CallLogService.getCallLogs(context: context);
      }
    });

    return PageRouteBuilder(
        settings: this,
        pageBuilder: (BuildContext context, animation, animation2) {
          return CallLogsScreen();
        });
  }
}

class CallLogsScreen extends StatefulWidget {
  CallLogsScreen({Key? key, this.title}) : super(key: key);
  final String? title;

  @override
  _CallLogsScreenState createState() => _CallLogsScreenState();
}

class _CallLogsScreenState extends State<CallLogsScreen> {

  @override
  Widget build(BuildContext context) {
    final currentUserWatcher = context.watch<AuthenticateProvider>().currentUser;
    return EScaffold(
        appBar: EAppBar(title: 'Lịch sử'),
        body: EListView(
          isLoading: context.watch<CallLogProvider>().loadingCallLogs,
          isEmpty: context.watch<CallLogProvider>().callLogs.length == 0,
          currentNextPointer: context.watch<CallLogProvider>().paging.after,
          listLength: context.watch<CallLogProvider>().callLogs.length,
          itemChildBuilder: (BuildContext context, int index) {
            final CallLog callLogRow = context.watch<CallLogProvider>().callLogs[index];
            return CallLogRow(callLog: callLogRow, key: ObjectKey(callLogRow.id));
          },
          onLoadMore: () async {
            WidgetsBinding.instance!.addPostFrameCallback((_) async {
              context.read<CallLogProvider>().setLoading(true);
              Future.delayed(Duration(seconds: 1), () async {
                await CallLogService.getCallLogs(context: context);
              });
            });
          },
          onRefresh: () async {
            if (currentUserWatcher!.extension != null && currentUserWatcher.extension!.id != null) {
              context.read<CallLogProvider>().setFilter(
                  extensionIds: [currentUserWatcher.extension!.id ?? ""],
                  hotlineIds: []
              );
              context.read<CallLogProvider>().setPaging(after: ".");
              CallLogService.getCallLogs(context: context);
            }
          },
        ),
        bottomMenu: EMenu(menuItems: [
          MenuItem(
              icon: Icons.phone_in_talk_outlined,
              label: 'Nghe gọi',
              route: '/call-center'),
          MenuItem(icon: Icons.list, label: 'Lịch sử', route: '/call-logs'),
          MenuItem(
              icon: Icons.account_circle_outlined,
              label: 'Danh bạ',
              route: '/contacts'),
          MenuItem(
              icon: Icons.settings_outlined,
              label: 'Thiết lập',
              route: '/setting'),
        ]));
  }
}
