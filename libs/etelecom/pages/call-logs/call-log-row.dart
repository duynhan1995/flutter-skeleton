import 'dart:async';

import 'package:etelecom/models/call-log/CallLog.dart';
import 'package:etelecom/models/contact/Contact.dart';
import 'package:etelecom/providers/call-log/call-log.provider.dart';
import 'package:etelecom/services/portsip.service.dart';
import 'package:etelecom/theme/theme.dart';
import 'package:etelecom/utils/string-handler.dart';
import 'package:etelecom/utils/toast.dart';
import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:provider/provider.dart';

import '../contacts/contact-create.dart';
import 'widgets/audio-record.dart';

Future<void> callOut(BuildContext context, String phone) async {
  try {
    await PortsipService.portsipCallOut(phone, context: context);
  } catch (e) {
    if ((e as dynamic)["code"] == "not_enough_balance") {
      EToast.error(context, "Không đủ số dư để thực hiện cuộc gọi");
    }
  }
}

class CallLogRow extends StatefulWidget {
  final CallLog callLog;

  CallLogRow({required this.callLog, Key? key});

  @override
  State<StatefulWidget> createState() {
    return _CallLogRowState();
  }
}

class _CallLogRowState extends State<CallLogRow> {
  @override
  Widget build(BuildContext context) {
    final playingAudioIdWatcher =
        context.watch<CallLogProvider>().playingAudioId;

    return Container(
      margin: EdgeInsets.only(bottom: 10),
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border.symmetric(
              vertical: BorderSide.none,
              horizontal: BorderSide(color: MainTheme.grayF5))),
      child: MaterialButton(
        padding: EdgeInsets.symmetric(
            vertical: 15,
            horizontal: playingAudioIdWatcher == widget.callLog.id ? 0 : 10),
        highlightColor: Colors.transparent,
        onPressed: () async {
          if (playingAudioIdWatcher == widget.callLog.id) {
            return;
          }
          await context.read<CallLogProvider>().stopAudio();
          callOut(context, widget.callLog.phone ?? "");
        },
        child: playingAudioIdWatcher == widget.callLog.id
            ? AudioRecord()
            : Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: CallLogInfo(callLog: widget.callLog),
            ),
            CallLogManager(
                callLog: widget.callLog,
                onPlayingAudio: (url) {
                  context
                      .read<CallLogProvider>()
                      .playAudio(widget.callLog.id ?? "", url);
                })
          ],
        ),
      ),
    );
  }
}

class CallLogInfo extends StatelessWidget {
  final CallLog callLog;

  CallLogInfo({required this.callLog});

  @override
  Widget build(BuildContext context) {
    final List<Widget> _iconsChildren = [
      Text(
        callLog.callStateDisplay?.toUpperCase() ?? "",
        style: TextStyle(color: MainTheme.gray999, fontSize: 9),
      ),
      if (callLog.duration! > 0)
        Container(
          padding: EdgeInsets.all(4),
          margin: EdgeInsets.only(top: 4),
          width: 50,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(5)),
              color: MainTheme.gray999.withOpacity(0.2)),
          child: Text('${StringHandler.compactSecondsToTime(callLog.duration)}',
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: MainTheme.gray222,
                  fontSize: 8,
                  fontWeight: FontWeight.w900)),
        )
    ];

    if (callLog.callStateIcon != null) {
      _iconsChildren.insert(
          0,
          Icon(
            callLog.callStateIcon,
            color: callLog.callStateColor,
            size: 27,
          ));
    }

    var callStartedAt =
        '${callLog.contact?.id != null ? '${callLog.contact?.phone} - ' : ''}';

    if (callLog.direction == 'ext') {
      callStartedAt = '${callLog.caller} → ${callLog.callee} - ';
    }
    if (callLog.startedAt == null || callLog.startedAt!.year == 1) {
      callStartedAt += '${StringHandler.datetimeToString(callLog.createdAt)}';
    } else {
      callStartedAt += '${StringHandler.datetimeToString(callLog.startedAt)}';
    }

    return Row(
      children: [
        Container(
          width: 60,
          child: Column(
            children: _iconsChildren,
          ),
        ),
        Expanded(
            child: Container(
              padding: EdgeInsets.only(left: 10, right: 15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  if (callLog.direction == 'ext')
                    Text(
                      'Nội bộ',
                      style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                  if (callLog.direction == 'ext')
                    Text(
                        '${callLog.callerFullName ?? callLog.caller} → ${callLog.calleeFullName ?? callLog.callee}',
                        style: TextStyle(
                            color: MainTheme.gray555,
                            fontSize: 14,
                            fontWeight: FontWeight.w400
                        )
                    ),
                  if (callLog.direction != 'ext')
                    Container(
                      margin: EdgeInsets.only(bottom: 2.5),
                      child: Text(
                        (callLog.contact?.id != null
                            ? callLog.contact?.fullName
                            : (callLog.direction == 'ext'
                            ? callLog.callerFullName
                            : callLog.phone)
                        ) ?? "-",
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            color: MainTheme.gray555,
                            fontWeight: FontWeight.w800,
                            fontSize: 15
                        ),
                      ),
                    ),
                  Text(
                    callStartedAt,
                    style: TextStyle(
                      fontSize: 11,
                      color: MainTheme.gray555,
                    ),
                  )
                ],
              ),
            )
        )
      ],
    );
  }
}

class CallLogManager extends StatelessWidget {
  final CallLog callLog;
  final Function(String url)? onPlayingAudio;

  CallLogManager({required this.callLog, this.onPlayingAudio});

  void bottomSheetUpdateCreate(BuildContext context) {
    CupertinoScaffold.showCupertinoModalBottomSheet(
        context: context,
        duration: Duration(milliseconds: 400),
        builder: (BuildContext context) {
          return ContactCreate(contact: Contact(phone: callLog.phone));
        });
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
            child: PopupMenuButton(
              padding: EdgeInsets.zero,
              itemBuilder: (BuildContext context) {
                final bool hasAudio = callLog.audioUrls != null &&
                    callLog.audioUrls!.isNotEmpty &&
                    callLog.audioUrls![0].isNotEmpty;

                final List<Map<String, dynamic>> popupMenuItems = [
                  {
                    "title": "Tạo danh bạ",
                    "value": "contact",
                    "color": MainTheme.gray555,
                    "disabled": false
                  },
                  {
                    "title": "Nghe lại file ghi âm",
                    "value": "audio",
                    "color": hasAudio ? MainTheme.gray555 : MainTheme.grayCCC,
                    "disabled": !hasAudio
                  }
                ];

                if (callLog.contact != null) {
                  popupMenuItems.removeAt(0);
                }

                return popupMenuItems.map((item) => PopupMenuItem(
                    enabled: !item["disabled"],
                    height: 35,
                    value: item["value"],
                    child: Text(
                        item["title"],
                        style: TextStyle(color: item["color"], fontSize: 13)
                    )
                )).toList();
              },
              onSelected: (value) {
                switch (value) {
                  case "contact":
                    bottomSheetUpdateCreate(context);
                    break;
                  case "audio":
                    final audioUrl = callLog.audioUrls?[0] ?? "";
                    if (audioUrl.isNotEmpty && onPlayingAudio != null) {
                      onPlayingAudio!(callLog.audioUrls?[0] ?? "");
                    }
                    break;
                }
              },
              child: Container(
                height: 35,
                padding: EdgeInsets.symmetric(vertical: 2.5, horizontal: 10),
                decoration: BoxDecoration(
                    border: Border.all(color: MainTheme.grayF5),
                    borderRadius: BorderRadius.circular(20)),
                child: Row(
                  children: [
                    Icon(Icons.arrow_drop_down, size: 20),
                    Container(
                      child: Text('Thao tác', style: TextStyle(fontSize: 13)),
                    )
                  ],
                ),
              ),
            )),
      ],
    );
  }
}
