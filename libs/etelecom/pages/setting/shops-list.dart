import 'package:etelecom/providers/authenticate/authenticate.provider.dart';
import 'package:etelecom/providers/navigate/navigate.provider.dart';
import 'package:etelecom/providers/shop/shop.provider.dart';
import 'package:etelecom/widgets/bases/base-appbar.dart';
import 'package:etelecom/widgets/bases/base-scaffold.dart';
import 'package:etelecom/widgets/list-view/loading-view.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'widgets/shops-list-item.dart';
import 'widgets/shops-list-wrapper.dart';

class ShopsListPage extends Page {
  ShopsListPage() : super(key: ValueKey('ShopsListPage'));

  @override
  Route createRoute(BuildContext context) {

    WidgetsBinding.instance!.addPostFrameCallback((_) {
      context.read<ShopProvider>().getShopsList(
        context.read<AuthenticateProvider>().currentAccountsList,
        context.read<AuthenticateProvider>().currentShop!,
      );
    });

    return PageRouteBuilder(
      settings: this,
      pageBuilder: (BuildContext context, animation, animation2) {
        var begin = Offset(-1.0, 0.0);
        var end = Offset.zero;
        var tween = Tween(begin: begin, end: end);
        var offsetAnimation = animation.drive(tween);
        return SlideTransition(
          position: offsetAnimation,
          child: ShopsListScreen(),
        );
      }
    );
  }
}

class ShopsListScreen extends StatefulWidget {
  ShopsListScreen({Key? key, this.title}) : super(key: key);

  final String? title;

  @override
  State<StatefulWidget> createState() {
    return _ShopsListScreenState();
  }
}

class _ShopsListScreenState extends State<ShopsListScreen> {
  @override
  Widget build(BuildContext context) {
    final currentShopWatcher = context.watch<AuthenticateProvider>().currentShop;

    return EScaffold(
      appBar: EAppBar(title: 'Danh sách cửa hàng'),
      body: context.watch<ShopProvider>().shopsLoading ? ListLoading() : ListView(
        children: [
          ShopsListWrapper(
            label: 'BẠN ĐANG Ở ĐÂY',
            shopListItems: [
              ShopsListItem(
                name: currentShopWatcher!.name ?? "",
                imageUrl: currentShopWatcher.imageUrl,
                code: currentShopWatcher.code,
                icon: Icons.check_circle_outline_sharp,
              )
            ]
          ),
          ShopsListWrapper(
            label: 'CỬA HÀNG KHÁC',
            shopListItems: context.watch<ShopProvider>().currentShopsList
                .map((shop) => ShopsListItem(
              name: shop.name ?? "",
              imageUrl: shop.imageUrl,
              code: shop.code,
              onAccess: () {
                context.read<AuthenticateProvider>().switchShop(shop.id!);
                context.read<NavigateProvider>().navigate('/setting');
              },
            )).toList()
          )
        ],
      ),
    );
  }

}


