import 'package:etelecom/theme/theme.dart';
import 'package:etelecom/widgets/avatar/avatar.dart';
import 'package:flutter/material.dart';

class ShopsListItem extends StatelessWidget {
  final String? imageUrl;
  final String name;
  final String? code;
  final IconData? icon;
  final VoidCallback? onAccess;

  ShopsListItem({
    this.imageUrl,
    required this.name,
    this.code,
    this.icon,
    this.onAccess
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            color: MainTheme.grayF5
          )
        )
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: Row(
              children: [
                EAvatar(
                  imageUrl: imageUrl ?? "",
                  width: 50,
                  height: 50,
                  margin: EdgeInsets.only(right: 10),
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        child: Text(
                          name,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            fontWeight: FontWeight.w700,
                            fontSize: 16
                          ),
                        ),
                      ),
                      Text(
                        code ?? "",
                        style: TextStyle(
                          fontSize: 15
                        ),
                      )
                    ],
                  ),
                )
              ],
            )
          ),
          onAccess == null ?
          Icon(
            icon,
            color: MainTheme.primaryColor,
            size: 30
          ) :
          Container(
            height: 35,
            padding: EdgeInsets.symmetric(vertical: 2.5, horizontal: 10),
            decoration: BoxDecoration(
              color: MainTheme.primaryColor,
              borderRadius: BorderRadius.circular(20)
            ),
            child: MaterialButton(
              padding: EdgeInsets.zero,
              onPressed: onAccess,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(Icons.login_outlined, size: 17, color: Colors.white),
                  Container(
                    margin: EdgeInsets.only(left: 5),
                    child: Text(
                      'Truy cập',
                      style: TextStyle(fontSize: 13, color: Colors.white)
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

}
