import 'package:etelecom/api/etop/user-api.dart';
import 'package:etelecom/providers/authenticate/authenticate.provider.dart';
import 'package:etelecom/theme/theme.dart';
import 'package:etelecom/utils/toast.dart';
import 'package:etelecom/widgets/bases/base-appbar.dart';
import 'package:etelecom/widgets/bases/base-scaffold.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class LoginPage extends Page {
  LoginPage() : super(key: ValueKey('LoginPage'));

  @override
  Route createRoute(BuildContext context) {
    return MaterialPageRoute(
        settings: this,
        builder: (BuildContext context) {
          return LoginScreen();
        });
  }
}

class LoginScreen extends StatefulWidget {
  final String? title;

  LoginScreen({Key? key, this.title}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final UserApi userApi = UserApi();

  final usernameController = TextEditingController();
  final passwordController = TextEditingController();

  Future<void> login(BuildContext context) async {
    final res = await UserApi.login(usernameController.text, passwordController.text);

    final String token = res["access_token"] ?? "";

    if (token.isEmpty) {
      return EToast.error(context, 'Đăng nhập thất bại. ${(res as dynamic)["msg"]}');
    } else {
      context.read<AuthenticateProvider>().updateToken(token);
      return context.read<AuthenticateProvider>().updateSessionInfo();
    }
  }

  @override
  void dispose() {
    usernameController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return EScaffold(
      appBar: EAppBar(title: 'eTelecom - Tổng đài chốt đơn'),
      body: Container(
        padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
        child: Column(
          children: [
            Container(
                margin: EdgeInsets.only(bottom: 20),
                child: TextFormField(
                  keyboardType: TextInputType.phone,
                  decoration: InputDecoration(
                    hintText: 'Nhập số điện thoại',
                    hintStyle:
                        TextStyle(fontWeight: FontWeight.w400, fontSize: 16),
                    labelText: 'Số điện thoại',
                    labelStyle:
                        TextStyle(fontWeight: FontWeight.w400, fontSize: 16),
                    contentPadding: EdgeInsets.only(bottom: 0, top: 0),
                  ),
                  style: TextStyle(
                      color: MainTheme.gray555,
                      fontSize: 18,
                      fontWeight: FontWeight.w600),
                  controller: usernameController,
                )),
            Container(
                margin: EdgeInsets.only(bottom: 20),
                child: TextFormField(
                  obscureText: true,
                  decoration: InputDecoration(
                    hintText: 'Nhập mật khẩu',
                    hintStyle:
                        TextStyle(fontWeight: FontWeight.w400, fontSize: 16),
                    labelText: 'Mật khẩu',
                    labelStyle:
                        TextStyle(fontWeight: FontWeight.w400, fontSize: 16),
                    contentPadding: EdgeInsets.only(bottom: 0, top: 0),
                  ),
                  style: TextStyle(
                      color: MainTheme.gray555,
                      fontSize: 18,
                      fontWeight: FontWeight.w600),
                  controller: passwordController,
                )),
            Container(
                child: Row(
                    children: [
                        Expanded(
                            child: MaterialButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(7),
                                    side: BorderSide(
                                        color: MainTheme.primaryColor, width: 1)),
                                onPressed: () async {
                                  await login(context);
                                },
                                child: Text('Đăng nhập',
                                    style: TextStyle(
                                        fontSize: 17, fontWeight: FontWeight.w400)),
                                textColor: Colors.white,
                                color: MainTheme.primaryColor,
                                minWidth: 300,
                                height: 50
                            )
                        )
                    ],
                )
            ),
          ],
        ),
      ),
    );
  }
}
