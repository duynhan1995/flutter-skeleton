import 'package:etelecom/app_config.dart';
import 'package:etelecom/providers/authenticate/authenticate.provider.dart';
import 'package:etelecom/providers/call-log/call-log.provider.dart';
import 'package:etelecom/providers/call-manager/calls.provider.dart';
import 'package:etelecom/providers/contact/contact.provider.dart';
import 'package:etelecom/providers/extension/extension.provider.dart';
import 'package:etelecom/providers/navigate/navigate.provider.dart';
import 'package:etelecom/providers/account-user/account-user.provider.dart';
import 'package:etelecom/providers/shop/shop.provider.dart';
import 'package:etelecom/theme/theme.dart';
import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:provider/provider.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:url_strategy/url_strategy.dart';

import 'etelecom-route-information-parser.dart';
import 'etelecom-router-delegate.dart';

void main({AppEnv env = AppEnv.dev}) async {
  WidgetsFlutterBinding.ensureInitialized();

  await AppConfig().loadConfig(client: AppClient.etelecom, env: env);

  initializeDateFormatting('vi');
  setPathUrlStrategy();

  runApp(MultiProvider(
    providers: [
      ChangeNotifierProvider(create: (_) => AuthenticateProvider()),
      ChangeNotifierProvider(create: (_) => NavigateProvider()),
      ChangeNotifierProvider(create: (_) => CallsProvider()),
      ChangeNotifierProvider(create: (_) => ContactProvider()),
      ChangeNotifierProvider(create: (_) => CallLogProvider()),
      ChangeNotifierProvider(create: (_) => ShopProvider()),
      ChangeNotifierProvider(create: (_) => ExtensionProvider()),
      ChangeNotifierProvider(create: (_) => AccountUserProvider()),
      ChangeNotifierProvider(create: (_) => AppConfig()),
    ],
    child: Main(),
  ));
}

class Main extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    EtelecomRouterDelegate _routerDelegate = EtelecomRouterDelegate();
    EtelecomRouteInformationParser _routeInformationParser =
        EtelecomRouteInformationParser();

    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus &&
            currentFocus.focusedChild != null) {
          FocusManager.instance.primaryFocus!.unfocus();
        }
      },
      child: MaterialApp.router(
          title: 'eTelecom',
          routerDelegate: _routerDelegate,
          routeInformationParser: _routeInformationParser,
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            fontFamily: GoogleFonts.openSans().fontFamily,
            primaryColor: MainTheme.primaryColor,
            visualDensity: VisualDensity.adaptivePlatformDensity,
          )),
    );
  }
}
