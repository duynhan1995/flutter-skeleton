import 'package:etelecom/providers/authenticate/authenticate.provider.dart';
import 'package:etelecom/providers/call-manager/calls.provider.dart';
import 'package:etelecom/providers/navigate/navigate.provider.dart';
import 'package:etelecom/services/http.service.dart';
import 'package:etelecom/services/portsip.service.dart';
import 'package:etelecom/widgets/calling/calling-state-incoming.dart';
import 'package:etelecom/widgets/calling/calling-state-outgoing.dart';
import 'package:etelecom/widgets/calling/calling-state-processing.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';

import 'etelecom-route-path.dart';
import 'pages/call-center/call-center.dart';
import 'pages/call-logs/call-logs.dart';
import 'pages/contacts/contacts.dart';
import 'pages/login/login.dart';
import 'pages/orders/orders.dart';
import 'pages/setting/setting.dart';
import 'pages/setting/shops-list.dart';

class EtelecomRouterDelegate extends RouterDelegate<EtelecomRoutePath>
    with ChangeNotifier, PopNavigatorRouterDelegateMixin<EtelecomRoutePath> {
  final GlobalKey<NavigatorState> navigatorKey;
  final PortsipService portsip = PortsipService();

  EtelecomRouterDelegate() : navigatorKey = GlobalKey<NavigatorState>();

  @override
  Widget build(BuildContext context) {
    PortsipService.hookNativeEvents(context);
    HttpService.injectContext(context);

    String _watchingToken = context.watch<AuthenticateProvider>().currentToken ?? '';
    bool _watchingAuthenticated = _watchingToken.length > 0;

    List<String> _routeSegments =
        context.watch<NavigateProvider>().route.split("/");

    CallingState _callingState =
        context.watch<CallsProvider>().currentState;

    TransitionDelegate<dynamic> transitionDelegate = NoAnimationTransitionDelegate();
    if (_routeSegments.length != 2) {
      transitionDelegate = DefaultTransitionDelegate();
    }

    return Navigator(
      key: navigatorKey,
      transitionDelegate: transitionDelegate,
      pages: [
        if (!_watchingAuthenticated) LoginPage(),
        if (_watchingAuthenticated &&
            _routeSegments.length >= 2 &&
            _routeSegments[1] == 'call-center' &&
            _callingState == CallingState.none)
          CallCenterPage(),
        if (_watchingAuthenticated &&
            _routeSegments.length >= 2 &&
            _routeSegments[1] == 'call-logs' &&
            _callingState == CallingState.none)
          CallLogsPage(),
        if (_watchingAuthenticated &&
            _routeSegments.length >= 2 &&
            _routeSegments[1] == 'contacts' &&
            _callingState == CallingState.none)
          ContactsPage(),
        if (_watchingAuthenticated &&
            _routeSegments.length >= 2 &&
            _routeSegments[1] == 'orders' &&
            _callingState == CallingState.none)
          OrdersPage(),
        if (_watchingAuthenticated &&
            _routeSegments.length >= 2 &&
            _routeSegments[1] == 'setting' &&
            _callingState == CallingState.none)
          SettingPage(),
        if (_watchingAuthenticated &&
            _routeSegments.length >= 3 &&
            _routeSegments[2] == 'shops' &&
            _callingState == CallingState.none)
          ShopsListPage(),
        if (_watchingAuthenticated && _callingState == CallingState.outgoing)
          CallingStateOutgoingPage(),
        if (_watchingAuthenticated && _callingState == CallingState.incoming)
          CallingStateIncomingPage(),
        if (_watchingAuthenticated && _callingState == CallingState.processing)
          CallingStateProcessingPage(),
      ],
      onPopPage: (route, result) {
        if (!route.didPop(result)) {
          return false;
        }

        if (_routeSegments.length >= 3 && _routeSegments[2] == 'shops') {
          context.read<NavigateProvider>().navigate('/setting');
        }

        return true;
      },
    );
  }

  @override
  Future<void> setNewRoutePath(EtelecomRoutePath path) async {
    print("setNewRoutePath $path");
  }
}

class NoAnimationTransitionDelegate extends TransitionDelegate<void> {
  @override
  Iterable<RouteTransitionRecord> resolve({
    required List<RouteTransitionRecord> newPageRouteHistory,
    required Map<RouteTransitionRecord?, RouteTransitionRecord> locationToExitingPageRoute,
    required Map<RouteTransitionRecord?, List<RouteTransitionRecord>> pageRouteToPagelessRoutes}) {

    final results = <RouteTransitionRecord>[];

    for (final pageRoute in newPageRouteHistory) {
      if (pageRoute.isWaitingForEnteringDecision) {
        pageRoute.markForAdd();
      }
      results.add(pageRoute);
    }

    for (final exitingPageRoute in locationToExitingPageRoute.values) {
      if (exitingPageRoute.isWaitingForExitingDecision) {
        exitingPageRoute.markForRemove();
        final pagelessRoutes = pageRouteToPagelessRoutes[exitingPageRoute];
        if (pagelessRoutes != null) {
          for (final pagelessRoute in pagelessRoutes) {
            pagelessRoute.markForRemove();
          }
        }
      }

      results.add(exitingPageRoute);
    }
    return results;

  }
}
