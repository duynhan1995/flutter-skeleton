import 'package:etelecom/app_config.dart';

import 'etelecom_app.dart' as App;

void main() {
  // set config to prod
  App.main(env: AppEnv.dev);
}
