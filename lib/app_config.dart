import 'dart:convert';
import 'package:camera/camera.dart';
import 'package:etelecom/services/http.service.dart';
import 'package:etelecom/theme/theme.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';

enum AppClient {
  etelecom,
  etelecom_cs,
  hubjs
}

const _$AppClientEnumMap = {
  AppClient.etelecom: 'etelecom',
  AppClient.etelecom_cs: 'etelecom-cs',
  AppClient.hubjs: 'hubjs',
};

enum AppEnv {
  dev,
  prod,
  sandbox
}

const _$AppEnvEnumMap = {
  AppEnv.dev: 'dev',
  AppEnv.prod: 'prod',
  AppEnv.sandbox: 'sandbox',
};

class AppFeatures {
  bool ticket;
  bool order;

  AppFeatures({
    this.ticket = false,
    this.order = false
  });
}

class AppConfig with ChangeNotifier, DiagnosticableTreeMixin {

  static String sipServer = "210.211.106.202";
  static bool videoCall = false;
  static CameraDescription? frontCamera;
  static CameraDescription? backCamera;

  static AppFeatures _features = AppFeatures(ticket: false, order: false);
  AppFeatures get features => _features;

  final String? apiUrl;
  final Map<String, dynamic>? primaryColor;

  AppConfig({this.apiUrl, this.primaryColor});

  Future<void> loadConfig({
    required AppClient? client, AppEnv? env
  }) async {
    try {
      // set default to dev if nothing was passed
      env = env ?? AppEnv.dev;

      // load the json file
      final contents = await rootBundle.loadString(
        'assets/config/${_$AppClientEnumMap[client]}/${_$AppEnvEnumMap[env]}.json',
      );

      // decode our json
      final json = jsonDecode(contents);

      // convert our JSON into an instance of our AppConfig class
      final config = AppConfig(
          apiUrl: json['apiUrl'],
          primaryColor: json['primaryColor']
      );

      HttpService.setHost(config.apiUrl ?? "");
      MainTheme.mapColorFromConfig(config.primaryColor);

      sipServer = json["voipUrl"] ?? "210.211.106.202";

      videoCall = json["videoCall"] ?? false;
      if (videoCall) {
        await checkAvailableCameras();
      }

      updateFeaturesFromConfig(json);

    } catch (e) {
      print("ERROR in loadConfig: $e");
    }
  }

  static Future<void> checkAvailableCameras() async {
    try {
      final cameras = await availableCameras();
      frontCamera = cameras.firstWhere((cam) => cam.lensDirection == CameraLensDirection.front);
      backCamera = cameras.firstWhere((cam) => cam.lensDirection == CameraLensDirection.back);
    } catch (e) {
      print("ERROR in checkAvailableCameras $e");
    }
  }

  updateFeaturesFromConfig(dynamic json) {
    final Map<String, dynamic> _jsonFeatures = json["features"];
    _features = AppFeatures(
        ticket: _jsonFeatures["ticket"] as bool,
        order: _jsonFeatures["order"] as bool
    );
    notifyListeners();
  }

}
