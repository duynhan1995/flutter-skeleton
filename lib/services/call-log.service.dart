import 'package:etelecom/api/shop/account-user.api.dart';
import 'package:etelecom/api/shop/call-log.api.dart';
import 'package:etelecom/api/shop/contact.api.dart';
import 'package:etelecom/api/shop/extension.api.dart';
import 'package:etelecom/models/account-user/AccountUser.dart';
import 'package:etelecom/models/base/paging/cursor-paging.dart';
import 'package:etelecom/models/call-log/CallLog.dart';
import 'package:etelecom/models/contact/Contact.dart';
import 'package:etelecom/models/extension/Extension.dart';
import 'package:etelecom/providers/authenticate/authenticate.provider.dart';
import 'package:etelecom/providers/call-log/call-log.provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CallLogService {

  static Future<void> getCallLogs({BuildContext? context, bool doLoading = true}) async {
    if (doLoading) {
      context!.read<CallLogProvider>().setLoading(true);
    }
    try {
      final _paging = context!.read<CallLogProvider>().paging;
      final _filter = context.read<CallLogProvider>().filter;

      final GetCallLogsRequest request = GetCallLogsRequest(
        paging: _paging,
        filter:_filter
      );

      final res = await CallLogApi.getCallLogs(request);

      final List<dynamic> _rawCallLogs = res["call_logs"];
      final dynamic _rawPaging = res["paging"];

      List<CallLog> callLogs = _rawCallLogs.map((log) => CallLog.fromJson(log)).toList();

      final List<String> contactIDs = callLogs.map((log) => log.contactId ?? "").toList();
      final List<Contact> _contacts = await _getContactsByIDs(contactIDs);

      final List<String> _extNumbersOut = callLogs.map((e) => ["ext_out", "ext"].contains(e.direction) ? e.callee : "").toList();
      final List<String> _extNumbersIn = callLogs.map((e) => ["ext_in", "ext"].contains(e.direction) ? e.caller : "").toList();
      final List<Extension> _extensionsList = await _getExtensionsByExtensionNumbers([..._extNumbersOut, ..._extNumbersIn]);

      final List<String> _userIds = _extensionsList.map((e) => e.userId ?? "").toList();
      final List<AccountUser> _accUsersList = await _getAccountUsersByUserIds(
        userIds: _userIds,
        extensionsList: _extensionsList
      );

      final CursorPaging paging = CursorPaging.fromJson(_rawPaging);
      context.read<CallLogProvider>().setPaging(next: paging.next);

      context.read<CallLogProvider>().setCallLogs(callLogs.map((log) {
        log.contact = _contacts.firstWhere(
          (cont) => cont.id == log.contactId,
          orElse: () => Contact()
        );

        return CallLog.mapCallLog(
          callLog: log,
          currentExtension: context.read<AuthenticateProvider>().currentUser!.extension!,
          accUsers: _accUsersList
        );
      }).toList());

    } catch (e) {
      print("ERROR in getCallLogs Service $e");
    }
    if (doLoading) {
      context!.read<CallLogProvider>().setLoading(false);
    }
  }

  static Future<List<AccountUser>> _getAccountUsersByUserIds({
    required List<String> userIds,
    required List<Extension> extensionsList
  }) async {
    try {
      if (userIds.isEmpty) {
        return [];
      }
      userIds = userIds.where((element) => element.isNotEmpty).toList();

      final Set<String> setOfUserIds = userIds.toSet();
      userIds.retainWhere((element) => setOfUserIds.remove(element));

      final GetAccountUsersFilter _filter = GetAccountUsersFilter(userIds: userIds);

      final res = await AccountUserApi.getAccountUsers(GetAccountUsersRequest(filter: _filter));

      final List<dynamic> _rawList = res["account_users"];

      final List<AccountUser> accUsers = _rawList.map((e) => AccountUser.fromJson(e)).toList();
      accUsers.forEach((element) {
        element.extensionNumber = extensionsList.firstWhere(
            (ext) => ext.userId == element.userId,
            orElse: () => Extension()
        ).extensionNumber;
      });

      return accUsers;
    } catch (e) {
      print("ERROR in CallLogService._getAccountUsersByUserIds $e");
      return [];
    }
  }

  static Future<List<Extension>> _getExtensionsByExtensionNumbers(List<String> extNumbers) async {
    try {
      if (extNumbers.isEmpty) {
        return [];
      }
      extNumbers = extNumbers.where((element) => element.isNotEmpty).toList();

      final Set<String> setOfExtNumbers = extNumbers.toSet();
      extNumbers.retainWhere((element) => setOfExtNumbers.remove(element));

      final GetExtensionsFilter _filter = GetExtensionsFilter(extensionNumbers: extNumbers);

      final res = await ExtensionApi.getExtensions(GetExtensionsRequest(filter: _filter));

      final List<dynamic> _rawList = res["extensions"];

      final List<Extension> extensions = _rawList.map((e) => Extension.fromJson(e)).toList();

      return extensions;
    } catch (e) {
      print("ERROR in CallLogService._getExtensionsByExtensionNumbers $e");
      return [];
    }
  }

  static Future<List<Contact>> _getContactsByIDs(List<String> contactIDs) async {
    try {
      if (contactIDs.isEmpty) {
        return [];
      }
      contactIDs = contactIDs.where((element) => element.isNotEmpty).toSet().toList();

      final GetContactsFilter filter = GetContactsFilter(ids: contactIDs);

      final res = await ContactApi.getContacts(GetContactsRequest(filter: filter));

      final List<dynamic> _rawContacts = res["contacts"];
      final List<Contact> contacts = _rawContacts
          .map((_cont) => Contact.fromJson(_cont)).toList();

      return contacts;
    } catch (e) {
      print("ERROR in CallLogService.getContactsByIDs $e");
      return [];
    }
  }

}
