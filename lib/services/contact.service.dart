import 'package:etelecom/api/shop/contact.api.dart';
import 'package:etelecom/models/base/paging/cursor-paging.dart';
import 'package:etelecom/models/contact/Contact.dart';
import 'package:etelecom/providers/contact/contact.provider.dart';
import 'package:etelecom/utils/array-handler.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ContactService {
  static Future<void> createContact(BuildContext context, CreateContactRequest request) async {
    try {
      await ContactApi.createContact(request);

      context.read<ContactProvider>().setPaging(after: ".");
      await getContacts(context: context, doLoading: false);
    } catch (e) {
      print("ERROR in createContact Service $e");
      throw e;
    }
  }

  static Future<void> getContacts({
    required BuildContext context,
    bool doLoading = true
  }) async {
    if (doLoading) {
      context.read<ContactProvider>().setLoading(true);
    }
    try {
      final _paging = context.read<ContactProvider>().currentPaging;

      final _filter = context.read<ContactProvider>().currentFilter;

      final GetContactsRequest request = GetContactsRequest(paging: _paging, filter: _filter);

      final res = await ContactApi.getContacts(request);

      final List<dynamic> _rawList = res["contacts"];
      final Map<String, dynamic> _rawPaging = res["paging"];

      final List<Contact> contacts = _rawList.map((e) => Contact.fromJson(e)).toList();
      final CursorPaging paging = CursorPaging.fromJson(_rawPaging);
      context.read<ContactProvider>().setPaging(next: paging.next);

      context.read<ContactProvider>().setContacts(contacts);
    } catch (e) {
      print("ERROR in getContacts Service $e");
    }
    if (doLoading) {
      context.read<ContactProvider>().setLoading(false);
    }
  }

  static Future<List<Contact>> searchContacts(String keyword) async {
    List<Contact> results = [];

    try {

      if (keyword.isEmpty) {
        final CursorPaging cursorPaging = CursorPaging(limit: 20);
        GetContactsRequest request = GetContactsRequest(paging: cursorPaging);

        dynamic res = await ContactApi.getContacts(request);

        List<dynamic> _rawList = res["contacts"];

        results = _rawList.map((e) => Contact.fromJson(e)).toList();

        return results;
      }

      final CursorPaging cursorPaging = CursorPaging(limit: 100);

      // note: search by name
      GetContactsFilter filter = GetContactsFilter(name: keyword);
      GetContactsRequest request = GetContactsRequest(paging: cursorPaging, filter: filter);

      dynamic res = await ContactApi.getContacts(request);
      List<dynamic> _rawContacts = res["contacts"];
      _rawContacts.map((e) => Contact.fromJson(e)).toList().forEach((element) {
        results = ArrayHandler<Contact>().upsert(results, element);
      });

      // note: search by phone
      filter = GetContactsFilter(phone: keyword);
      request = GetContactsRequest(paging: cursorPaging, filter: filter);

      res = await ContactApi.getContacts(request);
      _rawContacts = res["contacts"];
      _rawContacts.map((e) => Contact.fromJson(e)).toList().forEach((element) {
        results = ArrayHandler<Contact>().upsert(results, element);
      });

      return results;

    } catch (e) {
      print("ERROR in searchContacts $e");
      return [];
    }
  }

  static Future<void> updateContact({
    required BuildContext context,
    required UpdateContactRequest request,
    Function(Contact contact)? afterUpdateCb
  }) async {
    try {
      final res = await ContactApi.updateContact(request);
      final Contact _updatedContact = Contact.fromJson(res);

      if (afterUpdateCb != null) {
        afterUpdateCb(_updatedContact);
      }

      context.read<ContactProvider>().updateContacts([_updatedContact]);
    } catch (e) {
      print("ERROR in updateContact Service $e");
      throw e;
    }
  }

  static Future<void> deleteContact({
    required BuildContext context,
    required String id,
    Function? afterDeleteCb
  }) async {
    try {
      await ContactApi.deleteContact(id);

      if (afterDeleteCb != null) {
        afterDeleteCb();
      }

      context.read<ContactProvider>().setPaging(after: ".");
      await ContactService.getContacts(context: context, doLoading: false);
    } catch (e) {
      print("ERROR in deleteContact Service $e");
      throw e;
    }
  }
}
