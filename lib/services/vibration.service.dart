import 'dart:async';

import 'package:vibration/vibration.dart';

Timer? timer;

class VibrationService {

  static Future<void> startVibration() async {
    if (timer != null) {
      timer!.cancel();
    }

    bool? hasVibration = await Vibration.hasCustomVibrationsSupport();

    timer = Timer.periodic(new Duration(seconds: 2), (timer) async {
      if (hasVibration != null && hasVibration) {
        await Vibration.vibrate(duration: 1000);
      }
    });
  }

  static endVibration() {
    if (timer != null) {
      timer!.cancel();
    }
  }
}
