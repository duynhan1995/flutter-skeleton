import 'dart:async';
import 'dart:convert';

import 'package:etelecom/api/shop/account-user.api.dart';
import 'package:etelecom/api/shop/call-log.api.dart';
import 'package:etelecom/api/shop/call-session.api.dart';
import 'package:etelecom/api/shop/hotline.api.dart';
import 'package:etelecom/api/shop/summary.api.dart';
import 'package:etelecom/api/shop/contact.api.dart';
import 'package:etelecom/app_config.dart';
import 'package:etelecom/models/call-log/CallLog.dart';
import 'package:etelecom/models/contact/Contact.dart';
import 'package:etelecom/models/hotline/Hotlines.dart';
import 'package:etelecom/models/account-user/AccountUser.dart';
import 'package:etelecom/providers/authenticate/authenticate.provider.dart';
import 'package:etelecom/providers/call-log/call-log.provider.dart';
import 'package:etelecom/providers/call-manager/calls.provider.dart';
import 'package:etelecom/providers/contact/contact.provider.dart';
import 'package:etelecom/utils/native-permissions.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import 'package:contacts_service/contacts_service.dart' as ContactDevice;

const CHANNEL = 'vn.etelecom.appcall';
const platform = const MethodChannel(CHANNEL);
const DTMFMap = {
  "0": 0,
  "1": 1,
  "2": 2,
  "3": 3,
  "4": 4,
  "5": 5,
  "6": 6,
  "7": 7,
  "8": 8,
  "9": 9,
  "*": 10,
  "#": 11
};

late BuildContext portsipContext;

class PortsipService {
  static Future<void> portsipUnregister() async {
    try {
      await platform.invokeMethod("unregisterPortsip");
    } on PlatformException catch (e) {
      print("ERROR in unregisterPortSip $e");
    }
  }

  static Future<void> portsipRegister() async {
    try {
      final _extensionWatcher = portsipContext.read<AuthenticateProvider>().currentUser!.extension;

      if (_extensionWatcher != null
          && _extensionWatcher.extensionNumber!.isNotEmpty
          && _extensionWatcher.extensionPassword!.isNotEmpty
          && _extensionWatcher.tenantDomain!.isNotEmpty) {

        await platform.invokeMethod("registerPortsip", {
          'username': _extensionWatcher.extensionNumber,
          'password': _extensionWatcher.extensionPassword,
          'domain': _extensionWatcher.tenantDomain,
          'sipServer': AppConfig.sipServer
        });

      }

    } on PlatformException catch (e) {
      print("ERROR in registerPortSip $e");
    }
  }

  static Future<void> portsipCallOut(
      String phoneNumber,
      {bool isVideoCall = false, required BuildContext context}
  ) async {

    if (phoneNumber.isEmpty) {
      return;
    }

    final _callsReader = portsipContext.read<CallsProvider>();
    if (_callsReader.makingCall) {
      return;
    }

    try {

      await portsipContext.read<CallLogProvider>().stopAudio();

      if (!await checkPermissionsForMakingCall(context)) {
        return;
      }

      _callsReader.onMakeCall(true);

      final _authenticateReader = portsipContext.read<AuthenticateProvider>();

      final int _balance = await _calcBalanceUser();
      final _hotline = await _getHotline(_authenticateReader.currentUser!.extension!.hotlineId!);
      // static charge is 15VND per second
      int _minCallTime = 0;
      portsipContext.read<CallsProvider>().updateCallTime(0);
      _minCallTime = (_balance ~/ 15);
      if (!_hotline!.isFreeCharge!) {
        if (_minCallTime < 20) {
          _callsReader.onMakeCall(false);
          throw {"code": "not_enough_balance"};
        } else {
          _callsReader.updateCallTime(_minCallTime);
        }
      }

      _callsReader.updateIsVideoCall(isVideoCall);
      _callsReader.updateStates(CallingState.outgoing);

      await _makeCall(phoneNumber, isVideoCall);

      final caller = _authenticateReader.currentUser?.extension?.extensionNumber ?? "";

      portsipContext.read<CallLogProvider>().setNewCallLog(
        CreateCallLogRequest(
          callState: CallState.not_answered,
          direction: CallLog.isInternalCall(
              CallLog(callee: phoneNumber, caller: caller),
              _authenticateReader.currentExtensionsList
          ) ? "ext_out" : "out",
          callee: phoneNumber,
          caller: caller,
          extensionId: _authenticateReader.currentUser?.extension?.id,
          startedAt: DateTime.now().toUtc()
        )
      );

      _mapCallNumberToContactName(phoneNumber);
      _callsReader.onMakeCall(false);

    } catch (e) {
      print("ERROR in callOut $e");
      portsipHangUp();
      _callsReader.onMakeCall(false);
      throw e;
    }

  }

  static Future<void> _makeCall(String phoneNumber, bool isVideoCall) async {
    try {
      await platform.invokeMethod("callOut", {
        'phoneNumber': phoneNumber, 'videoCall': isVideoCall
      });
    } on PlatformException catch(e) {
      print("ERROR in makeCall() $e");
      throw e;
    }
  }

  static Future<bool> checkPermissionsForMakingCall(BuildContext context) async {
    final permissionGranted = await NativePermissions.checkPermissions([Permission.microphone, Permission.camera]);
    if (!permissionGranted) {
      NativePermissions.goToAppSettings(
          context: context,
          title: "Quyền truy cập microphone và máy ảnh",
          description: "eTelecom - Tổng đài CSKH muốn truy cập microphone và máy ảnh để thực hiện cuộc gọi."
      );

      return false;
    }

    return true;
  }

  static Future<void> portsipCallIn({
    required String phoneNumber,
    bool isVideoCall = false
  }) async {
    try {
      portsipContext.read<CallsProvider>().updateIsVideoCall(isVideoCall);
      portsipContext.read<CallsProvider>().updateStates(CallingState.incoming);

      final _authenticateReader = portsipContext.read<AuthenticateProvider>();

      final callee = _authenticateReader.currentUser?.extension?.extensionNumber ?? "";
      portsipContext.read<CallLogProvider>().setNewCallLog(
        CreateCallLogRequest(
          callState: CallState.not_answered,
            direction: CallLog.isInternalCall(
                CallLog(callee: callee, caller: phoneNumber),
                _authenticateReader.currentExtensionsList
            ) ? "ext_in" : "in",
          caller: phoneNumber,
          callee: callee,
          extensionId: _authenticateReader.currentUser?.extension?.id,
          startedAt: DateTime.now().toUtc()
        )
      );

      _mapCallNumberToContactName(phoneNumber);
    } on PlatformException catch (e) {
      print("ERROR in callIn $e");
      portsipReject();
    }
  }

  static Future<void> portsipHangUp() async {
    try {
      portsipContext.read<CallsProvider>().updateCallingName("");
      portsipContext.read<CallsProvider>().updateCallingNumber("");
      portsipContext.read<CallsProvider>().updateStates(CallingState.none);

      await platform.invokeMethod("hangUp");
    } on PlatformException catch (e) {
      print("ERROR in hangUp $e");
    }
  }

  static Future<void> portsipAnswer() async {
    try {
      portsipContext.read<CallsProvider>().updateStates(CallingState.processing);

      await platform.invokeMethod("answerCall");

      portsipContext.read<CallLogProvider>().setNewCallLog(CreateCallLogRequest(
          callState: CallState.answered
      ));
    } on PlatformException catch (e) {
      print("ERROR in answer $e");
      portsipHangUp();
    }
  }

  static Future<void> portsipReject() async {
    try {
      portsipContext.read<CallsProvider>().updateCallingName("");
      portsipContext.read<CallsProvider>().updateCallingNumber("");
      portsipContext.read<CallsProvider>().updateStates(CallingState.none);

      await platform.invokeMethod("rejectCall");
      await CallSessionApi.destroyCallSession(
        xSessionId: portsipContext.read<CallLogProvider>().newCallLog.externalSessionId ?? "",
        extensionId: portsipContext.read<AuthenticateProvider>().currentUser?.extension?.id ?? ""
      );
    } on PlatformException catch (e) {
      print("ERROR in reject $e");
    }
  }

  static Future<void> portsipHold() async {
    try {
      await platform.invokeMethod("hold");
    } on PlatformException catch (e) {
      print("ERROR in hold $e");
    }
  }

  static Future<void> portsipUnHold() async {
    try {
      await platform.invokeMethod("unHold");
    } on PlatformException catch (e) {
      print("ERROR in unHold $e");
    }
  }

  static Future<void> portsipSpeakerOn() async {
    try {
      await platform.invokeMethod("speakerOn");
    } on PlatformException catch (e) {
      print("ERROR in speakerOn $e");
    }
  }

  static Future<void> portsipSpeakerOff() async {
    try {
      await platform.invokeMethod("speakerOff");
    } on PlatformException catch (e) {
      print("ERROR in speakerOff $e");
    }
  }

  static Future<void> microphoneOn() async {
    try {
      await platform.invokeMethod("microphoneOn");
    } on PlatformException catch (e) {
      print("ERROR in microphoneOn $e");
    }
  }

  static Future<void> microphoneOff() async {
    try {
      await platform.invokeMethod("microphoneOff");
    } on PlatformException catch (e) {
      print("ERROR in microphoneOff $e");
    }
  }

  static Future<void> switchToFrontCamera() async {
    try {
      await platform.invokeMethod("frontCamera");
    } on PlatformException catch (e) {
      print("ERROR in switchToFrontCamera $e");
    }
  }

  static Future<void> switchToBackCamera() async {
    try {
      await platform.invokeMethod("backCamera");
    } on PlatformException catch (e) {
      print("ERROR in switchToBackCamera $e");
    }
  }

  static Future<void> cameraOn() async {
    try {
      await platform.invokeMethod("cameraOn");
    } on PlatformException catch (e) {
      print("ERROR in cameraOn $e");
    }
  }

  static Future<void> cameraOff() async {
    try {
      await platform.invokeMethod("cameraOff");
    } on PlatformException catch (e) {
      print("ERROR in cameraOff $e");
    }
  }

  static Future<void> sendDtmf(String rawCode) async {
    try {
      await platform.invokeMethod("sendDtmf", {
        'code': DTMFMap[rawCode]
      });
    } on PlatformException catch (e) {
      print("ERROR in sendDtmf $e");
    }
  }

  static Future<void> refer(String referTo) async {
    try {
      await platform.invokeMethod("refer", {
        'referTo': referTo
      });
    } on PlatformException catch (e) {
      print("ERROR in refer $e");
    }
  }

  static void hookNativeEvents(BuildContext context) {
    portsipContext = context;

    platform.setMethodCallHandler((call) async {
      var callsReader = portsipContext.read<CallsProvider>();

      try {
        switch (call.method) {
          case 'onRegisterFailure':

            String code = jsonDecode(call.arguments)["code"] ?? "";

            if (code != callsReader.portsipRegisterFailureCode) {
              callsReader.savePSRegisterFailureCode(code);
            }

            return;

          case 'callIn':
            String? caller = jsonDecode(call.arguments)["caller"];
            String callingNumber = caller!.split('sip:')[1].split('@')[0];

            bool? isVideoCall = jsonDecode(call.arguments)["isVideo"];

            portsipCallIn(
                phoneNumber: callingNumber,
                isVideoCall: isVideoCall == null ? false : isVideoCall
            );

            return;

          case 'callAnswered':
            callsReader.updateStates(CallingState.processing);

            portsipContext.read<CallLogProvider>().setNewCallLog(CreateCallLogRequest(
                callState: CallState.answered
            ));

            if (call.arguments != null && call.arguments.length > 0) {
              String? caller = jsonDecode(call.arguments)["caller"];
              _mapCallNumberToContactName(caller);
            }

            return;

          case 'callEnded':

            callsReader.updateCallingName("");
            callsReader.updateCallingNumber("");
            callsReader.updateStates(CallingState.none);
            callsReader.updateCallTime(0);

            if (callsReader.speakerOn) {
              callsReader.toggleSpeaker();
            }
            if (!callsReader.microphoneOn) {
              callsReader.toggleMicrophone();
            }
            if (!callsReader.cameraOn) {
              callsReader.toggleCamera();
            }
            if (!callsReader.frontCamera) {
              callsReader.switchCamera();
            }

            callsReader.disposeCamera();
            callsReader.toggleDtmfPad(reset: true);
            callsReader.toggleReferPad(reset: true);
            // VibrationService.endVibration();

            portsipContext.read<CallLogProvider>().setNewCallLog(CreateCallLogRequest(
                endedAt: DateTime.now().toUtc()
            ));

            CallLogApi.createCallLog(portsipContext.read<CallLogProvider>().newCallLog);

            return;

          case 'sipMessageResponse':
            if (call.arguments == null || call.arguments.length == 0) {
              return;
            }

            String sipMessage = jsonDecode(call.arguments)['sipMessage'];
            String _splitMessage = sipMessage.split('X-Session-Id:')[1];
            if (_splitMessage.isEmpty) {
              return;
            }

            String xSessionId = _splitMessage.substring(0, 18);
            portsipContext.read<CallLogProvider>().setNewCallLog(CreateCallLogRequest(
                externalSessionId: xSessionId
            ));

            return;

          case 'disposeCamera':
            await callsReader.disposeCamera();

            try {
              await platform.invokeMethod("finishDisposingCamera");
            } on PlatformException catch (e) {
              print("ERROR in finishDisposingCamera $e");
            }

            return;

        }
      } catch (e) {
        print("ERROR in _hookNativeEvents $e");
        hookNativeEvents(context);
      }
      return;
    });
  }

  static Future<void> _mapCallNumberToContactName(String? phone) async {

    if (phone != null && phone.isNotEmpty) {
      portsipContext.read<CallsProvider>().updateCallingNumber(phone);

      final _contact = await _getContactsByPhone(phone);

      if (_contact != null && _contact.fullName.isNotEmpty) {
        portsipContext.read<CallsProvider>().updateCallingName(_contact.fullName);
        portsipContext.read<CallLogProvider>().setNewCallLog(CreateCallLogRequest(
            contactId: _contact.id
        ));
      } else {
        final _relationship = await _getAccountUsersByPhone(phone);

        if (_relationship != null && _relationship.fullName != null && _relationship.fullName!.isNotEmpty) {

          portsipContext.read<CallsProvider>().updateCallingName(_relationship.fullName ?? "");
          portsipContext.read<CallsProvider>().updateCallingNumber('Nội bộ - ' + phone);

        } else {
          final contactsDevice = portsipContext.read<ContactProvider>().contactsDevice;
          if (contactsDevice.length > 0) {

            final _contactDevice = contactsDevice.firstWhere(
            (contact) => contact.phones!.toList().length > 0 && contact.phones!.toList()[0].value.toString() == phone,
            orElse: () => ContactDevice.Contact());

            if (_contactDevice.displayName!.isNotEmpty) {

              portsipContext.read<CallsProvider>().updateCallingName(_contactDevice.displayName ?? "");
              portsipContext.read<CallsProvider>().updateCallingNumber('Từ máy - ' + phone);

            }

          }
          
        }
      }
    }

  }

  static Future<Contact?> _getContactsByPhone(String phone) async {
    try {
      if (phone.length == 0) {
        return null;
      }

      final GetContactsFilter filter = GetContactsFilter(phone: phone);

      final res = await ContactApi.getContacts(GetContactsRequest(filter: filter));

      final List<dynamic> _rawContacts = res["contacts"];

      if (_rawContacts.isEmpty) {
        return null;
      }

      final Contact contact = Contact.fromJson(_rawContacts[0]);

      return contact;
    } catch (e) {
      print("ERROR in _getContactsByPhone $e");
      return null;
    }
  }

  static Future<AccountUser?> _getAccountUsersByPhone(String phone) async {
    try {
      if (phone.length == 0) {
        return null;
      }

      final GetAccountUsersFilter _filter = GetAccountUsersFilter(extensionNumber: phone);
      final GetAccountUsersRequest request = GetAccountUsersRequest(filter: _filter);

      final res = await AccountUserApi.getAccountUsers(request);

      final List<dynamic> _rawLists = res["account_users"];

      final List<AccountUser> accUsers = _rawLists.map((e) => AccountUser.fromJson(e)).toList();

      if (accUsers.isNotEmpty) {
        return accUsers[0];
      }

      return null;
    } catch (e) {
      print("ERROR in PortsipService._getAccountUsersByPhone $e");
      return null;
    }
  }

  static Future<int> _calcBalanceUser() async {
    try {
      final res = await SummaryApi.calcBalanceUser(CreditClassify.telecom);
      final int balance = res["telecom_balance"];

      return balance;
    } catch (e) {
      print("ERROR in _calcBalanceUser $e");
      return 0;
    }
  }

  static Future<Hotline?> _getHotline(String hotlineId) async {
    try {
      final res = await HotlineApi.getHotlines();

      final List<dynamic> _rawHotlines = (res as dynamic)["hotlines"];
      final List<Hotline> hotlines = _rawHotlines.map((line) => Hotline.fromJson(line)).toList();

      if (hotlines.length == 0) {
        return null;
      }
      final Hotline _hotline =
          hotlines.firstWhere((line) => line.id == hotlineId, orElse: () => Hotline());
      return _hotline;
    } catch (e) {
      print("ERROR in _getHotline $e");
      return null;
    }
  }

}
