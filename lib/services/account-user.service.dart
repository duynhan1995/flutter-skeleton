import 'package:etelecom/api/shop/account-user.api.dart';
import 'package:etelecom/api/shop/extension.api.dart';
import 'package:etelecom/models/account-user/AccountUser.dart';
import 'package:etelecom/models/base/paging/cursor-paging.dart';
import 'package:etelecom/models/extension/Extension.dart';
import 'package:etelecom/providers/account-user/account-user.provider.dart';
import 'package:etelecom/utils/array-handler.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AccountUserService {

  static Future<void> getAccountUsers({
    required BuildContext context,
    bool doLoading = true
  }) async {

    if (doLoading) {
      context.read<AccountUserProvider>().setLoading(true);
    }

    try {

      final _paging = context.read<AccountUserProvider>().currentPaging;
      final _filter = context.read<AccountUserProvider>().currentFilter;

      final GetAccountUsersRequest request = GetAccountUsersRequest(
          filter: _filter,
          paging: _paging
      );

      final listExtensions = await _getAllExtensions();
      final res = await AccountUserApi.getAccountUsers(request);

      final List<dynamic> _rawLists = res["account_users"];
      final Map<String, dynamic> _rawPaging = res["paging"];

      final List<AccountUser> accUsers = _rawLists.map((e) => AccountUser.fromJson(e)).toList();
      accUsers.forEach((element) {
        element.extensionNumber = listExtensions.firstWhere(
          (ext) => ext.userId == element.userId,
          orElse: () => Extension()
        ).extensionNumber;
      });

      final CursorPaging paging = CursorPaging.fromJson(_rawPaging);
      context.read<AccountUserProvider>().setPaging(next: paging.next);

      context.read<AccountUserProvider>().setAccountUsers(accUsers);

    } catch (e) {
      print("ERROR in AccountUserService.getAccountUsers $e");
    }
    if (doLoading) {
      context.read<AccountUserProvider>().setLoading(false);
    }
  }

  static Future<List<AccountUser>> searchAccountUsers(String keyword) async {
    List<AccountUser> results = [];
    try {

      final listExtensions = await _getAllExtensions();

      if (keyword.isEmpty) {
        final CursorPaging cursorPaging = CursorPaging(limit: 20);
        GetAccountUsersRequest request = GetAccountUsersRequest(paging: cursorPaging);

        dynamic res = await AccountUserApi.getAccountUsers(request);

        List<dynamic> _rawList = res["account_users"];

        results = _rawList.map((e) => AccountUser.fromJson(e)).toList();

        results.forEach((element) {
          element.extensionNumber = listExtensions.firstWhere(
            (ext) => ext.userId == element.userId,
            orElse: () => Extension()
          ).extensionNumber;
        });

        return results;
      }

      final CursorPaging cursorPaging = CursorPaging(limit: 100);

      // note: search by name
      GetAccountUsersFilter filter = GetAccountUsersFilter(name: keyword);
      GetAccountUsersRequest request = GetAccountUsersRequest(paging: cursorPaging, filter: filter);

      dynamic res = await AccountUserApi.getAccountUsers(request);
      List<dynamic> _rawList = res["account_users"];
      _rawList.map((e) => AccountUser.fromJson(e)).toList().forEach((element) {
        results = ArrayHandler<AccountUser>().upsert(results, element);
      });

      // note: search by phone
      filter = GetAccountUsersFilter(phone: keyword);
      request = GetAccountUsersRequest(paging: cursorPaging, filter: filter);

      res = await AccountUserApi.getAccountUsers(request);
      _rawList = res["account_users"];
      _rawList.map((e) => AccountUser.fromJson(e)).toList().forEach((element) {
        results = ArrayHandler<AccountUser>().upsert(results, element);
      });

      // note: search by extNumber
      filter = GetAccountUsersFilter(extensionNumber: keyword);
      request = GetAccountUsersRequest(paging: cursorPaging, filter: filter);

      res = await AccountUserApi.getAccountUsers(request);
      _rawList = res["account_users"];
      _rawList.map((e) => AccountUser.fromJson(e)).toList().forEach((element) {
        results = ArrayHandler<AccountUser>().upsert(results, element);
      });

      results.forEach((element) {
        element.extensionNumber = listExtensions.firstWhere(
          (ext) => ext.userId == element.userId,
          orElse: () => Extension()
        ).extensionNumber;
      });

      return results;

    } catch (e) {
      print("ERROR in searchContacts $e");
      return [];
    }
  }

  static Future<List<Extension>> _getAllExtensions() async {
    try {
      final res = await ExtensionApi.getExtensions(GetExtensionsRequest());
      final List<dynamic> _extensions = res["extensions"];
      final List<Extension> extensions = _extensions
          .map((extension) => Extension.fromJson(extension))
          .toList();
      return extensions;
    } catch (e) {
      print("ERROR in AccountUserService.getAllExtensions $e");
      return [];
    }
  }

}
