import 'package:etelecom/api/shop/account-user.api.dart';
import 'package:etelecom/api/shop/contact.api.dart';
import 'package:etelecom/api/shop/ticket.api.dart';
import 'package:etelecom/models/account-user/AccountUser.dart';
import 'package:etelecom/models/contact/Contact.dart';
import 'package:etelecom/models/ticket/Ticket.dart';
import 'package:etelecom/providers/account-user/account-user.provider.dart';
import 'package:etelecom/providers/ticket/ticket.provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class TicketService {
  static Future<void> createTicket(BuildContext context, CreateTicketRequest request) async {
    try {
      await TicketApi.createTicket(request);

      context.read<TicketProvider>().setPaging(offset: 0);
      await getTickets(context: context, doLoading: false);
    } catch (e) {
      print("ERROR in createTicket Service $e");
      throw e;
    }
  }

  static Future<void> getTickets({BuildContext? context, bool doLoading = true}) async {
    if (doLoading) {
      context!.read<TicketProvider>().setLoading(true);
    }
    try {
      final _paging = context!.read<TicketProvider>().paging;

      final _filter = context.read<TicketProvider>().filter;
      final GetTicketsRequest request = GetTicketsRequest(paging: _paging, filter: _filter);
      
      await TicketService.getTicketLabels(context: context);
      
      final res = await TicketApi.getTickets(request);

      final List<dynamic> _rawTickets = res["tickets"];
      final List<Ticket> tickets = _rawTickets.map((_ticket) => Ticket.fromJson(_ticket)).toList();
      
      final List<String> createdByIds = tickets.map((e) => e.createdBy).toList();
      final List<String> closedByIds = tickets.map((e) => e.closedBy).toList();
      final List<String> assignedIds = tickets.fold([], (value, element) => [...value, ...?element.assignedUserIds]);

      final List<AccountUser> accUsers = await _getAccountUsersByListUserIds([
        ...createdByIds,
        ...closedByIds,
        ...assignedIds
      ]);

      final List<String> contactIds = tickets.map((e) {
        if (e.refType == TicketRefType.contact) return e.refId;
        return "";
      }).where((element) => element.isNotEmpty).toList();

      final List<Contact> contacts = await _getContactsByIDs(contactIds);
      
      context.read<TicketProvider>().setTickets(tickets.map((_ticket) {
        return reMapTicket(
          context: context,
          ticket: _ticket,
          accUsers: accUsers,
          contacts: contacts
        );
      }).toList());
    } catch (e) {
      print("ERROR in getTickets Service $e");
    }
    if (doLoading) {
      context!.read<TicketProvider>().setLoading(false);
    }
  }
  
  static Future<List<AccountUser>> _getAccountUsersByListUserIds(List<String> userIds) async {
    try {

      final Set<String> setOfUserIds = userIds.toSet();
      userIds.retainWhere((element) => setOfUserIds.remove(element));

      final GetAccountUsersFilter _filter = GetAccountUsersFilter(
          userIds: userIds,
          hasExtension: null
      );
      final GetAccountUsersRequest _request = GetAccountUsersRequest(filter: _filter);

      final res = await AccountUserApi.getAccountUsers(_request);

      final List<dynamic> _rawList = res["account_users"];

      final List<AccountUser> accUsers = _rawList.map((e) => AccountUser.fromJson(e)).toList();

      return accUsers;
      
    } catch (e) {
      print("ERROR in TicketService._getAccountUsersByListUserIds $e");
      return [];
    }
  }

  static Future<List<Contact>> _getContactsByIDs(List<String> contactIds) async {
    try {
      if (contactIds.isEmpty) {
        return [];
      }

      final Set<String> setOfContactIds = contactIds.toSet();
      contactIds.retainWhere((element) => setOfContactIds.remove(element));

      final GetContactsFilter _filter = GetContactsFilter(ids: contactIds);

      final res = await ContactApi.getContacts(GetContactsRequest(filter: _filter));

      final List<dynamic> _rawList = res["contacts"];

      final List<Contact> contacts = _rawList.map((_cont) => Contact.fromJson(_cont)).toList();

      return contacts;
    } catch (e) {
      print("ERROR in TicketService._getContactsByIDs $e");
      return [];
    }
  }

  static Future<void> getTicketLabels({BuildContext? context}) async {
    try {
      final GetTicketLabelsFilter _filter = GetTicketLabelsFilter(type: "internal");
      final GetTicketLabelsRequest request = GetTicketLabelsRequest(tree: true, filter: _filter);
      final res = await TicketApi.getTicketLabels(request);
      final List<dynamic> _ticketLabels = res["ticket_labels"];
      final List<TicketLabel> ticketLabels = _ticketLabels.map((_ticketLabel) => TicketLabel.fromJson(_ticketLabel)).toList();
      context!.read<TicketProvider>().setTicketLabels(ticketLabels.toList());
    } catch (e) {
      print("ERROR in getTicketLabels Service $e");
    }
  }

  static Future<void> getTicketComments({BuildContext? context, String? ticketId}) async {
    try {
      final GetTicketCommentsRequest request = GetTicketCommentsRequest(filter: null, paging: null, ticketId: ticketId);
      final res = await TicketApi.getTicketComments(request);
      final List<dynamic> _ticketComments = res["ticket_comments"];
      final List<TicketComment> ticketComments = _ticketComments.map((_ticket) => TicketComment.fromJson(_ticket)).toList();
      context!.read<TicketProvider>().setTicketComments(ticketComments.map((_ticket) {
        _ticket.createdByUser = context.read<AccountUserProvider>().getAccountUserByUserId(_ticket.createdBy!);
        return _ticket;
      }).toList());
    } catch (e) {
      print("ERROR in getTicketLabels Service $e");
    }
  }

  static updateTicket(BuildContext context, Ticket _ticket) async {
    try {
      final accountUserIds =[..._ticket.assignedUserIds!, _ticket.createdBy, _ticket.confirmedBy, _ticket.closedBy];
      final List<AccountUser> accUsers = await _getAccountUsersByListUserIds(accountUserIds);
      final List<String> contactIds = _ticket.refType == TicketRefType.contact ? [_ticket.refId] : [];
      final List<Contact> contacts = await _getContactsByIDs(contactIds);

      context.read<TicketProvider>().setTicketActive(reMapTicket(
          context: context,
          ticket: _ticket,
          accUsers: accUsers,
          contacts: contacts
      ));
      var _tickets = context.read<TicketProvider>().tickets;
      context.read<TicketProvider>().setTickets(_tickets.map((ticket) {
        if (ticket.id == _ticket.id) {
          ticket.state = _ticket.state;
          ticket.stateDisplay = TICKET_STATE[_ticket.state];
        }
        return ticket;
      }).toList());
    } catch (e) {
      print("ERROR in updateTicket Service $e");
    }
    
  }

  static Future<void> assignTicket(BuildContext context, String ticketId, List<String> assignedUserIds) async {
    try {
      var res = await TicketApi.assignTicket(assignedUserIds, ticketId);
      var _res = Ticket.fromJson(res);
      await updateTicket(context, _res);
    } catch (e) {
      print("ERROR in assignTicket Service $e");
      throw (e);
    }
  }

  static Future<void> unAssignTicket(BuildContext context, String ticketId) async {
    try {
      var res = await TicketApi.unAssignTicket(ticketId);
      var _res = Ticket.fromJson(res);
      await updateTicket(context, _res);
    } catch (e) {
      print("ERROR in assignTicket Service $e");
      throw (e);
    }
  }

  static Future<void> confirmTicket(BuildContext context, String ticketId, [String? note]) async {
    try {
      var res = await TicketApi.confirmTicket(ticketId, note);
      var _res = Ticket.fromJson(res);
      await updateTicket(context, _res);
    } catch (e) {
      print("ERROR in confirmTicket Service $e");
      throw (e);
    }
  }

  static Future<void> closeTicket(BuildContext context, TicketState state, String ticketId, [String? note]) async {
    try {
      var res = await TicketApi.closeTicket(state, ticketId, note);
      var _res = Ticket.fromJson(res);
      await updateTicket(context, _res);
    } catch (e) {
      print("ERROR in closeTicket Service $e");
      throw (e);
    }
  }

  static Future<void> reopenTicket(BuildContext context, String ticketId, [String? note]) async {
    try {
      var res = await TicketApi.reopenTicket(ticketId, note);
      var _res = Ticket.fromJson(res);
      await updateTicket(context, _res);

    } catch (e) {
      print("ERROR in reopenTicket Service $e");
      throw (e);
    }
  }

  static Future<void> createTicketComment(BuildContext context, CreateTicketCommentRequest request) async {
    try {
      await TicketApi.createTicketComment(request);
    } catch (e) {
      print("ERROR in createTicketComment Service $e");
      throw e;
    }
  }

  static Ticket reMapTicket({
    required BuildContext context,
    required Ticket ticket,
    List<Contact>? contacts,
    List<AccountUser>? accUsers
  }) {

    if (contacts != null && contacts.isNotEmpty) {
      ticket.contact = contacts.firstWhere(
        (contact) => ticket.refId == contact.id,
        orElse: () => Contact()
      );
    }

    ticket.assignedUsers = [];
    if (accUsers != null && accUsers.isNotEmpty) {
      ticket.createdByUser = accUsers.firstWhere(
        (element) => element.userId == ticket.createdBy,
        orElse: () => AccountUser()
      );
      ticket.closedByUser = accUsers.firstWhere(
        (element) => element.userId == ticket.closedBy,
        orElse: () => AccountUser()
      );
      if (ticket.assignedUserIds!.isNotEmpty) {
        ticket.assignedUserIds!.forEach((assignedUserId) {
          final _accUser = accUsers.firstWhere(
            (element) => element.userId == assignedUserId,
            orElse: () => AccountUser()
          );
          if (_accUser.userId != null) {
            ticket.assignedUsers!.insert(0, _accUser);
          }
        });
      }
    }

    ticket.stateDisplay = TICKET_STATE[ticket.state];
    if (ticket.labelIds!.length > 0) {
      final _ticketLabels = context.read<TicketProvider>().ticketLabels;
      ticket.label = _ticketLabels.firstWhere((label) => label.id == ticket.labelIds![0], orElse: () => TicketLabel());
    }

    return ticket;
  }
}
