
import 'package:etelecom/services/portsip.service.dart';
import 'package:just_audio/just_audio.dart';

class EAudioService {
  static AudioPlayer audioPlayer = AudioPlayer();

  static Future<void> playLocalAudio(String audioFileName, {
    bool isLoop = false,
    bool speakerOn = true
  }) async {

    await audioPlayer.setAsset("assets/audios/$audioFileName");

    if (!speakerOn) {
      PortsipService.portsipSpeakerOff();
    }

    audioPlayer.play();

    if (isLoop) {
      audioPlayer.setLoopMode(LoopMode.one);
    } else {
      audioPlayer.setLoopMode(LoopMode.off);
    }

  }

  static Future<void> stopLocalAudio() async {
    await audioPlayer.stop();
  }
}
