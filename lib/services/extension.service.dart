import 'package:etelecom/api/shop/extension.api.dart';
import 'package:etelecom/models/extension/Extension.dart';
import 'package:etelecom/providers/extension/extension.provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ExtensionService {
  static Future<void> getExtensions({BuildContext? context}) async {
    try {
      final res = await ExtensionApi.getExtensions(GetExtensionsRequest());
      final List<dynamic> _extensions = res["extensions"];
      final List<Extension> extensions = _extensions
          .map((extension) => Extension.fromJson(extension))
          .toList();
      context?.read<ExtensionProvider>().setExtensions(extensions);
    } catch (e) {
      print("ERROR in ExtensionService.getExtensions $e");
      context?.read<ExtensionProvider>().setExtensions([]);
    }
  }
}
