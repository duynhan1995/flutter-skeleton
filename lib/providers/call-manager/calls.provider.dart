import 'dart:async';

import 'package:camera/camera.dart';
import 'package:etelecom/utils/string-handler.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';

import '../../app_config.dart';

enum CallingState {
  none,
  outgoing,
  incoming,
  processing
}

Timer? timer;

class CallsProvider with ChangeNotifier, DiagnosticableTreeMixin {

  String _portsipRegisterFailureCode = "";
  bool _portsipRegistering = false;

  int _callingTime = 0;
  CallingState _currentState = CallingState.none;
  bool _makingCall = false;
  String _callingName = "";
  String _callingNumber = "";
  bool _isVideoCall = false;
  bool _speakerOn = false;
  bool _microphoneOn = true;
  bool _frontCamera = true;
  bool _cameraOn = true;
  CameraController? _cameraController;
  int _minCallTime = 0;
  bool _showDtmfPad = false;
  bool _showReferPad = false;

  String get portsipRegisterFailureCode => _portsipRegisterFailureCode;
  bool get portsipRegistering => _portsipRegistering;

  String get callingTime => StringHandler.compactSecondsToTime(_callingTime);
  CallingState get currentState => _currentState;
  bool get makingCall => _makingCall;
  String get callingName => _callingName;
  String get callingNumber => _callingNumber;
  bool get isVideoCall => _isVideoCall;
  bool get speakerOn => _speakerOn;
  bool get microphoneOn => _microphoneOn;
  bool get frontCamera => _frontCamera;
  bool get cameraOn => _cameraOn;
  CameraController? get cameraController => _cameraController;
  int get minCallTime => _minCallTime;
  bool get showDtmfPad => _showDtmfPad;
  bool get showReferPad => _showReferPad;

  onRegisterPortsip(bool registering) {
    _portsipRegistering = registering;
    notifyListeners();
  }

  savePSRegisterFailureCode(String code) {
    _portsipRegisterFailureCode = code;
    notifyListeners();

    if (code.isNotEmpty) {
      onRegisterPortsip(true);
    } else {
      Timer(Duration(seconds: 2), () => onRegisterPortsip(false));
    }
  }

  onMakeCall(bool isMakingCall) {
    _makingCall = isMakingCall;
    notifyListeners();
  }

  startTalking() {
    if (timer != null) {
      timer!.cancel();
    }

    timer = Timer.periodic(new Duration(seconds: 1), (timer) {
      _callingTime = timer.tick;
      notifyListeners();
    });
  }

  endTalking() {
    _callingTime = 0;
    if (timer != null) {
      timer!.cancel();
    }
  }

  updateCallingName(String callingName) {
    _callingName = callingName;
    notifyListeners();
  }

  updateCallTime(int time) {
    _minCallTime = time;
    notifyListeners();
  }

  updateCallingNumber(String callingNumber) {
    _callingNumber = callingNumber;
    notifyListeners();
  }

  updateIsVideoCall(bool isVideo) {
    _isVideoCall = isVideo;
    notifyListeners();
  }

  updateStates(CallingState state) {
    _currentState = state;

    if (state == CallingState.processing) {
      startTalking();
    }
    if (state == CallingState.none) {
      endTalking();
    }

    notifyListeners();
  }

  toggleSpeaker() {
    _speakerOn = !_speakerOn;
    notifyListeners();
  }

  toggleMicrophone() {
    _microphoneOn = !_microphoneOn;
    notifyListeners();
  }

  switchCamera({bool reInit = false}) {
    _frontCamera = !_frontCamera;
    notifyListeners();

    if (reInit) {

      disposeCamera().then((value) {
        if (_frontCamera) {
          _cameraController = CameraController(AppConfig.frontCamera!, ResolutionPreset.max);
        } else {
          _cameraController = CameraController(AppConfig.backCamera!, ResolutionPreset.max);
        }

        _cameraController?.initialize().then((_) => notifyListeners());
      });

    }
  }

  toggleCamera() {
    _cameraOn = !_cameraOn;
    notifyListeners();
  }

  toggleDtmfPad({reset = false}) {
    if (reset) {
      _showDtmfPad = false;
    } else {
      _showDtmfPad = !_showDtmfPad;
    }
    notifyListeners();
  }

  toggleReferPad({reset = false}) {
    if (reset) {
      _showReferPad = false;
    } else {
      _showReferPad = !_showReferPad;
    }
    notifyListeners();
  }

  Future<void> initCameraController() async {
    if (AppConfig.frontCamera != null) {
      _cameraController = CameraController(AppConfig.frontCamera!, ResolutionPreset.max);
    } else if (AppConfig.backCamera != null) {
      _cameraController = CameraController(AppConfig.backCamera!, ResolutionPreset.max);
    }

    await _cameraController?.initialize();

    notifyListeners();
  }

  Future<void> disposeCamera() async {
    await _cameraController?.dispose();
    notifyListeners();
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
  }
}
