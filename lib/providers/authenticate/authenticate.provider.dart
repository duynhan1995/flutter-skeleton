import 'dart:convert';

import 'package:etelecom/api/etop/user-api.dart';
import 'package:etelecom/api/shop/extension.api.dart';
import 'package:etelecom/models/Account.dart';
import 'package:etelecom/models/shop/Shop.dart';
import 'package:etelecom/models/User.dart';
import 'package:etelecom/models/extension/Extension.dart';
import 'package:etelecom/services/portsip.service.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthenticateProvider with ChangeNotifier, DiagnosticableTreeMixin {

  AuthenticateProvider() {
    updateSessionInfo();
  }

  String? _token;
  User? _user;
  Permission? _permission;
  List<Account> _accountsList = [];
  List<Extension> _extensionsList = [];

  Shop? _shop;
  bool _sessionInfoUpdating = false;

  String? get currentToken => _token;
  User? get currentUser => _user;
  Permission? get currentPermission => _permission;
  List<Account> get currentAccountsList => _accountsList;
  List<Extension> get currentExtensionsList => _extensionsList;

  Shop? get currentShop => _shop;
  bool get sessionInfoUpdating => _sessionInfoUpdating;

  Future<SharedPreferences> checkToken() async {
    final prefs = await SharedPreferences.getInstance();
    try {
      updateToken(prefs.getString('token') ?? "");
      checkUser(prefs);
      checkShop(prefs);
    } catch (e) {
      print("ERROR in checkToken $e");
      updateToken("");
      resetEverything();
    }
    return prefs;
  }

  checkUser(SharedPreferences prefs) {
    String userStored = prefs.getString("user") ?? "";
    if (userStored.isNotEmpty) {
      final Map<String, dynamic> userMapped = jsonDecode(userStored);
      User user = User.fromJson(userMapped);

      updateUser(user);
    }
  }

  checkShop(SharedPreferences prefs) {
    String shopStored = prefs.getString("shop") ?? "";
    if (shopStored.isNotEmpty) {
      final Map<String, dynamic> shopMapped = jsonDecode(shopStored);
      Shop shop = Shop.fromJson(shopMapped);

      updateShop(shop);
    }
  }

  Future<void> updateSessionInfo() async {
    _sessionInfoUpdating = true;
    notifyListeners();

    try {
      SharedPreferences prefs = await checkToken();

      var res = await UserApi.sessionInfo(prefs.getString('token') ?? "");

      final List<dynamic> availableAccounts = res["available_accounts"] ?? [];

      if (availableAccounts.isNotEmpty) {
        final List<Account> accounts = availableAccounts.map((acc) {
          return Account.fromJson(acc);
        }).toList();

        if (accounts.length > 0) {
          final shopAccounts = accounts.where((acc) => acc.type == 'shop').toList();

          final _currAccId = prefs.getString('accountId') ?? "";
          final _firstAcc = shopAccounts[0];

          res = await UserApi.switchAccount(_currAccId.isNotEmpty ? _currAccId : (_firstAcc.id ?? ""));
          final String token = res["access_token"];
          final user = User.fromJson(res["user"]);
          final shop = Shop.fromJson(res["shop"]);
          final Permission permission = Permission.fromJson(res["account"]["user_account"]["permission"]);
          updateToken(token);
          storeAccountId(shop.id ?? "");
          updateUser(user);
          updatePermission(permission);
          updateAccountsList(shopAccounts);
          updateShop(shop);

          res = await ExtensionApi.getExtensions(GetExtensionsRequest());
          final List<dynamic> _rawExts = res["extensions"] ?? [];

          final List<Extension> _exts = _rawExts.map((ext) => Extension.fromJson(ext)).toList();

          currentUser!.extension = _exts.firstWhere((ext) => ext.userId == currentUser!.id, orElse: () => Extension(id: null));

          updateUser(currentUser);
          updateExtensionsList(_exts);

          PortsipService.portsipRegister();

        }
      } else {
        updateToken("");
        resetEverything();
      }
    } catch (e) {
      print("ERROR in updateSessionInfo $e");
    }

    _sessionInfoUpdating = false;
    notifyListeners();
  }

  Future<void> updateToken(String token) async {
    _token = token;
    notifyListeners();

    final prefs = await SharedPreferences.getInstance();
    prefs.setString('token', token);

    if (token.isEmpty) {
      PortsipService.portsipUnregister();
      prefs.clear();
      resetEverything();
    }
  }

  Future<void> updateUser(User? user) async {
    _user = user;
    notifyListeners();

    final prefs = await SharedPreferences.getInstance();
    if (user != null) {
      String stringUser = jsonEncode(user);
      prefs.setString("user", stringUser);
    } else {
      prefs.setString("user", "");
    }
  }

  Future<void> updatePermission(Permission? permission) async {
    _permission = permission;
    notifyListeners();
  }

  updateAccountsList(List<Account> accounts) async {
    _accountsList = accounts;
    notifyListeners();
  }

  Future<void> storeAccountId(String id) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('accountId', id);
  }

  updateShop(Shop? shop) async {
    _shop = shop;
    notifyListeners();

    final prefs = await SharedPreferences.getInstance();
    if (shop != null) {
      String stringShop = jsonEncode(shop);
      prefs.setString("shop", stringShop);
    } else {
      prefs.setString("shop", "");
    }
  }

  updateExtensionsList(List<Extension> extensions) async {
    _extensionsList = extensions;
    notifyListeners();
  }

  Future<void> switchShop(String shopId) async {
    try {
      PortsipService.portsipUnregister();

      dynamic res = await UserApi.switchAccount(shopId);
      final String token = res["access_token"];
      final user = User.fromJson(res["user"]);
      final shop = Shop.fromJson(res["shop"]);
      final permission = Permission.fromJson(res["account"]["user_account"]["permission"]);

      updateToken(token);
      storeAccountId(shopId);
      updateUser(user);
      updateShop(shop);
      updatePermission(permission);

      res = await ExtensionApi.getExtensions(GetExtensionsRequest());
      final List<dynamic> _rawExts = res["extensions"];

      final List<Extension> _exts = _rawExts.map((ext) => Extension.fromJson(ext)).toList();

      currentUser!.extension = _exts.firstWhere((ext) => ext.userId == currentUser!.id, orElse: () => Extension(id: null));

      updateUser(currentUser);
      updateExtensionsList(_exts);

      PortsipService.portsipRegister();
    } catch (e) {
      print("ERROR in switchShop $e");
    }
  }

  resetEverything() {
    updateUser(null);
    updateAccountsList([]);
    updateShop(null);
    storeAccountId("");
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
  }
}
