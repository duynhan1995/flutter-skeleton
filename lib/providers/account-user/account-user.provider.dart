import 'package:etelecom/api/shop/account-user.api.dart';
import 'package:etelecom/models/base/paging/cursor-paging.dart';
import 'package:etelecom/models/account-user/AccountUser.dart';
import 'package:etelecom/models/base/paging/paging.dart';
import 'package:etelecom/utils/array-handler.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';

class AccountUserProvider with ChangeNotifier, DiagnosticableTreeMixin {
  bool _loadingAccountUsers = false;
  List<AccountUser> _accountUsers = [];
  CursorPaging _paging = CursorPaging();
  GetAccountUsersFilter _filter = GetAccountUsersFilter();

  bool get loadingAccountUsers => _loadingAccountUsers;
  List<AccountUser> get accountUsers => _accountUsers;
  CursorPaging get currentPaging => _paging;
  GetAccountUsersFilter? get currentFilter => _filter;

  setPaging({String? after, int? limit, String next = ""}) {
    _paging = CursorPaging(after: after ?? currentPaging.after, next: next, limit: limit ?? DEFAULT_LIMIT);
    notifyListeners();
  }

  setFilter(GetAccountUsersFilter filter) {
    _filter = filter;
    notifyListeners();
  }

  setLoading(bool loading) {
    _loadingAccountUsers = loading;
    notifyListeners();
  }

  setAccountUsers(List<AccountUser> accUsers) {
    if (currentPaging.after == ".") {
      _accountUsers = accUsers;
    } else {
      accUsers.forEach((e) {
        _accountUsers = ArrayHandler<AccountUser>().upsert(_accountUsers, e);
      });
    }

    if (currentPaging.next.isNotEmpty) {
      setPaging(after: currentPaging.next);
    }

    notifyListeners();
  }

  updateAccountUsers(List<AccountUser> accUsers) {
    accUsers.forEach((element) {
      _accountUsers = ArrayHandler<AccountUser>().upsert(_accountUsers, element);
    });
    notifyListeners();
  }

  getAccountUserByUserId(String id) {
    final _relationship = accountUsers.firstWhere((r) => r.userId == id, orElse: () => AccountUser());
    return _relationship;
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
  }
}
