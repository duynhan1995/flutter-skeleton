import 'package:etelecom/models/extension/Extension.dart';
import 'package:etelecom/utils/array-handler.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';

class ExtensionProvider with ChangeNotifier, DiagnosticableTreeMixin {
  List<Extension> _extensions = [];

  List<Extension> get extensions => _extensions;

  setExtensions(List<Extension> extensions) {
    _extensions = extensions;
    notifyListeners();
  }

  updateExtensions(List<Extension> extensions) {
    extensions.forEach((extension) {
      _extensions = ArrayHandler<Extension>().upsert(_extensions, extension);
    });
    notifyListeners();
  }

  getExtensionById(String id) {
    final _extension = extensions.firstWhere((r) => r.id == id, orElse: () => Extension(id: null));
    return _extension;
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
  }
}
