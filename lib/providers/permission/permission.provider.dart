import 'package:flutter/foundation.dart';

class PermissionProvider with ChangeNotifier, DiagnosticableTreeMixin {
  bool _microphonePermitted = false;
  bool _cameraPermitted = false;

  bool get microphonePermitted => _microphonePermitted;
  bool get cameraPermitted => _cameraPermitted;
  bool get callPermitted => _microphonePermitted && _cameraPermitted;

  void updateMicrophonePermission(bool permitted) {
    _microphonePermitted = permitted;
    notifyListeners();
  }

  void updateCameraPermission(bool permitted) {
    _cameraPermitted = permitted;
    notifyListeners();
  }

}
