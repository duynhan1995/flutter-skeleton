import 'package:etelecom/api/shop/ticket.api.dart';
import 'package:etelecom/models/ticket/Ticket.dart';
import 'package:etelecom/models/base/paging/paging.dart';
import 'package:etelecom/utils/array-handler.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';

class TicketProvider with ChangeNotifier, DiagnosticableTreeMixin {
  bool _loadingTickets = false;
  List<Ticket> _tickets = [];
  Ticket? _ticketActive;
  List<TicketLabel> _ticketLabels = [];
  List<TicketComment> _ticketComments = [];
  Paging _paging = Paging();
  GetTicketsFilter? _filter;

  bool get loadingTickets => _loadingTickets;
  List<Ticket> get tickets => _tickets;
  Ticket? get ticketActive => _ticketActive;
  List<TicketLabel> get ticketLabels => _ticketLabels;
  List<TicketComment> get ticketComments => _ticketComments;
  Paging get paging => _paging;
  GetTicketsFilter? get filter => _filter;

  setPaging({int? offset, int? limit}) {
    _paging = Paging(offset: offset, limit: limit ?? DEFAULT_LIMIT);
    notifyListeners();
  }

  setFilter(GetTicketsFilter filter) {
    _filter = filter;
    notifyListeners();
  }

  setLoading(bool loading) {
    _loadingTickets = loading;
    notifyListeners();
  }

  setTickets(List<Ticket> ticketList) {
    if (paging.offset == 0) {
      _tickets = ticketList;
    } else {
      ticketList.forEach((ticket) {
        _tickets = ArrayHandler<Ticket>().upsert(_tickets, ticket);
      });
    }

    if (_tickets.length == paging.offset! + DEFAULT_LIMIT) {
      setPaging(offset: _tickets.length);
    }

    notifyListeners();
  }

  setTicketActive(Ticket ticket) {
    _ticketActive = ticket;
  }

  setTicketLabels(List<TicketLabel> ticketLabelList) {
    _ticketLabels = ticketLabelList;
    notifyListeners();
  }

  setTicketComments(List<TicketComment> ticketCommentList) {
    _ticketComments = ticketCommentList;
    notifyListeners();
  }

  updateTickets(List<Ticket> ticketList) {
    ticketList.forEach((ticket) {
      _tickets = ArrayHandler<Ticket>().upsert(_tickets, ticket);
    });
    notifyListeners();
  }

  updateTicketComments(List<TicketComment> ticketCommentList) {
    if (ticketComments.length == 0) {
      _ticketComments = ticketComments;
    }
    ticketCommentList.forEach((ticketComment) {
      _ticketComments = ArrayHandler<TicketComment>().upsert(_ticketComments, ticketComment);
    });
    notifyListeners();
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
  }
}
