import 'package:etelecom/api/shop/contact.api.dart';
import 'package:etelecom/models/base/paging/cursor-paging.dart';
import 'package:etelecom/models/contact/Contact.dart';
import 'package:etelecom/models/base/paging/paging.dart';
import 'package:etelecom/utils/array-handler.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:contacts_service/contacts_service.dart' as ContactDevice;

class ContactProvider with ChangeNotifier, DiagnosticableTreeMixin {

  bool _loadingContacts = false;
  List<Contact> _contacts = [];
  List<ContactDevice.Contact> _contactsDevice = [];
  CursorPaging _paging = CursorPaging();
  GetContactsFilter? _filter;

  bool get loadingContacts => _loadingContacts;
  List<Contact> get contacts => _contacts;
  List<ContactDevice.Contact> get contactsDevice => _contactsDevice;
  CursorPaging get currentPaging => _paging;
  GetContactsFilter? get currentFilter => _filter;

  setPaging({String? after, int? limit, String next = ""}) {
    _paging = CursorPaging(after: after ?? currentPaging.after, next: next, limit: limit ?? DEFAULT_LIMIT);
    notifyListeners();
  }

  setFilter(GetContactsFilter filter) {
    _filter = filter;
    notifyListeners();
  }

  setLoading(bool loading) {
    _loadingContacts = loading;
    notifyListeners();
  }

  setContacts(List<Contact> contactList) {
    if (currentPaging.after == ".") {
      _contacts = contactList;
    } else {
      contactList.forEach((cont) {
        _contacts = ArrayHandler<Contact>().upsert(_contacts, cont);
      });
    }

    if (currentPaging.next.isNotEmpty) {
      setPaging(after: currentPaging.next);
    }

    notifyListeners();
  }

  setContactsDevice(List<ContactDevice.Contact> contactList) {
    _contactsDevice = contactList;
    notifyListeners();
  }

  updateContacts(List<Contact> contactList) {
    contactList.forEach((cont) {
      _contacts = ArrayHandler<Contact>().upsert(_contacts, cont);
    });
    notifyListeners();
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
  }
}
