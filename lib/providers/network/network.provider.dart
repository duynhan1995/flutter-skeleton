import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

enum NetworkStatus {
  none,
  retrying,
  connected
}

class NetworkProvider with ChangeNotifier, DiagnosticableTreeMixin {
  final Connectivity connectivity = Connectivity();
  late StreamSubscription<ConnectivityResult> connectivitySubscription;

  NetworkStatus? _networkStatus;
  bool _checkingNetwork = false;

  NetworkStatus? get networkStatus => _networkStatus;
  bool get checkingNetwork => _checkingNetwork;

  NetworkProvider() {
    initConnectivity();
  }

  void checkNetwork(bool checking) {
    _checkingNetwork = checking;
    notifyListeners();
  }

  void retryNetwork() {
    _networkStatus = NetworkStatus.retrying;
    notifyListeners();
  }

  void networkConnected() {
    _networkStatus = NetworkStatus.connected;
    notifyListeners();
  }

  void lostConnection() {
    _networkStatus = NetworkStatus.none;
    notifyListeners();
  }

  void dispose() {
    super.dispose();
    _networkStatus = null;
    notifyListeners();
    connectivitySubscription.cancel();
  }

  Future<void> initConnectivity() async {
    ConnectivityResult result = ConnectivityResult.none;

    try {
      result = await connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print('ERROR in initConnectivity ${e.toString()}');
    }

    _updateConnectionStatus(result);
    connectivitySubscription = connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {

    switch (result) {
      case ConnectivityResult.wifi:
      case ConnectivityResult.mobile:
        if (_networkStatus == NetworkStatus.none) {
          checkNetwork(true);
        }
        retryNetwork();
        Timer(Duration(seconds: 2), () => networkConnected());
        Timer(Duration(seconds: 3), () => checkNetwork(false));
        break;
      case ConnectivityResult.none:
        checkNetwork(true);
        lostConnection();
        break;
      default:
        checkNetwork(false);
        lostConnection();
        break;
    }

  }

}
