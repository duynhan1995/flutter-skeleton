import 'package:etelecom/api/etop/user-api.dart';
import 'package:etelecom/models/Account.dart';
import 'package:etelecom/models/shop/Shop.dart';
import 'package:flutter/foundation.dart';

class ShopProvider with ChangeNotifier, DiagnosticableTreeMixin {

  List<Shop> _shopsList = [];
  bool _shopsLoading = false;

  List<Shop> get currentShopsList => _shopsList;
  bool get shopsLoading => _shopsLoading;

  Future<void> getShopsList(List<Account> shopAccounts, Shop currentShop) async {
    _shopsLoading = true;
    _shopsList = [];
    notifyListeners();

    try {

      if (shopAccounts.length > 0) {
        for (var acc in shopAccounts) {
          if (acc.id != currentShop.id) {
            final res = await UserApi.switchAccount(acc.id ?? "");
            final shop = Shop.fromJson(res["shop"]);

            _shopsList.add(shop);
          }
        }
      }

    } catch (e) {
      print("ERROR in updateShopsList $e");
    }

    _shopsLoading = false;
    notifyListeners();
  }

}
