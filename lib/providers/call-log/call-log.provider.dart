import 'dart:async';
import 'dart:io';
import 'dart:math';

import 'package:etelecom/api/shop/call-log.api.dart';
import 'package:etelecom/models/base/paging/cursor-paging.dart';
import 'package:etelecom/models/base/paging/paging.dart';
import 'package:etelecom/models/call-log/CallLog.dart';
import 'package:etelecom/utils/array-handler.dart';
import 'package:etelecom/utils/map-handler.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:just_audio/just_audio.dart';
import 'package:path_provider/path_provider.dart';
import 'package:http/http.dart' as HTTP;

class CallLogProvider with ChangeNotifier, DiagnosticableTreeMixin {

  Map<String, dynamic> _newCallLog = CreateCallLogRequest().toJson();

  AudioPlayer audioPlayer = AudioPlayer();
  StreamSubscription? _audioPositionSubscription;

  String _playingAudioId = "";
  bool _playing = true;
  bool _muted = false;
  int _position = 0;
  int _duration = 0;

  bool _loadingCallLogs = false;
  List<CallLog> _callLogs = [];
  CursorPaging _paging = CursorPaging();
  GetCallLogsFilter _filter = GetCallLogsFilter();

  CreateCallLogRequest get newCallLog => CreateCallLogRequest.fromJson(_newCallLog);

  String get playingAudioId => _playingAudioId;
  bool get playing => _playing;
  bool get muted => _muted;
  int get currentPosition => _position;
  int get currentDuration => _duration;

  bool get loadingCallLogs => _loadingCallLogs;
  List<CallLog> get callLogs => _callLogs;
  CursorPaging get paging => _paging;
  GetCallLogsFilter get filter => _filter;

  setPaging({String? after, int? limit, String next = ""}) {
    _paging = CursorPaging(after: after ?? _paging.after, next: next, limit: limit ?? DEFAULT_LIMIT);
    notifyListeners();
  }

  setFilter({List<String>? extensionIds, List<String>? hotlineIds}) {
    _filter = GetCallLogsFilter(
      extensionIds: extensionIds ?? _filter.extensionIds, 
      hotlineIds: hotlineIds ?? _filter.hotlineIds);
    notifyListeners();
  }

  setLoading(bool loading) {
    _loadingCallLogs = loading;
    notifyListeners();
  }

  setCallLogs(List<CallLog> callLogList) {
    if (paging.after == ".") {
      _callLogs = callLogList;
    } else {
      callLogList.forEach((log) {
        _callLogs = ArrayHandler<CallLog>().upsert(_callLogs, log);
      });
    }

    if (paging.next.isNotEmpty) {
      setPaging(after: paging.next);
    }

    notifyListeners();
  }

  Future<void> playAudio(String callLogId, String audioUrl) async {
    await stopAudio();

    _playingAudioId = callLogId;
    _playing = true;
    _muted = false;
    _position = 0;
    _duration = 0;

    notifyListeners();

    Duration? duration;

    if (Platform.isIOS) {
      // generate random number.
      var rng = Random();

      // get temporary directory of device.
      Directory tempDir = await getTemporaryDirectory();

      // get temporary path from temporary directory.
      String tempPath = tempDir.path;

      File file = File("$tempPath${rng.nextInt(100).toString()}.wav");

      // call http.get method and pass imageUrl into it to get response.
      HTTP.Response response = await HTTP.get(Uri.parse(audioUrl));

      // write bodyBytes received in response to file.
      await file.writeAsBytes(response.bodyBytes);

      duration = await audioPlayer.setFilePath(file.path);
    } else if (Platform.isAndroid) {
      duration = await audioPlayer.setUrl(audioUrl);
    }

    _duration = duration?.inSeconds ?? 0;
    notifyListeners();

    audioPlayer.play();
    _audioPositionSubscription = audioPlayer.positionStream.listen((pos) {
      _position = pos.inSeconds;
      if (currentPosition == currentDuration) {
        _playing = false;
      }
      notifyListeners();
    });

  }

  Future<void> stopAudio() async {
    _playingAudioId = "";
    _playing = false;
    notifyListeners();

    await _audioPositionSubscription?.cancel();
    await audioPlayer.stop();
  }

  void pauseAudio() {
    _playing = false;
    notifyListeners();

    audioPlayer.pause();
  }

  void continueAudio() {
    _playing = true;
    notifyListeners();

    audioPlayer.play();
  }

  void muteAudio() {
    _muted = true;
    notifyListeners();

    audioPlayer.setVolume(0);
  }

  void unmuteAudio() {
    _muted = false;
    notifyListeners();

    audioPlayer.setVolume(1);
  }

  void setNewCallLog(CreateCallLogRequest request) {
    final requestJson = MapHandler.trimNull(request.toJson());
    if (requestJson != null) {
      _newCallLog = {
        ..._newCallLog,
        ...requestJson
      };
      notifyListeners();
    }
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
  }
}
