import 'package:flutter/material.dart';

class ESpinner extends StatefulWidget {
  final IconData? icon;
  final Duration? duration;
  final double? size;
  final Color? color;

  const ESpinner({
    Key? key,
    @required this.icon,
    this.duration = const Duration(milliseconds: 1800),
    this.size = 18,
    this.color = Colors.white,
  }) : super(key: key);

  @override
  _SpinnerState createState() => _SpinnerState();
}

class _SpinnerState extends State<ESpinner> with SingleTickerProviderStateMixin {
  AnimationController? _controller;
  Widget? _child;

  @override
  void initState() {
    _controller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 2000),
    )..repeat();
    _child = Icon(widget.icon, size: widget.size, color: widget.color);

    super.initState();
  }

  @override
  void dispose() {
    _controller!.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return RotationTransition(
      turns: _controller!,
      child: _child,
    );
  }
}