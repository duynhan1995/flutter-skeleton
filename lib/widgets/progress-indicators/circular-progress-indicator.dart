import 'package:etelecom/theme/theme.dart';
import 'package:flutter/material.dart';

class ECircularProgressIndicator extends StatelessWidget {
  // A value of 0.0 means no progress and 1.0 means that progress is complete.
  final double? value;
  final Color? color;
  final double size;
  final double padding;

  ECircularProgressIndicator({
    this.value,
    this.color,
    this.size = 20,
    this.padding = 20
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.all(padding),
          child: SizedBox(
            width: size,
            height: size,
            child: CircularProgressIndicator(
              value: this.value,
              strokeWidth: 2,
              valueColor: AlwaysStoppedAnimation<Color>(this.color ?? MainTheme.primaryColor)
            ),
          ),
        )
      ]
    );
  }

}
