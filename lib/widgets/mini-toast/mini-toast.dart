import 'package:etelecom/theme/theme.dart';
import 'package:etelecom/widgets/bases/base-material-button.dart';
import 'package:flutter/material.dart';

class EMiniToast extends StatelessWidget {
  final Color color;
  final String title;
  final VoidCallback? onPressed;

  EMiniToast({
    required this.color,
    required this.title,
    this.onPressed
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 5),
      color: color,
      width: MediaQuery.of(context).size.width,
      child: EMaterialButton(
          onPressed: onPressed,
          child: Text(
            title,
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.white),
          )
      ),
    );
  }

}
