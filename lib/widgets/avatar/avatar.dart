import 'package:etelecom/theme/theme.dart';
import 'package:flutter/material.dart';

class EAvatar extends StatelessWidget {
  final String imageUrl;
  final double width;
  final double height;
  final EdgeInsetsGeometry? margin;
  final EdgeInsetsGeometry? padding;

  EAvatar({
    required this.imageUrl,
    required this.width,
    required this.height,
    this.margin,
    this.padding
  });

  @override
  Widget build(BuildContext context) {

    ImageProvider imageProvider = AssetImage("assets/images/placeholder.png");
    if (imageUrl.isNotEmpty) {
      imageProvider = NetworkImage(imageUrl);
    }

    return Container(
      margin: margin,
      padding: padding,
      width: width,
      height: height,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        border: Border.all(
          color: MainTheme.grayF5
        ),
        image: DecorationImage(
          fit: BoxFit.cover,
          image: imageProvider
        )
      )
    );
  }

}
