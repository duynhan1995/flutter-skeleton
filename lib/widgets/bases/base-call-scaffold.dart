import 'package:camera/camera.dart';
import 'package:etelecom/providers/call-manager/calls.provider.dart';
import 'package:etelecom/theme/theme.dart';
import 'package:etelecom/widgets/buttons/call-button.dart';
import 'package:etelecom/widgets/calling/calling-name-display.dart';
import 'package:etelecom/widgets/calling/dtmf-pad.dart';
import 'package:etelecom/widgets/calling/refer-pad.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../app_config.dart';

class ECallScaffold extends StatefulWidget {

  final List<CallButton>? bigCallButtons;
  final List<CallButton>? smallCallButtons;
  final String? statusText;
  final double? statusTextSize;
  final Color? statusTextColor;

  const ECallScaffold({
    Key? key,
    this.bigCallButtons,
    this.smallCallButtons,
    required this.statusText,
    this.statusTextSize = 13,
    this.statusTextColor
  }) : assert(bigCallButtons != null && bigCallButtons.length > 0),
       assert(statusText != null && statusText.length > 0),
       super(key: key);

  @override
  State<StatefulWidget> createState() => ECallScaffoldState();

}

class ECallScaffoldState extends State<ECallScaffold> {

  double scale = 0;

  @override
  void initState() {
    super.initState();

    final callsReader = context.read<CallsProvider>();
    if (AppConfig.videoCall
        && callsReader.isVideoCall
        && callsReader.currentState == CallingState.outgoing
    ) {
      initCameraController();
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  void initCameraController() {
    context.read<CallsProvider>().initCameraController().then((_) {
      final size = MediaQuery.of(context).size;
      setState(() {
        scale = size.aspectRatio * context.read<CallsProvider>().cameraController!.value.aspectRatio;
        if (scale < 1) scale = 1 / scale;
      });
    });
  }

  @override
  Widget build(BuildContext context) {

    List<List<CallButton>> bigButtonsLayoutDivided = [];
    for (int i = 0; i < widget.bigCallButtons!.length; i += 3) {
      final int startIndex = i;
      final int gap = widget.bigCallButtons!.length < 3
          ? widget.bigCallButtons!.length
          : (startIndex % 3 == 0 ? 3 : (widget.bigCallButtons!.length - 3) % 3);
      final int endIndex = startIndex + gap;

      bigButtonsLayoutDivided.add(
          widget.bigCallButtons!.getRange(startIndex, endIndex).toList()
      );
    }

    final _callWatcher = context.watch<CallsProvider>();

    return Scaffold(
        backgroundColor: MainTheme.gray333,
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          brightness: Brightness.dark,
          toolbarHeight: 0,
          backgroundColor: Colors.transparent,
          shadowColor: Colors.transparent,
        ),
        body: Stack(
          fit: StackFit.expand,
          children: [
            if (_callWatcher.cameraOn
                && _callWatcher.isVideoCall
                && _callWatcher.currentState == CallingState.outgoing
                && _callWatcher.cameraController != null)
              Transform.scale(
                scale: scale,
                child: Center(
                  child: CameraPreview(_callWatcher.cameraController!),
                ),
              ),
            SafeArea(
                minimum: EdgeInsets.symmetric(vertical: 0, horizontal: 0),
                child: Container(
                  padding: EdgeInsets.symmetric(vertical: 10, horizontal: 0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                              padding: EdgeInsets.symmetric(vertical: 7, horizontal: 15),
                              decoration: BoxDecoration(
                                  color: MainTheme.gray999.withOpacity(.3),
                                  borderRadius: BorderRadius.circular(20)
                              ),
                              child: Row(
                                children: [
                                  Icon(
                                    Icons.circle,
                                    color: widget.statusTextColor,
                                    size: 12.5,
                                  ),
                                  Container(margin: EdgeInsets.only(right: 5)),
                                  Text(
                                      widget.statusText ?? "",
                                      style: TextStyle(
                                          color: MainTheme.grayF5,
                                          fontWeight: FontWeight.w700,
                                          fontSize: widget.statusTextSize
                                      )
                                  )
                                ],
                              )
                          ),
                        ],
                      ),
                      if (_callWatcher.showDtmfPad)
                        Expanded(
                            child: EDtmfPad(),
                        ),
                      if (_callWatcher.showReferPad)
                        Expanded(
                            child: EReferPad()
                        ),
                      if (!_callWatcher.showDtmfPad && !_callWatcher.showReferPad)
                        Expanded(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Expanded(
                                  flex: 5,
                                  child: Container(
                                      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          Container(
                                              padding: EdgeInsets.symmetric(vertical: 20, horizontal: 0),
                                              child: Column(
                                                children: [
                                                  Icon(Icons.account_circle_outlined,
                                                      color: MainTheme.grayF5,
                                                      size: 120
                                                  ),
                                                  CallingNameDisplay(
                                                      callingNumber: context
                                                          .watch<CallsProvider>()
                                                          .callingNumber,
                                                      callingName: context
                                                          .watch<CallsProvider>()
                                                          .callingName
                                                  )
                                                ],
                                              )
                                          )
                                        ],
                                      ),
                                  ),
                              ),
                              Expanded(
                                  flex: 5,
                                  child: Container(
                                      color: Colors.transparent,
                                      padding: EdgeInsets.symmetric(vertical: 0, horizontal: 20),
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          if (widget.smallCallButtons != null && widget.smallCallButtons!.isNotEmpty)
                                            Row(
                                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                                              children: widget.smallCallButtons ?? [],
                                            ),
                                          Container(height: 20),
                                          Column(
                                              mainAxisAlignment: MainAxisAlignment.end,
                                              children: [
                                                for (int i = 0; i < bigButtonsLayoutDivided.length; i++)
                                                  Container(
                                                    margin: EdgeInsets.only(top: i > 0 ? 20 : 0),
                                                    child: Row(
                                                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                                                      crossAxisAlignment: CrossAxisAlignment.end,
                                                      children: bigButtonsLayoutDivided[i],
                                                    ),
                                                  )
                                              ]
                                          ),
                                        ],
                                      )
                                  )
                              )
                            ],
                          )
                        ),
                    ],
                  ),
                ),
            )
          ],
        )
    );
  }
}
