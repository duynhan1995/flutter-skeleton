import 'package:etelecom/theme/theme.dart';
import 'package:flutter/material.dart';

class EAppBarActionButton {
  final String title;
  final IconData? icon;
  final Function(BuildContext context) onPressed;

  EAppBarActionButton({
    required this.title,
    this.icon,
    required this.onPressed
  });
}

class EAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final Size preferredSize;
  final EAppBarActionButton? leftActionButton;
  final List<EAppBarActionButton>? rightActionButtons;
  final PreferredSizeWidget? bottom;
  final double? toolbarHeight;

  EAppBar({
    this.title = '',
    this.rightActionButtons,
    this.leftActionButton,
    this.bottom,
    this.toolbarHeight
  }) : preferredSize = Size.fromHeight(toolbarHeight ?? kToolbarHeight + (bottom != null ? bottom.preferredSize.height : 0));

  @override
  Widget build(BuildContext context) {
    List<Widget> _rightActionButtons = [];
    Widget? _leftActionButton;

    if(leftActionButton != null) {
      _leftActionButton = Container(
        padding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
        child: leftActionButton!.title.isNotEmpty ? MaterialButton(
            onPressed: () => leftActionButton!.onPressed(context),
            child: Row(
              children: [
                Icon(leftActionButton!.icon),
                Text(
                    leftActionButton!.title,
                    style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.w400
                    )
                ),
              ],
            )
        ) : IconButton(
            onPressed: () => leftActionButton!.onPressed(context),
            icon: Icon(leftActionButton!.icon)),
      );
    }

    if (rightActionButtons != null && rightActionButtons!.isNotEmpty) {
      _rightActionButtons = [
        Container(
          padding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
          child: Row(
            children: rightActionButtons!.map((btn) => btn.title.isNotEmpty ? MaterialButton(
              padding: EdgeInsets.symmetric(vertical: 0, horizontal: 5),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              onPressed: () => btn.onPressed(context),
              child: btn.title.isNotEmpty ? Text(
                  btn.title,
                  style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.w400
                  )
              ) 
                  : Icon(btn.icon),
              textColor: Colors.white,
              color: MainTheme.primaryColor,
            )
                : IconButton(
                onPressed: () => btn.onPressed(context),
                icon: Icon(btn.icon))
            ).toList(),
          )
        )
      ];
    }

    return this.title.isNotEmpty ? AppBar(
        centerTitle: true,
        shadowColor: Colors.transparent,
        backgroundColor: Colors.white,
        toolbarHeight: this.toolbarHeight ?? 60,
        iconTheme: IconThemeData(color: MainTheme.gray555),
        bottom: PreferredSize(
          child: Container(
            color: MainTheme.grayEEE,
            height: 1,
          ),
          preferredSize: Size.fromHeight(1),
        ),
        title: Text(
          this.title,
          style: TextStyle(
            fontSize: 17,
            fontWeight: FontWeight.w700,
            color: MainTheme.gray555
          )
        ),
        leading: _leftActionButton,
        actions: _rightActionButtons
    ) : AppBar(
        shadowColor: Colors.transparent,
        backgroundColor: Colors.white,
        toolbarHeight: this.toolbarHeight ?? 60,
    );
  }

}
