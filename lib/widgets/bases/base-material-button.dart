import 'package:flutter/material.dart';

class EMaterialButton extends StatelessWidget {
  final VoidCallback? onPressed;
  final Widget child;
  final ShapeBorder? shape;
  final Color? textColor;
  final Color? fillColor;
  final double? height;
  final EdgeInsetsGeometry? padding;

  EMaterialButton({
    this.onPressed,
    this.shape,
    this.padding = EdgeInsets.zero,
    this.height = 0,
    this.textColor,
    this.fillColor,
    required this.child
  });

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
        onPressed: onPressed,
        height: height,
        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
        padding: padding,
        shape: shape,
        textColor: textColor,
        color: fillColor,
        child: child
    );
  }

}
