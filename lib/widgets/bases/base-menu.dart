import 'package:etelecom/providers/navigate/navigate.provider.dart';
import 'package:etelecom/theme/theme.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MenuItem {
  MenuItem({
    required this.icon,
    required this.label,
    required this.route
  });

  IconData? icon;
  String? label;
  String? route;
}

class EMenu extends StatefulWidget {
  EMenu({Key? key, required this.menuItems}) : super(key: key);

  final List<MenuItem> menuItems;

  @override
  State<StatefulWidget> createState() => _EMenuState(menuItems);
}

class _EMenuState extends State<EMenu> {
  List<MenuItem> _menuItems = [];

  _EMenuState(List<MenuItem> items) {
    _menuItems = items;
  }

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      backgroundColor: Colors.white,
      items: _menuItems.map((item) => BottomNavigationBarItem(
        icon: Icon(item.icon, size: 27.5),
        label: item.label
      )).toList(),
      currentIndex: context.watch<NavigateProvider>().bottomMenuIndex,
      type: BottomNavigationBarType.fixed,
      selectedItemColor: MainTheme.primaryColor,
      selectedFontSize: 11,
      selectedLabelStyle: TextStyle(fontWeight: FontWeight.w900, height: 1.75),
      unselectedItemColor: MainTheme.gray555,
      unselectedFontSize: 11,
      unselectedLabelStyle: TextStyle(fontWeight: FontWeight.w900, height: 1.75),
      showUnselectedLabels: true,
      onTap: (index) {
        final route = _menuItems.elementAt(index).route;
        context.read<NavigateProvider>().navigate(route!, index);
      },
    );
  }
}
