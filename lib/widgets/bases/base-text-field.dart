import 'package:etelecom/theme/theme.dart';
import 'package:flutter/material.dart';

class ETextField extends StatefulWidget {

  final TextEditingController textController;
  final String labelText;
  final String hintText;
  final TextInputType keyboardType;
  final bool obscureText;
  final int? maxLines;
  final TextInputAction? textInputAction;
  final VoidCallback? onBlur;
  final VoidCallback? onSubmitted;
  final VoidCallback? onChanged;
  final EdgeInsetsGeometry contentPadding;
  final bool autoFocus;

  ETextField({
    required this.textController,
    this.hintText = "",
    this.labelText = "",
    this.keyboardType = TextInputType.text,
    this.obscureText = false,
    this.maxLines = 1,
    this.textInputAction,
    this.onBlur,
    this.onSubmitted,
    this.onChanged,
    this.contentPadding = const EdgeInsets.only(bottom: 8.0, top: 0),
    this.autoFocus = false
  });

  @override
  State<StatefulWidget> createState() => ETextFieldState();

}

class ETextFieldState extends State<ETextField> {
  FocusNode baseFocusNode = FocusNode();

  @override
  void initState() {
    super.initState();
    baseFocusNode.addListener(() {
      if (!baseFocusNode.hasFocus) {
        if (widget.onBlur != null) {
          widget.onBlur!();
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {

    return TextField(
      maxLines: widget.maxLines,
      obscureText: widget.obscureText,
      textInputAction: widget.textInputAction,
      keyboardType: widget.keyboardType,
      focusNode: baseFocusNode,
      autofocus: widget.autoFocus,
      onSubmitted: (_) => widget.onSubmitted != null ? widget.onSubmitted!() : null,
      onChanged: (_) => widget.onChanged != null ? widget.onChanged!() : null,
      decoration: InputDecoration(
        hintText: widget.hintText,
        hintStyle: TextStyle(
            fontWeight: FontWeight.w400,
            fontSize: 15
        ),
        labelText: widget.labelText,
        labelStyle: TextStyle(
            fontWeight: FontWeight.w400,
            color: MainTheme.gray555
        ),
        contentPadding: widget.contentPadding,
        focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(
                color: MainTheme.primaryColor, width: 2.0
            )
        ),
      ),
      style: TextStyle(
          color: MainTheme.gray555,
          fontWeight: FontWeight.w700
      ),
      controller: widget.textController,
    );
  }
}
