import 'package:etelecom/widgets/bases/base-appbar.dart';
import 'package:etelecom/widgets/bases/base-menu.dart';
import 'package:etelecom/widgets/static-toast/static-toast.dart';
import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

class EScaffold extends StatefulWidget {
  final EAppBar? appBar;
  final Widget body;
  final EMenu? bottomMenu;

  EScaffold({
    this.appBar,
    required this.body,
    this.bottomMenu
  });

  @override
  State<StatefulWidget> createState() => _EScaffoldState();
}

class _EScaffoldState extends State<EScaffold> {
  @override
  Widget build(BuildContext context) {

    return CupertinoScaffold(
      body: widget.bottomMenu != null
          ? Scaffold(
            appBar: widget.appBar,
            body: Column(
              children: [
                StaticToast(),
                Expanded(
                    child: widget.body
                )
              ],
            ),
            bottomNavigationBar: widget.bottomMenu,
          )
          : Scaffold(
            appBar: widget.appBar,
            body: Column(
              children: [
                StaticToast(),
                Expanded(
                    child: widget.body
                )
              ],
            )
          )
    );

  }

}
