import 'package:etelecom/app_config.dart';
import 'package:etelecom/providers/network/network.provider.dart';
import 'package:etelecom/providers/permission/permission.provider.dart';
import 'package:etelecom/theme/theme.dart';
import 'package:etelecom/utils/native-permissions.dart';
import 'package:etelecom/widgets/bases/base-appbar.dart';
import 'package:etelecom/widgets/bases/base-menu.dart';
import 'package:etelecom/widgets/bases/base-scaffold.dart';
import 'package:etelecom/widgets/calling/numpad.dart';
import 'package:etelecom/widgets/mini-toast/mini-toast.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';

class CallingStateNone extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _CallingStateNoneState();
}

class _CallingStateNoneState extends State<CallingStateNone> {

  @override
  void initState() {
    super.initState();
    _getMicAndCamPermissions();
  }

  Future<void> _getMicAndCamPermissions() async {
    final _permissionGranted = await NativePermissions.checkPermissions([
      Permission.microphone,
      Permission.camera
    ]);
    context.read<PermissionProvider>().updateMicrophonePermission(_permissionGranted);
    context.read<PermissionProvider>().updateCameraPermission(_permissionGranted);
  }

  @override
  Widget build(BuildContext context) {
    final callPermitted = context.watch<PermissionProvider>().callPermitted;
    final checkingNetwork = context.watch<NetworkProvider>().checkingNetwork;

    return EScaffold(
        appBar: EAppBar(
          toolbarHeight: 0,
        ),
        body: Column(
            children: [
                if (!callPermitted && !checkingNetwork)
                  EMiniToast(
                    color: MainTheme.warning,
                    title: "Vui lòng cấp quyền microphone và máy ảnh cho ứng dụng",
                    onPressed: () => NativePermissions.goToAppSettings(
                        context: context,
                        title: "Quyền truy cập microphone và máy ảnh",
                        description: "eTelecom - Tổng đài CSKH muốn truy cập microphone và máy ảnh để thực hiện cuộc gọi."
                    ),
                  ),
                Expanded(
                    child: Material(
                        color: Colors.white,
                        child: ENumPad(),
                    )
                )
            ],
        ),
        bottomMenu: EMenu(
            menuItems: [
              MenuItem(icon: Icons.phone_in_talk_outlined, label: 'Nghe gọi', route: '/call-center'),
              if (context.watch<AppConfig>().features.ticket)
                MenuItem(icon: Icons.confirmation_number_outlined, label: 'Ticket', route: '/tickets'),
              MenuItem(icon: Icons.list, label: 'Lịch sử', route: '/call-logs'),
              MenuItem(icon: Icons.account_circle_outlined, label: 'Danh bạ', route: '/contacts'),
              MenuItem(icon: Icons.settings_outlined, label: 'Thiết lập', route: '/setting'),
            ]
        )
    );
  }
}
