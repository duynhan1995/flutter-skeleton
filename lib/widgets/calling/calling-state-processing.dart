import 'dart:async';

import 'package:etelecom/providers/call-manager/calls.provider.dart';
import 'package:etelecom/services/portsip.service.dart';
import 'package:etelecom/theme/theme.dart';
import 'package:etelecom/utils/toast.dart';
import 'package:etelecom/widgets/bases/base-call-scaffold.dart';
import 'package:etelecom/widgets/buttons/call-button.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CallingStateProcessingPage extends Page {
  CallingStateProcessingPage()
      : super(key: ValueKey('CallingStateProcessingPage'));

  @override
  Route createRoute(BuildContext context) {
    return PageRouteBuilder(
        settings: this,
        pageBuilder: (BuildContext context, animation, animation2) {
          return CallingStateProcessingScreen();
        });
  }
}

class CallingStateProcessingScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _CallingStateProcessingState();
}

class _CallingStateProcessingState extends State<CallingStateProcessingScreen> {
  bool isHeld = false;
  Timer? timer;

  toggleHoldCall() {
    setState(() {
      isHeld = !isHeld;
    });

    if (isHeld) {
      PortsipService.portsipHold();
    } else {
      PortsipService.portsipUnHold();
    }
  }

  toggleSpeaker() {
    context.read<CallsProvider>().toggleSpeaker();

    bool speakerOn = context.read<CallsProvider>().speakerOn;

    if (speakerOn) {
      PortsipService.portsipSpeakerOn();
    } else {
      PortsipService.portsipSpeakerOff();
    }
  }

  toggleMute() {
    context.read<CallsProvider>().toggleMicrophone();

    bool mute = !context.read<CallsProvider>().microphoneOn;

    if (mute) {
      PortsipService.microphoneOff();
    } else {
      PortsipService.microphoneOn();
    }
  }

  checkBalanceToHangup() {
    if (timer != null) {
      EToast.error(context, 'Cuộc gọi bị ngắt do không đủ số dư');
      Future.delayed(Duration(seconds: 2), () async {
        PortsipService.portsipHangUp();
      });
      timer?.cancel();
    }
  }

  @override
  void initState() {
    final _minCallTime = context.read<CallsProvider>().minCallTime;
    if (_minCallTime > 0) {
      timer = Timer.periodic(Duration(seconds: _minCallTime - 2), (_) {
        checkBalanceToHangup();
      });
    }
    super.initState();
  }

  @override
  void dispose() {
    if (timer != null) timer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ECallScaffold(
      statusText: context.watch<CallsProvider>().callingTime,
      statusTextColor: isHeld ? Colors.orange : MainTheme.primaryColor,
      statusTextSize: 17,
      bigCallButtons: [
        CallButton(
            padding: 6,
            icon: Icons.dialpad_outlined,
            label: "Bàn phím",
            theme: Brightness.dark,
            onPressed: () => context.read<CallsProvider>().toggleDtmfPad()
        ),
        CallButton(
            padding: 6,
            icon: Icons.phone_forwarded_outlined,
            label: "Chuyển tiếp",
            theme: Brightness.dark,
            onPressed: () => context.read<CallsProvider>().toggleReferPad()
        ),
        CallButton(
            imageSize: 50,
            imageUrl: isHeld ? "pause_active.png" : "pause.png",
            label: isHeld ? 'Tiếp tục' : 'Giữ máy',
            theme: Brightness.dark,
            onPressed: () => toggleHoldCall()
        ),
        CallButton(
            imageSize: 50,
            imageUrl: context.watch<CallsProvider>().speakerOn
                ? "speaker_active.png"
                : "speaker.png",
            label: 'Loa ngoài',
            theme: Brightness.dark,
            onPressed: () => toggleSpeaker()
        ),
        CallButton(
            imageSize: 50,
            imageUrl: context.watch<CallsProvider>().microphoneOn
                ? "mic.png"
                : "mic_active.png",
            label:
                '${context.watch<CallsProvider>().microphoneOn ? 'Tắt' : 'Mở'} mic',
            theme: Brightness.dark,
            onPressed: () => toggleMute()
        ),
        CallButton(
            imageSize: 50,
            imageUrl: "stop.png",
            theme: Brightness.dark,
            label: 'Kết thúc',
            onPressed: () => PortsipService.portsipHangUp()
        )
      ],
    );
  }
}
