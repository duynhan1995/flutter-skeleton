import 'package:etelecom/theme/theme.dart';
import 'package:flutter/material.dart';

class CallingNameDisplay extends StatelessWidget {
  final String? callingName;
  final String? callingNumber;

  CallingNameDisplay({
    this.callingName,
    required this.callingNumber
  });

  @override
  Widget build(BuildContext context) {
    bool _displayName = callingName!.isNotEmpty;

    return Column(
        children: [
            if (_displayName)
              Container(
                  child: Text(
                    callingName ?? "",
                    style: TextStyle(
                        fontSize: 27,
                        fontWeight: FontWeight.w700,
                        color: MainTheme.grayF5
                    ),
                  )
              ),
            Container(
                padding: EdgeInsets.only(top: 10),
                child: Text(
                  callingNumber ?? "",
                  style: TextStyle(
                      fontSize: _displayName ? 17 : 27,
                      fontWeight: _displayName ? FontWeight.w400 : FontWeight.w700,
                      color: MainTheme.grayF5
                  ),
                )
            )
        ]
    );
  }

}
