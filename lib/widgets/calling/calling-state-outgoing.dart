import 'package:etelecom/providers/call-manager/calls.provider.dart';
import 'package:etelecom/services/portsip.service.dart';
import 'package:etelecom/theme/theme.dart';
import 'package:etelecom/widgets/bases/base-call-scaffold.dart';
import 'package:etelecom/widgets/buttons/call-button.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CallingStateOutgoingPage extends Page {
  CallingStateOutgoingPage() : super(key: ValueKey('CallingStateOutgoingPage'));

  @override
  Route createRoute(BuildContext context) {
    return PageRouteBuilder(
      settings: this,
      pageBuilder: (BuildContext context, animation, animation2) {
        return CallingStateOutgoingScreen();
      }
    );
  }
}

class CallingStateOutgoingScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _CallingStateOutgoingState();
}

class _CallingStateOutgoingState extends State<CallingStateOutgoingScreen> {

  toggleSpeaker() {
    context.read<CallsProvider>().toggleSpeaker();

    bool speakerOn = context.read<CallsProvider>().speakerOn;

    if (speakerOn) {
      PortsipService.portsipSpeakerOn();
    } else {
      PortsipService.portsipSpeakerOff();
    }
  }

  toggleMute() {
    context.read<CallsProvider>().toggleMicrophone();

    bool mute = !context.read<CallsProvider>().microphoneOn;

    if (mute) {
      PortsipService.microphoneOff();
    } else {
      PortsipService.microphoneOn();
    }
  }

  switchCamera() {
    context.read<CallsProvider>().switchCamera(reInit: true);

    bool frontCamera = context.read<CallsProvider>().frontCamera;

    if (frontCamera) {
      PortsipService.switchToFrontCamera();
    } else {
      PortsipService.switchToBackCamera();
    }
  }

  toggleCamera() {
    context.read<CallsProvider>().toggleCamera();

    bool cameraOn = context.read<CallsProvider>().cameraOn;

    if (cameraOn) {
      PortsipService.cameraOn();
    } else {
      PortsipService.cameraOff();
    }
  }

  @override
  Widget build(BuildContext context) {
    final List<CallButton> callButtonChildren = context.watch<CallsProvider>().isVideoCall
        ? [
            CallButton(
                touchAreaSize: 60.0,
                imageUrl: "flip.png",
                label: 'Đổi camera',
                theme: Brightness.dark,
                onPressed: () => switchCamera()
            ),
            CallButton(
                touchAreaSize: 60.0,
                imageUrl:
                    context.watch<CallsProvider>().cameraOn ? "cam.png" : "cam_active.png",
                label: '${context.watch<CallsProvider>().cameraOn ? 'Tắt' : 'Mở'} camera',
                theme: Brightness.dark,
                onPressed: () => toggleCamera()
            )
        ] : [
            CallButton(
                touchAreaSize: 60.0,
                imageUrl: context.watch<CallsProvider>().speakerOn
                    ? "speaker_active.png"
                    : "speaker.png",
                label: 'Loa ngoài',
                theme: Brightness.dark,
                onPressed: () => toggleSpeaker()
            )
        ];

    return ECallScaffold(
      statusText: 'ĐANG GỌI${context.watch<CallsProvider>().isVideoCall ? ' VIDEO ' : ' '}ĐẾN',
      statusTextColor: Colors.orange,
      smallCallButtons: [
        CallButton(
            touchAreaSize: 60.0,
            imageUrl: "pause.png",
            label: 'Giữ máy',
            theme: Brightness.dark,
            onPressed: null
        ),
        ...callButtonChildren,
        CallButton(
            touchAreaSize: 60.0,
            imageUrl: context.watch<CallsProvider>().microphoneOn
                ? "mic.png" : "mic_active.png",
            label: '${context.watch<CallsProvider>().microphoneOn ? 'Tắt' : 'Mở'} mic',
            theme: Brightness.dark,
            onPressed: () => toggleMute()
        ),
      ],
      bigCallButtons: [
        CallButton(
            touchAreaSize: 90,
            iconSize: 40,
            icon: Icons.call_end_outlined,
            foregroundColor: MainTheme.grayF5,
            backgroundColor: Colors.red,
            theme: Brightness.dark,
            label: 'Kết thúc',
            onPressed: () => PortsipService.portsipHangUp(),
        ),
      ],
    );
  }

}
