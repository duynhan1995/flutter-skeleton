import 'package:etelecom/providers/call-manager/calls.provider.dart';
import 'package:etelecom/services/portsip.service.dart';
import 'package:etelecom/theme/theme.dart';
import 'package:etelecom/widgets/bases/base-call-scaffold.dart';
import 'package:etelecom/widgets/buttons/call-button.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CallingStateIncomingPage extends Page {
  CallingStateIncomingPage() : super(key: ValueKey('CallingStateIncomingPage'));

  @override
  Route createRoute(BuildContext context) {
    return PageRouteBuilder(
        settings: this,
        pageBuilder: (BuildContext context, animation, animation2) {
          return CallingStateIncomingScreen();
        }
    );
  }
}

class CallingStateIncomingScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _CallingStateIncomingState();
}

class _CallingStateIncomingState extends State<CallingStateIncomingScreen> {

  toggleSpeaker() {
    context.read<CallsProvider>().toggleSpeaker();

    bool speakerOn = context.read<CallsProvider>().speakerOn;

    if (speakerOn) {
      PortsipService.portsipSpeakerOn();
    } else {
      PortsipService.portsipSpeakerOff();
    }
  }

  toggleMute() {
    context.read<CallsProvider>().toggleMicrophone();

    bool mute = !context.read<CallsProvider>().microphoneOn;

    if (mute) {
      PortsipService.microphoneOff();
    } else {
      PortsipService.microphoneOn();
    }
  }

  @override
  Widget build(BuildContext context) {
    return ECallScaffold(
        statusText: 'CUỘC GỌI${context.watch<CallsProvider>().isVideoCall ? ' VIDEO ' : ' '}ĐẾN TỪ',
        statusTextColor: Colors.orange,
        smallCallButtons: [
          CallButton(
              imageUrl: "pause.png",
              label: 'Giữ máy',
              theme: Brightness.dark,
              onPressed: null,
              touchAreaSize: 60.0,
          ),
          CallButton(
              imageUrl: context.watch<CallsProvider>().speakerOn
                  ? "speaker_active.png" : "speaker.png",
              label: 'Loa ngoài',
              theme: Brightness.dark,
              onPressed: () => toggleSpeaker(),
              touchAreaSize: 60.0,
          ),
          CallButton(
              imageUrl: context.watch<CallsProvider>().microphoneOn
                  ? "mic.png" : "mic_active.png",
              label: '${context.watch<CallsProvider>().microphoneOn ? 'Tắt' : 'Mở'} mic',
              theme: Brightness.dark,
              onPressed: () => toggleMute(),
              touchAreaSize: 60.0,
          ),
        ],
        bigCallButtons: [
            CallButton(
                touchAreaSize: 90.0,
                iconSize: 40.0,
                icon: Icons.call_end_outlined,
                foregroundColor: MainTheme.grayF5,
                backgroundColor: Colors.red,
                theme: Brightness.dark,
                label: 'Từ chối',
                onPressed: () => PortsipService.portsipReject(),
            ),
            CallButton(
                touchAreaSize: 90.0,
                iconSize: 40.0,
                icon: Icons.call_outlined,
                foregroundColor: MainTheme.grayF5,
                backgroundColor: MainTheme.primaryColor,
                theme: Brightness.dark,
                label: 'Nghe máy',
                onPressed: () => PortsipService.portsipAnswer(),
            ),
        ],
    );
  }

}
