import 'package:etelecom/providers/call-manager/calls.provider.dart';
import 'package:etelecom/services/portsip.service.dart';
import 'package:etelecom/theme/theme.dart';
import 'package:etelecom/widgets/buttons/call-button.dart';
import 'package:etelecom/widgets/forms/no-keyboard-text-field.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class EDtmfPad extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => EDtmfPadState();
}

class EDtmfPadState extends State<EDtmfPad> {
  final dtmfController = TextEditingController();
  double touchAreaSize = 92.5;
  double numpadWidth = 0.85;

  back() {
    context.read<CallsProvider>().toggleDtmfPad();
  }

  onNumpadPress(String numpad) {

    PortsipService.sendDtmf(numpad);

    final int _startPosition = dtmfController.selection.baseOffset;
    final int _endPosition = dtmfController.selection.extentOffset;

    final List<String> chars = dtmfController.text.split("");

    if (_startPosition == _endPosition) {
      if (_startPosition == -1) {
        chars.add(numpad);
      } else {
        chars.insert(_startPosition, numpad);
      }
    } else {
      chars.replaceRange(_startPosition, _endPosition, [numpad]);
    }

    dtmfController.value = TextEditingValue(
        text: chars.join(""),
        selection: TextSelection.collapsed(
            offset: _startPosition == -1 ? chars.length : (_startPosition + 1)
        )
    );

  }

  @override
  void dispose() {
    dtmfController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    touchAreaSize = MediaQuery.of(context).size.width * numpadWidth/3 - 20;
    if (MediaQuery.of(context).size.aspectRatio < 0.5) {
        touchAreaSize = MediaQuery.of(context).size.width * numpadWidth/3 - 15;
      }
    if (MediaQuery.of(context).size.width <= 320) {
      touchAreaSize = MediaQuery.of(context).size.width * numpadWidth / 3 - 20;
    }
    if (MediaQuery.of(context).size.width > 400) {
      if (MediaQuery.of(context).size.aspectRatio > 0.5) {
        touchAreaSize = 92.5;
      }
    }
    return Column(
        children: [
          // Phone input
          Expanded(
            child: Row(
              children: [
                Expanded(
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                    child: NoKeyboardTextField(
                      controller: dtmfController,
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 35,
                          fontWeight: FontWeight.w700
                      ),
                      decoration: InputDecoration(
                          border: InputBorder.none
                      ),
                      showCursor: true,
                      textAlign: TextAlign.center,
                      toolbarOptions: ToolbarOptions(
                          copy: true,
                          cut: true,
                          paste: true,
                          selectAll: true
                      ),
                      readOnly: true,
                      cursorHeight: 35,
                    ),
                  ),
                )
              ],
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width * numpadWidth,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      CallButton(
                          touchAreaSize: touchAreaSize,
                          theme: Brightness.dark,
                          onPressed: () => onNumpadPress("1"),
                          text: "1"
                      ),
                      CallButton(
                          touchAreaSize: touchAreaSize,
                          theme: Brightness.dark,
                          onPressed: () => onNumpadPress("2"),
                          text: "2"
                      ),
                      CallButton(
                          touchAreaSize: touchAreaSize,
                          theme: Brightness.dark,
                          onPressed: () => onNumpadPress("3"),
                          text: "3"
                      )
                    ],
                  ),
                ),
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      CallButton(
                          touchAreaSize: touchAreaSize,
                          theme: Brightness.dark,
                          onPressed: () => onNumpadPress("4"),
                          text: "4"
                      ),
                      CallButton(
                          touchAreaSize: touchAreaSize,
                          theme: Brightness.dark,
                          onPressed: () => onNumpadPress("5"),
                          text: "5"
                      ),
                      CallButton(
                          touchAreaSize: touchAreaSize,
                          theme: Brightness.dark,
                          onPressed: () => onNumpadPress("6"),
                          text: "6"
                      )
                    ],
                  ),
                ),
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      CallButton(
                          touchAreaSize: touchAreaSize,
                          theme: Brightness.dark,
                          onPressed: () => onNumpadPress("7"),
                          text: "7"
                      ),
                      CallButton(
                          touchAreaSize: touchAreaSize,
                          theme: Brightness.dark,
                          onPressed: () => onNumpadPress("8"),
                          text: "8"
                      ),
                      CallButton(
                          touchAreaSize: touchAreaSize,
                          theme: Brightness.dark,
                          onPressed: () => onNumpadPress("9"),
                          text: "9"
                      )
                    ],
                  ),
                ),
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      CallButton(
                          touchAreaSize: touchAreaSize,
                          imageSize: 60,
                          imageAsAnIcon: true,
                          imageUrl: "asterisk.png",
                          theme: Brightness.dark,
                          onPressed: () => onNumpadPress("*"),
                      ),
                      CallButton(
                          touchAreaSize: touchAreaSize,
                          theme: Brightness.dark,
                          onPressed: () => onNumpadPress("0"),
                          text: "0"
                      ),
                      CallButton(
                          touchAreaSize: touchAreaSize,
                          imageSize: 60,
                          imageAsAnIcon: true,
                          imageUrl: "tag.png",
                          theme: Brightness.dark,
                          onPressed: () => onNumpadPress("#"),
                      )
                    ],
                  ),
                ),
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CallButton(
                          touchAreaSize: touchAreaSize,
                          theme: Brightness.dark,
                          onPressed: () => back(),
                          icon: Icons.keyboard_hide_outlined,
                          label: 'Ẩn',
                          backgroundColor: Colors.black.withOpacity(.1),
                          borderColor: MainTheme.gray333,
                      )
                    ],
                  ),
                )
              ],
            ),
          )
        ]
    );
  }

}
