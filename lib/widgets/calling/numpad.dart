import 'package:etelecom/providers/call-manager/calls.provider.dart';
import 'package:etelecom/services/portsip.service.dart';
import 'package:etelecom/theme/theme.dart';
import 'package:etelecom/utils/string-handler.dart';
import 'package:etelecom/utils/toast.dart';
import 'package:etelecom/widgets/buttons/call-button.dart';
import 'package:etelecom/widgets/forms/no-keyboard-text-field.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../app_config.dart';

class ENumPad extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => ENumPadState();
}

class ENumPadState extends State<ENumPad> {
  final phoneNumberController = TextEditingController();
  double touchAreaSize = 92.5;
  double numpadWidth = 0.85;

  Future<void> callOut({bool videoCall = false}) async {
    try {
      final String _phone = StringHandler.formatPhoneNumberBeforeCall(phoneNumberController.text);
      await PortsipService.portsipCallOut(_phone, isVideoCall: videoCall, context: context);
    } catch (e) {
      if ((e as dynamic)["code"] == "not_enough_balance") {
        EToast.error(context, "Không đủ số dư để thực hiện cuộc gọi");
      }
    }
  }

  onNumpadPress(String numpad) {
    final int _startPosition = phoneNumberController.selection.baseOffset;
    final int _endPosition = phoneNumberController.selection.extentOffset;

    final List<String> chars = phoneNumberController.text.split("");

    if (_startPosition == _endPosition) {
      if (_startPosition == -1) {
        chars.add(numpad);
      } else {
        chars.insert(_startPosition, numpad);
      }
    } else {
      chars.replaceRange(_startPosition, _endPosition, [numpad]);
    }

    phoneNumberController.value = TextEditingValue(
        text: chars.join(""),
        selection: TextSelection.collapsed(
            offset: _startPosition == -1 ? chars.length : (_startPosition + 1)
        )
    );

  }

  onDeletePress() {
    final int _startPosition = phoneNumberController.selection.baseOffset;
    final int _endPosition = phoneNumberController.selection.extentOffset;

    final List<String> chars = phoneNumberController.text.split("");

    int _newOffset;

    if (chars.length == 0) {
      return;
    }

    if (_startPosition == _endPosition) {
      if (_startPosition == -1) {
        chars.removeLast();
      } else {
        chars.removeAt(_startPosition - 1);
      }

      _newOffset = _startPosition == -1 ? chars.length : (_startPosition - 1);
    } else {
      chars.replaceRange(_startPosition, _endPosition, []);

      _newOffset = _startPosition;
    }

    phoneNumberController.value = TextEditingValue(
        text: chars.join(""),
        selection: TextSelection.collapsed(
            offset: _newOffset
        )
    );

  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    phoneNumberController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    touchAreaSize = MediaQuery.of(context).size.width * numpadWidth/3 - 20;
    if (MediaQuery.of(context).size.aspectRatio < 0.5) {
      touchAreaSize = MediaQuery.of(context).size.width * numpadWidth/3 - 15;
    }
    if (MediaQuery.of(context).size.width <= 320) {
      touchAreaSize = MediaQuery.of(context).size.width * numpadWidth / 3 - 20;
    }
    if (MediaQuery.of(context).size.width > 400) {
      if (MediaQuery.of(context).size.aspectRatio > 0.5) {
        touchAreaSize = 92.5;
      }
    }
    return SafeArea(
        minimum: EdgeInsets.symmetric(vertical: 0, horizontal: 0),
        child: Column(
            children: [
              // Phone input
              Expanded(
                  child: Row(
                    children: [
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                          child: NoKeyboardTextField(
                            controller: phoneNumberController,
                            onChanged: (value) {
                              final formattedValue = StringHandler.formatPhoneNumberBeforeCall(value);
                              phoneNumberController.value = TextEditingValue(
                                text: formattedValue,
                                selection: TextSelection.collapsed(
                                    offset: formattedValue.length
                                )
                              );
                            },
                            style: TextStyle(
                                color: MainTheme.gray555,
                                fontSize: 35,
                                fontWeight: FontWeight.w700
                            ),
                            decoration: InputDecoration(
                                border: InputBorder.none
                            ),
                            showCursor: true,
                            textAlign: TextAlign.center,
                            toolbarOptions: ToolbarOptions(
                                copy: true,
                                cut: true,
                                paste: true,
                                selectAll: true
                            ),
                            readOnly: true,
                            cursorHeight: 35,
                          ),
                        ),
                      )
                    ],
                  ),
              ),
              Container(
                  padding: EdgeInsets.only(bottom: 10),
                  width: MediaQuery.of(context).size.width * numpadWidth,
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                          Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                CallButton(
                                    touchAreaSize: touchAreaSize,
                                    onPressed: () => onNumpadPress("1"),
                                    text: "1"
                                ),
                                CallButton(
                                    touchAreaSize: touchAreaSize,
                                    onPressed: () => onNumpadPress("2"),
                                    text: "2"
                                ),
                                CallButton(
                                    touchAreaSize: touchAreaSize,
                                    onPressed: () => onNumpadPress("3"),
                                    text: "3"
                                )
                              ],
                            ),
                          ),
                          Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                CallButton(
                                    touchAreaSize: touchAreaSize,
                                    onPressed: () => onNumpadPress("4"),
                                    text: "4"
                                ),
                                CallButton(
                                    touchAreaSize: touchAreaSize,
                                    onPressed: () => onNumpadPress("5"),
                                    text: "5"
                                ),
                                CallButton(
                                    touchAreaSize: touchAreaSize,
                                    onPressed: () => onNumpadPress("6"),
                                    text: "6"
                                )
                              ],
                            ),
                          ),
                          Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                CallButton(
                                    touchAreaSize: touchAreaSize,
                                    onPressed: () => onNumpadPress("7"),
                                    text: "7"
                                ),
                                CallButton(
                                    touchAreaSize: touchAreaSize,
                                    onPressed: () => onNumpadPress("8"),
                                    text: "8"
                                ),
                                CallButton(
                                    touchAreaSize: touchAreaSize,
                                    onPressed: () => onNumpadPress("9"),
                                    text: "9"
                                )
                              ],
                            ),
                          ),
                          Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                CallButton(
                                    touchAreaSize: touchAreaSize,
                                    onPressed: null,
                                    icon: Icons.account_circle_outlined,
                                    tooltip: "Gọi nội bộ (Đang phát triển)",
                                ),
                                CallButton(
                                    touchAreaSize: touchAreaSize,
                                    onPressed: () => onNumpadPress("0"),
                                    text: "0"
                                ),
                                CallButton(
                                    touchAreaSize: touchAreaSize,
                                    onPressed: () => onDeletePress(),
                                    icon: Icons.backspace_outlined,
                                )
                              ],
                            ),
                          ),
                          Container(
                              child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  children: [
                                      if (AppConfig.videoCall)
                                          CallButton(
                                              touchAreaSize: touchAreaSize,
                                              onPressed: context.watch<CallsProvider>().makingCall
                                                  ? null : () => callOut(videoCall: true),
                                              icon: Icons.videocam_outlined,
                                              foregroundColor: Colors.white,
                                              label: 'Gọi video',
                                              backgroundColor: Colors.orange,
                                              borderColor: Colors.orange
                                          ),
                                      if (AppConfig.videoCall)
                                          CallButton(
                                              touchAreaSize: touchAreaSize,
                                              onPressed: () => null,
                                              text: ".",
                                              foregroundColor: Colors.transparent,
                                              backgroundColor: Colors.transparent,
                                          ),
                                      CallButton(
                                          touchAreaSize: touchAreaSize,
                                          onPressed: context.watch<CallsProvider>().makingCall 
                                              ? null : () => callOut(),
                                          icon: Icons.phone_in_talk_outlined,
                                          foregroundColor: Colors.white,
                                          label: 'Gọi',
                                          backgroundColor: MainTheme.primaryColor,
                                          borderColor: MainTheme.primaryColor,
                                      )
                                  ],
                              ),
                          )
                      ],
                  ),
              )
            ]
        )
    );
  }

}
