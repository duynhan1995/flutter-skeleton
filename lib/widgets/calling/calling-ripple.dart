import 'package:etelecom/theme/theme.dart';
import 'package:flutter/material.dart';

class CallingRipple extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 50, horizontal: 0),
      child: Stack(
        alignment: AlignmentDirectional.center,
        children: [
          Icon(Icons.account_circle_outlined,
            color: MainTheme.grayCCC,
            size: 120
          )
        ],
      )
    );
  }

}
