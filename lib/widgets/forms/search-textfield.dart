import 'dart:async';

import 'package:etelecom/theme/theme.dart';
import 'package:etelecom/widgets/bases/base-text-field.dart';
import 'package:flutter/material.dart';

class ESearchTextField extends StatefulWidget {
  final TextEditingController controller;
  final String searchLabel;
  final String searchHint;
  final Function()? onBlur;
  final Function()? onSubmitted;
  final Function()? onChanged;
  final bool autoFocus;

  ESearchTextField({
    required this.controller,
    this.searchLabel = "",
    this.searchHint = "",
    this.onBlur,
    this.onSubmitted,
    this.onChanged,
    this.autoFocus = false
  });

  @override
  State<StatefulWidget> createState() {
    return _ESearchTextFieldState();
  }

}

class _ESearchTextFieldState extends State<ESearchTextField> {
  Timer? _debounce;

  void _onSearchChanged() {
    if (widget.onChanged != null) {
      if (_debounce?.isActive ?? false) _debounce?.cancel();
      _debounce = Timer(Duration(milliseconds: 750), () {
        widget.onChanged!();
      });
    }
  }

  void dispose() {
    _debounce?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
        children: [
            ETextField(
                textController: widget.controller,
                labelText: widget.searchLabel,
                hintText: widget.searchHint,
                autoFocus: widget.autoFocus,
                onBlur: widget.onBlur,
                onSubmitted: widget.onSubmitted,
                onChanged: _onSearchChanged,
                textInputAction: TextInputAction.search,
                contentPadding: EdgeInsets.only(bottom: 8.0, top: 0, right: 45)
            ),
            Positioned(
                width: 45,
                bottom: 0,
                right: 0,
                child: IconButton(
                    color: MainTheme.gray555,
                    onPressed: widget.controller.clear,
                    alignment: Alignment.bottomCenter,
                    icon: Icon(
                        Icons.clear,
                        size: 18
                    )
                )
            )
        ]
    );
  }

}
