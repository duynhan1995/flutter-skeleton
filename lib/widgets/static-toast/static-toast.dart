import 'package:etelecom/providers/call-manager/calls.provider.dart';
import 'package:etelecom/providers/network/network.provider.dart';
import 'package:etelecom/widgets/static-toast/extension-status.dart';
import 'package:etelecom/widgets/static-toast/network-status.dart';
import 'package:etelecom/widgets/static-toast/tenant-status.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class StaticToast extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    if (context.watch<NetworkProvider>().checkingNetwork) {
      return NetworkStatusToast();
    }
    if (context.watch<CallsProvider>().portsipRegistering) {
      return TenantStatusToast();
    }
    return ExtensionStatusToast();
  }
}
