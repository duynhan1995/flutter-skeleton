import 'package:etelecom/models/User.dart';
import 'package:etelecom/models/extension/Extension.dart';
import 'package:etelecom/providers/authenticate/authenticate.provider.dart';
import 'package:etelecom/theme/theme.dart';
import 'package:etelecom/widgets/mini-toast/mini-toast.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ExtensionStatusToast extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    final User? _user = context.watch<AuthenticateProvider>().currentUser;
    final bool _sessionInfoUpdating = context.watch<AuthenticateProvider>().sessionInfoUpdating;
    if (_user == null || _sessionInfoUpdating) {
      return Container();
    }

    final Extension? _extension = _user.extension;

    if (_extension == null || _extension.id == null) {
      return EMiniToast(
          color: MainTheme.danger,
          title: "Tài khoản của bạn chưa được gán máy nhánh"
      );
    } else {
      final bool unlimited = _extension.expiresAt?.year == 1;

      if (!unlimited) {
        final bool expired = _extension.expiresAt?.isBefore(DateTime.now()) ?? false;

        if (expired) {
          return EMiniToast(
              color: MainTheme.danger,
              title: "Máy nhánh của bạn đã hết hạn"
          );
        }
      }

      final String extNumber = _extension.extensionNumber ?? "";
      final String extPw = _extension.extensionPassword ?? "";
      final String extDomain = _extension.tenantDomain ?? "";

      if (extNumber.isEmpty || extPw.isEmpty || extDomain.isEmpty) {
        return EMiniToast(
            color: MainTheme.danger,
            title: "Đã có lỗi xảy ra với máy nhánh của bạn"
        );
      }

      return Container();
    }

  }
}
