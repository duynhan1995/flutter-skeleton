import 'package:etelecom/providers/call-manager/calls.provider.dart';
import 'package:etelecom/theme/theme.dart';
import 'package:etelecom/widgets/mini-toast/mini-toast.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class TenantStatusToast extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    final _failCode = context.watch<CallsProvider>().portsipRegisterFailureCode;

    if (_failCode.isNotEmpty) {
      return EMiniToast(
          color: MainTheme.danger,
          title: "Kết nối tổng đài thất bại (Mã lỗi: $_failCode)"
      );
    } else {
      return EMiniToast(
          color: MainTheme.primaryColor,
          title: "Kết nối tổng đài thành công"
      );
    }

  }
}
