import 'package:etelecom/providers/network/network.provider.dart';
import 'package:etelecom/theme/theme.dart';
import 'package:etelecom/widgets/mini-toast/mini-toast.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class NetworkStatusToast extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    final _networkStatus = context.watch<NetworkProvider>().networkStatus;

    if (_networkStatus == NetworkStatus.none) {
      return EMiniToast(
          color: MainTheme.danger,
          title: "Không có kết nối internet"
      );
    } else if (_networkStatus == NetworkStatus.retrying) {
      return EMiniToast(
          color: MainTheme.warning,
          title: "Đang kết nối internet ..."
      );
    } else {
      return EMiniToast(
          color: MainTheme.primaryColor,
          title: "Đã kết nối thành công"
      );
    }

  }
}
