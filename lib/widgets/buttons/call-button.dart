import 'package:etelecom/theme/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class CallButton extends StatelessWidget {

  final IconData? icon;
  final String? text;
  final String? imageUrl;
  final bool imageAsAnIcon;
  final double? iconSize;
  final double? textSize;//
  final double? imageSize;// icon & text size
  final double? padding;
  final Color? foregroundColor;
  final Color? backgroundColor;
  final Color? borderColor;
  final String label;
  final VoidCallback? onPressed;
  final String? tooltip;
  final Brightness? theme;
  final double touchAreaSize;

  CallButton({
    this.icon,
    this.text,
    this.imageUrl,
    this.imageAsAnIcon = false,
    this.iconSize = 32.5,
    this.textSize = 32.5,
    this.imageSize = 45,
    this.padding = 10,
    this.label = '',
    required this.onPressed,
    this.foregroundColor,
    this.backgroundColor,
    this.borderColor,
    this.tooltip,
    this.theme = Brightness.light,
    this.touchAreaSize = 80
  }) : assert (
    icon != null || (text != null && text.isNotEmpty) || (imageUrl != null && imageUrl.isNotEmpty)
  ), assert(imageUrl != null || (imageUrl == null && !imageAsAnIcon));

  @override
  Widget build(BuildContext context) {
    final defaultForegroundColor = theme == Brightness.light ? MainTheme.gray444 : Colors.white;
    final defaultBackgroundColor =
        theme == Brightness.light ? MainTheme.grayF5 : Colors.black.withOpacity(.25);
    final defaultDisableColor = Colors.white.withOpacity(.5);

    final defaultBorderColor = backgroundColor != null
        ? backgroundColor!
        : (theme == Brightness.dark ? MainTheme.gray555 : MainTheme.grayF5);

    final touchRatio = MediaQuery.of(context).size.aspectRatio < 0.5 ? 0.8 : 0.9;

    Widget? buttonContent;
    if (icon != null) {
      buttonContent = Icon(
          icon,
          color: onPressed != null
              ? (foregroundColor != null
                ? foregroundColor
                : defaultForegroundColor)
              : defaultDisableColor,
          size: iconSize,
      );
    } else if (imageUrl != null) {
      buttonContent = Image(
          width: imageSize,
          height: imageSize,
          image: AssetImage("assets/images/$imageUrl"),
          fit: BoxFit.cover,
          color: onPressed != null
              ? (foregroundColor != null
                ? foregroundColor
                : defaultForegroundColor)
              : defaultDisableColor,
      );
    } else {
      buttonContent = Text(
          text ?? "",
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: textSize,
              color: onPressed != null
                  ? (foregroundColor != null
                    ? foregroundColor
                    : defaultForegroundColor)
                  : defaultDisableColor,
              fontWeight: FontWeight.w600
          )
      );
    }

    return Column(
      children: [
        if (imageUrl != null && !imageAsAnIcon)
          Container(
              width: touchAreaSize,
              height: touchAreaSize,
              child: MaterialButton(
                minWidth: touchAreaSize,
                height: touchAreaSize,
                padding: EdgeInsets.all(0),
                splashColor: borderColor != null
                    ? borderColor!
                    : defaultBorderColor,
                onPressed: onPressed,
                color: Colors.transparent,
                disabledColor: Colors.transparent,
                disabledTextColor: Colors.transparent,
                elevation: 0,
                hoverElevation: 0,
                focusElevation: 0,
                highlightElevation: 0,
                disabledElevation: 0,
                highlightColor: Colors.transparent,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(100),
                ),
                child: Container(
                    width: touchAreaSize * touchRatio,
                    height: touchAreaSize * touchRatio,
                    child: Image(
                      width: imageSize,
                      height: imageSize,
                      image: AssetImage("assets/images/$imageUrl"),
                      fit: BoxFit.cover,
                      color: onPressed == null ? Colors.white.withOpacity(.6) : null,
                    ),
                ),
              ),
          ),
        if (imageUrl == null || imageAsAnIcon)
          Container(
            width: touchAreaSize,
            height: touchAreaSize,
            child: MaterialButton(
              minWidth: touchAreaSize,
              height: touchAreaSize,
              padding: EdgeInsets.all(0),
              splashColor: borderColor != null
                  ? borderColor!
                  : defaultBorderColor,
              onPressed: onPressed,
              color: Colors.transparent,
              disabledColor: Colors.transparent,
              disabledTextColor: Colors.transparent,
              elevation: 0,
              hoverElevation: 0,
              focusElevation: 0,
              highlightElevation: 0,
              disabledElevation: 0,
              highlightColor: Colors.transparent,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(100),
              ),
              child: Container(
                width: touchAreaSize * touchRatio,
                height: touchAreaSize * touchRatio,
                decoration: BoxDecoration(
                    color: backgroundColor != null
                        ? (onPressed != null
                        ? backgroundColor
                        : backgroundColor?.withOpacity(.5))
                        : defaultBackgroundColor,
                    borderRadius: BorderRadius.circular(100),
                    border: Border.all(
                        color: borderColor != null
                            ? borderColor!
                            : defaultBorderColor
                    )
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    buttonContent
                  ],
                ),
              ),
            ),
          ),
        if (label.isNotEmpty)
          Container(
            margin: EdgeInsets.only(top: 2.5),
            child: Text(
              label,
              style: TextStyle(
                  fontSize: 13,
                  color: onPressed != null ? defaultForegroundColor : defaultForegroundColor.withOpacity(.6)
              ),
            ),
          )
      ],
    );
  }

}
