import 'package:etelecom/models/base/paging/paging.dart';
import 'package:etelecom/theme/theme.dart';
import 'package:etelecom/widgets/progress-indicators/circular-progress-indicator.dart';
import 'package:etelecom/widgets/list-view/empty-view.dart';
import 'package:etelecom/widgets/list-view/loading-view.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ERow extends StatefulWidget {
  final Function? rowCreated;
  final Widget child;

  const ERow({
    Key? key,
    this.rowCreated,
    required this.child,
  });

  @override
  _ERowState createState() => _ERowState();
}
class _ERowState extends State<ERow> {
  @override
  void initState() {
    super.initState();
    if (widget.rowCreated != null) {
      widget.rowCreated!();
    }
  }

  @override
  Widget build(BuildContext context) {
    return widget.child;
  }
}


class EListView extends StatelessWidget {
  final bool isLoading;
  final bool isEmpty;
  final int currentOffset;
  final String currentNextPointer;
  final int listLength;
  final Widget Function(BuildContext context, int index) itemChildBuilder;

  final Future<dynamic> Function() onLoadMore;
  final Future<dynamic> Function() onRefresh;

  EListView({
    this.isLoading = false,
    this.isEmpty = false,
    this.currentOffset = 0, // use for normal Paging
    this.currentNextPointer = ".", // use for CursorPaging
    this.listLength = 0,
    required this.itemChildBuilder,
    required this.onLoadMore,
    required this.onRefresh
  });

  bool get isCursorPaging => currentNextPointer.isNotEmpty && currentNextPointer != "."
      && currentOffset == 0;

  @override
  Widget build(BuildContext context) {
    if (isLoading && (
        (currentOffset == 0 && !isCursorPaging) ||
        (currentNextPointer == "." && isCursorPaging)
    )) {
      return ListLoading();
    }

    if (isEmpty) {
      return ListEmpty();
    }

    return ChangeNotifierProvider<InfiniteScrollHandler>(
      create: (context) => InfiniteScrollHandler(),
      child: Consumer<InfiniteScrollHandler>(builder: (context, model, child) {
        final bool _loadMore = isLoading && (currentOffset > 0 || currentNextPointer != ".");

        return RefreshIndicator(
          color: MainTheme.primaryColor,
          child: ListView.builder(
            itemCount: listLength + (_loadMore ? 1 : 0),
            itemBuilder: (context, index) {
              if (index == listLength) {
                return ECircularProgressIndicator();
              }

              return ERow(
                rowCreated: () {
                  model.handleItemScrolled(
                    context: context,
                    index: index,
                    listLength: listLength,
                    onLoadMore: () => onLoadMore()
                  );
                },
                child: itemChildBuilder(context, index)
              );
            }
          ),
          onRefresh: () => onRefresh()
        );
      }),
    );
  }

}

class InfiniteScrollHandler extends ChangeNotifier {
  Future<dynamic> handleItemScrolled({
    BuildContext? context,
    int? index,
    int? listLength,
    Future<dynamic> Function()? onLoadMore
  }) async {

    if (listLength! < DEFAULT_LIMIT) {
      return;
    }

    final int itemPosition = index! + 1;

    final bool requestMoreData = itemPosition != 0 && (itemPosition % DEFAULT_LIMIT == 0 ||
        (itemPosition % DEFAULT_LIMIT > 0 && itemPosition == listLength));


    if (requestMoreData) {
      onLoadMore!();
    }
  }

}
