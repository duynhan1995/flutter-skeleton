import 'package:flutter/material.dart';

class ListLoading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Row(
          children: [
            Expanded(
                child: Text('Đang tải dữ liệu...', textAlign: TextAlign.center)
            )
          ],
        )
      ],
    );
  }
}
