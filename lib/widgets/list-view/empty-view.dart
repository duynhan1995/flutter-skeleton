import 'package:flutter/material.dart';

class ListEmpty extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Row(
          children: [
            Expanded(
                child: Text('Không có dữ liệu', textAlign: TextAlign.center)
            )
          ],
        )
      ],
    );
  }
}
