import 'package:etelecom/services/http.service.dart';

final HttpService http = HttpService();

class UserApi {
  static Future<dynamic> sessionInfo(String token) async {
    try {
      return await http.post('/api/etop.User/SessionInfo', {}, token);
    } catch (e) {
      print('ERROR in sessionInfo. $e');
      return e;
    }
  }

  static Future<dynamic> switchAccount(String accountId) async {
    try {
      return await http.post('/api/etop.User/SwitchAccount', {
        'account_id': accountId
      });
    } catch (e) {
      print('ERROR in SwitchAccount. $e');
      return e;
    }
  }

  static Future<dynamic> login(String login, String password) async {
    try {
      return await http.post('/api/etop.User/Login', {
        'account_type': 'shop',
        'login': login,
        'password': password
      });
    } catch (e) {
      print('ERROR in login. $e');
      return e;
    }
  }

  static Future<dynamic> changePassword(String currentPassword, String newPassword, String confirmPassword) async {
    try {
      return await http.post('/api/etop.User/ChangePassword', {
        'current_password': currentPassword,
        'new_password': newPassword,
        'confirm_password': confirmPassword
      });
    } catch (e) {
      print('ERROR in changePassword. $e');
      throw e;
    }
  }
}
