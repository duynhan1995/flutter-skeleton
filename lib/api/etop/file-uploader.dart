import 'dart:io';
import 'package:etelecom/services/http.service.dart';
import 'package:dio/dio.dart';

final HttpService http = HttpService();

class FileUploadApi {
  static fileUpload(File file) async {
    try {
      Dio dio = new Dio();
      Response response;
      var formData = FormData();
      formData.files.add(MapEntry("files", await MultipartFile.fromFile(file.path, filename: file.path.split('/').last)));
      response = await dio.post("${HttpService.hostUrl}/upload", data: formData);
      return response.data;
    } catch (e) {
      print('ERROR in fileUpload $e');
      throw e;
    }
  }
}