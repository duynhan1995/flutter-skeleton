import 'package:etelecom/models/base/paging/cursor-paging.dart';
import 'package:etelecom/models/call-log/CallLog.dart';
import 'package:etelecom/services/http.service.dart';
import 'package:etelecom/utils/map-handler.dart';

final http = HttpService();

class CallLogApi {
  static Future<dynamic> getCallLogs(GetCallLogsRequest request) async {
    try {
      return await http.post('/api/shop.Etelecom/GetCallLogs', request.toJson());
    } catch (e) {
      print('ERROR in getCallLogs Api. $e');
      throw e;
    }
  }

  static Future<dynamic> createCallLog(CreateCallLogRequest newCallLog) async {
    try {
      return await http.post('/api/shop.Etelecom/CreateCallLog', newCallLog.toJson());
    } catch (e) {
      print('ERROR in createCallLog Api. $e');
      throw e;
    }
  }
}

class GetCallLogsFilter {
  List<String>? extensionIds;
  List<String>? hotlineIds;
  GetCallLogsFilter({this.extensionIds, this.hotlineIds});

  Map<String, dynamic> toJson() =>
      {"extension_ids": extensionIds, "hotline_ids": hotlineIds};
}

class GetCallLogsRequest {
  CursorPaging? paging;
  GetCallLogsFilter? filter;

  GetCallLogsRequest({this.paging, this.filter});

  Map<String, dynamic>? toJson() {
    Map<String, dynamic>? _filterJson;
    Map<String, dynamic>? _pagingJson;

    if (filter != null) {
      _filterJson = filter!.toJson();
    }

    if (paging != null) {
      _pagingJson = paging!.toJson();
    }

    return MapHandler.trimNull({
      "filter": _filterJson,
      "paging": _pagingJson
    });
  }
}
