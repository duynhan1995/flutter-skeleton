import 'package:etelecom/services/http.service.dart';
import 'package:etelecom/utils/string-handler.dart';

enum CreditClassify {
  shipping, telecom
}

final HttpService http = HttpService();

class SummaryApi {
  static Future<dynamic> calcBalanceUser(CreditClassify creditClassify) async {
    try {
      return await http.post('/api/shop.Summary/CalcBalanceUser', {
        "credit_classify": StringHandler.enumToString(creditClassify)
      });
    } catch (e) {
      print('ERROR in getExtensions Api. $e');
      throw e;
    }
  }
}
