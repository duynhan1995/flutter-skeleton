import 'package:etelecom/models/base/paging/paging.dart';
import 'package:etelecom/models/ticket/Ticket.dart';
import 'package:etelecom/services/http.service.dart';
import 'package:etelecom/utils/map-handler.dart';
import 'package:etelecom/utils/string-handler.dart';

final HttpService http = HttpService();

class TicketApi {
  static Future<dynamic> createTicket(CreateTicketRequest request) async {
    try {
      return await http.post('/api/shop.Ticket/CreateTicket', request.toJson());
    } catch (e) {
      print('ERROR in createTicket Api. $e');
      throw e;
    }
  }

  static Future<dynamic> getTickets(GetTicketsRequest request) async {
    try {
      return await http.post('/api/shop.Ticket/GetTickets', request.toJson());
    } catch (e) {
      print('ERROR in getTickets Api. $e');
      throw e;
    }
  }

  static Future<dynamic> getTicketLabels(GetTicketLabelsRequest request) async {
    try {
      return await http.post(
          '/api/shop.Ticket/GetTicketLabels', request.toJson());
    } catch (e) {
      print('ERROR in getTicketLabels Api. $e');
      throw e;
    }
  }

  static Future<dynamic> deleteTicket(String id) async {
    try {
      return await http.post('/api/shop.Ticket/DeleteTicket', {"id": id});
    } catch (e) {
      print('ERROR in deleteTicket Api. $e');
      throw e;
    }
  }

  static Future<dynamic> getTicketComments(GetTicketCommentsRequest request) async {
    try {
      return await http.post('/api/shop.Ticket/GetTicketComments', request.toJson());
    } catch (e) {
      print('ERROR in GetTicketComments Api. $e');
      throw e;
    }
  }

  static Future<dynamic> assignTicket(List<String> assignedUserIds, String ticketId) async {
    try {
      return await http.post('/api/shop.Ticket/AssignTicket', {"assigned_user_ids": assignedUserIds, "ticket_id": ticketId});
    } catch (e) {
      print('ERROR in AssignTicket Api. $e');
      throw e;
    }
  }

  static Future<dynamic> unAssignTicket(String ticketId) async {
    try {
      return await http.post('/api/shop.Ticket/UnassignTicket', {"ticket_id": ticketId, "assigned_user_ids": []});
    } catch (e) {
      print('ERROR in UnassignTicket Api. $e');
      throw e;
    }
  }

  static Future<dynamic> closeTicket(TicketState state, String ticketId, [String? note]) async {
    try {
      final resquest = CloseTicketRequest(ticketId: ticketId, state: state, note: note ?? "");
      return await http.post('/api/shop.Ticket/CloseTicket', resquest.toJson());
    } catch (e) {
      print('ERROR in CloseTicket Api. $e');
      throw e;
    }
  }

  static Future<dynamic> confirmTicket(String ticketId, [String? note]) async {
    try {
      final resquest = UpdateTicketStateRequest(ticketId: ticketId, note: note ?? "");
      return await http.post('/api/shop.Ticket/ConfirmTicket', resquest.toJson());
    } catch (e) {
      print('ERROR in ConfirmTicket Api. $e');
      throw e;
    }
  }

  static Future<dynamic> reopenTicket(String ticketId, [String? note]) async {
    try {
      final resquest = UpdateTicketStateRequest(ticketId: ticketId, note: note ?? "");
      return await http.post('/api/shop.Ticket/ReopenTicket', resquest.toJson());
    } catch (e) {
      print('ERROR in ReopenTicket Api. $e');
      throw e;
    }
  }

  static Future<dynamic> createTicketComment(CreateTicketCommentRequest request) async {
    try {
      return await http.post('/api/shop.Ticket/CreateTicketComment', request.toJson());
    } catch (e) {
      print('ERROR in CreateTicketComment Api. $e');
      throw e;
    }
  }
}

class GetTicketLabelsRequest {
  bool? tree;
  GetTicketLabelsFilter? filter;
  GetTicketLabelsRequest({this.tree, this.filter});

  Map<String, dynamic>? toJson() {
    Map<String, dynamic>? _filterJson;

    if (filter != null) {
      _filterJson = filter!.toJson();
    }

    return MapHandler.trimNull({"filter": _filterJson, "tree": tree});
  }
}

class GetTicketCommentsRequest {
  String? ticketId;
  GetTicketLabelsFilter? filter;
  Paging? paging;
  GetTicketCommentsRequest({this.filter, this.paging, this.ticketId});

  Map<String, dynamic>? toJson() {
    Map<String, dynamic>? _filterJson;
    Map<String, dynamic>? _pagingJson;

    if (filter != null) {
      _filterJson = filter!.toJson();
    }

    if (paging != null) {
      _pagingJson = paging!.toJson();
    }

    return MapHandler.trimNull(
      {"filter": _filterJson, "paging": _pagingJson, "ticket_id": ticketId}
    );
  }
}

class CloseTicketRequest {
  String? ticketId;
  String? note;
  TicketState? state;
  CloseTicketRequest({this.ticketId, this.note, this.state});

  Map<String, dynamic>? toJson() {
    if (note == null) {
      return MapHandler.trimNull({"state": StringHandler.enumToString(state), "ticket_id": ticketId});
    } else {
      return MapHandler.trimNull({"state": StringHandler.enumToString(state), "ticket_id": ticketId, "note": note});
    }
  }
}

class UpdateTicketStateRequest {
  String? ticketId;
  String? note;
  UpdateTicketStateRequest({this.ticketId, this.note});

  Map<String, dynamic>? toJson() {
    if (note == null) {
      return MapHandler.trimNull({"ticket_id": ticketId});
    } else {
      return MapHandler.trimNull({"ticket_id": ticketId, "note": note});
    }
  }
}

class GetTicketLabelsFilter {
  String? type;

  GetTicketLabelsFilter({this.type});

  Map<String, dynamic> toJson() => {
        "type": type,
      };
}

class GetTicketCommentsFilter {
  String? createdBy;
  List<String>? ids;
  String? parentId;
  String? title;

  GetTicketCommentsFilter({this.createdBy, this.ids, this.parentId, this.title});

  Map<String, dynamic> toJson() => {
        "created_by": createdBy,
        "ids": ids,
        "parent_id": parentId,
        "title": title
      };
}

class GetTicketsRequest {
  GetTicketsFilter? filter;
  Paging? paging;

  GetTicketsRequest({this.paging, this.filter});

  Map<String, dynamic>? toJson() {
    Map<String, dynamic>? _filterJson;
    Map<String, dynamic>? _pagingJson;

    if (filter != null) {
      _filterJson = filter!.toJson();
    }

    if (paging != null) {
      _pagingJson = paging!.toJson();
    }

    return MapHandler.trimNull({"filter": _filterJson, "paging": _pagingJson});
  }
}

class CreateTicketRequest {
  String? description;
  List<String?>? label_ids;
  String? note;
  String? ref_code;
  String? ref_id;
  String? ref_type;
  String? source;
  String? title;
  String? type;

  CreateTicketRequest({
    this.description,
    this.label_ids,
    this.note,
    this.ref_code,
    this.ref_id,
    this.ref_type,
    this.source,
    this.title,
    this.type,
  });

  Map<String, dynamic> toJson() => {
    "description": description,
    "label_ids": label_ids,
    "note": note,
    "ref_code": ref_code,
    "ref_id": ref_id,
    "ref_type": ref_type,
    "source": source,
    "title": title,
    "type": type
  };
}

class CreateTicketCommentRequest {
  String? imageUrl;
  List<String>? imageUrls;
  String? message;
  String? parentId;
  String? ticketId;

  CreateTicketCommentRequest({
    this.imageUrl,
    this.imageUrls,
    this.message,
    this.parentId,
    this.ticketId
  });

  Map<String, dynamic> toJson() => {
    "image_url": imageUrl,
    "image_urls": imageUrls,
    "message": message,
    "parent_id": parentId,
    "ticket_id": ticketId,
  };
}

class GetTicketsFilter {
  List<String>? ids;
  String? accountId;
  List<String>? assignedUserId;
  String? closedBy;
  String? code;
  String? createdBy;
  List<String>? labelIds;
  String? refCode;
  String? refId;
  TicketRefType? refType;
  TicketState? state;
  String? title;
  List<TicketType>? types;

  GetTicketsFilter({
    this.ids,
    this.accountId,
    this.assignedUserId,
    this.closedBy,
    this.code,
    this.createdBy,
    this.labelIds,
    this.refCode,
    this.refId,
    this.refType,
    this.state,
    this.title,
    this.types
  });

  Map<String, dynamic> toJson() => {
    "ids": ids,
    "account_id": accountId,
    "assigned_user_id": assignedUserId,
    "closed_by": closedBy,
    "code": code,
    "created_by": createdBy,
    "label_ids": labelIds,
    "ref_code": refCode,
    "ref_id": refId,
    "ref_type": refType,
    "state": state == TicketState.open ? "new" : StringHandler.enumToString(state),
    "title": title,
    "types": types?.map((type) => StringHandler.enumToString(type)).toList()
  };
}
