import 'package:etelecom/services/http.service.dart';

final HttpService http = HttpService();

class HotlineApi {
  static Future<dynamic> getHotlines() async {
    try {
      return await http.post('/api/shop.Etelecom/GetHotlines', {});
    } catch (e) {
      print('ERROR in GetHotlines Api. $e');
      return [];
    }
  }
}