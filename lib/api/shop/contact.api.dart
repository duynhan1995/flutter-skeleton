import 'package:etelecom/models/base/paging/cursor-paging.dart';
import 'package:etelecom/services/http.service.dart';
import 'package:etelecom/utils/map-handler.dart';

final HttpService http = HttpService();

class ContactApi {
  static Future<dynamic> createContact(CreateContactRequest request) async {
    try {
      return await http.post('/api/shop.Contact/CreateContact', request.toJson());
    } catch (e) {
      print('ERROR in createContact Api. $e');
      throw e;
    }
  }

  static Future<dynamic> getContacts(GetContactsRequest request) async {
    try {
      return await http.post('/api/shop.Contact/GetContacts', request.toJson());
    } catch (e) {
      print('ERROR in getContacts Api. $e');
      throw e;
    }
  }

  static Future<dynamic> updateContact(UpdateContactRequest request) async {
    try {
      return await http.post('/api/shop.Contact/UpdateContact', request.toJson());
    } catch (e) {
      print('ERROR in updateContact Api. $e');
      throw e;
    }
  }

  static Future<dynamic> deleteContact(String id) async {
    try {
      return await http.post('/api/shop.Contact/DeleteContact', {"id": id});
    } catch (e) {
      print('ERROR in deleteContact Api. $e');
      throw e;
    }
  }
}

class GetContactsRequest {
  GetContactsFilter? filter;
  CursorPaging? paging;

  GetContactsRequest({this.paging, this.filter});

  Map<String, dynamic>? toJson() {
    Map<String, dynamic>? _filterJson;
    Map<String, dynamic>? _pagingJson;

    if (filter != null) {
      _filterJson = filter!.toJson();
    }

    if (paging != null) {
      _pagingJson = paging!.toJson();
    }

    return MapHandler.trimNull({
      "filter": _filterJson,
      "paging": _pagingJson
    });
  }
}

class CreateContactRequest {
  String? fullName;
  String? phone;

  CreateContactRequest({this.fullName, this.phone});

  Map<String, dynamic> toJson() => {
    "full_name": fullName,
    "phone": phone
  };
}

class UpdateContactRequest {
  String? fullName;
  String? phone;
  String? id;

  UpdateContactRequest({this.fullName, this.phone, this.id});

  Map<String, dynamic> toJson() => {
    "full_name": fullName,
    "phone": phone,
    "id": id
  };
}

class GetContactsFilter {
  List<String>? ids;
  String? name;
  String? phone;

  GetContactsFilter({
    this.ids,
    this.name,
    this.phone
  });

  Map<String, dynamic> toJson() => {
    "ids": ids,
    "name": name,
    "phone": phone,
  };
}
