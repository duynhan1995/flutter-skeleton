import 'package:etelecom/models/base/paging/cursor-paging.dart';
import 'package:etelecom/services/http.service.dart';
import 'package:etelecom/utils/map-handler.dart';

final HttpService http = HttpService();

class CallSessionApi {

  static destroyCallSession({
    required String xSessionId,
    required String extensionId
  }) async {
    try {
      await http.post('/api/shop.Etelecom/DestroyCallSession', MapHandler.trimNull({
        "external_session_id": xSessionId,
        "extension_id": extensionId
      }));
    } catch (e) {
      print('ERROR in CallSessionApi.destroyCallSession $e');
    }
  }

}
