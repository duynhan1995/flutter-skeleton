import 'package:etelecom/services/http.service.dart';
import 'package:etelecom/utils/map-handler.dart';

final HttpService http = HttpService();

class ExtensionApi {
  static Future<dynamic> getExtensions(GetExtensionsRequest request) async {
    try {
      return await http.post('/api/shop.Etelecom/GetExtensions', request.toJson());
    } catch (e) {
      print('ERROR in getExtensions Api. $e');
      return [];
    }
  }
}

class GetExtensionsRequest {
  GetExtensionsFilter? filter;

  GetExtensionsRequest({this.filter});

  Map<String, dynamic>? toJson() {
    Map<String, dynamic>? _filterJson;

    if (filter != null) {
      _filterJson = filter!.toJson();
    }

    return MapHandler.trimNull({
      "filter": _filterJson
    });
  }
}

class GetExtensionsFilter {
  List<String>? extensionNumbers;
  String? hotlineId;

  GetExtensionsFilter({
    this.extensionNumbers,
    this.hotlineId
  });

  Map<String, dynamic> toJson() => {
    "extension_numbers": extensionNumbers,
    "hotline_id": hotlineId
  };
}
