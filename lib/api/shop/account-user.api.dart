import 'package:etelecom/models/base/paging/cursor-paging.dart';
import 'package:etelecom/services/http.service.dart';
import 'package:etelecom/utils/map-handler.dart';

final HttpService http = HttpService();

class AccountUserApi {
  static Future<dynamic> getAccountUsers(GetAccountUsersRequest request) async {
    try {
      return await http.post('/api/shop.AccountUser/GetAccountUsers', request.toJson());
    } catch (e) {
      print('ERROR in GetRelationships Api. $e');
      throw e;
    }
  }
}

class GetAccountUsersRequest {
  GetAccountUsersFilter? filter;
  List<GetAccountUsersFilters>? filters;
  CursorPaging? paging;

  GetAccountUsersRequest({
    this.filter,
    this.paging,
    this.filters
  });

  Map<String, dynamic>? toJson() {
    Map<String, dynamic>? _filterJson;
    List<Map<String, dynamic>?>? _filtersJson;
    Map<String, dynamic>? _pagingJson;

    if (filters != null && filters!.isNotEmpty) {
      _filtersJson = filters!.map((e) => e.toJson()).toList();
    }

    if (filter != null) {
      _filterJson = filter!.toJson();
    }

    if (paging != null) {
      _pagingJson = paging!.toJson();
    }

    return MapHandler.trimNull({
      "filter": _filterJson,
      "filters": _filtersJson,
      "paging": _pagingJson
    });
  }
}

enum FiltersOperator {
  contains,
  equal,
  notEqual,
  lessThan,
  lessThanAndEqual,
  greaterThan,
  greaterThanAndEqual,
  inside,
  intersect,
}

const FiltersOperatorMap = {
  FiltersOperator.contains: 'c',
  FiltersOperator.equal: '=',
  FiltersOperator.notEqual: '!=',
  FiltersOperator.lessThan: '<',
  FiltersOperator.lessThanAndEqual: '<=',
  FiltersOperator.greaterThan: '>',
  FiltersOperator.greaterThanAndEqual: '>=',
  FiltersOperator.inside: 'in',
  FiltersOperator.intersect: '∩',
};



class GetAccountUsersFilters {
  String name;
  FiltersOperator op;
  String value;

  GetAccountUsersFilters({
    required this.name,
    required this.op,
    required this.value
  });

  Map<String, dynamic> toJson() => {
    "name": name,
    "op": FiltersOperatorMap[op],
    "value": value
  };
}

class GetAccountUsersFilter {
  String? extensionNumber;
  bool? hasExtension;
  String? name;
  String? phone;
  List<String>? roles;
  List<String>? userIds;

  GetAccountUsersFilter({
    this.extensionNumber,
    this.hasExtension = true,
    this.name,
    this.phone,
    this.roles,
    this.userIds
  });

  Map<String, dynamic> toJson() => {
    "extension_number": extensionNumber,
    "has_extension": hasExtension,
    "name": name,
    "phone": phone,
    "roles": roles,
    "user_ids": userIds
  };
}
