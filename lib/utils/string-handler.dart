import 'package:intl/intl.dart';

class StringHandler {

  static String secondsToTime(int? value) {
    if (value == null || value == 0) {
      return '00:00:00';
    }

    final hours = (value / 3600).floor();
    value %= 3600;

    final minutes = (value / 60).floor();
    final seconds = value % 60;
    return '${hours < 10 ? '0$hours' : hours}:${minutes < 10 ? '0$minutes' : minutes}:${seconds < 10 ? '0$seconds' : seconds}';
  }

  static String compactSecondsToTime(int? value) {
    if (value == null || value == 0) {
      return '00:00';
    }

    final hours = (value / 3600).floor();
    value %= 3600;

    final minutes = (value / 60).floor();
    final seconds = value % 60;

    if (hours == 0) {
      return '$minutes:${seconds < 10 ? '0$seconds' : seconds}';
    }

    return '$hours:${minutes < 10 ? '0$minutes' : minutes}:${seconds < 10 ? '0$seconds' : seconds}';
  }

  static String datetimeToString(DateTime? datetime) {
    if (datetime == null || datetime.toString().length == 0) {
      return "-";
    } else {
      return DateFormat('HH:mm dd/MM/yy').format(datetime.toLocal());
    }
  }

  static String enumToString(dynamic enumValue) {
    if (enumValue == null) {
      return "";
    }
    return enumValue.toString().split(".")[1];
  }

  static String formatPhoneNumberBeforeCall(String? phoneNumber) {
    if (phoneNumber == null) {
      return "";
    }
    phoneNumber = phoneNumber.replaceAll(RegExp(r'[+.\s-]*'), '');
    if (phoneNumber.length > 10) {
      phoneNumber = phoneNumber.replaceAll(RegExp(r'^(0084|084|\+84|\(\+84\)|84|\(84\))0?'), '0');
    }
    phoneNumber = phoneNumber.replaceAll(RegExp(r'[^0-9\*\#]*'), '');
    return phoneNumber;
  }

  static String makeSearchText(String str) {
    if (str.isNotEmpty) {
      str = str.toLowerCase();
      str = str.replaceAll("'", "");
      str = str.replaceAll(RegExp(r'à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ'), "a");
      str = str.replaceAll(RegExp(r'è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ'), "e");
      str = str.replaceAll(RegExp(r'ì|í|ị|ỉ|ĩ'), "i");
      str = str.replaceAll(RegExp(r'ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ'), "o");
      str = str.replaceAll(RegExp(r'ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ'), "u");
      str = str.replaceAll(RegExp(r'ỳ|ý|ỵ|ỷ|ỹ'), "y");
      str = str.replaceAll(RegExp(r'đ'), "d");
      str = str.replaceAll(RegExp(r'[,\-.!?~\s\|"+/\\:;<>{}\[\]=_()*&^%$#@`]+'), "");
    }
    return str;
  }

}
