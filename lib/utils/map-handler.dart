class MapHandler {
  static Map<String, dynamic>? trimNull (Map<String, dynamic>? json) {
    json!.removeWhere((key, value) {
      try {
        if (value == null || key.isEmpty || value.toString().isEmpty) {
          return true;
        }
        json[key] = trimNull(json[key]);
      } catch (e) {}
      return false;
    });

    return json;
  }
}
