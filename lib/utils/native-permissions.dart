import 'dart:io';

import 'package:etelecom/widgets/bases/base-material-button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';

class NativePermissions {
  static Future<bool> checkPermissions(List<Permission> permissions) async {

    await permissions.request();

    for (final permission in permissions) {
      final status = await permission.status;
      if (!status.isGranted) {
        return false;
      }
    }

    return true;
  }

  static void goToAppSettings({
    required BuildContext context,
    required String title,
    required String description
  }) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
            if (Platform.isIOS) {
              return CupertinoAlertDialog(
                  title: Text(title),
                  content: Text(description),
                  actions: [
                      CupertinoDialogAction(
                        child: Text('Đóng'),
                        onPressed: () => Navigator.of(context).pop(),
                      ),
                      CupertinoDialogAction(
                        child: Text('Thiết lập'),
                        onPressed: () {
                          Navigator.of(context).pop();
                          openAppSettings();
                        },
                      ),
                  ],
              );
            } else {
              return AlertDialog(
                title: Text(
                    title,
                    style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w600
                    )
                ),
                content: Text(description),
                actions: [
                  MaterialButton(
                    child: Text(
                        'Đóng',
                        style: TextStyle(
                            fontSize: 17
                        )
                    ),
                    onPressed: () => Navigator.of(context).pop(),
                  ),
                  MaterialButton(
                    child: Text(
                        'Thiết lập',
                        style: TextStyle(
                            color: Theme.of(context).primaryColor,
                            fontSize: 17
                        )
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                      openAppSettings();
                    },
                  ),
                ],
              );
            }
        }
    );
  }

}
