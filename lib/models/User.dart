import 'package:etelecom/models/extension/Extension.dart';

class User {
  String? block_reason;
  String? blocked_at;
  String? created_at;
  String? email;
  String? email_verification_sent_at;
  String? email_verified_at;
  String? full_name;
  String? id;
  bool? is_blocked;
  String? phone;
  String? phone_verification_sent_at;
  String? phone_verified_at;
  String? ref_aff;
  String? ref_sale;
  String? short_name;
  String? source;
  int? total_shop;
  String? updated_at;

  Extension? extension;

  User({
    this.block_reason,
    this.blocked_at,
    this.created_at,
    this.email,
    this.email_verification_sent_at,
    this.email_verified_at,
    this.full_name,
    this.id,
    this.is_blocked,
    this.phone,
    this.phone_verification_sent_at,
    this.phone_verified_at,
    this.ref_aff,
    this.ref_sale,
    this.short_name,
    this.source,
    this.total_shop,
    this.updated_at,
  });

  factory User.fromJson(Map<String, dynamic>? json) {
    if (json == null) {
      return User(id: null);
    }
    return User(
      block_reason: json["block_reason"],
      blocked_at: json["blocked_at"],
      created_at: json["created_at"],
      email: json["email"],
      email_verification_sent_at: json["email_verification_sent_at"],
      email_verified_at: json["email_verified_at"],
      full_name: json["full_name"],
      id: json["id"],
      is_blocked: json["is_blocked"],
      phone: json["phone"],
      phone_verification_sent_at: json["phone_verification_sent_at"],
      phone_verified_at: json["phone_verified_at"],
      ref_aff: json["ref_aff"],
      ref_sale: json["ref_sale"],
      short_name: json["short_name"],
      source: json["source"],
      total_shop: json["total_shop"],
      updated_at: json["updated_at"],
    );
  }

  Map<String, dynamic> toJson() => {
    "block_reason": block_reason,
    "blocked_at": blocked_at,
    "created_at": created_at,
    "email": email,
    "email_verification_sent_at": email_verification_sent_at,
    "email_verified_at": email_verified_at,
    "full_name": full_name,
    "id": id,
    "is_blocked": is_blocked,
    "phone": phone,
    "phone_verification_sent_at": phone_verification_sent_at,
    "phone_verified_at": phone_verified_at,
    "ref_aff": ref_aff,
    "ref_sale": ref_sale,
    "short_name": short_name,
    "source": source,
    "total_shop": total_shop,
    "updated_at": updated_at,
  };

}
