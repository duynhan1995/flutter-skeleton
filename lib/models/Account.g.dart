// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Account.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Account _$AccountFromJson(Map<String, dynamic> json) {
  return Account(
    access_token: json['access_token'] as String?,
    expires_in: json['expires_in'] as int?,
    id: json['id'] as String?,
    image_url: json['image_url'] as String?,
    name: json['name'] as String?,
    type: json['type'] as String?,
    url_slug: json['url_slug'] as String?,
  );
}

Map<String, dynamic> _$AccountToJson(Account instance) => <String, dynamic>{
      'access_token': instance.access_token,
      'expires_in': instance.expires_in,
      'id': instance.id,
      'image_url': instance.image_url,
      'name': instance.name,
      'type': instance.type,
      'url_slug': instance.url_slug,
    };

Permission _$PermissionFromJson(Map<String, dynamic> json) {
  return Permission(
    roles: (json['roles'] as List<dynamic>?)?.map((e) => e as String).toList(),
    permissions: (json['permissions'] as List<dynamic>?)
        ?.map((e) => e as String)
        .toList(),
  );
}

Map<String, dynamic> _$PermissionToJson(Permission instance) =>
    <String, dynamic>{
      'roles': instance.roles,
      'permissions': instance.permissions,
    };
