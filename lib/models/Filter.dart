import './ticket/Ticket.dart';

class TicketStateFilterOption {
  String name;
  TicketState? value;

  TicketStateFilterOption({this.name = "", this.value});
}
