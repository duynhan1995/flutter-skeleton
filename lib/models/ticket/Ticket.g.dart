// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Ticket.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Ticket _$TicketFromJson(Map<String, dynamic> json) {
  return Ticket(
    createdAt: json['created_at'] == null
        ? null
        : DateTime.parse(json['created_at'] as String),
    code: json['code'] as String?,
    id: json['id'] as String,
    updatedAt: json['updated_at'] as String?,
    title: json['title'] as String?,
    accountId: json['account_id'] as String?,
    shop: json['shop'] == null
        ? null
        : Shop.fromJson(json['shop'] as Map<String, dynamic>),
    assignedUserIds: (json['assigned_user_ids'] as List<dynamic>?)
        ?.map((e) => e as String)
        .toList(),
    assignedUsers: (json['assignedUsers'] as List<dynamic>?)
        ?.map((e) => AccountUser.fromJson(e as Map<String, dynamic>))
        .toList(),
    closedAt: json['closed_at'] == null
        ? null
        : DateTime.parse(json['closed_at'] as String),
    closedBy: json['closed_by'] as String,
    closedByUser: json['closedByUser'] == null
        ? null
        : AccountUser.fromJson(json['closedByUser'] as Map<String, dynamic>),
    confirmedAt: json['confirmed_at'] == null
        ? null
        : DateTime.parse(json['confirmed_at'] as String),
    confirmedBy: json['confirmed_by'] as String,
    confirmedByUser: json['confirmedByUser'] == null
        ? null
        : AccountUser.fromJson(json['confirmedByUser'] as Map<String, dynamic>),
    createdBy: json['created_by'] as String,
    createdByUser: json['createdByUser'] == null
        ? null
        : AccountUser.fromJson(json['createdByUser'] as Map<String, dynamic>),
    description: json['description'] as String?,
    labelIds:
        (json['label_ids'] as List<dynamic>?)?.map((e) => e as String).toList(),
    note: json['note'] as String?,
    refId: json['ref_id'] as String,
    refCode: json['ref_code'] as String?,
    refTypeDisplay: json['ref_type_display'] as String?,
    refType: _$enumDecodeNullable(_$TicketRefTypeEnumMap, json['ref_type']),
    sourceDisplay: json['source_display'] as String?,
    source: _$enumDecodeNullable(_$TicketSourceEnumMap, json['source']),
    stateDisplay: json['stateDisplay'] as String?,
    state: _$enumDecodeNullable(_$TicketStateEnumMap, json['state']),
    status: json['status'] as String?,
    updatedBy: json['updated_by'] as String?,
    contact: json['contact'] == null
        ? null
        : Contact.fromJson(json['contact'] as Map<String, dynamic>),
    type: _$enumDecodeNullable(_$TicketTypeEnumMap, json['type']),
    label: json['label'] == null
        ? null
        : TicketLabel.fromJson(json['label'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$TicketToJson(Ticket instance) => <String, dynamic>{
      'created_at': instance.createdAt?.toIso8601String(),
      'code': instance.code,
      'title': instance.title,
      'id': instance.id,
      'state': _$TicketStateEnumMap[instance.state],
      'stateDisplay': instance.stateDisplay,
      'updated_at': instance.updatedAt,
      'description': instance.description,
      'account_id': instance.accountId,
      'shop': instance.shop,
      'assigned_user_ids': instance.assignedUserIds,
      'assignedUsers': instance.assignedUsers,
      'closed_at': instance.closedAt?.toIso8601String(),
      'closed_by': instance.closedBy,
      'closedByUser': instance.closedByUser,
      'confirmed_at': instance.confirmedAt?.toIso8601String(),
      'confirmed_by': instance.confirmedBy,
      'confirmedByUser': instance.confirmedByUser,
      'created_by': instance.createdBy,
      'createdByUser': instance.createdByUser,
      'label_ids': instance.labelIds,
      'note': instance.note,
      'ref_id': instance.refId,
      'ref_code': instance.refCode,
      'ref_type': _$TicketRefTypeEnumMap[instance.refType],
      'ref_type_display': instance.refTypeDisplay,
      'source': _$TicketSourceEnumMap[instance.source],
      'source_display': instance.sourceDisplay,
      'status': instance.status,
      'updated_by': instance.updatedBy,
      'contact': instance.contact,
      'type': _$TicketTypeEnumMap[instance.type],
      'label': instance.label,
    };

K _$enumDecode<K, V>(
  Map<K, V> enumValues,
  Object? source, {
  K? unknownValue,
}) {
  if (source == null) {
    throw ArgumentError(
      'A value must be provided. Supported values: '
      '${enumValues.values.join(', ')}',
    );
  }

  return enumValues.entries.singleWhere(
    (e) => e.value == source,
    orElse: () {
      if (unknownValue == null) {
        throw ArgumentError(
          '`$source` is not one of the supported values: '
          '${enumValues.values.join(', ')}',
        );
      }
      return MapEntry(unknownValue, enumValues.values.first);
    },
  ).key;
}

K? _$enumDecodeNullable<K, V>(
  Map<K, V> enumValues,
  dynamic source, {
  K? unknownValue,
}) {
  if (source == null) {
    return null;
  }
  return _$enumDecode<K, V>(enumValues, source, unknownValue: unknownValue);
}

const _$TicketRefTypeEnumMap = {
  TicketRefType.order_trading: 'order_trading',
  TicketRefType.ffm: 'ffm',
  TicketRefType.money_transaction: 'money_transaction',
  TicketRefType.other: 'other',
  TicketRefType.contact: 'contact',
};

const _$TicketSourceEnumMap = {
  TicketSource.pos_web: 'pos_web',
  TicketSource.pos_app: 'pos_app',
  TicketSource.shipment_app: 'shipment_app',
  TicketSource.admin: 'admin',
  TicketSource.system: 'system',
  TicketSource.webphone: 'webphone',
  TicketSource.telecom_app_call: 'telecom_app_call',
  TicketSource.telecom_web_call: 'telecom_web_call',
  TicketSource.telecom_ext_call: 'telecom_ext_call',
  TicketSource.shipment_web: 'shipment_web',
};

const _$TicketStateEnumMap = {
  TicketState.unknown: 'unknown',
  TicketState.received: 'received',
  TicketState.processing: 'processing',
  TicketState.success: 'success',
  TicketState.fail: 'fail',
  TicketState.ignore: 'ignore',
  TicketState.cancel: 'cancel',
  TicketState.open: 'open',
};

const _$TicketTypeEnumMap = {
  TicketType.system: 'system',
  TicketType.internal: 'internal',
  TicketType.unknow: 'unknow',
};

TicketLabel _$TicketLabelFromJson(Map<String, dynamic> json) {
  return TicketLabel(
    code: json['code'] as String?,
    color: json['color'] as String?,
    id: json['id'] as String?,
    name: json['name'] as String?,
    parentId: json['parent_id'] as String?,
    shopId: json['shop_id'] as String?,
    type: _$enumDecodeNullable(_$TicketTypeEnumMap, json['type']),
  )..children = (json['children'] as List<dynamic>?)
      ?.map((e) => TicketLabel.fromJson(e as Map<String, dynamic>))
      .toList();
}

Map<String, dynamic> _$TicketLabelToJson(TicketLabel instance) =>
    <String, dynamic>{
      'children': instance.children,
      'code': instance.code,
      'color': instance.color,
      'id': instance.id,
      'name': instance.name,
      'parent_id': instance.parentId,
      'shop_id': instance.shopId,
      'type': _$TicketTypeEnumMap[instance.type],
    };

TicketComment _$TicketCommentFromJson(Map<String, dynamic> json) {
  return TicketComment(
    accountId: json['account_id'] as String?,
    createdAt: json['created_at'] == null
        ? null
        : DateTime.parse(json['created_at'] as String),
    createdBy: json['created_by'] as String?,
    deletedAt: json['deleted_at'] as String?,
    deletedBy: json['deleted_by'] as String?,
    id: json['id'] as String?,
    imageUrl: json['image_url'] as String?,
    from: json['from'],
    imageUrls: (json['image_urls'] as List<dynamic>?)
        ?.map((e) => e as String)
        .toList(),
    message: json['message'] as String?,
    parentId: json['parent_id'] as String?,
    ticketId: json['ticket_id'] as String?,
    updatedAt: json['updated_at'] as String?,
    createdByUser: json['createdByUser'] == null
        ? null
        : AccountUser.fromJson(json['createdByUser'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$TicketCommentToJson(TicketComment instance) =>
    <String, dynamic>{
      'account_id': instance.accountId,
      'created_at': instance.createdAt?.toIso8601String(),
      'created_by': instance.createdBy,
      'deleted_at': instance.deletedAt,
      'deleted_by': instance.deletedBy,
      'from': instance.from,
      'id': instance.id,
      'image_url': instance.imageUrl,
      'image_urls': instance.imageUrls,
      'message': instance.message,
      'parent_id': instance.parentId,
      'ticket_id': instance.ticketId,
      'updated_at': instance.updatedAt,
      'createdByUser': instance.createdByUser,
    };
