import 'package:etelecom/models/account-user/AccountUser.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:etelecom/models/contact/Contact.dart';
import 'package:etelecom/models/shop/Shop.dart';

part 'Ticket.g.dart';

@JsonSerializable()
class Ticket {
  @JsonKey(name: 'created_at')
  DateTime? createdAt;

  String? code;
  String? title;
  String id;
  TicketState? state;
  String? stateDisplay;

  @JsonKey(name: 'updated_at')
  String? updatedAt;

  String? description;
  @JsonKey(name: 'account_id')
  String? accountId;
  Shop? shop;

  @JsonKey(name: 'assigned_user_ids')
  List<String>? assignedUserIds;
  List<AccountUser>? assignedUsers;

  @JsonKey(name: 'closed_at')
  DateTime? closedAt;
  @JsonKey(name: 'closed_by')
  String closedBy;
  AccountUser? closedByUser;

  @JsonKey(name: 'confirmed_at')
  DateTime? confirmedAt;
  @JsonKey(name: 'confirmed_by')
  String confirmedBy;
  AccountUser? confirmedByUser;

  @JsonKey(name: 'created_by')
  String createdBy;
  AccountUser? createdByUser;

  @JsonKey(name: 'label_ids')
  List<String>? labelIds;
  String? note;
  @JsonKey(name: 'ref_id')
  String refId;
  @JsonKey(name: 'ref_code')
  String? refCode;
  @JsonKey(name: 'ref_type')
  TicketRefType? refType;
  @JsonKey(name: 'ref_type_display')
  String? refTypeDisplay;
  TicketSource? source;
  @JsonKey(name: 'source_display')
  String? sourceDisplay;

  String? status;
  @JsonKey(name: 'updated_by')
  String? updatedBy;

  Contact? contact;
  TicketType? type;
  TicketLabel? label;

  Ticket({
    this.createdAt,
    this.code,
    this.id = "",
    this.updatedAt,
    this.title,
    this.accountId,
    this.shop,
    this.assignedUserIds,
    this.assignedUsers,
    this.closedAt,
    this.closedBy = "",
    this.closedByUser,
    this.confirmedAt,
    this.confirmedBy = "",
    this.confirmedByUser,
    this.createdBy = "",
    this.createdByUser,
    this.description,
    this.labelIds,
    this.note,
    this.refId = "",
    this.refCode,
    this.refTypeDisplay,
    this.refType,
    this.sourceDisplay,
    this.source,
    this.stateDisplay,
    this.state,
    this.status,
    this.updatedBy,
    this.contact,
    this.type,
    this.label
  });

  factory Ticket.fromJson(Map<String, dynamic> json) {
    if (json["ref_type"].isEmpty) {
      json["ref_type"] = "other";
    }
    if (json["state"] == "new") {
      json["state"] = "open";
    }

    if (json["state"].isEmpty) {
      json["state"] = "unknown";
    }

    return _$TicketFromJson(json);
  }

  Map<String, dynamic> toJson() => _$TicketToJson(this);
}

@JsonSerializable()
class TicketLabel {
  List<TicketLabel>? children;
  String? code;
  String? color;
  String? id;
  String? name;
  @JsonKey(name: 'parent_id')
  String? parentId;
  @JsonKey(name: 'shop_id')
  String? shopId;
  TicketType? type;

  TicketLabel({
    this.code,
    this.color,
    this.id,
    this.name,
    this.parentId,
    this.shopId,
    this.type
  });

  factory TicketLabel.fromJson(Map<String, dynamic> json) =>
      _$TicketLabelFromJson(json);

  Map<String, dynamic> toJson() => _$TicketLabelToJson(this);
}

@JsonSerializable()
class TicketComment {
  @JsonKey(name: 'account_id')
  String? accountId;
  @JsonKey(name: 'created_at')
  DateTime? createdAt;
  @JsonKey(name: 'created_by')
  String? createdBy;
  @JsonKey(name: 'deleted_at')
  String? deletedAt;
  @JsonKey(name: 'deleted_by')
  String? deletedBy;
  Object? from;
  String? id;
  @JsonKey(name: 'image_url')
  String? imageUrl;
  @JsonKey(name: 'image_urls')
  List<String>? imageUrls;
  String? message;
  @JsonKey(name: 'parent_id')
  String? parentId;
  @JsonKey(name: 'ticket_id')
  String? ticketId;
  @JsonKey(name: 'updated_at')
  String? updatedAt;
  AccountUser? createdByUser;

  TicketComment({
    this.accountId,
    this.createdAt,
    this.createdBy,
    this.deletedAt,
    this.deletedBy,
    this.id,
    this.imageUrl,
    this.from,
    this.imageUrls,
    this.message,
    this.parentId,
    this.ticketId,
    this.updatedAt,
    this.createdByUser});

  factory TicketComment.fromJson(Map<String, dynamic> json) {
    return _$TicketCommentFromJson(json);
  }

  Map<String, dynamic> toJson() => _$TicketCommentToJson(this);
}

enum TicketSource {
  pos_web,
  pos_app,
  shipment_app,
  admin,
  system,
  webphone,
  telecom_app_call,
  telecom_web_call,
  telecom_ext_call,
  shipment_web
}
enum TicketType { system, internal, unknow }

enum TicketRefType { order_trading, ffm, money_transaction, other, contact }

enum TicketState {
  unknown,
  received,
  processing,
  success,
  fail,
  ignore,
  cancel,
  open
}

const TICKET_STATE = {
  TicketState.unknown: "Không xác định",
  TicketState.received: "Đã tiếp nhận",
  TicketState.processing: "Đang xử lý",
  TicketState.success: "Xử lý thành công",
  TicketState.fail: "Xử lý thất bại",
  TicketState.ignore: "Từ chối xử lý",
  TicketState.open: "Mới"
};

const TICKET_REF_TYPE = {
  "order_trading": "Đơn hàng trading",
  "ffm": "Đơn giao hàng",
  "money_transaction": "Phiên đối soát",
  "other": "Đối tượng khác",
  "contact": "Danh bạ"
};

const TICKET_SOURCE = {
  "pos_web": "eTop POS Web",
  "pos_app": "eTop POS App",
  "shipment_app": "Topship App",
  "admin": "Admin",
  "system": "Tự động từ hệ thống",
  "webphone": "Web phone extentions",
  "telecom_app_call": "App call",
  "telecom_web_call": "Web Call",
  "telecom_ext_call": "Extenstion Call",
  "shipment_web": "Topship Web"
};
