import 'package:etelecom/models/contact/Contact.dart';
import 'package:etelecom/models/extension/Extension.dart';
import 'package:etelecom/models/account-user/AccountUser.dart';
import 'package:etelecom/theme/theme.dart';
import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';

part 'CallLog.g.dart';

enum CallState {
  unknown,
  answered,
  not_answered
}

enum CallStatus {
  Z, P, S, N, NS
}

@JsonSerializable()
class CallLog {
  @JsonKey(name: 'created_at')
  DateTime? createdAt;

  String id;

  @JsonKey(name: 'audio_urls')
  List<String>? audioUrls;

  @JsonKey(name: 'shop_id')
  String? shopId;

  @JsonKey(name: 'updated_at')
  DateTime? updatedAt;

  @JsonKey(name: 'account_id')
  String? accountId;

  @JsonKey(name: 'call_state')
  CallState? callState;

  @JsonKey(name: 'call_status')
  CallStatus? callStatus;

  String callee;
  String caller;

  @JsonKey(name: 'contact_id')
  String? contactId;

  Contact? contact;

  String? direction;
  int? duration;

  @JsonKey(name: 'duration_postage')
  int? durationPostage;

  @JsonKey(name: 'ended_at')
  DateTime? endedAt;

  @JsonKey(name: 'extension_id')
  String? extensionId;

  @JsonKey(name: 'external_direction')
  String? externalDirection;

  @JsonKey(name: 'external_id')
  String? externalId;

  @JsonKey(name: 'hotline_id')
  String? hotlineId;
  int? postage;

  @JsonKey(name: 'started_at')
  DateTime? startedAt;

  @JsonKey(name: 'external_call_status')
  String? externalCallStatus;

  @JsonKey(name: 'external_session_id')
  String? externalSessionId;

  String? phone;

  String? callerFullName;
  String? calleeFullName;

  String? callStateDisplay;
  dynamic callStateIcon; // because JSON library does not support type IconData
  dynamic callStateColor;

  CallLog({
    this.id = "",
    this.caller = "",
    this.callee = "",
    this.phone
  });

  factory CallLog.fromJson(Map<String, dynamic> json) => _$CallLogFromJson(json);

  Map<String, dynamic> toJson() => _$CallLogToJson(this);

  static CallLog mapCallLog({
    required CallLog callLog,
    required Extension currentExtension,
    required List<AccountUser> accUsers
  }) {

    switch(callLog.direction) {
      case "out":
        callLog.callStateDisplay = 'Gọi ra';
        callLog.callStateIcon = Icons.call_made;
        callLog.callStateColor = MainTheme.gray555;

        callLog.phone = callLog.callee;
        break;
      case "in":
        if (callLog.callState == CallState.answered) {
          callLog.callStateDisplay = 'Gọi vào';
          callLog.callStateIcon = Icons.call_received;
          callLog.callStateColor = MainTheme.gray555;
        } else {
          callLog.callStateDisplay = 'Gọi nhỡ';
          callLog.callStateIcon = Icons.call_missed;
          callLog.callStateColor = Colors.red;
        }

        callLog.phone = callLog.caller;
        break;
      case "ext_out":
        callLog.callStateDisplay = 'Gọi ra';
        callLog.callStateIcon = Icons.call_made;
        callLog.callStateColor = MainTheme.gray555;

        callLog.phone = callLog.callee;

        callLog.callerFullName = accUsers.firstWhere(
          (element) => element.extensionNumber == currentExtension.extensionNumber,
          orElse: () => AccountUser()
        ).fullName;

        callLog.calleeFullName = accUsers.firstWhere(
          (element) => element.extensionNumber == callLog.callee,
          orElse: () => AccountUser()
        ).fullName;

        break;
      case "ext_in":
        if (callLog.callState == CallState.answered) {
          callLog.callStateDisplay = 'Gọi vào';
          callLog.callStateIcon = Icons.call_received;
          callLog.callStateColor = MainTheme.gray555;
        } else {
          callLog.callStateDisplay = 'Gọi nhỡ';
          callLog.callStateIcon = Icons.call_missed;
          callLog.callStateColor = Colors.red;
        }

        callLog.phone = callLog.caller;

        callLog.callerFullName = accUsers.firstWhere(
          (element) => element.extensionNumber == callLog.caller,
          orElse: () => AccountUser()
        ).fullName;

        callLog.calleeFullName = accUsers.firstWhere(
          (element) => element.extensionNumber == currentExtension.extensionNumber,
          orElse: () => AccountUser()
        ).fullName;

        break;

      default:
        callLog.callStateDisplay = 'Không xác định';
        callLog.phone = currentExtension.extensionNumber != callLog.callee ? callLog.callee : callLog.caller;
    }

    return callLog;
  }

  static bool isInternalCall(CallLog callLog, List<Extension> extensions) {
    final _isInternalCallee = extensions.indexWhere((ext) => ext.extensionNumber == callLog.callee);
    final _isInternalCaller = extensions.indexWhere((ext) => ext.extensionNumber == callLog.caller);
    return _isInternalCallee >= 0 && _isInternalCaller >= 0;
  }
}

@JsonSerializable()
class CreateCallLogRequest {
  @JsonKey(name: 'call_state')
  CallState? callState;

  String? callee;
  String? caller;

  @JsonKey(name: 'contact_id')
  String? contactId;
  String? direction;

  @JsonKey(name: 'extension_id')
  String? extensionId;

  @JsonKey(name: 'external_session_id')
  String? externalSessionId;

  @JsonKey(name: 'started_at')
  DateTime? startedAt;

  @JsonKey(name: 'ended_at')
  DateTime? endedAt;

  CreateCallLogRequest({
    this.callState,
    this.callee,
    this.caller,
    this.contactId,
    this.direction,
    this.extensionId,
    this.externalSessionId,
    this.startedAt,
    this.endedAt,
  });

  factory CreateCallLogRequest.fromJson(Map<String, dynamic> json) => _$CreateCallLogRequestFromJson(json);

  Map<String, dynamic> toJson() => _$CreateCallLogRequestToJson(this);

}
