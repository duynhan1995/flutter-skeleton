// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'CallLog.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CallLog _$CallLogFromJson(Map<String, dynamic> json) {
  return CallLog(
    id: json['id'] as String,
    caller: json['caller'] as String,
    callee: json['callee'] as String,
    phone: json['phone'] as String?,
  )
    ..createdAt = json['created_at'] == null
        ? null
        : DateTime.parse(json['created_at'] as String)
    ..audioUrls =
        (json['audio_urls'] as List<dynamic>?)?.map((e) => e as String).toList()
    ..shopId = json['shop_id'] as String?
    ..updatedAt = json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String)
    ..accountId = json['account_id'] as String?
    ..callState = _$enumDecodeNullable(_$CallStateEnumMap, json['call_state'])
    ..callStatus =
        _$enumDecodeNullable(_$CallStatusEnumMap, json['call_status'])
    ..contactId = json['contact_id'] as String?
    ..contact = json['contact'] == null
        ? null
        : Contact.fromJson(json['contact'] as Map<String, dynamic>)
    ..direction = json['direction'] as String?
    ..duration = json['duration'] as int?
    ..durationPostage = json['duration_postage'] as int?
    ..endedAt = json['ended_at'] == null
        ? null
        : DateTime.parse(json['ended_at'] as String)
    ..extensionId = json['extension_id'] as String?
    ..externalDirection = json['external_direction'] as String?
    ..externalId = json['external_id'] as String?
    ..hotlineId = json['hotline_id'] as String?
    ..postage = json['postage'] as int?
    ..startedAt = json['started_at'] == null
        ? null
        : DateTime.parse(json['started_at'] as String)
    ..externalCallStatus = json['external_call_status'] as String?
    ..externalSessionId = json['external_session_id'] as String?
    ..callerFullName = json['callerFullName'] as String?
    ..calleeFullName = json['calleeFullName'] as String?
    ..callStateDisplay = json['callStateDisplay'] as String?
    ..callStateIcon = json['callStateIcon']
    ..callStateColor = json['callStateColor'];
}

Map<String, dynamic> _$CallLogToJson(CallLog instance) => <String, dynamic>{
      'created_at': instance.createdAt?.toIso8601String(),
      'id': instance.id,
      'audio_urls': instance.audioUrls,
      'shop_id': instance.shopId,
      'updated_at': instance.updatedAt?.toIso8601String(),
      'account_id': instance.accountId,
      'call_state': _$CallStateEnumMap[instance.callState],
      'call_status': _$CallStatusEnumMap[instance.callStatus],
      'callee': instance.callee,
      'caller': instance.caller,
      'contact_id': instance.contactId,
      'contact': instance.contact,
      'direction': instance.direction,
      'duration': instance.duration,
      'duration_postage': instance.durationPostage,
      'ended_at': instance.endedAt?.toIso8601String(),
      'extension_id': instance.extensionId,
      'external_direction': instance.externalDirection,
      'external_id': instance.externalId,
      'hotline_id': instance.hotlineId,
      'postage': instance.postage,
      'started_at': instance.startedAt?.toIso8601String(),
      'external_call_status': instance.externalCallStatus,
      'external_session_id': instance.externalSessionId,
      'phone': instance.phone,
      'callerFullName': instance.callerFullName,
      'calleeFullName': instance.calleeFullName,
      'callStateDisplay': instance.callStateDisplay,
      'callStateIcon': instance.callStateIcon,
      'callStateColor': instance.callStateColor,
    };

K _$enumDecode<K, V>(
  Map<K, V> enumValues,
  Object? source, {
  K? unknownValue,
}) {
  if (source == null) {
    throw ArgumentError(
      'A value must be provided. Supported values: '
      '${enumValues.values.join(', ')}',
    );
  }

  return enumValues.entries.singleWhere(
    (e) => e.value == source,
    orElse: () {
      if (unknownValue == null) {
        throw ArgumentError(
          '`$source` is not one of the supported values: '
          '${enumValues.values.join(', ')}',
        );
      }
      return MapEntry(unknownValue, enumValues.values.first);
    },
  ).key;
}

K? _$enumDecodeNullable<K, V>(
  Map<K, V> enumValues,
  dynamic source, {
  K? unknownValue,
}) {
  if (source == null) {
    return null;
  }
  return _$enumDecode<K, V>(enumValues, source, unknownValue: unknownValue);
}

const _$CallStateEnumMap = {
  CallState.unknown: 'unknown',
  CallState.answered: 'answered',
  CallState.not_answered: 'not_answered',
};

const _$CallStatusEnumMap = {
  CallStatus.Z: 'Z',
  CallStatus.P: 'P',
  CallStatus.S: 'S',
  CallStatus.N: 'N',
  CallStatus.NS: 'NS',
};

CreateCallLogRequest _$CreateCallLogRequestFromJson(Map<String, dynamic> json) {
  return CreateCallLogRequest(
    callState: _$enumDecodeNullable(_$CallStateEnumMap, json['call_state']),
    callee: json['callee'] as String?,
    caller: json['caller'] as String?,
    contactId: json['contact_id'] as String?,
    direction: json['direction'] as String?,
    extensionId: json['extension_id'] as String?,
    externalSessionId: json['external_session_id'] as String?,
    startedAt: json['started_at'] == null
        ? null
        : DateTime.parse(json['started_at'] as String),
    endedAt: json['ended_at'] == null
        ? null
        : DateTime.parse(json['ended_at'] as String),
  );
}

Map<String, dynamic> _$CreateCallLogRequestToJson(
        CreateCallLogRequest instance) =>
    <String, dynamic>{
      'call_state': _$CallStateEnumMap[instance.callState],
      'callee': instance.callee,
      'caller': instance.caller,
      'contact_id': instance.contactId,
      'direction': instance.direction,
      'extension_id': instance.extensionId,
      'external_session_id': instance.externalSessionId,
      'started_at': instance.startedAt?.toIso8601String(),
      'ended_at': instance.endedAt?.toIso8601String(),
    };
