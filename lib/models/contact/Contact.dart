import 'package:json_annotation/json_annotation.dart';

part 'Contact.g.dart';
@JsonSerializable()
class Contact {
  @JsonKey(name: 'created_at')
  String? createdAt;

  @JsonKey(name: 'full_name')
  String fullName;

  String id;
  String phone;

  @JsonKey(name: 'shop_id')
  String? shopId;

  @JsonKey(name: 'updated_at')
  String? updatedAt;

  Contact({
    this.createdAt,
    this.fullName = "",
    this.id = "",
    this.phone = "",
    this.shopId,
    this.updatedAt,
  });

  factory Contact.fromJson(Map<String, dynamic> json) => _$ContactFromJson(json);

  Map<String, dynamic> toJson() => _$ContactToJson(this);
}
