// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Contact.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Contact _$ContactFromJson(Map<String, dynamic> json) {
  return Contact(
    createdAt: json['created_at'] as String?,
    fullName: json['full_name'] as String,
    id: json['id'] as String,
    phone: json['phone'] as String,
    shopId: json['shop_id'] as String?,
    updatedAt: json['updated_at'] as String?,
  );
}

Map<String, dynamic> _$ContactToJson(Contact instance) => <String, dynamic>{
      'created_at': instance.createdAt,
      'full_name': instance.fullName,
      'id': instance.id,
      'phone': instance.phone,
      'shop_id': instance.shopId,
      'updated_at': instance.updatedAt,
    };
