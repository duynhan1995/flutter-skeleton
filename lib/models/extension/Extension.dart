import 'package:json_annotation/json_annotation.dart';

part 'Extension.g.dart';

@JsonSerializable()
class Extension {

  @JsonKey(name: "account_id")
  String? accountId;

  @JsonKey(name: "created_at")
  DateTime? createdAt;

  @JsonKey(name: "expires_at")
  DateTime? expiresAt;

  @JsonKey(name: "extension_number")
  String? extensionNumber;

  @JsonKey(name: "extension_password")
  String? extensionPassword;

  @JsonKey(name: "hotline_id")
  String? hotlineId;
  String? id;

  @JsonKey(name: "subscription_id")
  String? subscriptionId;

  @JsonKey(name: "tenant_domain")
  String? tenantDomain;

  @JsonKey(name: "tenant_id")
  String? tenantId;

  @JsonKey(name: "updated_at")
  DateTime? updatedAt;

  @JsonKey(name: "user_id")
  String? userId;

  Extension({
    this.id
  });

  factory Extension.fromJson(Map<String, dynamic> json) => _$ExtensionFromJson(json);

  Map<String, dynamic> toJson() => _$ExtensionToJson(this);

}
