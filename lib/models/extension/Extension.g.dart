// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Extension.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Extension _$ExtensionFromJson(Map<String, dynamic> json) {
  return Extension(
    id: json['id'] as String?,
  )
    ..accountId = json['account_id'] as String?
    ..createdAt = json['created_at'] == null
        ? null
        : DateTime.parse(json['created_at'] as String)
    ..expiresAt = json['expires_at'] == null
        ? null
        : DateTime.parse(json['expires_at'] as String)
    ..extensionNumber = json['extension_number'] as String?
    ..extensionPassword = json['extension_password'] as String?
    ..hotlineId = json['hotline_id'] as String?
    ..subscriptionId = json['subscription_id'] as String?
    ..tenantDomain = json['tenant_domain'] as String?
    ..tenantId = json['tenant_id'] as String?
    ..updatedAt = json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String)
    ..userId = json['user_id'] as String?;
}

Map<String, dynamic> _$ExtensionToJson(Extension instance) => <String, dynamic>{
      'account_id': instance.accountId,
      'created_at': instance.createdAt?.toIso8601String(),
      'expires_at': instance.expiresAt?.toIso8601String(),
      'extension_number': instance.extensionNumber,
      'extension_password': instance.extensionPassword,
      'hotline_id': instance.hotlineId,
      'id': instance.id,
      'subscription_id': instance.subscriptionId,
      'tenant_domain': instance.tenantDomain,
      'tenant_id': instance.tenantId,
      'updated_at': instance.updatedAt?.toIso8601String(),
      'user_id': instance.userId,
    };
