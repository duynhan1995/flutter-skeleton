import 'package:json_annotation/json_annotation.dart';

part 'Account.g.dart';

@JsonSerializable()
class Account {
  String? access_token;
  int? expires_in;
  String? id;
  String? image_url;
  String? name;
  String? type;
  String? url_slug;

  Account({
    this.access_token,
    this.expires_in,
    this.id,
    this.image_url,
    this.name,
    this.type,
    this.url_slug
  });

  factory Account.fromJson(Map<String, dynamic>? json) {
    if (json == null) {
      return Account(id: null);
    }

    return Account(
      access_token: json["access_token"],
      expires_in: json["expires_in"],
      id: json["id"],
      image_url: json["image_url"],
      name: json["name"],
      type: json["type"],
      url_slug: json["url_slug"],
    );
  }

  Map<String, dynamic> toJson() => {
    "access_token": access_token,
    "expires_in": expires_in,
    "id": id,
    "image_url": image_url,
    "name": name,
    "type": type,
    "url_slug": url_slug,
  };
}

@JsonSerializable()
class Permission {
  List<String>? roles;
  List<String>? permissions;

  Permission({this.roles, this.permissions});

  factory Permission.fromJson(Map<String, dynamic> json) =>
      _$PermissionFromJson(json);

  Map<String, dynamic> toJson() => _$PermissionToJson(this);
}
