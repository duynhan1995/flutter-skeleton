// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Hotlines.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Hotline _$HotlineFromJson(Map<String, dynamic> json) {
  return Hotline(
    id: json['id'] as String?,
    connectionId: json['connection_id'] as String?,
    createdAt: json['created_at'] == null
        ? null
        : DateTime.parse(json['created_at'] as String),
    description: json['description'] as String?,
    hotline: json['hotline'] as String?,
    isFreeCharge: json['is_free_charge'] as bool?,
    name: json['name'] as String?,
    network: json['network'] as String?,
    ownerId: json['owner_id'] as String?,
    status: json['status'] as String?,
    updatedAt: json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String),
  );
}

Map<String, dynamic> _$HotlineToJson(Hotline instance) => <String, dynamic>{
      'connection_id': instance.connectionId,
      'created_at': instance.createdAt?.toIso8601String(),
      'description': instance.description,
      'hotline': instance.hotline,
      'name': instance.name,
      'network': instance.network,
      'owner_id': instance.ownerId,
      'id': instance.id,
      'status': instance.status,
      'updated_at': instance.updatedAt?.toIso8601String(),
      'is_free_charge': instance.isFreeCharge,
    };
