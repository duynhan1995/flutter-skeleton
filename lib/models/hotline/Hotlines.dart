import 'package:json_annotation/json_annotation.dart';

part 'Hotlines.g.dart';

@JsonSerializable()
class Hotline {
  @JsonKey(name: "connection_id")
  String? connectionId;

  @JsonKey(name: "created_at")
  DateTime? createdAt;

  String? description;
  String? hotline;
  String? name;
  String? network;

  @JsonKey(name: "owner_id")
  String? ownerId;

  String? id;
  String? status;

  @JsonKey(name: "updated_at")
  DateTime? updatedAt;

  @JsonKey(name: "is_free_charge")
  bool? isFreeCharge;

  Hotline({
    this.id,
    this.connectionId,
    this.createdAt,
    this.description,
    this.hotline,
    this.isFreeCharge = false,
    this.name,
    this.network,
    this.ownerId,
    this.status,
    this.updatedAt
  });

  factory Hotline.fromJson(Map<String, dynamic> json) {
    if (json['is_free_charge'] == null) {
      json['is_free_charge'] = false;
    }
    return _$HotlineFromJson(json);
  } 

  Map<String, dynamic> toJson() => _$HotlineToJson(this);

}