import 'package:etelecom/models/User.dart';
import 'package:json_annotation/json_annotation.dart';

part 'Shop.g.dart';

@JsonSerializable()
class Shop {
  dynamic? address;

  @JsonKey(name: 'bank_account')
  dynamic? bankAccount;

  @JsonKey(name: 'company_info')
  dynamic? companyInfo;

  @JsonKey(name: 'auto_create_ffm')
  bool? autoCreateFfm;

  @JsonKey(name: 'block_reason')
  String? blockReason;
  String? code;

  @JsonKey(name: 'created_at')
  DateTime? createdAt;

  String? email;
  String? id;

  @JsonKey(name: 'image_url')
  String? imageUrl;

  @JsonKey(name: 'inventory_overstock')
  bool? inventoryOverstock;

  @JsonKey(name: 'is_blocked')
  bool? isBlocked;

  @JsonKey(name: 'is_prior_money_transaction')
  bool? isPriorMoneyTransaction;

  @JsonKey(name: 'is_test')
  bool? isTest;

  @JsonKey(name: 'money_transaction_count')
  int? moneyTransactionCount;

  @JsonKey(name: 'money_transaction_rrule')
  String? moneyTransactionRrule;

  String? name;

  @JsonKey(name: 'owner_id')
  String? ownerId;

  String? phone;

  @JsonKey(name: 'ship_from_address_id')
  String? shipFromAddressId;

  @JsonKey(name: 'ship_to_address_id')
  String? shipToAddressId;

  @JsonKey(name: 'shipping_service_select_strategy')
  List<dynamic>? shippingServiceSelectStrategy;

  String? status;

  @JsonKey(name: 'survey_info')
  List<dynamic>? surveyInfo;

  @JsonKey(name: 'try_on')
  String? tryOn;

  @JsonKey(name: 'updated_at')
  String? updatedAt;

  User? user;

  @JsonKey(name: 'website_url')
  String? websiteUrl;

  String? token;

  Shop();

  factory Shop.fromJson(Map<String, dynamic> json) => _$ShopFromJson(json);

  Map<String, dynamic> toJson() => _$ShopToJson(this);

}
