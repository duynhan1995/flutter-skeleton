// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Shop.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Shop _$ShopFromJson(Map<String, dynamic> json) {
  return Shop()
    ..address = json['address']
    ..bankAccount = json['bank_account']
    ..companyInfo = json['company_info']
    ..autoCreateFfm = json['auto_create_ffm'] as bool?
    ..blockReason = json['block_reason'] as String?
    ..code = json['code'] as String?
    ..createdAt = json['created_at'] == null
        ? null
        : DateTime.parse(json['created_at'] as String)
    ..email = json['email'] as String?
    ..id = json['id'] as String?
    ..imageUrl = json['image_url'] as String?
    ..inventoryOverstock = json['inventory_overstock'] as bool?
    ..isBlocked = json['is_blocked'] as bool?
    ..isPriorMoneyTransaction = json['is_prior_money_transaction'] as bool?
    ..isTest = json['is_test'] as bool?
    ..moneyTransactionCount = json['money_transaction_count'] as int?
    ..moneyTransactionRrule = json['money_transaction_rrule'] as String?
    ..name = json['name'] as String?
    ..ownerId = json['owner_id'] as String?
    ..phone = json['phone'] as String?
    ..shipFromAddressId = json['ship_from_address_id'] as String?
    ..shipToAddressId = json['ship_to_address_id'] as String?
    ..shippingServiceSelectStrategy =
        json['shipping_service_select_strategy'] as List<dynamic>?
    ..status = json['status'] as String?
    ..surveyInfo = json['survey_info'] as List<dynamic>?
    ..tryOn = json['try_on'] as String?
    ..updatedAt = json['updated_at'] as String?
    ..user = json['user'] == null
        ? null
        : User.fromJson(json['user'] as Map<String, dynamic>?)
    ..websiteUrl = json['website_url'] as String?
    ..token = json['token'] as String?;
}

Map<String, dynamic> _$ShopToJson(Shop instance) => <String, dynamic>{
      'address': instance.address,
      'bank_account': instance.bankAccount,
      'company_info': instance.companyInfo,
      'auto_create_ffm': instance.autoCreateFfm,
      'block_reason': instance.blockReason,
      'code': instance.code,
      'created_at': instance.createdAt?.toIso8601String(),
      'email': instance.email,
      'id': instance.id,
      'image_url': instance.imageUrl,
      'inventory_overstock': instance.inventoryOverstock,
      'is_blocked': instance.isBlocked,
      'is_prior_money_transaction': instance.isPriorMoneyTransaction,
      'is_test': instance.isTest,
      'money_transaction_count': instance.moneyTransactionCount,
      'money_transaction_rrule': instance.moneyTransactionRrule,
      'name': instance.name,
      'owner_id': instance.ownerId,
      'phone': instance.phone,
      'ship_from_address_id': instance.shipFromAddressId,
      'ship_to_address_id': instance.shipToAddressId,
      'shipping_service_select_strategy':
          instance.shippingServiceSelectStrategy,
      'status': instance.status,
      'survey_info': instance.surveyInfo,
      'try_on': instance.tryOn,
      'updated_at': instance.updatedAt,
      'user': instance.user,
      'website_url': instance.websiteUrl,
      'token': instance.token,
    };
