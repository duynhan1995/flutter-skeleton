import 'package:json_annotation/json_annotation.dart';

part 'AccountUser.g.dart';

@JsonSerializable()
class AccountUser {
  String? id;
  @JsonKey(name: 'account_id')
  String? accountId;
  @JsonKey(name: 'user_id')
  String? userId;
  bool? deleted;
  String? email;
  @JsonKey(name: 'full_name')
  String? fullName;
  List<String>? permissions;
  String? phone;
  String? position;
  List<String>? roles;
  @JsonKey(name: 'short_name')
  String? shortName;
  @JsonKey(name: 'user_name')
  String? userName;

  String? extensionNumber;

  AccountUser({
    this.id,
    this.accountId,
    this.userId,
    this.deleted,
    this.email,
    this.fullName,
    this.permissions,
    this.phone,
    this.position,
    this.roles,
    this.shortName,
    this.userName
  });

  get roleDisplay {
    if(roles != null) {
      if(roles!.contains("owner"))
        return "Chủ công ty";
      if(roles!.contains("m_admin"))
        return "Admin";
      if(roles!.contains('salesman')) {
        if(roles!.contains('telecom_customerservice_management') && roles!.contains('staff_management'))
          return 'Quản lý';
        if (roles!.contains('telecom_customerservice') && roles!.length == 2)
          return 'Nhân viên';
      }
    }
    return "Khác";
  }

  factory AccountUser.fromJson(Map<String, dynamic> json) {
    json['id'] = json['user_id'];
    return _$AccountUserFromJson(json);
  }

  Map<String, dynamic> toJson() => _$AccountUserToJson(this);
}
