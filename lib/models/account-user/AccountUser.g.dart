// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'AccountUser.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AccountUser _$AccountUserFromJson(Map<String, dynamic> json) {
  return AccountUser(
    id: json['id'] as String?,
    accountId: json['account_id'] as String?,
    userId: json['user_id'] as String?,
    deleted: json['deleted'] as bool?,
    email: json['email'] as String?,
    fullName: json['full_name'] as String?,
    permissions: (json['permissions'] as List<dynamic>?)
        ?.map((e) => e as String)
        .toList(),
    phone: json['phone'] as String?,
    position: json['position'] as String?,
    roles: (json['roles'] as List<dynamic>?)?.map((e) => e as String).toList(),
    shortName: json['short_name'] as String?,
    userName: json['user_name'] as String?,
  )..extensionNumber = json['extensionNumber'] as String?;
}

Map<String, dynamic> _$AccountUserToJson(AccountUser instance) =>
    <String, dynamic>{
      'id': instance.id,
      'account_id': instance.accountId,
      'user_id': instance.userId,
      'deleted': instance.deleted,
      'email': instance.email,
      'full_name': instance.fullName,
      'permissions': instance.permissions,
      'phone': instance.phone,
      'position': instance.position,
      'roles': instance.roles,
      'short_name': instance.shortName,
      'user_name': instance.userName,
      'extensionNumber': instance.extensionNumber,
    };
