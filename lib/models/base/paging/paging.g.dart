// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'paging.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Paging _$PagingFromJson(Map<String, dynamic> json) {
  return Paging(
    offset: json['offset'] as int?,
    limit: json['limit'] as int?,
  )..sort = json['sort'] as String?;
}

Map<String, dynamic> _$PagingToJson(Paging instance) => <String, dynamic>{
      'limit': instance.limit,
      'offset': instance.offset,
      'sort': instance.sort,
    };
