// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cursor-paging.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CursorPaging _$CursorPagingFromJson(Map<String, dynamic> json) {
  return CursorPaging(
    after: json['after'] as String,
    before: json['before'] as String,
    limit: json['limit'] as int,
    sort: json['sort'] as String?,
    next: json['next'] as String,
    prev: json['prev'] as String,
  );
}

Map<String, dynamic> _$CursorPagingToJson(CursorPaging instance) =>
    <String, dynamic>{
      'after': instance.after,
      'before': instance.before,
      'limit': instance.limit,
      'next': instance.next,
      'prev': instance.prev,
      'sort': instance.sort,
    };
