import 'package:etelecom/models/base/paging/paging.dart';
import 'package:json_annotation/json_annotation.dart';

part 'cursor-paging.g.dart';

@JsonSerializable()
class CursorPaging {
  String after;
  String before;
  int limit;
  String next;
  String prev;
  String? sort;

  CursorPaging({
    this.after = ".",
    this.before = "",
    this.limit = DEFAULT_LIMIT,
    this.sort,
    this.next = "",
    this.prev = ""
  });

  factory CursorPaging.fromJson(Map<String, dynamic> json) => _$CursorPagingFromJson(json);

  Map<String, dynamic> toJson() => _$CursorPagingToJson(this);
}
