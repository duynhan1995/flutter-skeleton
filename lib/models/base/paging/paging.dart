import 'package:json_annotation/json_annotation.dart';


part 'paging.g.dart';

const int DEFAULT_LIMIT = 20;

@JsonSerializable()
class Paging {
  int? limit;
  int? offset;
  String? sort;

  Paging({
    this.offset = 0,
    this.limit = DEFAULT_LIMIT
  });

  factory Paging.fromJson(Map<String, dynamic> json) => _$PagingFromJson(json);

  Map<String, dynamic> toJson() => _$PagingToJson(this);
}
