import 'package:flutter/material.dart';

// Make color palette here:
// https://material.io/design/color/the-color-system.html#tools-for-picking-colors

class MainTheme {
  static const Color gray999 = Color(0xFF999999);
  static const Color gray888 = Color(0xFF888888);
  static const Color gray666 = Color(0xFF666666);
  static const Color gray555 = Color(0xFF555555);
  static const Color gray444 = Color(0xFF444444);
  static const Color gray333 = Color(0xFF333333);
  static const Color gray222 = Color(0xFF222222);
  static const Color grayEEE = Color(0xFFEEEEEE);
  static const Color grayF5 = Color(0xFFF5F5F5);
  static const Color grayAAA = Color(0xFFAAAAAA);
  static const Color grayBBB = Color(0xFFBBBBBB);
  static const Color grayCCC = Color(0xFFCCCCCC);
  static const Color danger = Color(0xFFE74C3C);
  static const Color warning = Color(0xFFF39C12);

  static MaterialColor primaryColor = MaterialColor(
    int.parse(_primaryColor),
    <int, Color>{
      50: Color(0xFFebfae7),
      100: Color(0xFFe5f8eb),
      200: Color(0xFFc0edcd),
      300: Color(0xFF95e1ad),
      400: Color(0xFF63d68b),
      500: Color(int.parse(_primaryColor)),
      600: Color(0xFF00c256),
      700: Color(0xFF00b24c),
      800: Color(0xFF009f3f),
      900: Color(0xFF008e33),
    },
  );
  static const String _primaryColor = "0xFF2FCC70";

  static mapColorFromConfig(Map<String, dynamic>? config) {
    if (config != null) {
      primaryColor = MaterialColor(
        int.parse(config["default500"]),
        <int, Color> {
          50: Color(int.parse(config["50"])),
          100: Color(int.parse(config["100"])),
          200: Color(int.parse(config["200"])),
          300: Color(int.parse(config["300"])),
          400: Color(int.parse(config["400"])),
          500: Color(int.parse(config["default500"])),
          600: Color(int.parse(config["600"])),
          700: Color(int.parse(config["700"])),
          800: Color(int.parse(config["800"])),
          900: Color(int.parse(config["900"])),
        }
      );
    }
  }

  static const IconData asterisk = IconData(0xe1eb, fontFamily: 'MaterialIcons');

}
