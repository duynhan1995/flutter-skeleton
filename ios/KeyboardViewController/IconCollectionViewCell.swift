//
//  IconCollectionViewCell.swift
//  KeyboardViewController
//
//  Created by utnhim on 1/8/21.
//

import UIKit

class IconCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var title: UILabel!
    static let identifier = "IconCollectionViewCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    public func configure(with name:String){
        title.text = name
    }
    
    static func nib()->UINib{
        return UINib(nibName: "IconCollectionViewCell", bundle: nil)
    }
}
