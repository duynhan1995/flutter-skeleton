//
//  ConvertOneLastCharacter.swift
//  KeyboardViewController
//
//  Created by utnhim on 12/18/20.
//

import Foundation
import UIKit

class ConvertOneLastCharacter:UIInputViewController{
    static func convert(characterLastest:String, textInput: String, textDocumentProxy: UITextDocumentProxy)->String{
        var valueText = textInput;
        switch characterLastest{
        case "d":
            switch textInput {
            case "d":
                valueText = "đ";
                break;
            case "D":
                valueText = "đ"
            default:
                break;
            }
            break;
        case "đ":
            switch textInput {
            case "d":
                valueText = "dd";
                break;
            case "D":
                valueText = "dd";
                break;
            default:
                break;
            }
            break;
        case "a":
            switch textInput {
            case "s":
                valueText = "á";
                break;
            case "S":
                valueText = "á";
                break;
            case "f":
                valueText = "à";
                break;
            case "F":
                valueText = "à";
                break;
            case "r":
                valueText = "ả";
                break;
            case "R":
                valueText = "ả";
                break;
            case "x":
                valueText = "ã";
                break;
            case "X":
                valueText = "ã";
                break;
            case "j":
                valueText = "ạ";
                break;
            case "J":
                valueText = "ạ";
                break;
            case "a":
                valueText = "â";
                break;
            case "A":
                valueText = "a";
                break;
            case "w":
                valueText = "ă";
                break;
            case "W":
                valueText = "ă";
                break;
            default:
                break;
            }
            break;
        case "á":
            switch textInput {
            case "s":
                valueText = "as";
                break;
            case "f":
                valueText = "à";
                break;
            case "r":
                valueText = "ả";
                break;
            case "x":
                valueText = "ã";
                break;
            case "j":
                valueText = "ạ";
                break;
            case "a":
                valueText = "ấ";
                break;
            case "w":
                valueText = "ắ";
                break;
            case "S":
                valueText = "as";
                break;
            case "F":
                valueText = "à";
                break;
            case "R":
                valueText = "ả";
                break;
            case "X":
                valueText = "ã";
                break;
            case "J":
                valueText = "ạ";
                break;
            case "A":
                valueText = "ấ";
                break;
            case "W":
                valueText = "ắ";
                break;
            default:
                break;
            }
            break;
        case "à":
            switch textInput {
            case "s":
                valueText = "á";
                break;
            case "f":
                valueText = "af";
                break;
            case "r":
                valueText = "ả";
                break;
            case "x":
                valueText = "ã";
                break;
            case "j":
                valueText = "ạ";
                break;
            case "a":
                valueText = "ầ";
                break;
            case "w":
                valueText = "ằ";
                break;
            case "S":
                valueText = "á";
                break;
            case "F":
                valueText = "af";
                break;
            case "R":
                valueText = "ả";
                break;
            case "X":
                valueText = "ã";
                break;
            case "J":
                valueText = "ạ";
                break;
            case "A":
                valueText = "ầ";
                break;
            case "W":
                valueText = "ằ";
                break;
            default:
                break;
            }
            break;
        case "ả":
            switch textInput {
            case "s":
                valueText = "á";
                break;
            case "f":
                valueText = "à";
                break;
            case "r":
                valueText = "ar";
                break;
            case "x":
                valueText = "ã";
                break;
            case "j":
                valueText = "ạ";
                break;
            case "a":
                valueText = "ẩ";
                break;
            case "w":
                valueText = "ẳ";
                break;
            case "S":
                valueText = "á";
                break;
            case "F":
                valueText = "à";
                break;
            case "R":
                valueText = "ar";
                break;
            case "X":
                valueText = "ã";
                break;
            case "J":
                valueText = "ạ";
                break;
            case "A":
                valueText = "ẩ";
                break;
            case "W":
                valueText = "ẳ";
                break;
            default:
                break;
            }
            break;
        case "ã":
            switch textInput {
            case "s":
                valueText = "á";
                break;
            case "f":
                valueText = "à";
                break;
            case "r":
                valueText = "ả";
                break;
            case "x":
                valueText = "ax";
                break;
            case "j":
                valueText = "ạ";
                break;
            case "a":
                valueText = "ẫ";
                break;
            case "w":
                valueText = "ẵ";
                break;
            case "S":
                valueText = "á";
                break;
            case "F":
                valueText = "à";
                break;
            case "R":
                valueText = "ả";
                break;
            case "X":
                valueText = "ax";
                break;
            case "J":
                valueText = "ạ";
                break;
            case "A":
                valueText = "ẫ";
                break;
            case "W":
                valueText = "ẵ";
                break;
            default:
                break;
            }
            break;
        case "ạ":
            switch textInput {
            case "s":
                valueText = "á";
                break;
            case "f":
                valueText = "à";
                break;
            case "r":
                valueText = "ả";
                break;
            case "x":
                valueText = "ã";
                break;
            case "j":
                valueText = "aj";
                break;
            case "a":
                valueText = "ậ";
                break;
            case "w":
                valueText = "ặ";
                break;
            case "S":
                valueText = "á";
                break;
            case "F":
                valueText = "à";
                break;
            case "R":
                valueText = "ả";
                break;
            case "X":
                valueText = "ã";
                break;
            case "J":
                valueText = "aj";
                break;
            case "A":
                valueText = "ậ";
                break;
            case "W":
                valueText = "ặ";
                break;
            default:
                break;
            }
            break;
        case "ă":
            switch textInput {
            case "s":
                valueText = "ắ";
                break;
            case "f":
                valueText = "ằ";
                break;
            case "r":
                valueText = "ẳ";
                break;
            case "x":
                valueText = "ẵ";
                break;
            case "j":
                valueText = "ặ";
                break;
            case "w":
                valueText = "aw";
                break;
            case "a":
                valueText = "â";
                break;
            case "S":
                valueText = "ắ";
                break;
            case "F":
                valueText = "ằ";
                break;
            case "R":
                valueText = "ẳ";
                break;
            case "X":
                valueText = "ẵ";
                break;
            case "J":
                valueText = "ặ";
                break;
            case "W":
                valueText = "aw";
                break;
            case "A":
                valueText = "â";
                break;
            default:
                break;
            }
            break;
        case "ắ":
            switch textInput {
            case "s":
                valueText = "ăs";
                break;
            case "f":
                valueText = "ằ";
                break;
            case "r":
                valueText = "ẳ";
                break;
            case "x":
                valueText = "ẵ";
                break;
            case "j":
                valueText = "ặ";
                break;
            case "w":
                valueText = "áw";
                break;
            case "a":
                valueText = "ấ";
                break;
            case "S":
                valueText = "ăs";
                break;
            case "F":
                valueText = "ằ";
                break;
            case "R":
                valueText = "ẳ";
                break;
            case "X":
                valueText = "ẵ";
                break;
            case "J":
                valueText = "ặ";
                break;
            case "W":
                valueText = "áw";
                break;
            case "A":
                valueText = "ấ";
                break;
            default:
                break;
            }
            break;
        case "ằ":
            switch textInput {
            case "s":
                valueText = "ắ";
                break;
            case "f":
                valueText = "ăf";
                break;
            case "r":
                valueText = "ẳ";
                break;
            case "x":
                valueText = "ẵ";
                break;
            case "j":
                valueText = "ặ";
                break;
            case "w":
                valueText = "àw";
                break;
            case "a":
                valueText = "ầ";
                break;
            case "S":
                valueText = "ắ";
                break;
            case "F":
                valueText = "ăf";
                break;
            case "R":
                valueText = "ẳ";
                break;
            case "X":
                valueText = "ẵ";
                break;
            case "J":
                valueText = "ặ";
                break;
            case "W":
                valueText = "àw";
                break;
            case "A":
                valueText = "ầ";
                break;
            default:
                break;
            }
            break;
        case "ẳ":
            switch textInput {
            case "s":
                valueText = "ắ";
                break;
            case "f":
                valueText = "ằ";
                break;
            case "r":
                valueText = "ăr";
                break;
            case "x":
                valueText = "ẵ";
                break;
            case "j":
                valueText = "ặ";
                break;
            case "w":
                valueText = "ảw";
                break;
            case "a":
                valueText = "ẩ";
                break;
            case "S":
                valueText = "ắ";
                break;
            case "F":
                valueText = "ằ";
                break;
            case "R":
                valueText = "ăr";
                break;
            case "X":
                valueText = "ẵ";
                break;
            case "J":
                valueText = "ặ";
                break;
            case "W":
                valueText = "ảw";
                break;
            case "A":
                valueText = "ẩ";
                break;
            default:
                break;
            }
            break;
        case "ẵ":
            switch textInput {
            case "s":
                valueText = "ắ";
                break;
            case "f":
                valueText = "ằ";
                break;
            case "r":
                valueText = "ẳ";
                break;
            case "x":
                valueText = "ăx";
                break;
            case "j":
                valueText = "ặ";
                break;
            case "w":
                valueText = "ãw";
                break;
            case "a":
                valueText = "ẫ";
                break;
            case "S":
                valueText = "ắ";
                break;
            case "F":
                valueText = "ằ";
                break;
            case "R":
                valueText = "ẳ";
                break;
            case "X":
                valueText = "ăx";
                break;
            case "J":
                valueText = "ặ";
                break;
            case "W":
                valueText = "ãw";
                break;
            case "A":
                valueText = "ẫ";
                break;
            default:
                break;
            }
            break;
        case "ặ":
            switch textInput {
            case "s":
                valueText = "ắ";
                break;
            case "f":
                valueText = "ằ";
                break;
            case "r":
                valueText = "ẳ";
                break;
            case "x":
                valueText = "ẵ";
                break;
            case "j":
                valueText = "ăj";
                break;
            case "w":
                valueText = "ạw";
                break;
            case "a":
                valueText = "ậ";
                break;
            case "S":
                valueText = "ắ";
                break;
            case "F":
                valueText = "ằ";
                break;
            case "R":
                valueText = "ẳ";
                break;
            case "X":
                valueText = "ẵ";
                break;
            case "J":
                valueText = "ăj";
                break;
            case "W":
                valueText = "ạw";
                break;
            case "A":
                valueText = "ậ";
                break;
            default:
                break;
            }
            break;
        case "â":
            switch textInput {
            case "s":
                valueText = "ấ";
                break;
            case "f":
                valueText = "ầ";
                break;
            case "r":
                valueText = "ẩ";
                break;
            case "x":
                valueText = "ẫ";
                break;
            case "j":
                valueText = "ậ";
                break;
            case "a":
                valueText = "aa";
                break;
            case "w":
                valueText = "ă";
                break;
            case "S":
                valueText = "ấ";
                break;
            case "F":
                valueText = "ầ";
                break;
            case "R":
                valueText = "ẩ";
                break;
            case "X":
                valueText = "ẫ";
                break;
            case "J":
                valueText = "ậ";
                break;
            case "A":
                valueText = "aa";
                break;
            case "W":
                valueText = "ă";
                break;
            default:
                break;
            }
            break;
        case "ấ":
            switch textInput {
            case "s":
                valueText = "âs";
                break;
            case "f":
                valueText = "ầ";
                break;
            case "r":
                valueText = "ẩ";
                break;
            case "x":
                valueText = "ẫ";
                break;
            case "j":
                valueText = "ậ";
                break;
            case "a":
                valueText = "áa";
                break;
            case "w":
                valueText = "ắ";
                break;
            case "S":
                valueText = "âS";
                break;
            case "F":
                valueText = "ầ";
                break;
            case "R":
                valueText = "ẩ";
                break;
            case "X":
                valueText = "ẫ";
                break;
            case "J":
                valueText = "ậ";
                break;
            case "A":
                valueText = "áA";
                break;
            case "W":
                valueText = "ắ";
                break;
            default:
                break;
            }
            break;
        case "ầ":
            switch textInput {
            case "s":
                valueText = "ấ";
                break;
            case "f":
                valueText = "âf";
                break;
            case "r":
                valueText = "ẩ";
                break;
            case "x":
                valueText = "ẫ";
                break;
            case "j":
                valueText = "ậ";
                break;
            case "a":
                valueText = "àa";
                break;
            case "w":
                valueText = "ằ";
                break;
            case "S":
                valueText = "ấ";
                break;
            case "F":
                valueText = "âF";
                break;
            case "R":
                valueText = "ẩ";
                break;
            case "X":
                valueText = "ẫ";
                break;
            case "J":
                valueText = "ậ";
                break;
            case "A":
                valueText = "àA";
                break;
            case "W":
                valueText = "ằ";
                break;
            default:
                break;
            }
            break;
        case "ẩ":
            switch textInput {
            case "s":
                valueText = "ấ";
                break;
            case "f":
                valueText = "ầ";
                break;
            case "r":
                valueText = "âr";
                break;
            case "x":
                valueText = "ẫ";
                break;
            case "j":
                valueText = "ậ";
                break;
            case "a":
                valueText = "ảa";
                break;
            case "w":
                valueText = "ẳ";
                break;
            case "S":
                valueText = "ấ";
                break;
            case "F":
                valueText = "ầ";
                break;
            case "R":
                valueText = "âR";
                break;
            case "X":
                valueText = "ẫ";
                break;
            case "J":
                valueText = "ậ";
                break;
            case "A":
                valueText = "ảA";
                break;
            case "W":
                valueText = "ẳ";
                break;
            default:
                break;
            }
            break;
        case "ẫ":
            switch textInput {
            case "s":
                valueText = "ấ";
                break;
            case "f":
                valueText = "ầ";
                break;
            case "r":
                valueText = "ẩ";
                break;
            case "x":
                valueText = "âx";
                break;
            case "j":
                valueText = "ậ";
                break;
            case "a":
                valueText = "ãa";
                break;
            case "w":
                valueText = "ẵ";
                break;
            case "S":
                valueText = "ấ";
                break;
            case "F":
                valueText = "ầ";
                break;
            case "R":
                valueText = "ẩ";
                break;
            case "X":
                valueText = "âX";
                break;
            case "J":
                valueText = "ậ";
                break;
            case "A":
                valueText = "ãA";
                break;
            case "W":
                valueText = "ẵ";
                break;
            default:
                break;
            }
            break;
        case "ậ":
            switch textInput {
            case "s":
                valueText = "ấ";
                break;
            case "f":
                valueText = "ầ";
                break;
            case "r":
                valueText = "ẩ";
                break;
            case "x":
                valueText = "ẫ";
                break;
            case "j":
                valueText = "âj";
                break;
            case "a":
                valueText = "ạa";
                break;
            case "w":
                valueText = "ặ";
                break;
            case "S":
                valueText = "ấ";
                break;
            case "F":
                valueText = "ầ";
                break;
            case "R":
                valueText = "ẩ";
                break;
            case "X":
                valueText = "ẫ";
                break;
            case "J":
                valueText = "âJ";
                break;
            case "A":
                valueText = "ạA";
                break;
            case "W":
                valueText = "ặ";
                break;
            default:
                break;
            }
            break;
        case "e":
            switch textInput {
            case "s":
                valueText = "é";
                break;
            case "f":
                valueText = "è";
                break;
            case "r":
                valueText = "ẻ";
                break;
            case "x":
                valueText = "ẽ";
                break;
            case "j":
                valueText = "ẹ";
                break;
            case "e":
                valueText = "ê";
                break;
            case "S":
                valueText = "é";
                break;
            case "F":
                valueText = "è";
                break;
            case "R":
                valueText = "ẻ";
                break;
            case "X":
                valueText = "ẽ";
                break;
            case "J":
                valueText = "ẹ";
                break;
            case "E":
                valueText = "ê";
                break;
            default:
                break;
            }
            break;
        case "é":
            switch textInput {
            case "s":
                valueText = "es";
                break;
            case "f":
                valueText = "è";
                break;
            case "r":
                valueText = "ẻ";
                break;
            case "x":
                valueText = "ẽ";
                break;
            case "j":
                valueText = "ẹ";
                break;
            case "e":
                valueText = "ế";
                break;
            case "S":
                valueText = "eS";
                break;
            case "F":
                valueText = "è";
                break;
            case "R":
                valueText = "ẻ";
                break;
            case "X":
                valueText = "ẽ";
                break;
            case "J":
                valueText = "ẹ";
                break;
            case "E":
                valueText = "ế";
                break;
            default:
                break;
            }
            break;
        case "è":
            switch textInput {
            case "s":
                valueText = "é";
                break;
            case "f":
                valueText = "ef";
                break;
            case "r":
                valueText = "ẻ";
                break;
            case "x":
                valueText = "ẽ";
                break;
            case "j":
                valueText = "ẹ";
                break;
            case "e":
                valueText = "ề";
                break;
            case "S":
                valueText = "é";
                break;
            case "F":
                valueText = "eF";
                break;
            case "R":
                valueText = "ẻ";
                break;
            case "X":
                valueText = "ẽ";
                break;
            case "J":
                valueText = "ẹ";
                break;
            case "E":
                valueText = "ề";
                break;
            default:
                break;
            }
            break;
        case "ẻ":
            switch textInput {
            case "s":
                valueText = "é";
                break;
            case "f":
                valueText = "è";
                break;
            case "r":
                valueText = "er";
                break;
            case "x":
                valueText = "ẽ";
                break;
            case "j":
                valueText = "ẹ";
                break;
            case "e":
                valueText = "ể";
                break;
            case "S":
                valueText = "é";
                break;
            case "F":
                valueText = "è";
                break;
            case "R":
                valueText = "er";
                break;
            case "X":
                valueText = "ẽ";
                break;
            case "J":
                valueText = "ẹ";
                break;
            case "E":
                valueText = "ể";
                break;
            default:
                break;
            }
            break;
        case "ẽ":
            switch textInput {
            case "s":
                valueText = "é";
                break;
            case "f":
                valueText = "è";
                break;
            case "r":
                valueText = "ẻ";
                break;
            case "x":
                valueText = "ex";
                break;
            case "j":
                valueText = "ẹ";
                break;
            case "e":
                valueText = "ễ";
                break;
            case "S":
                valueText = "é";
                break;
            case "F":
                valueText = "è";
                break;
            case "R":
                valueText = "ẻ";
                break;
            case "X":
                valueText = "eX";
                break;
            case "J":
                valueText = "ẹ";
                break;
            case "E":
                valueText = "ễ";
                break;
            default:
                break;
            }
            break;
        case "ẹ":
            switch textInput {
            case "s":
                valueText = "é";
                break;
            case "f":
                valueText = "è";
                break;
            case "r":
                valueText = "ẻ";
                break;
            case "x":
                valueText = "ẽ";
                break;
            case "j":
                valueText = "ej";
                break;
            case "e":
                valueText = "ệ";
                break;
            case "S":
                valueText = "é";
                break;
            case "F":
                valueText = "è";
                break;
            case "R":
                valueText = "ẻ";
                break;
            case "X":
                valueText = "ẽ";
                break;
            case "J":
                valueText = "eJ";
                break;
            case "E":
                valueText = "ệ";
                break;
            default:
                break;
            }
            break;
        case "ê":
            switch textInput {
            case "s":
                valueText = "ế";
                break;
            case "f":
                valueText = "ề";
                break;
            case "r":
                valueText = "ể";
                break;
            case "x":
                valueText = "ễ";
                break;
            case "j":
                valueText = "ệ";
                break;
            case "e":
                valueText = "ee";
                break;
            case "S":
                valueText = "ế";
                break;
            case "F":
                valueText = "ề";
                break;
            case "R":
                valueText = "ể";
                break;
            case "X":
                valueText = "ễ";
                break;
            case "J":
                valueText = "ệ";
                break;
            case "E":
                valueText = "eE";
                break;
            default:
                break;
            }
            break;
        case "ế":
            switch textInput {
            case "s":
                valueText = "ês";
                break;
            case "f":
                valueText = "ề";
                break;
            case "r":
                valueText = "ể";
                break;
            case "x":
                valueText = "ễ";
                break;
            case "j":
                valueText = "ệ";
                break;
            case "e":
                valueText = "ée";
                break;
            case "S":
                valueText = "êS";
                break;
            case "F":
                valueText = "ề";
                break;
            case "R":
                valueText = "ể";
                break;
            case "X":
                valueText = "ễ";
                break;
            case "J":
                valueText = "ệ";
                break;
            case "E":
                valueText = "ée";
                break;
            default:
                break;
            }
            break;
        case "ề":
            switch textInput {
            case "s":
                valueText = "ế";
                break;
            case "f":
                valueText = "êf";
                break;
            case "r":
                valueText = "ể";
                break;
            case "x":
                valueText = "ễ";
                break;
            case "j":
                valueText = "ệ";
                break;
            case "e":
                valueText = "èe";
                break;
            case "S":
                valueText = "ế";
                break;
            case "F":
                valueText = "êF";
                break;
            case "R":
                valueText = "ể";
                break;
            case "X":
                valueText = "ễ";
                break;
            case "J":
                valueText = "ệ";
                break;
            case "E":
                valueText = "èE";
                break;
            default:
                break;
            }
            break;
        case "ể":
            switch textInput {
            case "s":
                valueText = "ế";
                break;
            case "f":
                valueText = "ề";
                break;
            case "r":
                valueText = "êr";
                break;
            case "x":
                valueText = "ễ";
                break;
            case "j":
                valueText = "ệ";
                break;
            case "e":
                valueText = "ẻe";
                break;
            case "S":
                valueText = "ế";
                break;
            case "F":
                valueText = "ề";
                break;
            case "R":
                valueText = "êR";
                break;
            case "X":
                valueText = "ễ";
                break;
            case "J":
                valueText = "ệ";
                break;
            case "E":
                valueText = "ẻe";
                break;
            default:
                break;
            }
            break;
        case "ễ":
            switch textInput {
            case "s":
                valueText = "ế";
                break;
            case "f":
                valueText = "ề";
                break;
            case "r":
                valueText = "ể";
                break;
            case "x":
                valueText = "êx";
                break;
            case "j":
                valueText = "ệ";
                break;
            case "e":
                valueText = "ẽe";
                break;
            case "S":
                valueText = "ế";
                break;
            case "F":
                valueText = "ề";
                break;
            case "R":
                valueText = "ể";
                break;
            case "X":
                valueText = "êX";
                break;
            case "J":
                valueText = "ệ";
                break;
            case "E":
                valueText = "ẽE";
                break;
            default:
                break;
            }
            break;
        case "ệ":
            switch textInput {
            case "s":
                valueText = "ế";
                break;
            case "f":
                valueText = "ề";
                break;
            case "r":
                valueText = "ể";
                break;
            case "x":
                valueText = "ễ";
                break;
            case "j":
                valueText = "êj";
                break;
            case "e":
                valueText = "ẹe";
                break;
            case "S":
                valueText = "ế";
                break;
            case "F":
                valueText = "ề";
                break;
            case "R":
                valueText = "ể";
                break;
            case "X":
                valueText = "ễ";
                break;
            case "J":
                valueText = "êJ";
                break;
            case "E":
                valueText = "ẹE";
                break;
            default:
                break;
            }
            break;
        case "i":
            switch textInput {
            case "s":
                valueText = "í";
                break;
            case "f":
                valueText = "ì";
                break;
            case "r":
                valueText = "ỉ";
                break;
            case "x":
                valueText = "ĩ";
                break;
            case "j":
                valueText = "ị";
                break;
            case "S":
                valueText = "í";
                break;
            case "F":
                valueText = "ì";
                break;
            case "R":
                valueText = "ỉ";
                break;
            case "X":
                valueText = "ĩ";
                break;
            case "J":
                valueText = "ị";
                break;
            default:
                break;
            }
            break;
        case "í":
            switch textInput {
            case "s":
                valueText = "is";
                break;
            case "f":
                valueText = "ì";
                break;
            case "r":
                valueText = "ỉ";
                break;
            case "x":
                valueText = "ĩ";
                break;
            case "j":
                valueText = "ị";
                break;
            case "S":
                valueText = "iS";
                break;
            case "F":
                valueText = "ì";
                break;
            case "R":
                valueText = "ỉ";
                break;
            case "X":
                valueText = "ĩ";
                break;
            case "J":
                valueText = "ị";
                break;
            default:
                break;
            }
            break;
        case "ì":
            switch textInput {
            case "s":
                valueText = "í";
                break;
            case "f":
                valueText = "if";
                break;
            case "r":
                valueText = "ỉ";
                break;
            case "x":
                valueText = "ĩ";
                break;
            case "j":
                valueText = "ị";
                break;
            case "S":
                valueText = "í";
                break;
            case "F":
                valueText = "iF";
                break;
            case "R":
                valueText = "ỉ";
                break;
            case "X":
                valueText = "ĩ";
                break;
            case "J":
                valueText = "ị";
                break;
            default:
                break;
            }
            break;
        case "ỉ":
            switch textInput {
            case "s":
                valueText = "í";
                break;
            case "f":
                valueText = "ì";
                break;
            case "r":
                valueText = "ir";
                break;
            case "x":
                valueText = "ĩ";
                break;
            case "j":
                valueText = "ị";
                break;
            case "S":
                valueText = "í";
                break;
            case "F":
                valueText = "ì";
                break;
            case "R":
                valueText = "iR";
                break;
            case "X":
                valueText = "ĩ";
                break;
            case "J":
                valueText = "ị";
                break;
            default:
                break;
            }
            break;
        case "ĩ":
            switch textInput {
            case "s":
                valueText = "í";
                break;
            case "f":
                valueText = "ì";
                break;
            case "r":
                valueText = "ỉ";
                break;
            case "x":
                valueText = "ix";
                break;
            case "j":
                valueText = "ị";
                break;
            case "S":
                valueText = "í";
                break;
            case "F":
                valueText = "ì";
                break;
            case "R":
                valueText = "ỉ";
                break;
            case "X":
                valueText = "iX";
                break;
            case "J":
                valueText = "ị";
                break;
            default:
                break;
            }
            break;
        case "ị":
            switch textInput {
            case "s":
                valueText = "í";
                break;
            case "f":
                valueText = "ì";
                break;
            case "r":
                valueText = "ỉ";
                break;
            case "x":
                valueText = "ĩ";
                break;
            case "j":
                valueText = "ij";
                break;
            case "S":
                valueText = "í";
                break;
            case "F":
                valueText = "ì";
                break;
            case "R":
                valueText = "ỉ";
                break;
            case "X":
                valueText = "ĩ";
                break;
            case "J":
                valueText = "iJ";
                break;
            default:
                break;
            }
            break;
        case "y":
            switch textInput {
            case "s":
                valueText = "ý";
                break;
            case "f":
                valueText = "ỳ";
                break;
            case "r":
                valueText = "ỷ";
                break;
            case "x":
                valueText = "ỹ";
                break;
            case "j":
                valueText = "ỵ";
                break;
            case "S":
                valueText = "ý";
                break;
            case "F":
                valueText = "ỳ";
                break;
            case "R":
                valueText = "ỷ";
                break;
            case "X":
                valueText = "ỹ";
                break;
            case "J":
                valueText = "ỵ";
                break;
            default:
                break;
            }
            break;
        case "ý":
            switch textInput {
            case "s":
                valueText = "ys";
                break;
            case "f":
                valueText = "ỳ";
                break;
            case "r":
                valueText = "ỷ";
                break;
            case "x":
                valueText = "ỹ";
                break;
            case "j":
                valueText = "ỵ";
                break;
            case "S":
                valueText = "yS";
                break;
            case "F":
                valueText = "ỳ";
                break;
            case "R":
                valueText = "ỷ";
                break;
            case "X":
                valueText = "ỹ";
                break;
            case "J":
                valueText = "ỵ";
                break;
            default:
                break;
            }
            break;
        case "ỳ":
            switch textInput {
            case "s":
                valueText = "ý";
                break;
            case "f":
                valueText = "yf";
                break;
            case "r":
                valueText = "ỷ";
                break;
            case "x":
                valueText = "ỹ";
                break;
            case "j":
                valueText = "ỵ";
                break;
            case "S":
                valueText = "ý";
                break;
            case "F":
                valueText = "yF";
                break;
            case "R":
                valueText = "ỷ";
                break;
            case "X":
                valueText = "ỹ";
                break;
            case "J":
                valueText = "ỵ";
                break;
            default:
                break;
            }
            break;
        case "ỷ":
            switch textInput {
            case "s":
                valueText = "ý";
                break;
            case "f":
                valueText = "ỳ";
                break;
            case "r":
                valueText = "yr";
                break;
            case "x":
                valueText = "ỹ";
                break;
            case "j":
                valueText = "ỵ";
                break;
            case "S":
                valueText = "ý";
                break;
            case "F":
                valueText = "ỳ";
                break;
            case "R":
                valueText = "yR";
                break;
            case "X":
                valueText = "ỹ";
                break;
            case "J":
                valueText = "ỵ";
                break;
            default:
                break;
            }
            break;
        case "ỹ":
            switch textInput {
            case "s":
                valueText = "ý";
                break;
            case "f":
                valueText = "ỳ";
                break;
            case "r":
                valueText = "ỷ";
                break;
            case "x":
                valueText = "yx";
                break;
            case "j":
                valueText = "ỵ";
                break;
            case "S":
                valueText = "ý";
                break;
            case "F":
                valueText = "ỳ";
                break;
            case "R":
                valueText = "ỷ";
                break;
            case "X":
                valueText = "yX";
                break;
            case "J":
                valueText = "ỵ";
                break;
            default:
                break;
            }
            break;
        case "ỵ":
            switch textInput {
            case "s":
                valueText = "ý";
                break;
            case "f":
                valueText = "ỳ";
                break;
            case "r":
                valueText = "ỷ";
                break;
            case "x":
                valueText = "ỹ";
                break;
            case "j":
                valueText = "yj";
                break;
            case "S":
                valueText = "ý";
                break;
            case "F":
                valueText = "ỳ";
                break;
            case "R":
                valueText = "ỷ";
                break;
            case "X":
                valueText = "ỹ";
                break;
            case "J":
                valueText = "yj";
                break;
            default:
                break;
            }
            break;
        case "u":
            switch textInput {
            case "s":
                valueText = "ú";
                break;
            case "f":
                valueText = "ù";
                break;
            case "r":
                valueText = "ủ";
                break;
            case "x":
                valueText = "ũ";
                break;
            case "j":
                valueText = "ụ";
                break;
            case "w":
                valueText = "ư";
                break;
            case "S":
                valueText = "ú";
                break;
            case "F":
                valueText = "ù";
                break;
            case "R":
                valueText = "ủ";
                break;
            case "X":
                valueText = "ũ";
                break;
            case "J":
                valueText = "ụ";
                break;
            case "W":
                valueText = "ư";
                break;
            default:
                break;
            }
            break;
        case "ú":
            switch textInput {
            case "s":
                valueText = "us";
                break;
            case "f":
                valueText = "ù";
                break;
            case "r":
                valueText = "ủ";
                break;
            case "x":
                valueText = "ũ";
                break;
            case "j":
                valueText = "ụ";
                break;
            case "w":
                valueText = "ứ";
                break;
            case "S":
                valueText = "uS";
                break;
            case "F":
                valueText = "ù";
                break;
            case "R":
                valueText = "ủ";
                break;
            case "X":
                valueText = "ũ";
                break;
            case "J":
                valueText = "ụ";
                break;
            case "W":
                valueText = "ứ";
                break;
            default:
                break;
            }
            break;
        case "ù":
            switch textInput {
            case "s":
                valueText = "ú";
                break;
            case "f":
                valueText = "uf";
                break;
            case "r":
                valueText = "ủ";
                break;
            case "x":
                valueText = "ũ";
                break;
            case "j":
                valueText = "ụ";
                break;
            case "w":
                valueText = "ừ";
                break;
            case "S":
                valueText = "ú";
                break;
            case "F":
                valueText = "uF";
                break;
            case "R":
                valueText = "ủ";
                break;
            case "X":
                valueText = "ũ";
                break;
            case "J":
                valueText = "ụ";
                break;
            case "W":
                valueText = "ừ";
                break;
            default:
                break;
            }
            break;
        case "ủ":
            switch textInput {
            case "s":
                valueText = "ú";
                break;
            case "f":
                valueText = "ù";
                break;
            case "r":
                valueText = "ur";
                break;
            case "x":
                valueText = "ũ";
                break;
            case "j":
                valueText = "ụ";
                break;
            case "w":
                valueText = "ử";
                break;
            case "S":
                valueText = "ú";
                break;
            case "F":
                valueText = "ù";
                break;
            case "R":
                valueText = "uR";
                break;
            case "X":
                valueText = "ũ";
                break;
            case "J":
                valueText = "ụ";
                break;
            case "W":
                valueText = "ử";
                break;
            default:
                break;
            }
            break;
        case "ũ":
            switch textInput {
            case "s":
                valueText = "ú";
                break;
            case "f":
                valueText = "ù";
                break;
            case "r":
                valueText = "ủ";
                break;
            case "x":
                valueText = "ux";
                break;
            case "j":
                valueText = "ụ";
                break;
            case "w":
                valueText = "ữ";
                break;
            case "S":
                valueText = "ú";
                break;
            case "F":
                valueText = "ù";
                break;
            case "R":
                valueText = "ủ";
                break;
            case "X":
                valueText = "uX";
                break;
            case "J":
                valueText = "ụ";
                break;
            case "W":
                valueText = "ữ";
                break;
            default:
                break;
            }
            break;
        case "ụ":
            switch textInput {
            case "s":
                valueText = "ú";
                break;
            case "f":
                valueText = "ù";
                break;
            case "r":
                valueText = "ủ";
                break;
            case "x":
                valueText = "ũ";
                break;
            case "j":
                valueText = "uj";
                break;
            case "w":
                valueText = "ự";
                break;
            case "S":
                valueText = "ú";
                break;
            case "F":
                valueText = "ù";
                break;
            case "R":
                valueText = "ủ";
                break;
            case "X":
                valueText = "ũ";
                break;
            case "J":
                valueText = "uJ";
                break;
            case "W":
                valueText = "ự";
                break;
            default:
                break;
            }
            break;
        case "ư":
            switch textInput {
            case "s":
                valueText = "ứ";
                break;
            case "f":
                valueText = "ừ";
                break;
            case "r":
                valueText = "ử";
                break;
            case "x":
                valueText = "ữ";
                break;
            case "j":
                valueText = "ự";
                break;
            case "w":
                valueText = "uw";
                break;
            case "S":
                valueText = "ứ";
                break;
            case "F":
                valueText = "ừ";
                break;
            case "R":
                valueText = "ử";
                break;
            case "X":
                valueText = "ữ";
                break;
            case "J":
                valueText = "ự";
                break;
            case "W":
                valueText = "uW";
                break;
            default:
                break;
            }
            break;
        case "ứ":
            switch textInput {
            case "s":
                valueText = "ưs";
                break;
            case "f":
                valueText = "ừ";
                break;
            case "r":
                valueText = "ử";
                break;
            case "x":
                valueText = "ữ";
                break;
            case "j":
                valueText = "ự";
                break;
            case "w":
                valueText = "úw";
                break;
            case "S":
                valueText = "ưS";
                break;
            case "F":
                valueText = "ừ";
                break;
            case "R":
                valueText = "ử";
                break;
            case "X":
                valueText = "ữ";
                break;
            case "J":
                valueText = "ự";
                break;
            case "W":
                valueText = "úW";
                break;
            default:
                break;
            }
            break;
        case "ừ":
            switch textInput {
            case "s":
                valueText = "ứ";
                break;
            case "f":
                valueText = "ưf";
                break;
            case "r":
                valueText = "ử";
                break;
            case "x":
                valueText = "ữ";
                break;
            case "j":
                valueText = "ự";
                break;
            case "w":
                valueText = "ùw";
                break;
            case "S":
                valueText = "ứ";
                break;
            case "F":
                valueText = "ưF";
                break;
            case "R":
                valueText = "ử";
                break;
            case "X":
                valueText = "ữ";
                break;
            case "J":
                valueText = "ự";
                break;
            case "W":
                valueText = "ùW";
                break;
            default:
                break;
            }
            break;
        case "ử":
            switch textInput {
            case "s":
                valueText = "ứ";
                break;
            case "f":
                valueText = "ừ";
                break;
            case "r":
                valueText = "ưr";
                break;
            case "x":
                valueText = "ữ";
                break;
            case "j":
                valueText = "ự";
                break;
            case "w":
                valueText = "ủw";
                break;
            case "S":
                valueText = "ứ";
                break;
            case "F":
                valueText = "ừ";
                break;
            case "R":
                valueText = "ưR";
                break;
            case "X":
                valueText = "ữ";
                break;
            case "J":
                valueText = "ự";
                break;
            case "W":
                valueText = "ủW";
                break;
            default:
                break;
            }
            break;
        case "ữ":
            switch textInput {
            case "s":
                valueText = "ứ";
                break;
            case "f":
                valueText = "ừ";
                break;
            case "r":
                valueText = "ử";
                break;
            case "x":
                valueText = "ữ";
                break;
            case "j":
                valueText = "ự";
                break;
            case "w":
                valueText = "ũW";
                break;
            case "S":
                valueText = "ứ";
                break;
            case "F":
                valueText = "ừ";
                break;
            case "R":
                valueText = "ử";
                break;
            case "X":
                valueText = "ữ";
                break;
            case "J":
                valueText = "ự";
                break;
            case "W":
                valueText = "ũW";
                break;
            default:
                break;
            }
            break;
        case "ự":
            switch textInput {
            case "s":
                valueText = "ứ";
                break;
            case "f":
                valueText = "ừ";
                break;
            case "r":
                valueText = "ử";
                break;
            case "x":
                valueText = "ữ";
                break;
            case "j":
                valueText = "ưj";
                break;
            case "w":
                valueText = "ụw";
                break;
            case "S":
                valueText = "ứ";
                break;
            case "F":
                valueText = "ừ";
                break;
            case "R":
                valueText = "ử";
                break;
            case "X":
                valueText = "ữ";
                break;
            case "J":
                valueText = "ưJ";
                break;
            case "W":
                valueText = "ụW";
                break;
            default:
                break;
            }
            break;
        case "o":
            switch textInput {
            case "s":
                valueText = "ó";
                break;
            case "f":
                valueText = "ò";
                break;
            case "r":
                valueText = "ỏ";
                break;
            case "x":
                valueText = "õ";
                break;
            case "j":
                valueText = "ọ";
                break;
            case "w":
                valueText = "ơ";
                break;
            case "o":
                valueText = "ô";
                break;
            case "S":
                valueText = "ó";
                break;
            case "F":
                valueText = "ò";
                break;
            case "R":
                valueText = "ỏ";
                break;
            case "X":
                valueText = "õ";
                break;
            case "J":
                valueText = "ọ";
                break;
            case "W":
                valueText = "ơ";
                break;
            case "O":
                valueText = "ô";
                break;
            default:
                break;
            }
            break;
        case "ó":
            switch textInput {
            case "s":
                valueText = "os";
                break;
            case "f":
                valueText = "ò";
                break;
            case "r":
                valueText = "ỏ";
                break;
            case "x":
                valueText = "õ";
                break;
            case "j":
                valueText = "ọ";
                break;
            case "w":
                valueText = "ớ";
                break;
            case "o":
                valueText = "ố";
                break;
            case "S":
                valueText = "oS";
                break;
            case "F":
                valueText = "ò";
                break;
            case "R":
                valueText = "ỏ";
                break;
            case "X":
                valueText = "õ";
                break;
            case "J":
                valueText = "ọ";
                break;
            case "W":
                valueText = "ớ";
                break;
            case "O":
                valueText = "ố";
                break;
            default:
                break;
            }
            break;
        case "ò":
            switch textInput {
            case "s":
                valueText = "ó";
                break;
            case "f":
                valueText = "of";
                break;
            case "r":
                valueText = "ỏ";
                break;
            case "x":
                valueText = "õ";
                break;
            case "j":
                valueText = "ọ";
                break;
            case "w":
                valueText = "ờ";
                break;
            case "o":
                valueText = "ồ";
                break;
            case "S":
                valueText = "ó";
                break;
            case "F":
                valueText = "oF";
                break;
            case "R":
                valueText = "ỏ";
                break;
            case "X":
                valueText = "õ";
                break;
            case "J":
                valueText = "ọ";
                break;
            case "W":
                valueText = "ờ";
                break;
            case "O":
                valueText = "ồ";
                break;
            default:
                break;
            }
            break;
        case "ỏ":
            switch textInput {
            case "s":
                valueText = "ó";
                break;
            case "f":
                valueText = "ò";
                break;
            case "r":
                valueText = "or";
                break;
            case "x":
                valueText = "õ";
                break;
            case "j":
                valueText = "ọ";
                break;
            case "w":
                valueText = "ở";
                break;
            case "o":
                valueText = "ổ";
                break;
            case "S":
                valueText = "ó";
                break;
            case "F":
                valueText = "ò";
                break;
            case "R":
                valueText = "oR";
                break;
            case "X":
                valueText = "õ";
                break;
            case "J":
                valueText = "ọ";
                break;
            case "W":
                valueText = "ở";
                break;
            case "O":
                valueText = "ổ";
                break;
            default:
                break;
            }
            break;
        case "õ":
            switch textInput {
            case "s":
                valueText = "ó";
                break;
            case "f":
                valueText = "ò";
                break;
            case "r":
                valueText = "ỏ";
                break;
            case "x":
                valueText = "ox";
                break;
            case "j":
                valueText = "ọ";
                break;
            case "w":
                valueText = "ỡ";
                break;
            case "o":
                valueText = "ỗ";
                break;
            case "S":
                valueText = "ó";
                break;
            case "F":
                valueText = "ò";
                break;
            case "R":
                valueText = "ỏ";
                break;
            case "X":
                valueText = "oX";
                break;
            case "J":
                valueText = "ọ";
                break;
            case "W":
                valueText = "ỡ";
                break;
            case "O":
                valueText = "ỗ";
                break;
            default:
                break;
            }
            break;
        case "ọ":
            switch textInput {
            case "s":
                valueText = "ó";
                break;
            case "f":
                valueText = "ò";
                break;
            case "r":
                valueText = "ỏ";
                break;
            case "x":
                valueText = "õ";
                break;
            case "j":
                valueText = "oj";
                break;
            case "w":
                valueText = "ợ";
                break;
            case "o":
                valueText = "ộ";
                break;
            case "S":
                valueText = "ó";
                break;
            case "F":
                valueText = "ò";
                break;
            case "R":
                valueText = "ỏ";
                break;
            case "X":
                valueText = "õ";
                break;
            case "J":
                valueText = "oJ";
                break;
            case "W":
                valueText = "ợ";
                break;
            case "O":
                valueText = "ộ";
                break;
            default:
                break;
            }
            break;
        case "ơ":
            switch textInput {
            case "s":
                valueText = "ớ";
                break;
            case "f":
                valueText = "ờ";
                break;
            case "r":
                valueText = "ở";
                break;
            case "x":
                valueText = "ỡ";
                break;
            case "j":
                valueText = "ợ";
                break;
            case "w":
                valueText = "ow";
                break;
            case "o":
                valueText = "ô";
                break;
            case "S":
                valueText = "ớ";
                break;
            case "F":
                valueText = "ờ";
                break;
            case "R":
                valueText = "ở";
                break;
            case "X":
                valueText = "ỡ";
                break;
            case "J":
                valueText = "ợ";
                break;
            case "W":
                valueText = "oW";
                break;
            case "O":
                valueText = "ô";
                break;
            default:
                break;
            }
            break;
        case "ớ":
            switch textInput {
            case "s":
                valueText = "ơs";
                break;
            case "f":
                valueText = "ờ";
                break;
            case "r":
                valueText = "ở";
                break;
            case "x":
                valueText = "ỡ";
                break;
            case "j":
                valueText = "ợ";
                break;
            case "w":
                valueText = "ów";
                break;
            case "o":
                valueText = "ố";
                break;
            case "S":
                valueText = "ơS";
                break;
            case "F":
                valueText = "ờ";
                break;
            case "R":
                valueText = "ở";
                break;
            case "X":
                valueText = "ỡ";
                break;
            case "J":
                valueText = "ợ";
                break;
            case "W":
                valueText = "óW";
                break;
            case "O":
                valueText = "ố";
                break;
            default:
                break;
            }
            break;
        case "ờ":
            switch textInput {
            case "s":
                valueText = "ớ";
                break;
            case "f":
                valueText = "ơf";
                break;
            case "r":
                valueText = "ở";
                break;
            case "x":
                valueText = "ỡ";
                break;
            case "j":
                valueText = "ợ";
                break;
            case "w":
                valueText = "òw";
                break;
            case "o":
                valueText = "ồ";
                break;
            case "S":
                valueText = "ớ";
                break;
            case "F":
                valueText = "ơF";
                break;
            case "R":
                valueText = "ở";
                break;
            case "X":
                valueText = "ỡ";
                break;
            case "J":
                valueText = "ợ";
                break;
            case "W":
                valueText = "òW";
                break;
            case "O":
                valueText = "ồ";
                break;
            default:
                break;
            }
            break;
        case "ở":
            switch textInput {
            case "s":
                valueText = "ớ";
                break;
            case "f":
                valueText = "ờ";
                break;
            case "r":
                valueText = "ơr";
                break;
            case "x":
                valueText = "ỡ";
                break;
            case "j":
                valueText = "ợ";
                break;
            case "w":
                valueText = "ỏw";
                break;
            case "o":
                valueText = "ổ";
                break;
            case "S":
                valueText = "ớ";
                break;
            case "F":
                valueText = "ờ";
                break;
            case "R":
                valueText = "ơR";
                break;
            case "X":
                valueText = "ỡ";
                break;
            case "J":
                valueText = "ợ";
                break;
            case "W":
                valueText = "ỏW";
                break;
            case "O":
                valueText = "ổ";
                break;
            default:
                break;
            }
            break;
        case "ỡ":
            switch textInput {
            case "s":
                valueText = "ớ";
                break;
            case "f":
                valueText = "ờ";
                break;
            case "r":
                valueText = "ở";
                break;
            case "x":
                valueText = "ơx";
                break;
            case "j":
                valueText = "ợ";
                break;
            case "w":
                valueText = "õw";
                break;
            case "o":
                valueText = "ỗ";
                break;
            case "S":
                valueText = "ớ";
                break;
            case "F":
                valueText = "ờ";
                break;
            case "R":
                valueText = "ở";
                break;
            case "X":
                valueText = "ơX";
                break;
            case "J":
                valueText = "ợ";
                break;
            case "W":
                valueText = "õW";
                break;
            case "O":
                valueText = "ỗ";
                break;
            default:
                break;
            }
            break;
        case "ợ":
            switch textInput {
            case "s":
                valueText = "ớ";
                break;
            case "f":
                valueText = "ờ";
                break;
            case "r":
                valueText = "ở";
                break;
            case "x":
                valueText = "ỡ";
                break;
            case "j":
                valueText = "ơj";
                break;
            case "w":
                valueText = "ọw";
                break;
            case "o":
                valueText = "ộ";
                break;
            case "S":
                valueText = "ớ";
                break;
            case "F":
                valueText = "ờ";
                break;
            case "R":
                valueText = "ở";
                break;
            case "X":
                valueText = "ỡ";
                break;
            case "J":
                valueText = "ơJ";
                break;
            case "W":
                valueText = "ọW";
                break;
            case "O":
                valueText = "ộ";
                break;
            default:
                break;
            }
            break;
        case "ô":
            switch textInput {
            case "s":
                valueText = "ố";
                break;
            case "f":
                valueText = "ồ";
                break;
            case "r":
                valueText = "ổ";
                break;
            case "x":
                valueText = "ỗ";
                break;
            case "j":
                valueText = "ộ";
                break;
            case "o":
                valueText = "oo";
                break;
            case "w":
                valueText = "ơ";
                break;
            case "S":
                valueText = "ố";
                break;
            case "F":
                valueText = "ồ";
                break;
            case "R":
                valueText = "ổ";
                break;
            case "X":
                valueText = "ỗ";
                break;
            case "J":
                valueText = "ộ";
                break;
            case "O":
                valueText = "oO";
                break;
            case "W":
                valueText = "ơ";
                break;
            default:
                break;
            }
            break;
        case "ố":
            switch textInput {
            case "s":
                valueText = "ôs";
                break;
            case "f":
                valueText = "ồ";
                break;
            case "r":
                valueText = "ổ";
                break;
            case "x":
                valueText = "ỗ";
                break;
            case "j":
                valueText = "ộ";
                break;
            case "o":
                valueText = "óo";
                break;
            case "w":
                valueText = "ớ";
                break;
            case "S":
                valueText = "ôS";
                break;
            case "F":
                valueText = "ồ";
                break;
            case "R":
                valueText = "ổ";
                break;
            case "X":
                valueText = "ỗ";
                break;
            case "J":
                valueText = "ộ";
                break;
            case "O":
                valueText = "óO";
                break;
            case "W":
                valueText = "ớ";
                break;
            default:
                break;
            }
            break;
        case "ồ":
            switch textInput {
            case "s":
                valueText = "ố";
                break;
            case "f":
                valueText = "ôf";
                break;
            case "r":
                valueText = "ổ";
                break;
            case "x":
                valueText = "ỗ";
                break;
            case "j":
                valueText = "ộ";
                break;
            case "o":
                valueText = "òo";
                break;
            case "w":
                valueText = "ờ";
                break;
            case "S":
                valueText = "ố";
                break;
            case "F":
                valueText = "ôF";
                break;
            case "R":
                valueText = "ổ";
                break;
            case "X":
                valueText = "ỗ";
                break;
            case "J":
                valueText = "ộ";
                break;
            case "O":
                valueText = "òO";
                break;
            case "W":
                valueText = "ờ";
                break;
            default:
                break;
            }
            break;
        case "ổ":
            switch textInput {
            case "s":
                valueText = "ố";
                break;
            case "f":
                valueText = "ồ";
                break;
            case "r":
                valueText = "ôr";
                break;
            case "x":
                valueText = "ỗ";
                break;
            case "j":
                valueText = "ộ";
                break;
            case "o":
                valueText = "ỏo";
                break;
            case "w":
                valueText = "ở";
                break;
            case "S":
                valueText = "ố";
                break;
            case "F":
                valueText = "ồ";
                break;
            case "R":
                valueText = "ôR";
                break;
            case "X":
                valueText = "ỗ";
                break;
            case "J":
                valueText = "ộ";
                break;
            case "O":
                valueText = "ỏO";
                break;
            case "W":
                valueText = "ở";
                break;
            default:
                break;
            }
            break;
        case "ỗ":
            switch textInput {
            case "s":
                valueText = "ố";
                break;
            case "f":
                valueText = "ồ";
                break;
            case "r":
                valueText = "ổ";
                break;
            case "x":
                valueText = "ôx";
                break;
            case "j":
                valueText = "ộ";
                break;
            case "o":
                valueText = "õo";
                break;
            case "w":
                valueText = "ỡ";
                break;
            case "S":
                valueText = "ố";
                break;
            case "F":
                valueText = "ồ";
                break;
            case "R":
                valueText = "ổ";
                break;
            case "X":
                valueText = "ôX";
                break;
            case "J":
                valueText = "ộ";
                break;
            case "O":
                valueText = "õO";
                break;
            case "W":
                valueText = "ỡ";
                break;
            default:
                break;
            }
            break;
        case "ộ":
            switch textInput {
            case "s":
                valueText = "ố";
                break;
            case "f":
                valueText = "ồ";
                break;
            case "r":
                valueText = "ổ";
                break;
            case "x":
                valueText = "ỗ";
                break;
            case "j":
                valueText = "ộ";
                break;
            case "o":
                valueText = "ọo";
                break;
            case "w":
                valueText = "ợ";
                break;
            case "S":
                valueText = "ố";
                break;
            case "F":
                valueText = "ồ";
                break;
            case "R":
                valueText = "ổ";
                break;
            case "X":
                valueText = "ỗ";
                break;
            case "J":
                valueText = "ộ";
                break;
            case "O":
                valueText = "ọO";
                break;
            case "W":
                valueText = "ợ";
                break;
            default:
                break;
            }
            break;
        // UP CHARACTER
        case "D":
            switch textInput {
            case "d":
                valueText = "Đ";
                break;
            case "D":
                valueText = "Đ"
            default:
                break;
            }
            break;
        case "Đ":
            switch textInput {
            case "d":
                valueText = "Dd";
                break;
            case "D":
                valueText = "DD";
                break;
            default:
                break;
            }
            break;
        case "A":
            switch textInput {
            case "s":
                valueText = "Á";
                break;
            case "S":
                valueText = "Á";
                break;
            case "f":
                valueText = "À";
                break;
            case "F":
                valueText = "À";
                break;
            case "r":
                valueText = "Ả";
                break;
            case "R":
                valueText = "Ả";
                break;
            case "x":
                valueText = "Ã";
                break;
            case "X":
                valueText = "Ã";
                break;
            case "j":
                valueText = "Ạ";
                break;
            case "J":
                valueText = "Ạ";
                break;
            case "a":
                valueText = "Â";
                break;
            case "A":
                valueText = "Â";
                break;
            case "w":
                valueText = "Ă";
                break;
            case "W":
                valueText = "Ă";
                break;
            default:
                break;
            }
            break;
        case "Á":
            switch textInput {
            case "s":
                valueText = "As";
                break;
            case "f":
                valueText = "À";
                break;
            case "r":
                valueText = "Ả";
                break;
            case "x":
                valueText = "Ã";
                break;
            case "j":
                valueText = "Ạ";
                break;
            case "a":
                valueText = "Ấ";
                break;
            case "w":
                valueText = "Ắ";
                break;
            case "S":
                valueText = "AS";
                break;
            case "F":
                valueText = "À";
                break;
            case "R":
                valueText = "Ả";
                break;
            case "X":
                valueText = "Ã";
                break;
            case "J":
                valueText = "Ạ";
                break;
            case "A":
                valueText = "Ấ";
                break;
            case "W":
                valueText = "Ắ";
                break;
            default:
                break;
            }
            break;
        case "À":
            switch textInput {
            case "s":
                valueText = "Á";
                break;
            case "f":
                valueText = "Af";
                break;
            case "r":
                valueText = "Ả";
                break;
            case "x":
                valueText = "Ã";
                break;
            case "j":
                valueText = "Ạ";
                break;
            case "a":
                valueText = "Ầ";
                break;
            case "w":
                valueText = "Ằ";
                break;
            case "S":
                valueText = "Á";
                break;
            case "F":
                valueText = "AF";
                break;
            case "R":
                valueText = "Ả";
                break;
            case "X":
                valueText = "Ã";
                break;
            case "J":
                valueText = "Ạ";
                break;
            case "A":
                valueText = "Ầ";
                break;
            case "W":
                valueText = "Ằ";
                break;
            default:
                break;
            }
            break;
        case "Ả":
            switch textInput {
            case "s":
                valueText = "Á";
                break;
            case "f":
                valueText = "À";
                break;
            case "r":
                valueText = "Ar";
                break;
            case "x":
                valueText = "Ã";
                break;
            case "j":
                valueText = "Ạ";
                break;
            case "a":
                valueText = "Ấ";
                break;
            case "w":
                valueText = "Ắ";
                break;
            case "S":
                valueText = "Ả";
                break;
            case "F":
                valueText = "À";
                break;
            case "R":
                valueText = "AR";
                break;
            case "X":
                valueText = "Ã";
                break;
            case "J":
                valueText = "Ạ";
                break;
            case "A":
                valueText = "Ẩ";
                break;
            case "W":
                valueText = "Ẳ";
                break;
            default:
                break;
            }
            break;
        case "Ã":
            switch textInput {
            case "s":
                valueText = "Á";
                break;
            case "f":
                valueText = "À";
                break;
            case "r":
                valueText = "Ả";
                break;
            case "x":
                valueText = "Ax";
                break;
            case "j":
                valueText = "Ạ";
                break;
            case "a":
                valueText = "Ầ";
                break;
            case "w":
                valueText = "Ằ";
                break;
            case "S":
                valueText = "Á";
                break;
            case "F":
                valueText = "À";
                break;
            case "R":
                valueText = "Ả";
                break;
            case "X":
                valueText = "AX";
                break;
            case "J":
                valueText = "Ạ";
                break;
            case "A":
                valueText = "Ẫ";
                break;
            case "W":
                valueText = "Ẵ";
                break;
            default:
                break;
            }
            break;
        case "Ạ":
            switch textInput {
            case "s":
                valueText = "Á";
                break;
            case "f":
                valueText = "À";
                break;
            case "r":
                valueText = "Ả";
                break;
            case "x":
                valueText = "Ã";
                break;
            case "j":
                valueText = "Aj";
                break;
            case "a":
                valueText = "Ầ";
                break;
            case "w":
                valueText = "Ằ";
                break;
            case "S":
                valueText = "Á";
                break;
            case "F":
                valueText = "À";
                break;
            case "R":
                valueText = "Ả";
                break;
            case "X":
                valueText = "Ã";
                break;
            case "J":
                valueText = "AJ";
                break;
            case "A":
                valueText = "Ầ";
                break;
            case "W":
                valueText = "Ằ";
                break;
            default:
                break;
            }
            break;
        case "Ă":
            switch textInput {
            case "s":
                valueText = "Ắ";
                break;
            case "f":
                valueText = "Ằ";
                break;
            case "r":
                valueText = "Ẳ";
                break;
            case "x":
                valueText = "Ẵ";
                break;
            case "j":
                valueText = "Ặ";
                break;
            case "w":
                valueText = "Aw";
                break;
            case "a":
                valueText = "Â";
                break;
            case "S":
                valueText = "Ắ";
                break;
            case "F":
                valueText = "Ằ";
                break;
            case "R":
                valueText = "Ẳ";
                break;
            case "X":
                valueText = "Ẵ";
                break;
            case "J":
                valueText = "Ặ";
                break;
            case "W":
                valueText = "AW";
                break;
            case "A":
                valueText = "Â";
                break;
            default:
                break;
            }
            break;
        case "Ắ":
            switch textInput {
            case "s":
                valueText = "Ăs";
                break;
            case "f":
                valueText = "Ằ";
                break;
            case "r":
                valueText = "Ẳ";
                break;
            case "x":
                valueText = "Ẵ";
                break;
            case "j":
                valueText = "Ặ";
                break;
            case "w":
                valueText = "Áw";
                break;
            case "a":
                valueText = "Ấ";
                break;
            case "S":
                valueText = "ĂS";
                break;
            case "F":
                valueText = "Ằ";
                break;
            case "R":
                valueText = "Ẳ";
                break;
            case "X":
                valueText = "Ẵ";
                break;
            case "J":
                valueText = "Ặ";
                break;
            case "W":
                valueText = "ÁW";
                break;
            case "A":
                valueText = "Ấ";
                break;
            default:
                break;
            }
            break;
        case "Ằ":
            switch textInput {
            case "s":
                valueText = "Ắ";
                break;
            case "f":
                valueText = "Ăf";
                break;
            case "r":
                valueText = "Ẳ";
                break;
            case "x":
                valueText = "Ẵ";
                break;
            case "j":
                valueText = "Ặ";
                break;
            case "w":
                valueText = "Àw";
                break;
            case "a":
                valueText = "Ầ";
                break;
            case "S":
                valueText = "Ắ";
                break;
            case "F":
                valueText = "ĂF";
                break;
            case "R":
                valueText = "Ẳ";
                break;
            case "X":
                valueText = "Ẵ";
                break;
            case "J":
                valueText = "Ặ";
                break;
            case "W":
                valueText = "ÀW";
                break;
            case "A":
                valueText = "Ầ";
            default:
                break;
            }
            break;
        case "Ẳ":
            switch textInput {
            case "s":
                valueText = "Ắ";
                break;
            case "f":
                valueText = "Ằ";
                break;
            case "r":
                valueText = "Ăr";
                break;
            case "x":
                valueText = "Ẵ";
                break;
            case "j":
                valueText = "Ặ";
                break;
            case "w":
                valueText = "Ảw";
                break;
            case "a":
                valueText = "Â";
                break;
            case "S":
                valueText = "Ắ";
                break;
            case "F":
                valueText = "Ằ";
                break;
            case "R":
                valueText = "ĂR";
                break;
            case "X":
                valueText = "Ẵ";
                break;
            case "J":
                valueText = "Ặ";
                break;
            case "W":
                valueText = "ẢW";
                break;
            case "A":
                valueText = "Ẩ";
            default:
                break;
            }
            break;
        case "Ẵ":
            switch textInput {
            case "s":
                valueText = "Ắ";
                break;
            case "f":
                valueText = "Ằ";
                break;
            case "r":
                valueText = "Ẳ";
                break;
            case "x":
                valueText = "Ăx";
                break;
            case "j":
                valueText = "Ặ";
                break;
            case "w":
                valueText = "Ãw";
                break;
            case "a":
                valueText = "Ẫ";
                break;
            case "S":
                valueText = "Ắ";
                break;
            case "F":
                valueText = "Ằ";
                break;
            case "R":
                valueText = "Ẳ";
                break;
            case "X":
                valueText = "ĂX";
                break;
            case "J":
                valueText = "Ặ";
                break;
            case "W":
                valueText = "Ãw";
                break;
            case "A":
                valueText = "Ẫ";
                break;
            default:
                break;
            }
            break;
        case "Ặ":
            switch textInput {
            case "s":
                valueText = "Ắ";
                break;
            case "f":
                valueText = "Ằ";
                break;
            case "r":
                valueText = "Ẳ";
                break;
            case "x":
                valueText = "Ẵ";
                break;
            case "j":
                valueText = "Ăj";
                break;
            case "w":
                valueText = "Ạw";
                break;
            case "a":
                valueText = "Ậ";
                break;
            case "S":
                valueText = "Ắ";
                break;
            case "F":
                valueText = "Ằ";
                break;
            case "R":
                valueText = "Ẳ";
                break;
            case "X":
                valueText = "Ẵ";
                break;
            case "J":
                valueText = "ĂJ";
                break;
            case "W":
                valueText = "ẠW";
                break;
            case "A":
                valueText = "Ậ";
            default:
                break;
            }
            break;
        case "Â":
            switch textInput {
            case "s":
                valueText = "Ấ";
                break;
            case "f":
                valueText = "Ầ";
                break;
            case "r":
                valueText = "Ẩ";
                break;
            case "x":
                valueText = "Ẫ";
                break;
            case "j":
                valueText = "Ậ";
                break;
            case "a":
                valueText = "Aa";
                break;
            case "w":
                valueText = "Ă";
                break;
            case "S":
                valueText = "Ấ";
                break;
            case "F":
                valueText = "Ầ";
                break;
            case "R":
                valueText = "Ẩ";
                break;
            case "X":
                valueText = "Ẫ";
                break;
            case "J":
                valueText = "Ậ";
                break;
            case "A":
                valueText = "AA";
                break;
            case "W":
                valueText = "Ă";
                break;
            default:
                break;
            }
            break;
        case "Ấ":
            switch textInput {
            case "s":
                valueText = "ÂS";
                break;
            case "f":
                valueText = "Ầ";
                break;
            case "r":
                valueText = "Ẩ";
                break;
            case "x":
                valueText = "Ẫ";
                break;
            case "j":
                valueText = "Ậ";
                break;
            case "a":
                valueText = "Áa";
                break;
            case "w":
                valueText = "Ă";
                break;
            case "S":
                valueText = "ÂS";
                break;
            case "F":
                valueText = "Ầ";
                break;
            case "R":
                valueText = "Ẩ";
                break;
            case "X":
                valueText = "Ẫ";
                break;
            case "J":
                valueText = "Ậ";
                break;
            case "A":
                valueText = "ÁA";
                break;
            case "W":
                valueText = "Ă";
                break;
            default:
                break;
            }
            break;
        case "Ầ":
            switch textInput {
            case "s":
                valueText = "Ấ";
                break;
            case "f":
                valueText = "ÂF";
                break;
            case "r":
                valueText = "Ẩ";
                break;
            case "x":
                valueText = "Ẫ";
                break;
            case "j":
                valueText = "Ậ";
                break;
            case "a":
                valueText = "Àa";
                break;
            case "w":
                valueText = "Ă";
                break;
            case "S":
                valueText = "Ấ";
                break;
            case "F":
                valueText = "ÂF";
                break;
            case "R":
                valueText = "Ẩ";
                break;
            case "X":
                valueText = "Ẫ";
                break;
            case "J":
                valueText = "Ậ";
                break;
            case "A":
                valueText = "ÀA";
                break;
            case "W":
                valueText = "Ă";
                break;
            default:
                break;
            }
            break;
        case "Ẩ":
            switch textInput {
            case "s":
                valueText = "Ấ";
                break;
            case "f":
                valueText = "Ầ";
                break;
            case "r":
                valueText = "ÂR";
                break;
            case "x":
                valueText = "Ẫ";
                break;
            case "j":
                valueText = "Ậ";
                break;
            case "a":
                valueText = "Ảa";
                break;
            case "w":
                valueText = "Ă";
                break;
            case "S":
                valueText = "Ấ";
                break;
            case "F":
                valueText = "Ầ";
                break;
            case "R":
                valueText = "ÂR";
                break;
            case "X":
                valueText = "Ẫ";
                break;
            case "J":
                valueText = "Ậ";
                break;
            case "A":
                valueText = "ẢA";
                break;
            case "W":
                valueText = "Ă";
                break;
            default:
                break;
            }
            break;
        case "Ẫ":
            switch textInput {
            case "s":
                valueText = "Ấ";
                break;
            case "f":
                valueText = "Ầ";
                break;
            case "r":
                valueText = "Ẩ";
                break;
            case "x":
                valueText = "Âx";
                break;
            case "j":
                valueText = "Ậ";
                break;
            case "a":
                valueText = "Ãa";
                break;
            case "w":
                valueText = "Ă";
                break;
            case "S":
                valueText = "Ấ";
                break;
            case "F":
                valueText = "Ầ";
                break;
            case "R":
                valueText = "Ẩ";
                break;
            case "X":
                valueText = "ÂX";
                break;
            case "J":
                valueText = "Ậ";
                break;
            case "A":
                valueText = "ÃA";
                break;
            case "W":
                valueText = "Ă";
                break;
            default:
                break;
            }
            break;
        case "Ậ":
            switch textInput {
            case "s":
                valueText = "Ấ";
                break;
            case "f":
                valueText = "Ầ";
                break;
            case "r":
                valueText = "Ẩ";
                break;
            case "x":
                valueText = "Ẫ";
                break;
            case "j":
                valueText = "Âj";
                break;
            case "a":
                valueText = "Ạa";
                break;
            case "w":
                valueText = "Ă";
                break;
            case "S":
                valueText = "Ấ";
                break;
            case "F":
                valueText = "Ầ";
                break;
            case "R":
                valueText = "Ẩ";
                break;
            case "X":
                valueText = "Ẫ";
                break;
            case "J":
                valueText = "ÂJ";
                break;
            case "A":
                valueText = "ẠA";
                break;
            case "W":
                valueText = "Ă";
                break;
            default:
                break;
            }
            break;
        case "E":
            switch textInput {
            case "s":
                valueText = "É";
                break;
            case "f":
                valueText = "È";
                break;
            case "r":
                valueText = "Ẻ";
                break;
            case "x":
                valueText = "Ẽ";
                break;
            case "j":
                valueText = "Ẹ";
                break;
            case "e":
                valueText = "Ê";
                break;
            case "S":
                valueText = "É";
                break;
            case "F":
                valueText = "È";
                break;
            case "R":
                valueText = "Ẻ";
                break;
            case "X":
                valueText = "Ẽ";
                break;
            case "J":
                valueText = "Ẹ";
                break;
            case "E":
                valueText = "Ê";
                break;
            default:
                break;
            }
            break;
        case "É":
            switch textInput {
            case "s":
                valueText = "Es";
                break;
            case "f":
                valueText = "È";
                break;
            case "r":
                valueText = "Ẻ";
                break;
            case "x":
                valueText = "Ẽ";
                break;
            case "j":
                valueText = "Ẹ";
                break;
            case "e":
                valueText = "Ế";
                break;
            case "S":
                valueText = "ES";
                break;
            case "F":
                valueText = "È";
                break;
            case "R":
                valueText = "Ẻ";
                break;
            case "X":
                valueText = "Ẽ";
                break;
            case "J":
                valueText = "Ẹ";
                break;
            case "E":
                valueText = "Ế";
                break;
            default:
                break;
            }
            break;
        case "È":
            switch textInput {
            case "s":
                valueText = "É";
                break;
            case "f":
                valueText = "Ef";
                break;
            case "r":
                valueText = "Ẻ";
                break;
            case "x":
                valueText = "Ẽ";
                break;
            case "j":
                valueText = "Ẹ";
                break;
            case "e":
                valueText = "Ề";
                break;
            case "S":
                valueText = "É";
                break;
            case "F":
                valueText = "EF";
                break;
            case "R":
                valueText = "Ẻ";
                break;
            case "X":
                valueText = "Ẽ";
                break;
            case "J":
                valueText = "Ẹ";
                break;
            case "E":
                valueText = "Ề";
                break;
            default:
                break;
            }
            break;
        case "Ẻ":
            switch textInput {
            case "s":
                valueText = "É";
                break;
            case "f":
                valueText = "È";
                break;
            case "r":
                valueText = "Er";
                break;
            case "x":
                valueText = "Ẽ";
                break;
            case "j":
                valueText = "Ẹ";
                break;
            case "e":
                valueText = "Ể";
                break;
            case "S":
                valueText = "É";
                break;
            case "F":
                valueText = "È";
                break;
            case "R":
                valueText = "ER";
                break;
            case "X":
                valueText = "Ẽ";
                break;
            case "J":
                valueText = "Ẹ";
                break;
            case "E":
                valueText = "Ể";
                break;
            default:
                break;
            }
            break;
        case "Ẽ":
            switch textInput {
            case "s":
                valueText = "É";
                break;
            case "f":
                valueText = "È";
                break;
            case "r":
                valueText = "Ẻ";
                break;
            case "x":
                valueText = "Ex";
                break;
            case "j":
                valueText = "Ẹ";
                break;
            case "e":
                valueText = "Ễ";
                break;
            case "S":
                valueText = "É";
                break;
            case "F":
                valueText = "È";
                break;
            case "R":
                valueText = "Ẻ";
                break;
            case "X":
                valueText = "EX";
                break;
            case "J":
                valueText = "Ẹ";
                break;
            case "E":
                valueText = "Ễ";
                break;
            default:
                break;
            }
            break;
        case "Ẹ":
            switch textInput {
            case "s":
                valueText = "É";
                break;
            case "f":
                valueText = "È";
                break;
            case "r":
                valueText = "Ẻ";
                break;
            case "x":
                valueText = "Ẽ";
                break;
            case "j":
                valueText = "Ej";
                break;
            case "e":
                valueText = "Ệ";
                break;
            case "S":
                valueText = "É";
                break;
            case "F":
                valueText = "È";
                break;
            case "R":
                valueText = "Ẻ";
                break;
            case "X":
                valueText = "Ẽ";
                break;
            case "J":
                valueText = "EJ";
                break;
            case "E":
                valueText = "Ệ";
                break;
            default:
                break;
            }
            break;
        case "Ê":
            switch textInput {
            case "s":
                valueText = "Ế";
                break;
            case "f":
                valueText = "Ề";
                break;
            case "r":
                valueText = "Ể";
                break;
            case "x":
                valueText = "Ễ";
                break;
            case "j":
                valueText = "Ệ";
                break;
            case "e":
                valueText = "Ee";
                break;
            case "S":
                valueText = "Ế";
                break;
            case "F":
                valueText = "Ề";
                break;
            case "R":
                valueText = "Ể";
                break;
            case "X":
                valueText = "Ễ";
                break;
            case "J":
                valueText = "Ệ";
                break;
            case "E":
                valueText = "EE";
                break;
            default:
                break;
            }
            break;
        case "Ế":
            switch textInput {
            case "s":
                valueText = "Ês";
                break;
            case "f":
                valueText = "Ề";
                break;
            case "r":
                valueText = "Ể";
                break;
            case "x":
                valueText = "Ễ";
                break;
            case "j":
                valueText = "Ệ";
                break;
            case "e":
                valueText = "Ée";
                break;
            case "S":
                valueText = "ÊS";
                break;
            case "F":
                valueText = "Ề";
                break;
            case "R":
                valueText = "Ể";
                break;
            case "X":
                valueText = "Ễ";
                break;
            case "J":
                valueText = "Ệ";
                break;
            case "E":
                valueText = "ÉE";
                break;
            default:
                break;
            }
            break;
        case "Ề":
            switch textInput {
            case "s":
                valueText = "Ế";
                break;
            case "f":
                valueText = "Êf";
                break;
            case "r":
                valueText = "Ể";
                break;
            case "x":
                valueText = "Ễ";
                break;
            case "j":
                valueText = "Ệ";
                break;
            case "e":
                valueText = "Ee";
                break;
            case "S":
                valueText = "Ế";
                break;
            case "F":
                valueText = "ÊF";
                break;
            case "R":
                valueText = "Ể";
                break;
            case "X":
                valueText = "Ễ";
                break;
            case "J":
                valueText = "Ệ";
                break;
            case "E":
                valueText = "ÈE";
                break;
            default:
                break;
            }
            break;
        case "Ể":
            switch textInput {
            case "s":
                valueText = "Ế";
                break;
            case "f":
                valueText = "Ề";
                break;
            case "r":
                valueText = "ÊR";
                break;
            case "x":
                valueText = "Ễ";
                break;
            case "j":
                valueText = "Ệ";
                break;
            case "e":
                valueText = "Ẻe";
                break;
            case "S":
                valueText = "Ế";
                break;
            case "F":
                valueText = "Ề";
                break;
            case "R":
                valueText = "ÊR";
                break;
            case "X":
                valueText = "Ễ";
                break;
            case "J":
                valueText = "Ệ";
                break;
            case "E":
                valueText = "ẺE";
                break;
            default:
                break;
            }
            break;
        case "Ễ":
            switch textInput {
            case "s":
                valueText = "Ế";
                break;
            case "f":
                valueText = "Ề";
                break;
            case "r":
                valueText = "Ể";
                break;
            case "x":
                valueText = "Êx";
                break;
            case "j":
                valueText = "Ệ";
                break;
            case "e":
                valueText = "Ẽe";
                break;
            case "S":
                valueText = "Ế";
                break;
            case "F":
                valueText = "Ề";
                break;
            case "R":
                valueText = "Ể";
                break;
            case "X":
                valueText = "ÊX";
                break;
            case "J":
                valueText = "Ệ";
                break;
            case "E":
                valueText = "ẼE";
                break;
            default:
                break;
            }
            break;
        case "Ệ":
            switch textInput {
            case "s":
                valueText = "Ế";
                break;
            case "f":
                valueText = "Ề";
                break;
            case "r":
                valueText = "Ể";
                break;
            case "x":
                valueText = "Ễ";
                break;
            case "j":
                valueText = "Êj";
                break;
            case "e":
                valueText = "Ẹe";
                break;
            case "S":
                valueText = "Ế";
                break;
            case "F":
                valueText = "Ề";
                break;
            case "R":
                valueText = "Ể";
                break;
            case "X":
                valueText = "Ễ";
                break;
            case "J":
                valueText = "ÊJ";
                break;
            case "E":
                valueText = "ẸE";
                break;
            default:
                break;
            }
            break;
        case "I":
            switch textInput {
            case "s":
                valueText = "Í";
                break;
            case "f":
                valueText = "Ì";
                break;
            case "r":
                valueText = "Ỉ";
                break;
            case "x":
                valueText = "Ĩ";
                break;
            case "j":
                valueText = "Ị";
                break;
            case "S":
                valueText = "Í";
                break;
            case "F":
                valueText = "Ì";
                break;
            case "R":
                valueText = "Ỉ";
                break;
            case "X":
                valueText = "Ĩ";
                break;
            case "J":
                valueText = "Ị";
                break;
            default:
                break;
            }
            break;
        case "Í":
            switch textInput {
            case "s":
                valueText = "Is";
                break;
            case "f":
                valueText = "Ì";
                break;
            case "r":
                valueText = "Ỉ";
                break;
            case "x":
                valueText = "Ĩ";
                break;
            case "j":
                valueText = "Ị";
                break;
            case "S":
                valueText = "IS";
                break;
            case "F":
                valueText = "Ì";
                break;
            case "R":
                valueText = "Ỉ";
                break;
            case "X":
                valueText = "Ĩ";
                break;
            case "J":
                valueText = "Ị";
                break;
            default:
                break;
            }
            break;
        case "Ì":
            switch textInput {
            case "s":
                valueText = "Í";
                break;
            case "f":
                valueText = "If";
                break;
            case "r":
                valueText = "Ỉ";
                break;
            case "x":
                valueText = "Ĩ";
                break;
            case "j":
                valueText = "Ị";
                break;
            case "S":
                valueText = "Í";
                break;
            case "F":
                valueText = "IF";
                break;
            case "R":
                valueText = "Ỉ";
                break;
            case "X":
                valueText = "Ĩ";
                break;
            case "J":
                valueText = "Ị";
                break;
            default:
                break;
            }
            break;
        case "Ỉ":
            switch textInput {
            case "s":
                valueText = "Í";
                break;
            case "f":
                valueText = "Ì";
                break;
            case "r":
                valueText = "Ir";
                break;
            case "x":
                valueText = "Ĩ";
                break;
            case "j":
                valueText = "Ị";
                break;
            case "S":
                valueText = "Í";
                break;
            case "F":
                valueText = "Ì";
                break;
            case "R":
                valueText = "IR";
                break;
            case "X":
                valueText = "Ĩ";
                break;
            case "J":
                valueText = "Ị";
                break;
            default:
                break;
            }
            break;
        case "Ĩ":
            switch textInput {
            case "s":
                valueText = "Í";
                break;
            case "f":
                valueText = "Ì";
                break;
            case "r":
                valueText = "Ỉ";
                break;
            case "x":
                valueText = "Ix";
                break;
            case "j":
                valueText = "Ị";
                break;
            case "S":
                valueText = "Í";
                break;
            case "F":
                valueText = "Ì";
                break;
            case "R":
                valueText = "Ỉ";
                break;
            case "X":
                valueText = "IX";
                break;
            case "J":
                valueText = "Ị";
                break;
            default:
                break;
            }
            break;
        case "Ị":
            switch textInput {
            case "s":
                valueText = "Í";
                break;
            case "f":
                valueText = "Ì";
                break;
            case "r":
                valueText = "Ỉ";
                break;
            case "x":
                valueText = "Ĩ";
                break;
            case "j":
                valueText = "Ij";
                break;
            case "S":
                valueText = "Í";
                break;
            case "F":
                valueText = "Ì";
                break;
            case "R":
                valueText = "Ỉ";
                break;
            case "X":
                valueText = "Ĩ";
                break;
            case "J":
                valueText = "IJ";
                break;
            default:
                break;
            }
            break;
        case "Y":
            switch textInput {
            case "s":
                valueText = "Ý";
                break;
            case "f":
                valueText = "Ỳ";
                break;
            case "r":
                valueText = "Ỷ";
                break;
            case "x":
                valueText = "Ỹ";
                break;
            case "j":
                valueText = "Ỵ";
                break;
            case "S":
                valueText = "Ý";
                break;
            case "F":
                valueText = "Ỳ";
                break;
            case "R":
                valueText = "Ỷ";
                break;
            case "X":
                valueText = "Ỹ";
                break;
            case "J":
                valueText = "Ỵ";
                break;
            default:
                break;
            }
            break;
        case "Ý":
            switch textInput {
            case "s":
                valueText = "Ys";
                break;
            case "f":
                valueText = "Ỳ";
                break;
            case "r":
                valueText = "Ỷ";
                break;
            case "x":
                valueText = "Ỹ";
                break;
            case "j":
                valueText = "Ỵ";
                break;
            case "S":
                valueText = "YS";
                break;
            case "F":
                valueText = "Ỳ";
                break;
            case "R":
                valueText = "Ỷ";
                break;
            case "X":
                valueText = "Ỹ";
                break;
            case "J":
                valueText = "Ỵ";
                break;
            default:
                break;
            }
            break;
        case "Ỳ":
            switch textInput {
            case "s":
                valueText = "Ý";
                break;
            case "f":
                valueText = "Yf";
                break;
            case "r":
                valueText = "Ỷ";
                break;
            case "x":
                valueText = "Ỹ";
                break;
            case "j":
                valueText = "Ỵ";
                break;
            case "S":
                valueText = "Ý";
                break;
            case "F":
                valueText = "YF";
                break;
            case "R":
                valueText = "Ỷ";
                break;
            case "X":
                valueText = "Ỹ";
                break;
            case "J":
                valueText = "Ỵ";
                break;
            default:
                break;
            }
            break;
        case "Ỷ":
            switch textInput {
            case "s":
                valueText = "Ý";
                break;
            case "f":
                valueText = "Ỳ";
                break;
            case "r":
                valueText = "Yr";
                break;
            case "x":
                valueText = "Ỹ";
                break;
            case "j":
                valueText = "Ỵ";
                break;
            case "S":
                valueText = "Ý";
                break;
            case "F":
                valueText = "Ỳ";
                break;
            case "R":
                valueText = "YR";
                break;
            case "X":
                valueText = "Ỹ";
                break;
            case "J":
                valueText = "Ỵ";
                break;
            default:
                break;
            }
            break;
        case "Ỹ":
            switch textInput {
            case "s":
                valueText = "Ý";
                break;
            case "f":
                valueText = "Ỳ";
                break;
            case "r":
                valueText = "Ỷ";
                break;
            case "x":
                valueText = "Yx";
                break;
            case "j":
                valueText = "Ỵ";
                break;
            case "S":
                valueText = "Ý";
                break;
            case "F":
                valueText = "Ỳ";
                break;
            case "R":
                valueText = "Ỷ";
                break;
            case "X":
                valueText = "YX";
                break;
            case "J":
                valueText = "Ỵ";
                break;
            default:
                break;
            }
            break;
        case "Ỵ":
            switch textInput {
            case "s":
                valueText = "Ý";
                break;
            case "f":
                valueText = "Ỳ";
                break;
            case "r":
                valueText = "Ỷ";
                break;
            case "x":
                valueText = "Ỹ";
                break;
            case "j":
                valueText = "Yj";
                break;
            case "S":
                valueText = "Ý";
                break;
            case "F":
                valueText = "Ỳ";
                break;
            case "R":
                valueText = "Ỷ";
                break;
            case "X":
                valueText = "Ỹ";
                break;
            case "J":
                valueText = "YJ";
                break;
            default:
                break;
            }
            break;
        case "U":
            switch textInput {
            case "s":
                valueText = "Ú";
                break;
            case "f":
                valueText = "Ù";
                break;
            case "r":
                valueText = "Ủ";
                break;
            case "x":
                valueText = "Ũ";
                break;
            case "j":
                valueText = "Ụ";
                break;
            case "w":
                valueText = "Ư";
                break;
            case "S":
                valueText = "Ú";
                break;
            case "F":
                valueText = "Ù";
                break;
            case "R":
                valueText = "Ủ";
                break;
            case "X":
                valueText = "Ũ";
                break;
            case "J":
                valueText = "Ụ";
                break;
            case "W":
                valueText = "Ư";
                break;
            default:
                break;
            }
            break;
        case "Ú":
            switch textInput {
            case "s":
                valueText = "Us";
                break;
            case "f":
                valueText = "Ù";
                break;
            case "r":
                valueText = "Ủ";
                break;
            case "x":
                valueText = "Ũ";
                break;
            case "j":
                valueText = "Ụ";
                break;
            case "w":
                valueText = "Ư";
                break;
            case "S":
                valueText = "US";
                break;
            case "F":
                valueText = "Ù";
                break;
            case "R":
                valueText = "Ủ";
                break;
            case "X":
                valueText = "Ũ";
                break;
            case "J":
                valueText = "Ụ";
                break;
            case "W":
                valueText = "Ứ";
                break;
            default:
                break;
            }
            break;
        case "Ù":
            switch textInput {
            case "s":
                valueText = "Ú";
                break;
            case "f":
                valueText = "Uf";
                break;
            case "r":
                valueText = "Ủ";
                break;
            case "x":
                valueText = "Ũ";
                break;
            case "j":
                valueText = "Ụ";
                break;
            case "w":
                valueText = "Ư";
                break;
            case "S":
                valueText = "Ú";
                break;
            case "F":
                valueText = "UF";
                break;
            case "R":
                valueText = "Ủ";
                break;
            case "X":
                valueText = "Ũ";
                break;
            case "J":
                valueText = "Ụ";
                break;
            case "W":
                valueText = "Ừ";
                break;
            default:
                break;
            }
            break;
        case "Ủ":
            switch textInput {
            case "s":
                valueText = "Ú";
                break;
            case "f":
                valueText = "Ù";
                break;
            case "r":
                valueText = "Ur";
                break;
            case "x":
                valueText = "Ũ";
                break;
            case "j":
                valueText = "Ụ";
                break;
            case "w":
                valueText = "Ư";
                break;
            case "S":
                valueText = "Ú";
                break;
            case "F":
                valueText = "Ù";
                break;
            case "R":
                valueText = "UR";
                break;
            case "X":
                valueText = "Ũ";
                break;
            case "J":
                valueText = "Ụ";
                break;
            case "W":
                valueText = "Ử";
                break;
            default:
                break;
            }
            break;
        case "Ũ":
            switch textInput {
            case "s":
                valueText = "Ú";
                break;
            case "f":
                valueText = "Ù";
                break;
            case "r":
                valueText = "Ủ";
                break;
            case "x":
                valueText = "Ux";
                break;
            case "j":
                valueText = "Ụ";
                break;
            case "w":
                valueText = "Ư";
                break;
            case "S":
                valueText = "Ú";
                break;
            case "F":
                valueText = "Ù";
                break;
            case "R":
                valueText = "Ủ";
                break;
            case "X":
                valueText = "UX";
                break;
            case "J":
                valueText = "Ụ";
                break;
            case "W":
                valueText = "Ữ";
                break;
            default:
                break;
            }
            break;
        case "Ụ":
            switch textInput {
            case "s":
                valueText = "Ú";
                break;
            case "f":
                valueText = "Ù";
                break;
            case "r":
                valueText = "Ủ";
                break;
            case "x":
                valueText = "Ũ";
                break;
            case "j":
                valueText = "Uj";
                break;
            case "w":
                valueText = "Ư";
                break;
            case "S":
                valueText = "Ú";
                break;
            case "F":
                valueText = "Ù";
                break;
            case "R":
                valueText = "Ủ";
                break;
            case "X":
                valueText = "Ũ";
                break;
            case "J":
                valueText = "UJ";
                break;
            case "W":
                valueText = "Ự";
                break;
            default:
                break;
            }
            break;
        case "Ư":
            switch textInput {
            case "s":
                valueText = "Ứ";
                break;
            case "f":
                valueText = "Ừ";
                break;
            case "r":
                valueText = "Ử";
                break;
            case "x":
                valueText = "Ữ";
                break;
            case "j":
                valueText = "Ự";
                break;
            case "w":
                valueText = "Uw";
                break;
            case "S":
                valueText = "Ứ";
                break;
            case "F":
                valueText = "Ừ";
                break;
            case "R":
                valueText = "Ử";
                break;
            case "X":
                valueText = "Ữ";
                break;
            case "J":
                valueText = "Ự";
                break;
            case "W":
                valueText = "UW";
                break;
            default:
                break;
            }
            break;
        case "Ứ":
            switch textInput {
            case "s":
                valueText = "Ưs";
                break;
            case "f":
                valueText = "Ừ";
                break;
            case "r":
                valueText = "Ử";
                break;
            case "x":
                valueText = "Ữ";
                break;
            case "j":
                valueText = "Ự";
                break;
            case "w":
                valueText = "Úw";
                break;
            case "S":
                valueText = "ƯS";
                break;
            case "F":
                valueText = "Ừ";
                break;
            case "R":
                valueText = "Ử";
                break;
            case "X":
                valueText = "Ữ";
                break;
            case "J":
                valueText = "Ự";
                break;
            case "W":
                valueText = "ÚW";
                break;
            default:
                break;
            }
            break;
        case "Ừ":
            switch textInput {
            case "s":
                valueText = "Ứ";
                break;
            case "f":
                valueText = "Ưf";
                break;
            case "r":
                valueText = "Ử";
                break;
            case "x":
                valueText = "Ữ";
                break;
            case "j":
                valueText = "Ự";
                break;
            case "w":
                valueText = "Ùw";
                break;
            case "S":
                valueText = "Ứ";
                break;
            case "F":
                valueText = "ƯF";
                break;
            case "R":
                valueText = "Ử";
                break;
            case "X":
                valueText = "Ữ";
                break;
            case "J":
                valueText = "Ự";
                break;
            case "W":
                valueText = "ÙW";
                break;
            default:
                break;
            }
            break;
        case "Ử":
            switch textInput {
            case "s":
                valueText = "Ứ";
                break;
            case "f":
                valueText = "Ừ";
                break;
            case "r":
                valueText = "Ưr";
                break;
            case "x":
                valueText = "Ữ";
                break;
            case "j":
                valueText = "Ự";
                break;
            case "w":
                valueText = "Ủw";
                break;
            case "S":
                valueText = "Ứ";
                break;
            case "F":
                valueText = "Ừ";
                break;
            case "R":
                valueText = "ƯR";
                break;
            case "X":
                valueText = "Ữ";
                break;
            case "J":
                valueText = "Ự";
                break;
            case "W":
                valueText = "ỦW";
                break;
            default:
                break;
            }
            break;
        case "Ữ":
            switch textInput {
            case "s":
                valueText = "Ứ";
                break;
            case "f":
                valueText = "Ừ";
                break;
            case "r":
                valueText = "Ử";
                break;
            case "x":
                valueText = "Ưx";
                break;
            case "j":
                valueText = "Ự";
                break;
            case "w":
                valueText = "Ũw";
                break;
            case "S":
                valueText = "Ứ";
                break;
            case "F":
                valueText = "Ừ";
                break;
            case "R":
                valueText = "Ử";
                break;
            case "X":
                valueText = "ƯX";
                break;
            case "J":
                valueText = "Ự";
                break;
            case "W":
                valueText = "ŨW";
                break;
            default:
                break;
            }
            break;
        case "Ự":
            switch textInput {
            case "s":
                valueText = "Ứ";
                break;
            case "f":
                valueText = "Ừ";
                break;
            case "r":
                valueText = "Ử";
                break;
            case "x":
                valueText = "Ữ";
                break;
            case "j":
                valueText = "Ưj";
                break;
            case "w":
                valueText = "Ụw";
                break;
            case "S":
                valueText = "Ứ";
                break;
            case "F":
                valueText = "Ừ";
                break;
            case "R":
                valueText = "Ử";
                break;
            case "X":
                valueText = "Ữ";
                break;
            case "J":
                valueText = "ƯJ";
                break;
            case "W":
                valueText = "ỤW";
                break;
            default:
                break;
            }
            break;
        case "O":
            switch textInput {
            case "s":
                valueText = "Ó";
                break;
            case "f":
                valueText = "Ò";
                break;
            case "r":
                valueText = "Ỏ";
                break;
            case "x":
                valueText = "Õ";
                break;
            case "j":
                valueText = "Ọ";
                break;
            case "w":
                valueText = "Ơ";
                break;
            case "o":
                valueText = "Ô";
                break;
            case "S":
                valueText = "Ó";
                break;
            case "F":
                valueText = "Ò";
                break;
            case "R":
                valueText = "Ỏ";
                break;
            case "X":
                valueText = "Õ";
                break;
            case "J":
                valueText = "Ọ";
                break;
            case "W":
                valueText = "Ơ";
                break;
            case "O":
                valueText = "Ô";
                break;
            default:
                break;
            }
            break;
        case "Ó":
            switch textInput {
            case "s":
                valueText = "Os";
                break;
            case "f":
                valueText = "Ò";
                break;
            case "r":
                valueText = "Ỏ";
                break;
            case "x":
                valueText = "Õ";
                break;
            case "j":
                valueText = "Ọ";
                break;
            case "w":
                valueText = "Ớ";
                break;
            case "o":
                valueText = "Ố";
                break;
            case "S":
                valueText = "OS";
                break;
            case "F":
                valueText = "Ò";
                break;
            case "R":
                valueText = "Ỏ";
                break;
            case "X":
                valueText = "Õ";
                break;
            case "J":
                valueText = "Ọ";
                break;
            case "W":
                valueText = "Ớ";
                break;
            case "O":
                valueText = "Ố";
                break;
            default:
                break;
            }
            break;
        case "Ò":
            switch textInput {
            case "s":
                valueText = "Ó";
                break;
            case "f":
                valueText = "Of";
                break;
            case "r":
                valueText = "Ỏ";
                break;
            case "x":
                valueText = "Õ";
                break;
            case "j":
                valueText = "Ọ";
                break;
            case "w":
                valueText = "Ờ";
                break;
            case "o":
                valueText = "Ồ";
                break;
            case "S":
                valueText = "Ó";
                break;
            case "F":
                valueText = "OF";
                break;
            case "R":
                valueText = "Ỏ";
                break;
            case "X":
                valueText = "Õ";
                break;
            case "J":
                valueText = "Ọ";
                break;
            case "W":
                valueText = "Ờ";
                break;
            case "O":
                valueText = "Ồ";
                break;
            default:
                break;
            }
            break;
        case "Ỏ":
            switch textInput {
            case "s":
                valueText = "Ó";
                break;
            case "f":
                valueText = "Ò";
                break;
            case "r":
                valueText = "Or";
                break;
            case "x":
                valueText = "Õ";
                break;
            case "j":
                valueText = "Ọ";
                break;
            case "w":
                valueText = "Ơ";
                break;
            case "o":
                valueText = "Ô";
                break;
            case "S":
                valueText = "Ó";
                break;
            case "F":
                valueText = "Ò";
                break;
            case "R":
                valueText = "OR";
                break;
            case "X":
                valueText = "Õ";
                break;
            case "J":
                valueText = "Ọ";
                break;
            case "W":
                valueText = "Ở";
                break;
            case "O":
                valueText = "Ổ";
                break;
            default:
                break;
            }
            break;
        case "Õ":
            switch textInput {
            case "s":
                valueText = "Ó";
                break;
            case "f":
                valueText = "Ò";
                break;
            case "r":
                valueText = "Ỏ";
                break;
            case "x":
                valueText = "Ox";
                break;
            case "j":
                valueText = "Ọ";
                break;
            case "w":
                valueText = "Ỡ";
                break;
            case "o":
                valueText = "Ỗ";
                break;
            case "S":
                valueText = "Ó";
                break;
            case "F":
                valueText = "Ò";
                break;
            case "R":
                valueText = "Ỏ";
                break;
            case "X":
                valueText = "OX";
                break;
            case "J":
                valueText = "Ọ";
                break;
            case "W":
                valueText = "Ỡ";
                break;
            case "O":
                valueText = "Ỗ";
                break;
            default:
                break;
            }
            break;
        case "Ọ":
            switch textInput {
            case "s":
                valueText = "Ó";
                break;
            case "f":
                valueText = "Ò";
                break;
            case "r":
                valueText = "Ỏ";
                break;
            case "x":
                valueText = "Õ";
                break;
            case "j":
                valueText = "Oj";
                break;
            case "w":
                valueText = "Ợ";
                break;
            case "o":
                valueText = "Ộ";
                break;
            case "S":
                valueText = "Ó";
                break;
            case "F":
                valueText = "Ò";
                break;
            case "R":
                valueText = "Ỏ";
                break;
            case "X":
                valueText = "Õ";
                break;
            case "J":
                valueText = "OJ";
                break;
            case "W":
                valueText = "Ợ";
                break;
            case "O":
                valueText = "Ộ";
                break;
            default:
                break;
            }
            break;
        case "Ơ":
            switch textInput {
            case "s":
                valueText = "Ớ";
                break;
            case "f":
                valueText = "Ờ";
                break;
            case "r":
                valueText = "Ở";
                break;
            case "x":
                valueText = "Ỡ";
                break;
            case "j":
                valueText = "Ợ";
                break;
            case "w":
                valueText = "Ow";
                break;
            case "o":
                valueText = "Ô";
                break;
            case "S":
                valueText = "Ớ";
                break;
            case "F":
                valueText = "Ờ";
                break;
            case "R":
                valueText = "Ở";
                break;
            case "X":
                valueText = "Ỡ";
                break;
            case "J":
                valueText = "Ợ";
                break;
            case "W":
                valueText = "OW";
                break;
            case "O":
                valueText = "Ô";
                break;
            default:
                break;
            }
            break;
        case "Ớ":
            switch textInput {
            case "s":
                valueText = "Ơs";
                break;
            case "f":
                valueText = "Ờ";
                break;
            case "r":
                valueText = "Ở";
                break;
            case "x":
                valueText = "Ỡ";
                break;
            case "j":
                valueText = "Ợ";
                break;
            case "w":
                valueText = "Ów";
                break;
            case "o":
                valueText = "Ố";
                break;
            case "S":
                valueText = "ƠS";
                break;
            case "F":
                valueText = "Ờ";
                break;
            case "R":
                valueText = "Ở";
                break;
            case "X":
                valueText = "Ỡ";
                break;
            case "J":
                valueText = "Ợ";
                break;
            case "W":
                valueText = "ÓW";
                break;
            case "O":
                valueText = "Ố";
                break;
            default:
                break;
            }
            break;
        case "Ờ":
            switch textInput {
            case "s":
                valueText = "Ớ";
                break;
            case "f":
                valueText = "Ơf";
                break;
            case "r":
                valueText = "Ở";
                break;
            case "x":
                valueText = "Ỡ";
                break;
            case "j":
                valueText = "Ợ";
                break;
            case "w":
                valueText = "Òw";
                break;
            case "o":
                valueText = "Ồ";
                break;
            case "S":
                valueText = "Ớ";
                break;
            case "F":
                valueText = "ƠF";
                break;
            case "R":
                valueText = "Ở";
                break;
            case "X":
                valueText = "Ỡ";
                break;
            case "J":
                valueText = "Ợ";
                break;
            case "W":
                valueText = "ÒW";
                break;
            case "O":
                valueText = "Ồ";
                break;
            default:
                break;
            }
            break;
        case "Ở":
            switch textInput {
            case "s":
                valueText = "Ớ";
                break;
            case "f":
                valueText = "Ờ";
                break;
            case "r":
                valueText = "Ơr";
                break;
            case "x":
                valueText = "Ỡ";
                break;
            case "j":
                valueText = "Ợ";
                break;
            case "w":
                valueText = "Ỏw";
                break;
            case "o":
                valueText = "Ổ";
                break;
            case "S":
                valueText = "Ớ";
                break;
            case "F":
                valueText = "Ờ";
                break;
            case "R":
                valueText = "ƠR";
                break;
            case "X":
                valueText = "Ỡ";
                break;
            case "J":
                valueText = "Ợ";
                break;
            case "W":
                valueText = "ỎW";
                break;
            case "O":
                valueText = "Ổ";
                break;
            default:
                break;
            }
            break;
        case "Ỡ":
            switch textInput {
            case "s":
                valueText = "Ớ";
                break;
            case "f":
                valueText = "Ờ";
                break;
            case "r":
                valueText = "Ở";
                break;
            case "x":
                valueText = "Ơx";
                break;
            case "j":
                valueText = "Ợ";
                break;
            case "w":
                valueText = "Õw";
                break;
            case "o":
                valueText = "Ỗ";
                break;
            case "S":
                valueText = "Ớ";
                break;
            case "F":
                valueText = "Ờ";
                break;
            case "R":
                valueText = "Ở";
                break;
            case "X":
                valueText = "ƠX";
                break;
            case "J":
                valueText = "Ợ";
                break;
            case "W":
                valueText = "ÕW";
                break;
            case "O":
                valueText = "Ỗ";
                break;
            default:
                break;
            }
            break;
        case "Ợ":
            switch textInput {
            case "s":
                valueText = "Ớ";
                break;
            case "f":
                valueText = "Ờ";
                break;
            case "r":
                valueText = "Ở";
                break;
            case "x":
                valueText = "Ỡ";
                break;
            case "j":
                valueText = "Ơj";
                break;
            case "w":
                valueText = "Ọw";
                break;
            case "o":
                valueText = "ÔJ";
                break;
            case "S":
                valueText = "Ớ";
                break;
            case "F":
                valueText = "Ờ";
                break;
            case "R":
                valueText = "Ở";
                break;
            case "X":
                valueText = "Ỡ";
                break;
            case "J":
                valueText = "ƠJ";
                break;
            case "W":
                valueText = "ỌW";
                break;
            case "O":
                valueText = "Ộ";
                break;
            default:
                break;
            }
            break;
        case "Ô":
            switch textInput {
            case "s":
                valueText = "Ố";
                break;
            case "f":
                valueText = "Ồ";
                break;
            case "r":
                valueText = "Ổ";
                break;
            case "x":
                valueText = "Ỗ";
                break;
            case "j":
                valueText = "Ộ";
                break;
            case "o":
                valueText = "Oo";
                break;
            case "w":
                valueText = "Ơ";
                break;
            case "S":
                valueText = "Ố";
                break;
            case "F":
                valueText = "Ồ";
                break;
            case "R":
                valueText = "Ổ";
                break;
            case "X":
                valueText = "Ỗ";
                break;
            case "J":
                valueText = "Ộ";
                break;
            case "O":
                valueText = "OO";
                break;
            case "W":
                valueText = "Ơ";
                break;
            default:
                break;
            }
            break;
        case "Ố":
            switch textInput {
            case "s":
                valueText = "Ôs";
                break;
            case "f":
                valueText = "Ồ";
                break;
            case "r":
                valueText = "Ổ";
                break;
            case "x":
                valueText = "Ỗ";
                break;
            case "j":
                valueText = "Ộ";
                break;
            case "o":
                valueText = "Óo";
                break;
            case "w":
                valueText = "Ớ";
                break;
            case "S":
                valueText = "ÔS";
                break;
            case "F":
                valueText = "Ồ";
                break;
            case "R":
                valueText = "Ổ";
                break;
            case "X":
                valueText = "Ỗ";
                break;
            case "J":
                valueText = "Ộ";
                break;
            case "O":
                valueText = "ÓO";
                break;
            case "W":
                valueText = "Ớ";
                break;
            default:
                break;
            }
            break;
        case "Ồ":
            switch textInput {
            case "s":
                valueText = "Ố";
                break;
            case "f":
                valueText = "Ôf";
                break;
            case "r":
                valueText = "Ổ";
                break;
            case "x":
                valueText = "Ỗ";
                break;
            case "j":
                valueText = "Ộ";
                break;
            case "o":
                valueText = "Òo";
                break;
            case "w":
                valueText = "Ờ";
                break;
            case "S":
                valueText = "Ố";
                break;
            case "F":
                valueText = "ÔF";
                break;
            case "R":
                valueText = "Ổ";
                break;
            case "X":
                valueText = "Ỗ";
                break;
            case "J":
                valueText = "Ộ";
                break;
            case "O":
                valueText = "ÒO";
                break;
            case "W":
                valueText = "Ờ";
                break;
            default:
                break;
            }
            break;
        case "Ổ":
            switch textInput {
            case "s":
                valueText = "Ố";
                break;
            case "f":
                valueText = "Ồ";
                break;
            case "r":
                valueText = "Ôr";
                break;
            case "x":
                valueText = "Ỗ";
                break;
            case "j":
                valueText = "Ộ";
                break;
            case "o":
                valueText = "Ỏo";
                break;
            case "w":
                valueText = "Ở";
                break;
            case "S":
                valueText = "Ố";
                break;
            case "F":
                valueText = "Ồ";
                break;
            case "R":
                valueText = "ÔR";
                break;
            case "X":
                valueText = "Ỗ";
                break;
            case "J":
                valueText = "Ộ";
                break;
            case "O":
                valueText = "ỎO";
                break;
            case "W":
                valueText = "Ở";
                break;
            default:
                break;
            }
            break;
        case "Ỗ":
            switch textInput {
            case "s":
                valueText = "Ố";
                break;
            case "f":
                valueText = "Ồ";
                break;
            case "r":
                valueText = "Ổ";
                break;
            case "x":
                valueText = "Ôx";
                break;
            case "j":
                valueText = "Ộ";
                break;
            case "o":
                valueText = "Õo";
                break;
            case "w":
                valueText = "Ỡ";
                break;
            case "S":
                valueText = "Ố";
                break;
            case "F":
                valueText = "Ồ";
                break;
            case "R":
                valueText = "Ổ";
                break;
            case "X":
                valueText = "ÔX";
                break;
            case "J":
                valueText = "Ộ";
                break;
            case "O":
                valueText = "ÕO";
                break;
            case "W":
                valueText = "Ỡ";
                break;
            default:
                break;
            }
            break;
        case "Ộ":
            switch textInput {
            case "s":
                valueText = "Ố";
                break;
            case "f":
                valueText = "Ồ";
                break;
            case "r":
                valueText = "Ổ";
                break;
            case "x":
                valueText = "Ỗ";
                break;
            case "j":
                valueText = "Ôj";
                break;
            case "o":
                valueText = "Ọo";
                break;
            case "w":
                valueText = "Ợ";
                break;
            case "S":
                valueText = "Ố";
                break;
            case "F":
                valueText = "Ồ";
                break;
            case "R":
                valueText = "Ổ";
                break;
            case "X":
                valueText = "Ỗ";
                break;
            case "J":
                valueText = "ÔJ";
                break;
            case "O":
                valueText = "ỌO";
                break;
            case "W":
                valueText = "Ợ";
                break;
            default:
                break;
            }
            break;
        default:
            break
        }
        if(valueText != textInput){
            textDocumentProxy.deleteBackward();
        }
        return valueText;
    }
}
