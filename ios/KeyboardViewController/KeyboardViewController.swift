//
//  KeyboardViewController.swift
//  CustomKeyboard
//
//  Created by utnhim on 12/11/20.
//

import UIKit

class KeyboardViewController:  UIInputViewController, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    
    @IBOutlet weak var heightViewInput: NSLayoutConstraint!
    @IBOutlet var nextKeyboardButton: UIButton!
    
    @IBOutlet weak var stackView1: UIStackView!
    
    @IBOutlet weak var stackView2: UIStackView!
    
    @IBOutlet weak var stackView3: UIStackView!
    @IBOutlet weak var collectionViewEmoji: UICollectionView!
    
    @IBOutlet weak var viewKeyboardEmoji: UIView!
    var expandedHeight : CGFloat = 1
    // character low cap
    var characterLowcap1 = ["q","w","e","r","t","y","u","i","o","p"];
    var characterLowcap2 = ["a","s","d","f","g","h","j","k","l"];
    var characterLowcap3 = ["z","x","c","v","b","n","m"];
    // character upcap
    var characterUpcap1 = ["Q","W","E","R","T","Y","U","I","O","P"];
    var characterUpcap2 = ["A","S","D","F","G","H","J","K","L"];
    var characterUpcap3 = ["Z","X","C","V","B","N","M"];
    // keyboar number symbol
    var characterNumberSymbol1 = ["1","2","3","4","5","6","7","8","9","0"];
    var characterNumberSymbol2 = ["-","/",":",";","(",")","₫","&","@","\u{22}"];
    var characterNumberSymbol3 = [".",",","?","!","'"];
    // keyboar symbol
    var characterSymbol1 = ["[","]","{","}","#","%","^","*","+","="];
    var characterSymbol2 = ["_","\u{5c}","\u{7c}","\u{7e}","\u{3c}","\u{3e}","\u{24}","\u{A5}","€","\u{b7}"];
    var saveTextInput = [""];
    var constraint228 = NSLayoutConstraint();
    var constraint458 = NSLayoutConstraint();
    var boleanShift = false;
    var boleanDoubleShift = false;
    var boleanNumber = false;
    
    
    @IBOutlet weak var buttonShift: UIButton!
    @IBOutlet weak var buttonNumber: UIButton!
    @IBOutlet weak var buttonDelete: UIButton!
    @IBOutlet weak var buttonEtop: UIButton!
    @IBOutlet weak var buttonSmile: UIButton!
    @IBOutlet weak var buttonReturn: UIButton!
    @IBOutlet weak var space: UIButton!
    
    var listEmoji = [String]();
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        // Add custom view sizing constraints here
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        stackView2.arrangedSubviews.forEach({$0.removeFromSuperview()});
        let userDefaults = UserDefaults(suiteName: "group.unkeyboard");
        let name = userDefaults?.string(forKey: "name");
        initViewDefault();
        
        constraint228 = NSLayoutConstraint(item: self.view,
                                           attribute: .height,
                                           relatedBy: .equal,
                                           toItem: nil,
                                           attribute: .notAnAttribute,
                                           multiplier: 0.0,
                                           constant: 228) // Set custom height here
        constraint458 = NSLayoutConstraint(item: self.view,
                                           attribute: .height,
                                           relatedBy: .equal,
                                           toItem: nil,
                                           attribute: .notAnAttribute,
                                           multiplier: 0.0,
                                           constant: 485) // Set custom height here
        
                self.view.removeConstraint(constraint458);
                self.view.addConstraint(constraint228)
                heightViewInput.constant = 0;
//        self.view.removeConstraint(constraint228);
//        self.view.addConstraint(constraint458)
//        heightViewInput.constant = 285;
        
        buttonShift.layer.cornerRadius = 5;
        buttonNumber.layer.cornerRadius = 5;
        buttonDelete.layer.cornerRadius = 5;
        buttonEtop.layer.cornerRadius = 5;
        buttonReturn.layer.cornerRadius = 5;
        buttonSmile.layer.cornerRadius = 5;
        space.layer.cornerRadius = 5;
        
        buttonReturn.addTarget(self, action: #selector(textFieldShouldReturn(_:)), for: .touchUpInside)
        
        initListEmoji()

        collectionViewEmoji.delegate = self;
        collectionViewEmoji.dataSource = self;
        collectionViewEmoji.register(IconCollectionViewCell.nib(), forCellWithReuseIdentifier: IconCollectionViewCell.identifier)    }
    
    func initListEmoji()->Void{
        let emojiRanges = [
            0x1F600...0x1F64F, // Emoticons
//            8400...8447, // Combining Diacritical Marks for Symbols
//            9100...9300, // Misc items
//            0x2600...0x26FF,   // Misc symbols
//            0x2700...0x27BF,   // Dingbats
//            0xFE00...0xFE0F,   // Variation Selectors
//            0x1F018...0x1F270, // Various asian characters
            0x1F300...0x1F5FF, // Misc Symbols and Pictographs
//            0x1F680...0x1F6FF, // Transport and Map
            0x1F1E6...0x1F1FF, // Regional country flags
            0x1F900...0x1F9FF,  // Supplemental Symbols and Pictographs
//            65024...65039, // Variation selectorÏ
        ]

        for range in emojiRanges {
            for i in range {
                let c = String(UnicodeScalar(i)!)
                listEmoji.append(c);
            }
        }
//        let emojiRanges = [
//            0x1F601...0x1F64F,
//            0x2702...0x27B0,
//            0x1F680...0x1F6C0,
//            0x1F170...0x1F251
//        ]
//
//        for range in emojiRanges {
//            for i in range {
//                let c = String(UnicodeScalar(i)!)
//                listEmoji.append(c);
//            }
//        }
    }
    
    //init icon emoji
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionViewEmoji.deselectItem(at: indexPath, animated: true);
        textDocumentProxy.insertText(listEmoji[indexPath.row])
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listEmoji.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: IconCollectionViewCell.identifier, for: indexPath) as! IconCollectionViewCell
        cell.configure(with: listEmoji[indexPath.row])
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 6
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 6
    }
    @IBAction func clickDeleteEmoji(_ sender: Any) {
        textDocumentProxy.deleteBackward()
    }
    @IBAction func clickHideKeyboardEmoji(_ sender: Any) {
        viewKeyboardEmoji.isHidden = true
    }
    
    func clearItemStackView(){
        stackView1.arrangedSubviews.forEach({$0.removeFromSuperview()});
        stackView2.arrangedSubviews.forEach({$0.removeFromSuperview()});
        stackView3.arrangedSubviews.forEach({$0.removeFromSuperview()});
    }
    
    func addButton(title: String, stackView: UIStackView){
        let button = UIButton();
        button.setTitle(title, for: .normal);
        button.setTitleColor(.black, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 25.5, weight: .light);
        button.titleLabel?.textAlignment = .justified;
        button.backgroundColor = UIColor.white;
        button.layer.cornerRadius = 5;
        button.layer.shadowOpacity = 0.2;
        button.layer.shadowOffset = CGSize(width: 0, height: 2);
        button.layer.shadowColor = UIColor.black.cgColor;
        button.addTarget(self, action: #selector(keyPressed), for: .touchUpInside)
        stackView.addArrangedSubview(button);
    }
    
    func initViewDefault(){
        for character in characterLowcap1 {
            addButton(title: character, stackView: stackView1);
        }
        stackView2.directionalLayoutMargins = NSDirectionalEdgeInsets(top: 0, leading: 24, bottom: 0, trailing: 24)
        for character in characterLowcap2 {
            addButton(title: character, stackView: stackView2);
        }
        for character in characterLowcap3 {
            addButton(title: character, stackView: stackView3);
        }
    }
    
    func initViewUpcap(){
        for character in characterUpcap1 {
            addButton(title: character, stackView: stackView1);
        }
        stackView2.directionalLayoutMargins = NSDirectionalEdgeInsets(top: 0, leading: 24, bottom: 0, trailing: 24)
        for character in characterUpcap2 {
            addButton(title: character, stackView: stackView2);
        }
        for character in characterUpcap3 {
            addButton(title: character, stackView: stackView3);
        }
    }
    func initViewNumberSymbol(){
        for character in characterNumberSymbol1 {
            addButton(title: character, stackView: stackView1);
        }
        stackView2.directionalLayoutMargins = NSDirectionalEdgeInsets(top: 0, leading: 24, bottom: 0, trailing: 24)
        for character in characterNumberSymbol2 {
            addButton(title: character, stackView: stackView2);
        }
        for character in characterNumberSymbol3 {
            addButton(title: character, stackView: stackView3);
        }
    }
    
    func initViewCharacterSymbol(){
        for character in characterSymbol1 {
            addButton(title: character, stackView: stackView1);
        }
        stackView2.directionalLayoutMargins = NSDirectionalEdgeInsets(top: 0, leading: 24, bottom: 0, trailing: 24)
        for character in characterSymbol2 {
            addButton(title: character, stackView: stackView2);
        }
        for character in characterNumberSymbol3 {
            addButton(title: character, stackView: stackView3);
        }
    }
    
    override func viewWillLayoutSubviews() {
        //        self.nextKeyboardButton.isHidden = !self.needsInputModeSwitchKey
        //        super.viewWillLayoutSubviews()
    }
    
    override func textWillChange(_ textInput: UITextInput?) {
        // The app is about to change the document's contents. Perform any preparation here.
    }
    
    override func textDidChange(_ textInput: UITextInput?) {
        // The app has just changed the document's contents, the document context has been updated.
        //        var textColor: UIColor
        //        let proxy = self.textDocumentProxy
        //        if proxy.keyboardAppearance == UIKeyboardAppearance.dark {
        //            textColor = UIColor.white
        //        } else {
        //            textColor = UIColor.black
        //        }
        //        self.nextKeyboardButton.setTitleColor(textColor, for: [])
        
    }
    @IBAction func onClickSmile(_ sender: Any) {
        viewKeyboardEmoji.isHidden = false
    }
    
    @IBAction func onClickShift(_ sender: Any) {
        
        if(buttonShift.titleLabel?.text == "#+="){
            buttonShift.setTitle("123", for: .normal)
            buttonShift.titleLabel?.font = .systemFont(ofSize: 14);
            clearItemStackView()
            initViewCharacterSymbol()
        }else if(buttonShift.titleLabel?.text == "123"){
            buttonShift.setTitle("#+=", for: .normal)
            buttonShift.titleLabel?.font = .systemFont(ofSize: 14);
            clearItemStackView()
            initViewNumberSymbol()
        }else{
            if(!boleanShift){
                clearItemStackView();
                initViewUpcap();
                boleanShift = true;
            }else{
                clearItemStackView();
                initViewDefault()
                boleanShift = false;
                boleanDoubleShift = false;
                buttonShift.setTitle("⇧", for: .normal)
                buttonShift.titleLabel?.font = .systemFont(ofSize: 25);
            }
        }
    }
    
    @IBAction func onClickDoubleShift(_ sender: Any) {
        boleanDoubleShift = true;
        boleanShift = true;
        clearItemStackView();
        initViewUpcap();
        buttonShift.setTitle("⇪", for: .normal)
        buttonShift.titleLabel?.font = .systemFont(ofSize: 25);
    }
    
    
    @IBAction func onClickNumber(_ sender: Any) {
        if(!boleanNumber){
            clearItemStackView();
            initViewNumberSymbol();
            boleanNumber = true;
            buttonShift.setTitle("#+=", for: .normal)
            buttonShift.titleLabel?.font = .systemFont(ofSize: 14);
            buttonNumber.setTitle("ABC", for: .normal)
        }else{
            clearItemStackView();
            initViewDefault();
            boleanNumber = false;
            buttonShift.setTitle("⇧", for: .normal)
            buttonShift.titleLabel?.font = .systemFont(ofSize: 23);
            buttonNumber.setTitle("123", for: .normal)
        }
    }
    
    @IBAction func onClickEtop(_ sender: Any) {
        // set layout
        //        if(heightViewInput.constant == 257){
        //            self.view.removeConstraint(constraint458);
        //            self.view.addConstraint(constraint228)
        //            heightViewInput.constant = 0;
        //        }else{
        //            self.view.removeConstraint(constraint228);
        //            self.view.addConstraint(constraint458);
        //            heightViewInput.constant = 257;
        //        }
        //        for i in 0x1F601...0x1F64F {
        //            let c = String(UnicodeScalar(i) ?? "-")
        //            print(c)
        //        }
    }
    
    @IBAction func onClickReturn(_ sender: Any) {
        //        UIApplication.sharedApplication().sendAction("resignFirstResponder", to:nil, from:nil, forEvent:nil)
        //        self.advanceToNextInputMode()
        //        self.done
        self.dismissKeyboard()
    }
    @objc func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    
    @objc func openURL(_ url: URL) {
        return
    }
    
    func openApp(_ urlstring:String) {
        
        var responder: UIResponder? = self as UIResponder
        let selector = #selector(openURL(_:))
        while responder != nil {
            if responder!.responds(to: selector) && responder != self {
                responder!.perform(selector, with: URL(string: urlstring)!)
                return
            }
            responder = responder?.next
        }
    }
    @IBAction func clickSpace(_ sender: Any) {
        textDocumentProxy.insertText(" ");
        saveTextInput = [""];
        //        openApp ("https://thitruongsi.com")
    }
    
    @IBAction func clickDelete(_ sender: Any) {
        textDocumentProxy.deleteBackward();
        let precedingText = textDocumentProxy.documentContextBeforeInput ?? "";
        if(saveTextInput.count>1){
            for _ in 0..<precedingText.count{
                textDocumentProxy.deleteBackward();
            }
            textDocumentProxy.insertText(saveTextInput[saveTextInput.count-1]);
            saveTextInput.removeLast();
        }
    }
    
    @IBAction func keyPressed(_ sender: UIButton){
        let userDefaults = UserDefaults(suiteName: "group.unkeyboard");
        let name = userDefaults?.string(forKey: "name");
        
        if(boleanShift && !boleanDoubleShift){
            clearItemStackView();
            initViewDefault()
        }
        
        var string = sender.titleLabel!.text;
        let precedingText = textDocumentProxy.documentContextBeforeInput ?? "";
        let afterInputText = textDocumentProxy.documentContextAfterInput ?? "";
        
        saveTextInput.append(precedingText);
        let arrayPrecedingText = precedingText.map{String($0)};
        if(arrayPrecedingText.count > 0 && arrayPrecedingText[arrayPrecedingText.count-1] == " "){
            textDocumentProxy.insertText("\(string!)");
            return;
        }
        
        let splitString = precedingText.split(separator: " ");
        if(splitString.count>0){
            let value = unikeyCustomsAddText(textBefore: String(splitString[splitString.count-1]), textInput: string!);            string = value;
        }
        
        textDocumentProxy.insertText("\(string!)");
    }
    
    func unikeyCustomsAddText(textBefore: String, textInput: String)->String{
        let arrayTextBefore = textBefore.map { String($0) };
        let characterLastest = arrayTextBefore[arrayTextBefore.count-1];
        
        let typeConvert = initTypeConvertCharacter(arrayString: arrayTextBefore, textInput: textInput);
        
        switch typeConvert{
        case 1:
            if(arrayTextBefore.count > 1 ){
                let arrayTwo = ["k","z","q","w","j",
                                "K","Z","Q","W","J"];
                let arraySpecialWithK = ["i","í","ì","ỉ","ĩ","ị",
                                         "e","é","è","ẻ","ẽ","ẹ",
                                         "ê","ế","ề","ể","ễ","ệ",
                                         "I","Í","Ì","Ỉ","Ĩ","Ị",
                                         "E","É","È","Ẻ","Ẽ","Ẹ",
                                         "Ê","Ế","Ề","Ể","Ễ","Ệ",];
                if(arrayTwo.contains(arrayTextBefore[arrayTextBefore.count-2])){
                    if(arrayTextBefore.count == 2 && arrayTextBefore[arrayTextBefore.count-2] == "k" && arraySpecialWithK.contains(arrayTextBefore[arrayTextBefore.count-1])){
                        return ConvertOneLastCharacter.convert(characterLastest: characterLastest, textInput:textInput, textDocumentProxy: textDocumentProxy);
                    }
                    if(arrayTextBefore.count == 2 && arrayTextBefore[arrayTextBefore.count-2] == "K" && arraySpecialWithK.contains(arrayTextBefore[arrayTextBefore.count-1])){
                        return ConvertOneLastCharacter.convert(characterLastest: characterLastest, textInput:textInput, textDocumentProxy: textDocumentProxy);
                    }
                    return textInput;
                }
                if(arrayTextBefore.count > 2){
                    let arraySpecial = ["u","ư","o","i",
                                        "U","Ư","O","I"];
                    let arrayValidateTwoFisrt = ["th","ch","ng","gh","kh",
                                                 "Th","Ch","Ng","Gh","Kh",
                                                 "tH","cH","nG","gH","kH",
                                                 "TH","CH","NG","GH","KH"];
                    let twoTextFirst = arrayTextBefore[0] + arrayTextBefore[1];
                    if(!arraySpecial.contains(arrayTextBefore[1]) && !arrayValidateTwoFisrt.contains(twoTextFirst)){
                        return textInput;
                    }
                }
            }
            return ConvertOneLastCharacter.convert(characterLastest: characterLastest, textInput:textInput, textDocumentProxy: textDocumentProxy);
        case 2:
            var twoCharacterLastest = arrayTextBefore[arrayTextBefore.count-2];
            if(arrayTextBefore.count > 2 ){
                let arrayTwo = ["k","z","q","w","j","s","f","r","x","j",
                                "K","Z","Q","W","J","S","F","R","X","J"];
                if(arrayTwo.contains(arrayTextBefore[arrayTextBefore.count-1])){
                    return textInput;
                }
                if(arrayTextBefore.count > 3 && arrayTwo.contains(arrayTextBefore[arrayTextBefore.count-3])){
                    return textInput;
                }
            }
            let validateStringSpecial = ["i","u","ư",
                                         "I","U","Ư"]
            if(validateStringSpecial.contains(twoCharacterLastest)){
                let arrayUn = ["n","c","t"];
                if(arrayUn.contains(arrayTextBefore[arrayTextBefore.count - 1])){
                    textDocumentProxy.deleteBackward();
                    return ConvertOneLastCharacter.convert(characterLastest: twoCharacterLastest, textInput:textInput,textDocumentProxy: textDocumentProxy) + arrayTextBefore[arrayTextBefore.count - 1];
                }
                twoCharacterLastest = arrayTextBefore[arrayTextBefore.count-1];
                var resultText = ConvertOneLastCharacter.convert(characterLastest: twoCharacterLastest, textInput:textInput, textDocumentProxy: textDocumentProxy);
                let arrayow = ["ơ","ớ","ở","ở","ỡ","ợ",
                               "Ơ","Ớ","Ờ","Ở","Ỡ","Ợ"];
                let arrayoo = ["ô","ố","ồ","ổ","ỗ","ộ",
                               "Ô","Ố","Ồ","Ổ","Ỗ","Ộ",];
                
                var characterD = ""
                let arrayDD = ["d","đ","D","Đ"];
                if(arrayDD.contains(arrayTextBefore[0])){
                    characterD = arrayTextBefore[0];
                }
                
                if(validateStringSpecial.contains(arrayTextBefore[arrayTextBefore.count-1]) &&
                    validateStringSpecial.contains(arrayTextBefore[arrayTextBefore.count-2])){
                    resultText = ConvertOneLastCharacter.convert(characterLastest: arrayTextBefore[arrayTextBefore.count-2], textInput:textInput, textDocumentProxy: textDocumentProxy);
                    return resultText + arrayTextBefore[arrayTextBefore.count-1]
                }
                
                if(arrayow.contains(resultText) && arrayTextBefore[arrayTextBefore.count-2] == "u" ){
                    textDocumentProxy.deleteBackward();
                    
                    return characterD + "ư" + ConvertOneLastCharacter.convert(characterLastest: twoCharacterLastest, textInput:textInput, textDocumentProxy: textDocumentProxy)
                }
                if(arrayoo.contains(resultText) && arrayTextBefore[arrayTextBefore.count-2] == "ư" ){
                    textDocumentProxy.deleteBackward();
                    return characterD + "u" + ConvertOneLastCharacter.convert(characterLastest: twoCharacterLastest, textInput:textInput, textDocumentProxy: textDocumentProxy)
                }
                if(arrayow.contains(resultText) && arrayTextBefore[arrayTextBefore.count-2] == "U" ){
                    textDocumentProxy.deleteBackward();
                    return characterD + "Ư" + ConvertOneLastCharacter.convert(characterLastest: twoCharacterLastest, textInput:textInput, textDocumentProxy: textDocumentProxy)
                }
                if(arrayoo.contains(resultText) && arrayTextBefore[arrayTextBefore.count-2] == "Ư" ){
                    textDocumentProxy.deleteBackward();
                    return characterD + "U" + ConvertOneLastCharacter.convert(characterLastest: twoCharacterLastest, textInput:textInput, textDocumentProxy: textDocumentProxy)
                }
                
                return resultText;
            }
            
            textDocumentProxy.deleteBackward();
            
            var characterUO = "";
            var resultTwo = ConvertOneLastCharacter.convert(characterLastest: twoCharacterLastest, textInput:textInput, textDocumentProxy: textDocumentProxy)
            let arrayow = ["ơ","ớ","ở","ở","ỡ","ợ",
                           "Ơ","Ớ","Ờ","Ở","Ỡ","Ợ"];
            let arrayoo = ["ô","ố","ồ","ổ","ỗ","ộ",
                           "Ô","Ố","Ồ","Ổ","Ỗ","Ộ",];
            
            if(arrayow.contains(resultTwo) && arrayTextBefore[arrayTextBefore.count-3] == "u" ){
                textDocumentProxy.deleteBackward();
                characterUO = "ư";
            }
            if(arrayoo.contains(resultTwo) && arrayTextBefore[arrayTextBefore.count-3] == "ư" ){
                textDocumentProxy.deleteBackward();
                characterUO = "u";
            }
            if(arrayTextBefore.count > 3 && arrayow.contains(resultTwo) && arrayTextBefore[arrayTextBefore.count-4] == "U" ){
                textDocumentProxy.deleteBackward();
                characterUO = "Ư";
            }
            if(arrayTextBefore.count > 3 && arrayoo.contains(resultTwo) && arrayTextBefore[arrayTextBefore.count-4] == "Ư" ){
                textDocumentProxy.deleteBackward();
                characterUO = "U";
            }
            
            let result = resultTwo.map{String($0)}
            if(result.count>1){
                return characterUO + result[0] + arrayTextBefore[arrayTextBefore.count-1] + result[1];
            }
            return characterUO + resultTwo + arrayTextBefore[arrayTextBefore.count-1];
        case 3:
            let threeCharacterLastest = arrayTextBefore[arrayTextBefore.count-3];
            let nodeTwoCharacterLastest = arrayTextBefore[arrayTextBefore.count-2] + arrayTextBefore[arrayTextBefore.count-1];
            let arrayValidateTwoLastest = ["ng","nh","ch",
                                           "Ng","Nh","Ch",
                                           "nG","nH","cH",
                                           "NG","NH","CH"];
            if(arrayTextBefore.count <= 7 && arrayValidateTwoLastest.contains(nodeTwoCharacterLastest)){
                if(arrayTextBefore.count > 3){
                    let arrayTwo = ["k","z","q","w","j","f","j",
                                    "K","Z","Q","W","J","F","J",];
                    
                    if(arrayTwo.contains(arrayTextBefore[arrayTextBefore.count-4])){
                        return textInput;
                    }
                    
                    let arrayTwoSpecial = ["s","r","x","d",
                                           "S","R","X","D"]
                    if(!arrayTwoSpecial.contains(arrayTextBefore[0]) && arrayTwoSpecial.contains(arrayTextBefore[arrayTextBefore.count-4])){
                        return textInput;
                    }
                    
                    var characterUO = "";
                    let resultThree = ConvertOneLastCharacter.convert(characterLastest: threeCharacterLastest, textInput:textInput, textDocumentProxy: textDocumentProxy)
                    let arrayow = ["ơ","ớ","ở","ở","ỡ","ợ",
                                   "Ơ","Ớ","Ờ","Ở","Ỡ","Ợ"];
                    let arrayoo = ["ô","ố","ồ","ổ","ỗ","ộ",
                                   "Ô","Ố","Ồ","Ổ","Ỗ","Ộ",];
                    
                    if(arrayow.contains(resultThree) && arrayTextBefore[arrayTextBefore.count-4] == "u" ){
                        textDocumentProxy.deleteBackward();
                        characterUO = "ư";
                    }
                    if(arrayoo.contains(resultThree) && arrayTextBefore[arrayTextBefore.count-4] == "ư" ){
                        textDocumentProxy.deleteBackward();
                        characterUO = "u";
                    }
                    if(arrayow.contains(resultThree) && arrayTextBefore[arrayTextBefore.count-4] == "U" ){
                        textDocumentProxy.deleteBackward();
                        characterUO = "Ư";
                    }
                    if(arrayoo.contains(resultThree) && arrayTextBefore[arrayTextBefore.count-4] == "Ư" ){
                        textDocumentProxy.deleteBackward();
                        characterUO = "U";
                    }
                    textDocumentProxy.deleteBackward();
                    textDocumentProxy.deleteBackward();
                    
                    let result = resultThree.map{String($0)}
                    if(result.count>1){
                        return characterUO + result[0] + nodeTwoCharacterLastest + result[1];
                    }
                    return characterUO + resultThree + nodeTwoCharacterLastest;
                }
            }
            break;
        case 4:
            let arrayDD = ["d","đ","D","Đ"];
            if(arrayTextBefore.count < 6){
                if(arrayDD.contains(arrayTextBefore[0]) && arrayDD.contains(textInput)){
                    var stringD = ""
                    if(arrayTextBefore.count > 1){
                        for index in 1...arrayTextBefore.count-1{
                            textDocumentProxy.deleteBackward();
                            stringD = stringD + arrayTextBefore[index];
                        }
                    }
                    let resultD = ConvertOneLastCharacter.convert(characterLastest: arrayTextBefore[0], textInput:textInput, textDocumentProxy: textDocumentProxy);
                    let result = resultD.map{String($0)}
                    if(result.count>1){
                        return result[0] + stringD + result[1];
                    }
                    return result[0] + stringD;
                }
            }
            break;
        case 5:
            //            let arrayEE = ["e","E","ê","Ê"]
            if(arrayTextBefore.count > 2 && arrayTextBefore.count < 6){
                let resultE = ConvertOneLastCharacter.convert(characterLastest: arrayTextBefore[arrayTextBefore.count - 1], textInput: textInput, textDocumentProxy: textDocumentProxy);
                return resultE;
                //                let result = resultE.map{String($0)}
                //                if(result.count>1){
                //                    return result[0] + stringD + result[1];
                //                }
                //                return result[0] + stringD;
            }
            break;
        default:
            break;
        }
        
        return textInput;
    }
    
    func initTypeConvertCharacter(arrayString: Array<String>, textInput: String)->Int{
        let characterLastest = arrayString[arrayString.count-1];
        
        let arrayValidateInput = ["a","o","w","e","d","s","f","x","r","j",
                                  "A","O","W","E","D","S","F","X","R","J"];
        let arrayValidateOne = ["a","á","à","ả","ã","ạ",
                                "ă","ắ","ằ","ẳ","ẵ","ặ",
                                "â","ấ","ầ","ẩ","ẫ","ậ",
                                "o","ó","ò","ỏ","õ","ọ",
                                "ô","ố","ồ","ổ","ỗ","ộ",
                                "ơ","ớ","ờ","ở","ỡ","ợ",
                                "u","ú","ù","ủ","ũ","ụ",
                                "ư","ứ","ừ","ử","ữ","ự",
                                "i","í","ì","ỉ","ĩ","ị",
                                "y","ý","ỳ","ỷ","ỹ","ỵ",
                                "e","é","è","ẻ","ẽ","ẹ",
                                "ê","ế","ề","ể","ễ","ệ",
                                "A","Á","À","Ả","Ã","Ạ",
                                "Ă","Ắ","Ằ","Ẳ","Ẵ","Ặ",
                                "Â","Ấ","Ầ","Ẩ","Ẫ","Ậ",
                                "O","Ó","Ò","Ỏ","Õ","Ọ",
                                "Ơ","Ớ","Ờ","Ở","Ỡ","Ợ",
                                "Ô","Ố","Ồ","Ổ","Ỗ","Ộ",
                                "U","Ú","Ù","Ủ","Ũ","Ụ",
                                "Ư","Ứ","Ừ","Ử","Ữ","Ự",
                                "I","Í","Ì","Ỉ","Ĩ","Ị",
                                "Y","Ý","Ỳ","Ủ","Ỹ","Ỵ",
                                "E","É","È","Ẻ","Ẽ","Ẹ",
                                "Ê","Ế","Ề","Ể","Ễ","Ệ",];
        
        if(arrayValidateInput.contains(textInput) && arrayString.count <= 7){
            let arraydD = ["d","đ","D","Đ"];
            if(arraydD.contains(arrayString[0]) && arraydD.contains(textInput) ){
                return 4;
            }else if(arraydD.contains(textInput)){
                return 0;
            }
            
            let arrayeE = ["e","E","ê","Ê"];
            if(arrayString.count>2 && arrayeE.contains(textInput) && arrayeE.contains(arrayString[arrayString.count - 1]) && arrayString[arrayString.count - 2] == "y" &&
                arrayString[arrayString.count - 3] == "u"){
                return 5
            }
            
            if(arrayValidateOne.contains(characterLastest) && arrayString.count == 1){
                return 1;
            }else if(arrayString.count>1){
                let characterTwoLastest = arrayString[arrayString.count-2];
                
                if(!arrayValidateOne.contains(characterTwoLastest)){
                    if(arrayString.count >= 3){
                        let characterThreeLastest = arrayString[arrayString.count-3];
                        if(arrayValidateOne.contains(characterThreeLastest)){
                            return 3;
                        }
                    }
                    return 1;
                }else{
                    if(arrayString.count == 3 && arrayString[0] == "q"){
                        return 1;
                    }
                    if(arrayString.count == 3 && arrayString[0] == "Q"){
                        return 1;
                    }
                    if(arrayValidateOne.contains(characterTwoLastest)){
                        let arrayoo = ["ô","ố","ồ","ổ","ỗ","ộ","Ô","Ố","Ồ","Ổ","Ỗ","Ộ"];
                        if((textInput == "o" || textInput == "O") && arrayString.count>2 && !arrayoo.contains(arrayString[arrayString.count-2]) && arrayString[arrayString.count-1] != "i" ){
                            let arraySpecialNMCT = ["n","m","c","t"];
                            if(arraySpecialNMCT.contains(arrayString[arrayString.count - 1])){
                                return 2;
                            }
                            return 1;
                        }
                        
                        //                        if(arraydD.contains(textInput) )
                        return 2;
                    }
                }
            }
        }
        return 0;
    }
}
