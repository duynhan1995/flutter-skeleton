//
//  StringHandler.swift
//  Runner
//
//  Created by nathan on 16/04/2021.
//

import Foundation

class StringHandler {
  static func secondsToTime(value: Int?) -> String {
    if (value == nil || value == 0) {
      return "00:00";
    }

    var _value = value
    
    let hours = Int((Double(_value!) / 3600).rounded(.down))
    _value! %= 3600

    let minutes: Int = Int((Double(_value!) / 60).rounded(.down))
    let seconds = _value! % 60

    if (hours == 0) {
      return "\(minutes):\(seconds < 10 ? "0\(seconds)" : "\(seconds)")"
    }

    return "\(hours):\(minutes < 10 ? "0\(minutes)" : "\(minutes)"):\(seconds < 10 ? "0\(seconds)" : "\(seconds)")"
  }
}
