import UIKit
import Flutter
import PushKit
import UserNotifications
import PortSIPVoIPSDK

@available(iOS 10.0, *)
@UIApplicationMain class AppDelegate: FlutterAppDelegate, PKPushRegistryDelegate {
  
  private let channel = "vn.etelecom.appcall"
  
  private var portsipService: PortsipService! = PortsipService()
  
  private let voipRegistry: PKPushRegistry! = PKPushRegistry(queue: DispatchQueue.main)
  
  private var fController: FlutterViewController!
  
  private var _VoIPPushToken: String = ""
  private var _APNsPushToken: String = ""
  
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {
  
    UserDefaults.standard.register(defaults: ["CallKit": true])
    UserDefaults.standard.register(defaults: ["PushNotification": true])
    UserDefaults.standard.register(defaults: ["ForceBackground": true])
  
    fController = window?.rootViewController as? FlutterViewController
    
    FlutterMethodChannel(name: channel, binaryMessenger: fController.binaryMessenger).setMethodCallHandler({
      [weak self] (call: FlutterMethodCall, result: FlutterResult) -> Void in
      // Note: this method is invoked on the UI thread.
      
      print("E-TELECOM METHOD \(call.method)")
      
      switch call.method {
      case "registerPortsip":
        let args = call.arguments as? [String: Any]
        let username = args?["username"] as? String
        let password = args?["password"] as? String
        let domain = args?["domain"] as? String
        let server = args?["sipServer"] as? String
        
        if username != nil && password != nil && domain != nil {
          let res = self?.registerPortSip(username: username!, password: password!, domain: domain!, sipServer: server!)
          if res == 0 || res == -60021 || res == -60095 || res == -60098 {
            result(Int32(res!))
          } else {
            result(FlutterError(code: "REGISTER FAILED", message: "Kết nối không thành công.", details: nil))
          }
        } else {
          result(FlutterError(code: "NOT AUTHENTICATED", message: "Username và password không đúng.", details: nil))
        }
        
      case "unregisterPortsip":
        if (self?.portsipService != nil) {
          self?.updatePushStatusToSipServer(willPush: false)
          self?.portsipService.unregisterPortsip()
        }
        
      case "callOut":
        let args = call.arguments as? [String: Any]
        let phoneNumber = args?["phoneNumber"] as? String
        let videoCall = args?["videoCall"] as? Bool
        
        if phoneNumber != nil {
          let res = self?.portsipService.callOut(phoneNumber: phoneNumber!, videoCall: videoCall ?? false)
          if res! {
            result(nil)
          } else {
            result(FlutterError(code: "CALL FAILED", message: "Không thể thực hiện cuộc gọi", details: nil))
          }
        }
      case "hangUp":
        let res = self?.portsipService.hangUp()
        if res! {
          result(nil)
        } else {
          result(FlutterError(code: "HANGUP FAILED", message: "Không thể kết thúc cuộc gọi", details: nil))
        }
      case "answerCall":
        let res = self?.portsipService.answerCall()
        if res! {
          result(nil)
        } else {
          result(FlutterError(code: "ANSWER_CALL FAILED", message: "Không thể trả lời cuộc gọi", details: nil))
        }
      case "rejectCall":
        let res = self?.portsipService.rejectCall()
        if res! {
          result(nil)
        } else {
          result(FlutterError(code: "REJECT_CALL FAILED", message: "Không thể từ chối cuộc gọi", details: nil))
        }
      case "hold":
        let res = self?.portsipService.hold()
        if res! {
          result(nil)
        } else {
          result(FlutterError(code: "HOLD_CALL FAILED", message: "Không thể giữ máy", details: nil))
        }
      case "unHold":
        let res = self?.portsipService.unHold()
        if res! {
          result(nil)
        } else {
          result(FlutterError(code: "UNHOLD_CALL FAILED", message: "Không thể tiếp tục cuộc gọi", details: nil))
        }
      case "speakerOn":
        self?.portsipService.speakerOn()
        result(nil)
      case "speakerOff":
        self?.portsipService.speakerOff()
        result(nil)
      case "microphoneOn":
        self?.portsipService.turnOnMicrophone()
        result(nil)
      case "microphoneOff":
        self?.portsipService.turnOffMicrophone()
        result(nil)
      case "frontCamera":
        self?.portsipService.switchToFrontCamera()
        result(nil)
      case "backCamera":
        self?.portsipService.switchToBackCamera()
        result(nil)
      case "cameraOn":
        self?.portsipService.turnOnCamera()
        result(nil)
      case "cameraOff":
        self?.portsipService.turnOffCamera()
        result(nil)
      case "sendDtmf":
        let args = call.arguments as? [String: Any]
        let code = args?["code"] as? Int32
        let res = self?.portsipService.sendDtmf(code: code!)
        if (res == 0) {
          result(nil)
        } else {
          result(FlutterError(code: "SEND DTMF FAILED", message: "Có lỗi xảy ra khi gửi DTMF", details: nil))
        }
      case "refer":
        let args = call.arguments as? [String: Any]
        let referTo = args?["referTo"] as? String
        let res = self?.portsipService.refer(referTo: referTo!)
        if (res == 0) {
          result(nil)
        } else {
          result(FlutterError(code: "REFER FAILED", message: "Có lỗi xảy ra khi chuyển tiếp cuộc gọi", details: nil))
        }
        
      default:
        result(FlutterMethodNotImplemented)
      }
    })
    
    GeneratedPluginRegistrant.register(with: self)
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
  
  override func application(
    _ application: UIApplication,
    didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data
  ) {
    
    let tokenParts = deviceToken.map { data in String(format: "%02.2hhx", data) }
    _APNsPushToken = tokenParts.joined()
    print("E-TELECOM _APNsPushToken: \(_APNsPushToken)")
    
    updatePushStatusToSipServer(willPush: true)
    
  }
  
  func pushRegistry(_: PKPushRegistry, didUpdate pushCredentials: PKPushCredentials, for _: PKPushType) {
  
    let tokenParts = pushCredentials.token.map { data in String(format: "%02.2hhx", data) }
    _VoIPPushToken = tokenParts.joined()
    print("E-TELECOM _VoIPPushToken: \(_VoIPPushToken)")
    
    updatePushStatusToSipServer(willPush: true)
    
  }
  
  func pushRegistry(_: PKPushRegistry, didReceiveIncomingPushWith payload: PKPushPayload, for _: PKPushType) {
    
    if UIApplication.shared.applicationState == .active {
      return
    }

    if portsipService != nil {
      portsipService.processPushMessageFromPortPBX(payload.dictionaryPayload, completion: {})
    }
    
  }
  
  func pushRegistry(_ registry: PKPushRegistry, didReceiveIncomingPushWith payload: PKPushPayload, for type: PKPushType, completion: @escaping () -> Void) {
    
    if UIApplication.shared.applicationState == .active {
      return
    }

    if portsipService != nil {
      portsipService.processPushMessageFromPortPBX(payload.dictionaryPayload, completion: completion)
    }
    
  }
  
  override func applicationDidEnterBackground(_: UIApplication) {
    print("E-TELECOM applicationDidEnterBackground")
    
    if portsipService != nil {
      portsipService.startKeepAwake()
    }
  }
  
  override func applicationWillEnterForeground(_: UIApplication) {
    print("E-TELECOM applicationWillEnterForeground")
    
    if portsipService != nil {
      portsipService.stopKeepAwake()
    }
  }
  
  func updatePushStatusToSipServer(willPush: Bool) {
    if _VoIPPushToken.isEmpty || _APNsPushToken.isEmpty {
      return
    }
    
    portsipService.clearAddedSipMessageHeaders()

    let bundleIdentifier: String = Bundle.main.bundleIdentifier!
    let token = "\(_VoIPPushToken)|\(_APNsPushToken)"
    let pushMessage = "device-os=ios;device-uid=\(token);allow-call-push=\(willPush);allow-message-push=\(willPush);app-id=\(bundleIdentifier)"
  
    portsipService.addSipMessageHeader(pushMessage: pushMessage)
    portsipService.refreshRegistrationPortsip()
    
    CallManager.pushMessage = pushMessage
  }
  
  private func registerPortSip(username: String, password: String, domain: String, sipServer: String) -> Int32 {
    PortsipService.fController = fController
    
    if !CallManager.portsipRegistered {
      CallManager.portsipExtension = username
      CallManager.portsipPassword = password
      CallManager.portsipDomain = domain
      CallManager.portsipServer = sipServer
      
      initNotification()
      portsipService.registerPortsip()
    } else {
      portsipService.refreshRegistrationPortsip()
    }
    
    return 0
  }
  
  private func initNotification() {
    voipRegistry.delegate = self
    voipRegistry.desiredPushTypes = [PKPushType.voIP]
  
    let notifiCenter = UNUserNotificationCenter.current()
    notifiCenter.delegate = self
    notifiCenter.requestAuthorization(options: [.alert, .sound, .badge]) { accepted, _ in
    
      if !accepted {
        print("Permission granted: \(accepted)")
      }
    }
  
    UIApplication.shared.registerForRemoteNotifications()
  }
  
}
