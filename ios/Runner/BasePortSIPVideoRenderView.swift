//
//  BasePortSIPVideoRenderView.swift
//  Runner
//
//  Created by nathan on 09/04/2021.
//

import UIKit
import PortSIPVoIPSDK

@IBDesignable
class BasePortSIPVideoRenderView: PortSIPVideoRenderView {
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
        
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
}
