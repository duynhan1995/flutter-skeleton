import PortSIPVoIPSDK

open class BasePortsipDelegate: NSObject, PortSIPEventDelegate {
  
  public func onRegisterSuccess(_ statusText: UnsafeMutablePointer<Int8>!, statusCode: Int32, sipMessage: UnsafeMutablePointer<Int8>!) {
    print("BASE ONREGISTERSUCCESS")
  }
  
  public func onRegisterFailure(_ statusText: UnsafeMutablePointer<Int8>!, statusCode: Int32, sipMessage: UnsafeMutablePointer<Int8>!) {
    print("BASE ONREGISTERFAILURE")
  }
  
  public func onInviteIncoming(_ sessionId: Int, callerDisplayName: UnsafeMutablePointer<Int8>!, caller: UnsafeMutablePointer<Int8>!, calleeDisplayName: UnsafeMutablePointer<Int8>!, callee: UnsafeMutablePointer<Int8>!, audioCodecs: UnsafeMutablePointer<Int8>!, videoCodecs: UnsafeMutablePointer<Int8>!, existsAudio: Bool, existsVideo: Bool, sipMessage: UnsafeMutablePointer<Int8>!) {
    print("BASE ONINVITEINCOMING")
  }
  
  public func onInviteTrying(_ sessionId: Int) {
    print("BASE ONINVITETRYING")
  }
  
  public func onInviteSessionProgress(_ sessionId: Int, audioCodecs: UnsafeMutablePointer<Int8>!, videoCodecs: UnsafeMutablePointer<Int8>!, existsEarlyMedia: Bool, existsAudio: Bool, existsVideo: Bool, sipMessage: UnsafeMutablePointer<Int8>!) {
    print("BASE ONINVITESESSIONPROGRESS")
  }
  
  public func onInviteRinging(_ sessionId: Int, statusText: UnsafeMutablePointer<Int8>!, statusCode: Int32, sipMessage: UnsafeMutablePointer<Int8>!) {
    print("BASE ONINVITERINGING")
  }
  
  public func onInviteAnswered(_ sessionId: Int, callerDisplayName: UnsafeMutablePointer<Int8>!, caller: UnsafeMutablePointer<Int8>!, calleeDisplayName: UnsafeMutablePointer<Int8>!, callee: UnsafeMutablePointer<Int8>!, audioCodecs: UnsafeMutablePointer<Int8>!, videoCodecs: UnsafeMutablePointer<Int8>!, existsAudio: Bool, existsVideo: Bool, sipMessage: UnsafeMutablePointer<Int8>!) {
    print("BASE ONINVITEANSWERED")
  }
  
  public func onInviteFailure(_ sessionId: Int, reason: UnsafeMutablePointer<Int8>!, code: Int32, sipMessage: UnsafeMutablePointer<Int8>!) {
    print("BASE ONINVITEFAILURE")
  }
  
  public func onInviteConnected(_ sessionId: Int) {
    print("BASE ONINVITECONNECTED")
  }
  
  public func onInviteBeginingForward(_ forwardTo: UnsafeMutablePointer<Int8>!) {
    print("BASE ONINVITEBEGININGFORWARD")
  }
  
  public func onInviteClosed(_ sessionId: Int, sipMessage: UnsafeMutablePointer<Int8>!) {
    print("BASE ONINVITECLOSED")
  }
  
  public func onInviteUpdated(_ sessionId: Int, audioCodecs: UnsafeMutablePointer<Int8>!, videoCodecs: UnsafeMutablePointer<Int8>!, screenCodecs: UnsafeMutablePointer<Int8>!, existsAudio: Bool, existsVideo: Bool, existsScreen: Bool, sipMessage: UnsafeMutablePointer<Int8>!) {
    print("BASE ONINVITEUPDATED")
  }
  
  public func onDialogStateUpdated(_ BLFMonitoredUri: UnsafeMutablePointer<Int8>!, blfDialogState BLFDialogState: UnsafeMutablePointer<Int8>!, blfDialogId BLFDialogId: UnsafeMutablePointer<Int8>!, blfDialogDirection BLFDialogDirection: UnsafeMutablePointer<Int8>!) {
    print("BASE ONDIALOGSTATEUPDATED")
  }
  
  public func onRTPPacketCallback(_ sessionId: Int, mediaType: Int32, direction: DIRECTION_MODE, rtpPacket RTPPacket: UnsafeMutablePointer<UInt8>!, packetSize: Int32) {
    print("BASE ONRTPPACKETCALLBACK")
  }
  
  public func onRemoteHold(_ sessionId: Int) {
    print("BASE ONREMOTEHOLD")
  }
  
  public func onRemoteUnHold(_ sessionId: Int, audioCodecs: UnsafeMutablePointer<Int8>!, videoCodecs: UnsafeMutablePointer<Int8>!, existsAudio: Bool, existsVideo: Bool) {
    print("BASE ONREMOTEUNHOLD")
  }
  
  public func onReceivedRefer(_ sessionId: Int, referId: Int, to: UnsafeMutablePointer<Int8>!, from: UnsafeMutablePointer<Int8>!, referSipMessage: UnsafeMutablePointer<Int8>!) {
    print("BASE ONRECEIVEDREFER")
  }
  
  public func onReferAccepted(_ sessionId: Int) {
    print("BASE ONREFERACCEPTED")
  }
  
  public func onReferRejected(_ sessionId: Int, reason: UnsafeMutablePointer<Int8>!, code: Int32) {
    print("BASE ONREFERREJECTED")
  }
  
  public func onTransferTrying(_ sessionId: Int) {
    print("BASE ONTRANSFERTRYING")
  }
  
  public func onTransferRinging(_ sessionId: Int) {
    print("BASE ONTRANSFERRINGING")
  }
  
  public func onACTVTransferSuccess(_ sessionId: Int) {
    print("BASE ONACTVTRANSFERSUCCESS")
  }
  
  public func onACTVTransferFailure(_ sessionId: Int, reason: UnsafeMutablePointer<Int8>!, code: Int32) {
    print("BASE ONACTVTRANSFERFAILURE")
  }
  
  public func onReceivedSignaling(_ sessionId: Int, message: UnsafeMutablePointer<Int8>!) {
    print("BASE ONRECEIVEDSIGNALING")
  }
  
  public func onSendingSignaling(_ sessionId: Int, message: UnsafeMutablePointer<Int8>!) {
    print("BASE ONSENDINGSIGNALING")
  }
  
  public func onWaitingVoiceMessage(_ messageAccount: UnsafeMutablePointer<Int8>!, urgentNewMessageCount: Int32, urgentOldMessageCount: Int32, newMessageCount: Int32, oldMessageCount: Int32) {
    print("BASE ONWAITINGVOICEMESSAGE")
  }
  
  public func onWaitingFaxMessage(_ messageAccount: UnsafeMutablePointer<Int8>!, urgentNewMessageCount: Int32, urgentOldMessageCount: Int32, newMessageCount: Int32, oldMessageCount: Int32) {
    print("BASE ONWAITINGFAXMESSAGE")
  }
  
  public func onRecvDtmfTone(_ sessionId: Int, tone: Int32) {
    print("BASE ONRECVDTMFTONE")
  }
  
  public func onRecvOptions(_ optionsMessage: UnsafeMutablePointer<Int8>!) {
    print("BASE ONRECVOPTIONS")
  }
  
  public func onRecvInfo(_ infoMessage: UnsafeMutablePointer<Int8>!) {
    print("BASE ONRECVINFO")
  }
  
  public func onRecvNotifyOfSubscription(_ subscribeId: Int, notifyMessage: UnsafeMutablePointer<Int8>!, messageData: UnsafeMutablePointer<UInt8>!, messageDataLength: Int32) {
    print("BASE ONRECVNOTIFYOFSUBSCRIPTION")
  }
  
  public func onPresenceRecvSubscribe(_ subscribeId: Int, fromDisplayName: UnsafeMutablePointer<Int8>!, from: UnsafeMutablePointer<Int8>!, subject: UnsafeMutablePointer<Int8>!) {
    print("BASE ONPRESENCERECVSUBSCRIBE")
  }
  
  public func onPresenceOnline(_ fromDisplayName: UnsafeMutablePointer<Int8>!, from: UnsafeMutablePointer<Int8>!, stateText: UnsafeMutablePointer<Int8>!) {
    print("BASE ONPRESENCEONLINE")
  }
  
  public func onPresenceOffline(_ fromDisplayName: UnsafeMutablePointer<Int8>!, from: UnsafeMutablePointer<Int8>!) {
    print("BASE ONPRESENCEOFFLINE")
  }
  
  public func onRecvMessage(_ sessionId: Int, mimeType: UnsafeMutablePointer<Int8>!, subMimeType: UnsafeMutablePointer<Int8>!, messageData: UnsafeMutablePointer<UInt8>!, messageDataLength: Int32) {
    print("BASE ONRECVMESSAGE")
  }
  
  public func onRecvOutOfDialogMessage(_ fromDisplayName: UnsafeMutablePointer<Int8>!, from: UnsafeMutablePointer<Int8>!, toDisplayName: UnsafeMutablePointer<Int8>!, to: UnsafeMutablePointer<Int8>!, mimeType: UnsafeMutablePointer<Int8>!, subMimeType: UnsafeMutablePointer<Int8>!, messageData: UnsafeMutablePointer<UInt8>!, messageDataLength: Int32, sipMessage: UnsafeMutablePointer<Int8>!) {
    print("BASE ONRECVOUTOFDIALOGMESSAGE")
  }
  
  public func onSendMessageSuccess(_ sessionId: Int, messageId: Int) {
    print("BASE ONSENDMESSAGESUCCESS")
  }
  
  public func onSendMessageFailure(_ sessionId: Int, messageId: Int, reason: UnsafeMutablePointer<Int8>!, code: Int32) {
    print("BASE ONSENDMESSAGEFAILURE")
  }
  
  public func onSendOutOfDialogMessageSuccess(_ messageId: Int, fromDisplayName: UnsafeMutablePointer<Int8>!, from: UnsafeMutablePointer<Int8>!, toDisplayName: UnsafeMutablePointer<Int8>!, to: UnsafeMutablePointer<Int8>!) {
    print("BASE ONSENDOUTOFDIALOGMESSAGESUCCESS")
  }
  
  public func onSendOutOfDialogMessageFailure(_ messageId: Int, fromDisplayName: UnsafeMutablePointer<Int8>!, from: UnsafeMutablePointer<Int8>!, toDisplayName: UnsafeMutablePointer<Int8>!, to: UnsafeMutablePointer<Int8>!, reason: UnsafeMutablePointer<Int8>!, code: Int32) {
    print("BASE ONSENDOUTOFDIALOGMESSAGEFAILURE")
  }
  
  public func onSubscriptionFailure(_ subscribeId: Int, statusCode: Int32) {
    print("BASE ONSUBSCRIPTIONFAILURE")
  }
  
  public func onSubscriptionTerminated(_ subscribeId: Int) {
    print("BASE ONSUBSCRIPTIONTERMINATED")
  }
  
  public func onPlayAudioFileFinished(_ sessionId: Int, fileName: UnsafeMutablePointer<Int8>!) {
    print("BASE ONPLAYAUDIOFILEFINISHED")
  }
  
  public func onPlayVideoFileFinished(_ sessionId: Int) {
    print("BASE ONPLAYVIDEOFILEFINISHED")
  }
  
  public func onReceivedRTPPacket(_ sessionId: Int, isAudio: Bool, rtpPacket RTPPacket: UnsafeMutablePointer<UInt8>!, packetSize: Int32) {
    print("BASE ONRECEIVEDRTPPACKET")
  }
  
  public func onSendingRTPPacket(_ sessionId: Int, isAudio: Bool, rtpPacket RTPPacket: UnsafeMutablePointer<UInt8>!, packetSize: Int32) {
    print("BASE ONSENDINGRTPPACKET")
  }
  
  public func onAudioRawCallback(_ sessionId: Int, audioCallbackMode: Int32, data: UnsafeMutablePointer<UInt8>!, dataLength: Int32, samplingFreqHz: Int32) {
    print("BASE ONAUDIORAWCALLBACK")
  }
  
  public func onVideoRawCallback(_ sessionId: Int, videoCallbackMode: Int32, width: Int32, height: Int32, data: UnsafeMutablePointer<UInt8>!, dataLength: Int32) -> Int32 {
    print("BASE ONVIDEORAWCALLBACK")
    return 0
  }
  
}
