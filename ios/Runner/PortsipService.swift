import CallKit
import PortSIPVoIPSDK
import AVFoundation

@available(iOS 10.0, *)
class PortsipService: BasePortsipDelegate, CXProviderDelegate, UIApplicationDelegate {
  
  let FLUTTER_METHOD_CHANNEL = "vn.etelecom.appcall"
  
  static var portsipSDK: PortSIPSDK! = PortSIPSDK()
  static var fController: FlutterViewController!
  private var videoCallView: VideoCallViewController!

  static var cxProvider: CXProvider?
  static var callController: CXCallController?
  
  var audioPlayer: AVAudioPlayer?
  private var timer: Timer?
  
  override init() {
    super.init()
    self.initCXProvider()
  }
  
  private func initCXProvider() {
    let infoDic = Bundle.main.infoDictionary!
    let localizedName = infoDic["CFBundleName"] as! String

    let providerConfiguration = CXProviderConfiguration(localizedName: localizedName)
    providerConfiguration.supportsVideo = true
    providerConfiguration.maximumCallsPerCallGroup = 1
    providerConfiguration.supportedHandleTypes = [.phoneNumber]
    
    PortsipService.cxProvider = CXProvider(configuration: providerConfiguration)
    PortsipService.cxProvider?.setDelegate(self, queue: nil)
    
    PortsipService.callController = CXCallController()
  }
  
  private func initPortsipSDK() {
    
    PortsipService.portsipSDK.delegate = self
    PortsipService.portsipSDK.enableCallKit(true)
    
    PortsipService.portsipSDK.initialize(
      TRANSPORT_TCP, localIP: "0.0.0.0", localSIPPort: Int32(10002), loglevel: PORTSIP_LOG_DEBUG, logPath: "",
      maxLine: 8, agent: "PortSIP SDK for IOS", audioDeviceLayer: 0, videoDeviceLayer: 0, tlsCertificatesRootPath: "",
      tlsCipherList: "", verifyTLSCertificate: false, dnsServers: "")
  
    PortsipService.portsipSDK.addAudioCodec(AUDIOCODEC_OPUS)
    PortsipService.portsipSDK.addAudioCodec(AUDIOCODEC_G729)
    PortsipService.portsipSDK.addAudioCodec(AUDIOCODEC_PCMA)
    PortsipService.portsipSDK.addAudioCodec(AUDIOCODEC_PCMU)
    PortsipService.portsipSDK.setAudioSamples(20, maxPtime: 60)
  
    PortsipService.portsipSDK.addVideoCodec(VIDEO_CODEC_H264)
    PortsipService.portsipSDK.addVideoCodec(VIDEO_CODEC_H263_1998)
    PortsipService.portsipSDK.addVideoCodec(VIDEO_CODEC_VP8)
    PortsipService.portsipSDK.addVideoCodec(VIDEO_CODEC_VP9)
    
    PortsipService.portsipSDK.setVideoBitrate(-1, bitrateKbps: 512)
    PortsipService.portsipSDK.setVideoFrameRate(-1, frameRate: 20)
    PortsipService.portsipSDK.setVideoResolution(480, height: 640)
    PortsipService.portsipSDK.setVideoDeviceId(1)
    PortsipService.portsipSDK.setVideoNackStatus(true)
  
    PortsipService.portsipSDK.setInstanceId(UIDevice.current.identifierForVendor?.uuidString)

    PortsipService.portsipSDK.setLicenseKey("PORTSIP_UC_LICENSE")

    print("E-TELECOM InitialSDK \(PortsipService.portsipSDK.getVersion())")
    
  }
  
  func registerPortsip() {
    
    PortsipService.portsipSDK.unInitialize()
    initPortsipSDK()
    
    if (!CallManager.pushMessage.isEmpty) {
      clearAddedSipMessageHeaders()
      addSipMessageHeader(pushMessage: CallManager.pushMessage)
    }
    
    PortsipService.portsipSDK.removeUser()
    var code = PortsipService.portsipSDK.setUser(
      CallManager.portsipExtension, displayName: CallManager.portsipExtension, authName: CallManager.portsipExtension, password: CallManager.portsipPassword, userDomain: CallManager.portsipDomain,
      sipServer: CallManager.portsipServer, sipServerPort: Int32(5063),
      stunServer: "", stunServerPort: 0, outboundServer: "", outboundServerPort: 0)
    
    print("E-TELECOM ON SET USER \(code)")
    
    if code != 0 {
      return
    }
    
    PortsipService.portsipSDK.unRegisterServer()
    code = PortsipService.portsipSDK.registerServer(90, retryTimes: 0)

    print("E-TELECOM ON REGISTER SERVER \(code)")
    
  }
  
  func refreshRegistrationPortsip() {
    PortsipService.portsipSDK.refreshRegistration(0)
  }
  
  func unregisterPortsip() {
    CallManager.portsipRegistered = false
    CallManager.portsipExtension = ""
    CallManager.portsipPassword = ""
    CallManager.portsipDomain = ""
    CallManager.portsipServer = ""
    CallManager.pushMessage = ""
    
    PortsipService.portsipSDK.removeUser()
    PortsipService.portsipSDK.unRegisterServer()
    PortsipService.portsipSDK.unInitialize()
  }
  
  func clearAddedSipMessageHeaders() {
    PortsipService.portsipSDK.clearAddedSipMessageHeaders()
  }
  
  func addSipMessageHeader(pushMessage: String) {
    PortsipService.portsipSDK.addSipMessageHeader(-1, methodName: "REGISTER", msgType: 1, headerName: "x-p-push", headerValue: pushMessage)
  }
  
  func callOut(phoneNumber: String, videoCall: Bool = false) -> Bool {
    CallManager.isVideoCall = videoCall
    CallManager.sessionID = PortsipService.portsipSDK.call(phoneNumber, sendSdp: true, videoCall: videoCall)

    if (CallManager.sessionID <= 0) {
      return false
    }
    
    UIDevice.current.isProximityMonitoringEnabled = true
    playOutgoingCallSound()
    if (videoCall) {
      speakerOn()
    } else {
      speakerOff()
    }
    
    return true
  }
  
  func hangUp() -> Bool {
    let code = PortsipService.portsipSDK.hangUp(CallManager.sessionID)
    
    if (CallManager.uuid != nil) {
      reportClosedCall(uuid: CallManager.uuid!)
    } else {
      endACall()
    }
    
    return code == 0
  }
  
  func answerCall() -> Bool {
    stopVibration()
    
    let code = PortsipService.portsipSDK.answerCall(CallManager.sessionID, videoCall: CallManager.isVideoCall)

    return code == 0
  }
  
  func rejectCall() -> Bool {
    let code = PortsipService.portsipSDK.rejectCall(CallManager.sessionID, code: 486)
    
    endACall()
    
    return code == 0
  }
  
  func hold() -> Bool {
    let code = PortsipService.portsipSDK.hold(CallManager.sessionID)
    
    return code == 0
  }
  
  func unHold() -> Bool {
    let code = PortsipService.portsipSDK.unHold(CallManager.sessionID)
    
    return code == 0
  }

  func speakerOn() {
    CallManager.speakerOn = true
    if (!CallManager.isIncomingCall || audioPlayer == nil) {
      PortsipService.portsipSDK.setLoudspeakerStatus(true)
    }
    UIDevice.current.isProximityMonitoringEnabled = false
  }

  func speakerOff() {
    CallManager.speakerOn = false
    if (!CallManager.isIncomingCall || audioPlayer == nil) {
      PortsipService.portsipSDK.setLoudspeakerStatus(false)
    }
    UIDevice.current.isProximityMonitoringEnabled = true
  }

  func turnOnMicrophone() {
    CallManager.micIsOn = true
    if (CallManager.callIsOnGoing) {
      PortsipService.portsipSDK.muteSession(
        CallManager.sessionID,
        muteIncomingAudio: false,
        muteOutgoingAudio: false,
        muteIncomingVideo: false,
        muteOutgoingVideo: false
      )
    }
  }
  
  func turnOffMicrophone() {
    CallManager.micIsOn = false
    if (CallManager.callIsOnGoing) {
      PortsipService.portsipSDK.muteSession(
        CallManager.sessionID,
        muteIncomingAudio: false,
        muteOutgoingAudio: true,
        muteIncomingVideo: false,
        muteOutgoingVideo: false
      )
    }
  }
  
  func switchToFrontCamera() {
    CallManager.frontCamera = true
    PortsipService.portsipSDK.setVideoDeviceId(1)
  }
  
  func switchToBackCamera() {
    CallManager.frontCamera = false
    PortsipService.portsipSDK.setVideoDeviceId(0)
  }
  
  func turnOnCamera() {
    CallManager.camIsOn = true
  }
  
  func turnOffCamera() {
    CallManager.camIsOn = false
  }

  func sendDtmf(code: Int32) -> Int32 {
    PortsipService.portsipSDK.sendDtmf(CallManager.sessionID, dtmfMethod: DTMF_RFC2833, code: code, dtmfDration: 160, playDtmfTone: true)
  }

  func refer(referTo: String) -> Int32 {
    PortsipService.portsipSDK.refer(CallManager.sessionID, referTo: referTo)
  }

  func initMediaSession() {
    CallManager.callIsOnGoing = true
    PortsipService.portsipSDK.muteSession(
      CallManager.sessionID,
      muteIncomingAudio: false,
      muteOutgoingAudio: !CallManager.micIsOn,
      muteIncomingVideo: false,
      muteOutgoingVideo: false
    )
    
    PortsipService.portsipSDK.setLoudspeakerStatus(CallManager.speakerOn)
  }
  
  func endACall() {
    stopSound()
    stopVibration()
    
    flutterMethodChannel(method: "callEnded", args: "")
    
    speakerOff()
    turnOnMicrophone()
    turnOnCamera()
    switchToFrontCamera()
    
    CallManager.resetStates()
    
    DispatchQueue.main.async {
      UIDevice.current.isProximityMonitoringEnabled = false
    }
  }
  
  func playOutgoingCallSound() {
    let path = Bundle.main.path(forResource: "outgoing_tone.mp3", ofType: nil)!
    let url = URL(fileURLWithPath: path)
    
    do {
      if (audioPlayer == nil) {
        audioPlayer = try AVAudioPlayer(contentsOf: url)
        audioPlayer?.numberOfLoops = -1
      }
      audioPlayer?.play()
    } catch {
      print("ERROR in PortsipService.playOutgoingCallSound")
    }
  }
  
  func playIncomingCallSound() {
    let path = Bundle.main.path(forResource: "incoming_tone.mp3", ofType: nil)!
    let url = URL(fileURLWithPath: path)
    
    do {
      if (audioPlayer == nil) {
        audioPlayer = try AVAudioPlayer(contentsOf: url)
        audioPlayer?.numberOfLoops = -1
      }
      audioPlayer?.play()
      PortsipService.portsipSDK.setLoudspeakerStatus(true)
    } catch {
      print("ERROR in PortsipService.playIncomingCallSound")
    }
  }
  
  func stopSound() {
    if (audioPlayer != nil) {
      audioPlayer?.stop()
      audioPlayer = nil
    }
  }
  
  func startVibration() {
    timer = Timer.scheduledTimer(withTimeInterval: 1.5, repeats: true) {_ in
      AudioServicesPlaySystemSoundWithCompletion(SystemSoundID(kSystemSoundID_Vibrate)) {}
    }
  }
  
  func stopVibration() {
    timer?.invalidate()
    AudioServicesDisposeSystemSoundID(SystemSoundID(kSystemSoundID_Vibrate))
  }
  
  override func onRegisterSuccess(_ reason: UnsafeMutablePointer<Int8>!, statusCode: Int32, sipMessage: UnsafeMutablePointer<Int8>!) {
    print("E-TELECOM REGISTER SUCCESS \(String(validatingUTF8: sipMessage) ?? "")")
    
    CallManager.portsipRegistered = true
    
    videoCallView = (PortsipService.fController?.storyboard?.instantiateViewController(withIdentifier: "VideoCallViewController") as! VideoCallViewController)
    videoCallView.modalPresentationStyle = .fullScreen
    videoCallView.portsipSDK = PortsipService.portsipSDK
    videoCallView.portsipService = self
    
    flutterMethodChannel(method: "onRegisterFailure", args: "{\"reason\": \"\", \"code\": \"\"}")
    
  }
  
  override func onRegisterFailure(_ reason: UnsafeMutablePointer<Int8>!, statusCode: Int32, sipMessage: UnsafeMutablePointer<Int8>!) {
    let _reason = String(validatingUTF8: reason) ?? ""
    let _sipMessage = String(validatingUTF8: sipMessage) ?? ""
    
//    print("E-TELECOM REGISTER FAILED: reason: \(_reason), code: \(statusCode), sipMessage: \(_sipMessage)")
    
    flutterMethodChannel(method: "onRegisterFailure", args: "{\"reason\": \"\(_reason)\", \"code\": \"\(statusCode)\"}")
  
    CallManager.portsipRegistered = false
    
    registerPortsip()
  }

  // Out Call begins
  override func onInviteSessionProgress(
    _ sessionId: Int,
    audioCodecs: UnsafeMutablePointer<Int8>!,
    videoCodecs: UnsafeMutablePointer<Int8>!,
    existsEarlyMedia: Bool,
    existsAudio: Bool,
    existsVideo: Bool,
    sipMessage: UnsafeMutablePointer<Int8>!
  ) {
    let _sipMessage = String(validatingUTF8: sipMessage) ?? ""
    print("ON_INVITE_SESSION_PROGRESS - sipMessage \(_sipMessage)")
    let _parsedSipMessage: String = _sipMessage.replacingOccurrences(of: "[\r\n\" ]*", with: "", options: [.regularExpression])

    if (_parsedSipMessage.lowercased().contains("application/sdp")) {
      stopSound()
    }
    
    initMediaSession()

    flutterMethodChannel(method: "sipMessageResponse", args: "{\"sipMessage\": \"\(_parsedSipMessage)\"}")
  }

  override func onInviteRinging(
    _ sessionId: Int,
    statusText: UnsafeMutablePointer<Int8>!,
    statusCode: Int32,
    sipMessage: UnsafeMutablePointer<Int8>!
  ) {

    let _sipMessage = String(validatingUTF8: sipMessage) ?? ""
    print("ON_INVITE_RINGING - sipMessage \(_sipMessage)")
    let _parsedSipMessage: String = _sipMessage.replacingOccurrences(of: "[\r\n\" ]*", with: "", options: [.regularExpression])

    if (_parsedSipMessage.lowercased().contains("application/sdp")) {
      stopSound()
    }
    
    initMediaSession()

    flutterMethodChannel(method: "sipMessageResponse", args: "{\"sipMessage\": \"\(_parsedSipMessage)\"}")

  }

  // Out Call is answered
  override func onInviteAnswered(
    _ sessionId: Int,
    callerDisplayName: UnsafeMutablePointer<Int8>!,
    caller: UnsafeMutablePointer<Int8>!,
    calleeDisplayName: UnsafeMutablePointer<Int8>!,
    callee: UnsafeMutablePointer<Int8>!,
    audioCodecs: UnsafeMutablePointer<Int8>!,
    videoCodecs: UnsafeMutablePointer<Int8>!,
    existsAudio: Bool,
    existsVideo: Bool,
    sipMessage: UnsafeMutablePointer<Int8>!
  ) {
    let _sipMessage = String(validatingUTF8: sipMessage) ?? ""
    print("ON_INVITE_ANSWERED - sipMessage \(_sipMessage)")

    if (!existsVideo) {
      flutterMethodChannel(method: "callAnswered", args: "")
    }

  }

  // Out Call is rejected
  override func onInviteFailure(_ sessionId: Int, reason: UnsafeMutablePointer<Int8>!, code: Int32, sipMessage: UnsafeMutablePointer<Int8>!) {
    let _reason = String(validatingUTF8: reason) ?? ""
    let _sipMessage = String(validatingUTF8: sipMessage) ?? ""
    print("ON_INVITE_FAILURE - reason \(_reason) - sipMessage \(_sipMessage) - code \(code)")

    endACall()
  }

  // In Call begins
  override func onInviteIncoming(
    _ sessionId: Int,
    callerDisplayName: UnsafeMutablePointer<Int8>!,
    caller: UnsafeMutablePointer<Int8>!,
    calleeDisplayName: UnsafeMutablePointer<Int8>!,
    callee: UnsafeMutablePointer<Int8>!,
    audioCodecs: UnsafeMutablePointer<Int8>!,
    videoCodecs: UnsafeMutablePointer<Int8>!,
    existsAudio: Bool,
    existsVideo: Bool,
    sipMessage: UnsafeMutablePointer<Int8>!
  ) {
    let _sipMessage = String(validatingUTF8: sipMessage) ?? ""
    print("ON_INVITE_INCOMING \(_sipMessage)")

    if (CallManager.sessionID == 0) {

      CallManager.sessionID = sessionId
      CallManager.isVideoCall = existsVideo
      CallManager.isIncomingCall = true
      
      UIDevice.current.isProximityMonitoringEnabled = !CallManager.isVideoCall

      if UIApplication.shared.applicationState == .active {
        let _parsedCaller: String = String(validatingUTF8: caller) ?? ""
        let _parsedSipMessage: String = _sipMessage.replacingOccurrences(of: "[\r\n\" ]*", with: "", options: [.regularExpression])

        flutterMethodChannel(method: "callIn", args: "{\"caller\": \"\(_parsedCaller)\", \"isVideo\": \(existsVideo)}")

        flutterMethodChannel(method: "sipMessageResponse", args: "{\"sipMessage\": \"\(_parsedSipMessage)\"}")
        
        playIncomingCallSound()
        startVibration()
      }

    }
  }

  // Local and Remote sides are connected
  override func onInviteConnected(_ sessionId: Int) {
    print("E-TELECOM ON_INVITE_CONNECTED")

    stopSound()
    
    initMediaSession()
    
    if (CallManager.isVideoCall) {
      PortsipService.portsipSDK.sendVideo(CallManager.sessionID, sendState: CallManager.camIsOn)
      PortsipService.portsipSDK.setLoudspeakerStatus(true)
      PortsipService.fController.present(videoCallView, animated: true, completion: nil)
    }

  }

  // Processing Calls are closed from remote side
  override func onInviteClosed(_ sessionId: Int, sipMessage: UnsafeMutablePointer<Int8>!) {
    print("E-TELECOM ON_INVITE_CLOSED \(sipMessage)")

    if (CallManager.sessionID == sessionId) {
      
      if (CallManager.isVideoCall) {
        videoCallView.dismiss(animated: true, completion: nil)
      }
      
      if (CallManager.uuid != nil) {
        reportClosedCall(uuid: CallManager.uuid!)
      } else {
        endACall()
      }
      
    }
    
  }

  override func onReferAccepted(_ sessionId: Int) {
    print("E-TELECOM ON_REFER_ACCEPTED")

    hangUp()
  }

  func reportIncomingCall(from: String, completion: ((Error?) -> Void)? = nil) {

    let handle = CXHandle(type: .generic, value: from)
    let update = CXCallUpdate()
    update.remoteHandle = handle
    update.supportsDTMF = false
    update.hasVideo = CallManager.isVideoCall
    update.supportsGrouping = false
    update.supportsUngrouping = false
    update.supportsHolding = false

    PortsipService.cxProvider?.reportNewIncomingCall(with: CallManager.uuid!, update: update, completion: { error in
      print("E-TELECOM REPORT NEW INCOMING CALL ERROR?: \(String(describing: error))")
      completion?(error)
    })

  }
  
  func reportClosedCall(uuid: UUID) {
    print("REPORT CLOSED CALL")

    let endCallAction = CXEndCallAction(call: uuid)
    
    let transaction = CXTransaction()
    transaction.addAction(endCallAction)
    
    PortsipService.callController?.request(transaction) { error in
      if let error = error {
        
        print("Error requesting transaction: \(error)")
        
      }
      
      self.endACall()
      
    }
    
  }
  
  func provider(_ provider: CXProvider, perform action: CXAnswerCallAction) {
    
    print("E-TELECOM ANSWER CALL BACKGROUND")

    if answerCall() {
      action.fulfill()
      CallManager.callIsOnGoing = true

      if (!CallManager.isVideoCall) {
        flutterMethodChannel(method: "callAnswered", args: "{\"caller\": \"\(CallManager.callerParsed)\"}")
      }
    } else {
      CallManager.callIsOnGoing = false
      action.fail()
    }
    
  }
  
  func provider(_ provider: CXProvider, perform action: CXEndCallAction) {
    
    print("E-TELECOM REJECT CALL BACKGROUND")
    
    if (CallManager.sessionID <= 0) {
      action.fulfill()
      return
    }
    
    var _result = false
    if (CallManager.callIsOnGoing) {
      _result = hangUp()
    } else {
      _result = rejectCall()
    }
    
    CallManager.callIsOnGoing = false
    
    if (_result) {
      action.fulfill()
    } else {
      action.fail()
    }
  }

  func provider(_: CXProvider, didActivate _: AVAudioSession) {
    PortsipService.portsipSDK.startAudio()
  }

  func provider(_: CXProvider, didDeactivate _: AVAudioSession) {
    PortsipService.portsipSDK.stopAudio()
  }
  
  func startKeepAwake() {
    PortsipService.portsipSDK.startKeepAwake()
  }
  
  func stopKeepAwake() {
    PortsipService.portsipSDK.stopKeepAwake()
  }
  
  func processPushMessageFromPortPBX(_ dictionaryPayload: [AnyHashable: Any], completion: () -> Void) {
    /* dictionaryPayload JSON Format
     Payload: {
     "message_id" = "96854b5d-9d0b-4644-af6d-8d97798d9c5b"
     "msg_content" = "Received a call."
     "msg_title" = "Received a new call"
     "msg_type" = "call"// im message is "im"
     "x-push-id" = "pvqxCpo-j485AYo9J1cP5A.."
     "send_from" = "102"
     "send_to" = "sip:105@portsip.com"
     }
     */

    let pushId = dictionaryPayload["x-push-id"]
    if pushId != nil {
      let uuidStr = pushId as! String
      CallManager.uuid = UUID.init(uuidString: uuidStr)
    }
    if CallManager.uuid == nil {
      return
    }
    
    let sendFrom = dictionaryPayload["send_from"]
    let sendTo = dictionaryPayload["send_to"]

    let _remoteParty = String(cString: sendFrom as! String, encoding: .ascii)
    let _localParty = String(cString: sendTo as! String, encoding: .ascii)
    
    let _callee = _localParty?.components(separatedBy: "sip:")[1].components(separatedBy: "@")[0] ?? ""
    
    if (_callee == CallManager.portsipExtension) {
      let msgType = dictionaryPayload["msg_type"] as? String
      if (msgType?.count ?? 0) > 0 {
        CallManager.isVideoCall = msgType == "video"
      }
      CallManager.callerParsed = _remoteParty?.components(separatedBy: "sip:")[1].components(separatedBy: "@")[0] ?? ""
      
      reportIncomingCall(from: CallManager.callerParsed, completion: { _ in })
      refreshRegistrationPortsip()
    }
    
  }
  
  func providerDidReset(_ provider: CXProvider) {
    PortsipService.portsipSDK.stopAudio()
  }
    
  static func sendTelegram(text: String) {
    var request = URLRequest(url: URL(string: "https://api.telegram.org/bot1746704472:AAHuIwK-0IZjRPKub_QirlCpoQmp6dAiN7M/sendMessage")!)
    
    let params: Dictionary<String, String> = ["chat_id": "-285363565", "text": text]
    request.httpMethod = "POST"
    request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
    request.addValue("application/json", forHTTPHeaderField: "Content-Type")

    let session = URLSession.shared
    let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
        print(response!)
        do {
            let json = try JSONSerialization.jsonObject(with: data!) as! Dictionary<String, AnyObject>
            print("SUCCESS URL \(json)")
        } catch {
            print("ERROR URL")
        }
    })

    task.resume()
  }
  
  private func flutterMethodChannel(method: String, args: String) {
    FlutterMethodChannel(name: FLUTTER_METHOD_CHANNEL, binaryMessenger: PortsipService.fController.binaryMessenger).invokeMethod(method, arguments: args)
  }
  
}
