//
//  VideoCallViewController.swift
//  Runner
//
//  Created by nathan on 06/02/2021.
//

import UIKit
import PortSIPVoIPSDK

@IBDesignable
class BaseCornerRadius: UIView {
  @IBInspectable var cornerRadius: CGFloat = 0 {
    didSet {
      layer.cornerRadius = cornerRadius
      layer.masksToBounds = cornerRadius > 0
    }
  }

  @IBInspectable var borderWidth: CGFloat = 0 {
    didSet {
      layer.borderWidth = borderWidth
    }
  }

  @IBInspectable var borderColor: UIColor? {
    didSet {
      layer.borderColor = borderColor?.cgColor
    }
  }
}

@available(iOS 10.0, *)
class VideoCallViewController : UIViewController {
  @IBOutlet var remoteVideo: BasePortSIPVideoRenderView!
  @IBOutlet var localVideo: BasePortSIPVideoRenderView!
  @IBOutlet var hangupBtn: UIButton!
  @IBOutlet var holdBtn: UIButton!
  @IBOutlet var unHoldBtn: UIButton!
  @IBOutlet var camOnBtn: UIButton!
  @IBOutlet var camOffBtn: UIButton!
  @IBOutlet var micOnBtn: UIButton!
  @IBOutlet var micOffBtn: UIButton!
  @IBOutlet var flipBtn: UIButton!
  @IBOutlet var timeLabel: UILabel!
  
  var portsipSDK: PortSIPSDK!
  var portsipService: PortsipService!

  private var timer: Timer?
  
  override func viewDidLoad() {
    preCheckStateOfButtons()
    
    localVideo.initVideoRender()
    remoteVideo.initVideoRender()
  }
  
  override func viewDidAppear(_ animated: Bool) {
    preCheckStateOfButtons()

    startTimer()
    
    portsipSDK.displayLocalVideo(true, mirror: true, localVideoWindow: localVideo)
    portsipSDK.setRemoteVideoWindow(CallManager.sessionID, remoteVideoWindow: remoteVideo)
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    stopTimer()
    timeLabel.text = "00:00"

    portsipSDK.displayLocalVideo(false, mirror: false, localVideoWindow: nil)
    portsipSDK.setRemoteVideoWindow(CallManager.sessionID, remoteVideoWindow: nil)
  }

  private func startTimer() {
    var callingTime = 0
    stopTimer()
    timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { _ in
      callingTime += 1
      self.timeLabel.text = StringHandler.secondsToTime(value: callingTime)
    }
  }

  func stopTimer() {
    timer?.invalidate()
  }
  
  private func preCheckStateOfButtons() {
    holdBtn.isHidden = CallManager.isHeld
    unHoldBtn.isHidden = !CallManager.isHeld
    camOnBtn.isHidden = !CallManager.camIsOn
    camOffBtn.isHidden = CallManager.camIsOn
    micOnBtn.isHidden = !CallManager.micIsOn
    micOffBtn.isHidden = CallManager.micIsOn
  }
  
  @IBAction func hangup() {
    dismiss(animated: true, completion: nil)
    
    portsipService.hangUp()
  }
  
  @IBAction func hold() {
    holdBtn.isHidden = true
    unHoldBtn.isHidden = false
    portsipService.hold()
    portsipSDK.sendVideo(CallManager.sessionID, sendState: false)
  }
  
  @IBAction func unHold() {
    holdBtn.isHidden = false
    unHoldBtn.isHidden = true
    portsipService.unHold()
    portsipSDK.sendVideo(CallManager.sessionID, sendState: true)
  }
  
  @IBAction func turnOffCam() {
    camOnBtn.isHidden = true
    camOffBtn.isHidden = false
    portsipSDK.sendVideo(CallManager.sessionID, sendState: false)
  }
  
  @IBAction func turnOnCam() {
    camOnBtn.isHidden = false
    camOffBtn.isHidden = true
    portsipSDK.sendVideo(CallManager.sessionID, sendState: true)
  }
  
  @IBAction func turnOffMic() {
    micOnBtn.isHidden = true
    micOffBtn.isHidden = false
    portsipSDK.muteSession(
      CallManager.sessionID,
      muteIncomingAudio: false,
      muteOutgoingAudio: true,
      muteIncomingVideo: false,
      muteOutgoingVideo: false
    )
  }
  
  @IBAction func turnOnMic() {
    micOnBtn.isHidden = false
    micOffBtn.isHidden = true
    portsipSDK.muteSession(
      CallManager.sessionID,
      muteIncomingAudio: false,
      muteOutgoingAudio: false,
      muteIncomingVideo: false,
      muteOutgoingVideo: false
    )
  }
  
  @IBAction func flipCamera() {
    CallManager.frontCamera = !CallManager.frontCamera
    if (CallManager.frontCamera) {
      portsipSDK.setVideoDeviceId(1)
    } else {
      portsipSDK.setVideoDeviceId(0)
    }
  }
  
}
