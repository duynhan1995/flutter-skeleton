//
//  CallManager.swift
//  Runner
//
//  Created by nathan on 15/04/2021.
//

import Foundation

class CallManager {
  
  static var frontCamera: Bool = true
  static var isHeld: Bool = false
  static var camIsOn: Bool = true
  static var micIsOn: Bool = true
  static var speakerOn: Bool = false

  static var portsipExtension: String = ""
  static var portsipPassword: String = ""
  static var portsipDomain: String = ""
  static var portsipServer: String = ""
  static var sessionID: Int = 0
  static var callIsOnGoing: Bool = false
  static var isVideoCall: Bool = false
  static var isIncomingCall: Bool = false
    
  static var uuid: UUID? = nil
  static var portsipRegistered: Bool = false
  static var callerParsed: String = ""

  static var pushMessage: String = ""
  
  static func resetStates() {
    frontCamera = true
    isHeld = false
    camIsOn = true
    micIsOn = true
    speakerOn = false
    sessionID = 0
    callIsOnGoing = false
    isVideoCall = false
    isIncomingCall = false
    
    uuid = nil
    callerParsed = ""
  }
  
}
