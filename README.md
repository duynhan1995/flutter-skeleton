
# eTelecom App

Calling & Creating Orders app by eTelecom.

## How to run (for using hot-reload, hot-restart)

- `flutter pub get` -> to install all packages required.
- `./scripts/run.sh $APP $PLATFORM $ENV $BUILD_OPTION` -> to run the app (`$BUILD_OPTION` is optional). Example: if you'd like to run **eTelecom** **Mobile app** in **Production** environment, just run `./scripts/run.sh etelecom mobile prod`. In case you want to run the app with `release` mode for better performance (but you cannot see logs like in `debug` mode), run `./scripts/run.sh etelecom mobile prod --release`.

*** `$APP` are the specific apps with different config, theme but they share same business code in behind (like `apis`, `services`, `models`, ...).

## How to build (for building/distributing app with Android Studio/XCode)

-> `./scripts/build.sh $APP $ENV $BUILD_OPTION`. Other arguments are like `run.sh`.

## Modify config files (in case you want to add/modify apps configs)

1. go to `assets/config/$APP/$ENV.json`. If there is none, create 2 or more files depends on how many environments you want to have.
   
   1.1/ You're just able to add/modify certain config keys such are: `apiUrl`, `primaryColor` (view `assets/config/etelecom/dev.json)` for details).

2. edit file `pubspec.yaml`: add 1 line in `assets: ` -> `- assets/config/$APP/` to allow the app to read files in that folder.

### Explain variables

- $APP: `etelecom`, ...
- $ENV: `dev`, `prod`, `stage`, `sandbox`
- $BUILD_OPTION: `--release`

## JSON-serialization

1. Follow the instruction [here](https://flutter.dev/docs/development/data-and-backend/json).
2. REMEMBER!!! `flutter pub run build_runner build --delete-conflicting-outputs` -> to automatically generate fromJson and toJson for Models.

## Launching assets

### Icons

- **Android:** 
    
1. Open `android` folder by Android Studio.
2. Follow this instruction: https://developer.android.com/studio/write/image-asset-studio#create-adaptive

- **iOS:**

1. Open `ios` folder -> `Runner.xcworkspace` by XCode.
2. Open `Runner` -> `Runner` -> `Assets/xcassets` then create an `iOS App Icon` [View Example Image](https://photos.app.goo.gl/vrWeBMoETEbZoZeY9)
3. Make all icons that match all sizes shown in [this list](https://photos.app.goo.gl/SG6Fb8iXC23Aw1278)

### Splash Screen

- **Android:**

1. Open `android` folder by Android Studio.
2. Make an image with ratio between Width/Height is 1/2
3. In Android View, go to `app` -> `res` -> `mipmap` -> copy your image here
4. In Android View, go to `app` -> `res` -> `drawable` -> open `launch_background.xml` -> add those lines of codes if there is nothing there. [View Example Image](https://photos.app.goo.gl/x8SmPcXkQUqQGEzz9)
```
<?xml version="1.0" encoding="utf-8"?>
<!-- Modify this file to customize your launch splash screen -->
<layer-list xmlns:android="http://schemas.android.com/apk/res/android">
    <item android:drawable="@android:color/white" />

    <!-- You can insert your own image assets here -->
    <item>
        <bitmap
            android:gravity="fill"
            android:src="@mipmap/launch_background" />
    </item>
</layer-list>
```

- **iOS:**

1. Open `ios` folder -> `Runner.xcworkspace` by XCode.
2. Follow this instruction: https://medium.com/flawless-app-stories/change-splash-screen-in-ios-app-for-dummies-the-better-way-e385327219e. If you cannot access the page above, try using a VPN app!

# Request permissions

- **Android:** 
    
1. Open `android` folder by Android Studio.
2. `MainActivity.kt` add these lines of code wherever you want to open a dialog requesting permissions

Example: Request permission for `MICROPHONE`

```
when (PackageManager.PERMISSION_GRANTED) {
    ContextCompat.checkSelfPermission(activity, Manifest.permission.RECORD_AUDIO) -> {
        // TODO when permission is granted => do something...
    }
    else -> {
        ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.RECORD_AUDIO), 0) // open dialog requesting permissions
        
        // TODO when permission is denied => do something...
    }
}
```

- **iOS:**

1. Open `ios` folder -> `Runner.xcworkspace` by XCode.
2. Right click `Info.plist` -> open as `Source Code`
3. Add these lines of code for specific permission inside `<dict></dict>` tag

Example: Request permission for `MICROPHONE`

```
<key>NSMicrophoneUsageDescription</key>
<string>${Message to show in the dialog}</string>
```

