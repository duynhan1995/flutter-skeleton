find ./lib/ -name '*-route-information-parser.dart' -delete
find ./lib/ -name '*-route-path.dart' -delete
find ./lib/ -name '*-router-delegate.dart' -delete
find ./lib/ -name '*_app.dart' -delete
find ./lib/ -name '*_dev.dart' -delete
find ./lib/ -name '*_prod.dart' -delete
find ./lib/ -name '*_sandbox.dart' -delete
rm -rf lib/pages lib/main.dart
