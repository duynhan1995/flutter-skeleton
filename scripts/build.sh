export APP=$1;
export PLATFORM=$2;
export ENV=$3;
export BUILD_OPTION=$4;

./scripts/prepare.sh ${APP} ${ENV} ${BUILD_OPTION}

case $PLATFORM in
  "mobile")
    flutter build ios ${BUILD_OPTION}
    flutter build apk ${BUILD_OPTION}
  ;;
  "web")
    flutter build web ${BUILD_OPTION}
  ;;
esac
