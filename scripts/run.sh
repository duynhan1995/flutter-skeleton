export APP=$1;
export PLATFORM=$2;
export ENV=$3;
export BUILD_OPTION=$4;

./scripts/clean.sh

#ios
cp -R assets/config/${APP}/ios/Assets.xcassets ios/Runner/
cp -R assets/config/${APP}/ios/Info.plist ios/Runner/
cp -R assets/config/${APP}/ios/Podfile ios/
cp -R assets/config/${APP}/ios/GoogleService-Info.plist ios/Runner/

#android
cp -R assets/config/${APP}/android/res android/app/src/main/
cp -R assets/config/${APP}/android/AndroidManifest.xml android/app/src/main/
cp -R assets/config/${APP}/android/google-services.json android/app/

#assets
cp -R assets/config/${APP}/images/* assets/images/

case $PLATFORM in
  "mobile")
    flutter run -t libs/${APP}/${APP}_${ENV}.dart ${BUILD_OPTION}
  ;;
  "web")
    flutter run -t libs/${APP}/${APP}_${ENV}.dart -d chrome --web-port=3000 ${BUILD_OPTION}
  ;;
esac
