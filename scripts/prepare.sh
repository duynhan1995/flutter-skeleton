export APP=$1;
export ENV=$2;
export BUILD_OPTION=$3;

./scripts/clean.sh

#ios
cp -R assets/config/${APP}/ios/Assets.xcassets ios/Runner/
cp -R assets/config/${APP}/ios/Info.plist ios/Runner/
cp -R assets/config/${APP}/ios/Podfile ios/
cp -R assets/config/${APP}/ios/GoogleService-Info.plist ios/Runner/

#android
cp -R assets/config/${APP}/android/res android/app/src/main/
cp -R assets/config/${APP}/android/AndroidManifest.xml android/app/src/main/
cp -R assets/config/${APP}/android/google-services.json android/app/

#assets
cp -R assets/config/${APP}/images/* assets/images/

cp -R libs/${APP}/ lib/
mv lib/${APP}_${ENV}.dart lib/main.dart
